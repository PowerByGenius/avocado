#-------------------------------------------------
#
# Project created by QtCreator 2015-10-02T08:49:27
#
#-------------------------------------------------

QT       += core gui
CONFIG   += C++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = avocado
TEMPLATE = app


SOURCES +=\
    avocado_pagemainwidget.cpp \
    avocado_mainwindow.cpp \
    avocado_main.cpp \
    avocado_sourcelistview.cpp \
    avocado_collectionview.cpp

HEADERS  += \
    avocado_pagemainwidget.h \
    avocado_mainwindow.h \
    avocado_sourcelistview.h \
    avocado_collectionview.h

FORMS    += \
    avocado_pagemainwidget.ui \
    avocado_mainwindow.ui
