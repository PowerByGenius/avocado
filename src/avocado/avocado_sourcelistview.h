#ifndef SOURCELISTVIEW_H
#define SOURCELISTVIEW_H

#include <QTreeView>
#include <QItemDelegate>
#include <QStandardItemModel>

class SourceListModel : public QStandardItemModel
{
    Q_OBJECT
public:

    enum ItemRoles {NodeRole = Qt::UserRole + 100,
                    };

    enum NodeRoles {NodeRoleLibrary,
                        NodeRoleLooseNotes,
                        NodeRoleDueForStudy,
                        NodeRoleFlagged,
                        NodeRoleTrash,
                    NodeRoleRecent,
                        NodeRoleRecentlyStudied,
                        NodeRoleRecentlyIncorrect,
                        NodeRoleRecentlyLastImport,
                        NodeRoleRecentlyAdded,
                    NodeRoleStacks,
                        NodeRoleCase,
                        NodeRoleStack
                   };

    explicit SourceListModel(QObject *parent = 0);
    virtual ~SourceListModel() {}

protected:
    QStandardItem *m_itemLooseNotes;
    QStandardItem *m_itemDueForStudy;
    QStandardItem *m_itemFlagged;
    QStandardItem *m_itemTrash;

    QStandardItem *m_itemRecentlyStudied;
    QStandardItem *m_itemRecentlyIncorrect;
    QStandardItem *m_itemRecentlyLastImport;
    QStandardItem *m_itemRecentlyAdded;

    QStandardItem *m_itemStacks;

public slots:
    void updateLibraryData (const QVariantList &list);
    void updateStacksData  (const QVariantList &list);
signals:
    void stacksNodeCreated (const QVariantMap &data);
    void stacksNodeChanged (const QVariantMap &data);
    void stacksNodeDeleted (const QVariantMap &data);
};



// -----------------------------------------------------------------------------

class SourceListDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit SourceListDelegate(QObject * parent = 0);
    virtual ~SourceListDelegate() {}
};



// -----------------------------------------------------------------------------

class SourceListView : public QTreeView
{
    Q_OBJECT
public:
    explicit SourceListView(QWidget *parent = 0);
    virtual ~SourceListView() {}
    friend class SourceListModel;
private:
    SourceListModel *m_model;
protected:
    void setModel(QAbstractItemModel *model){QTreeView::setModel(model);}
signals:

public slots:
};

#endif // SOURCELISTVIEW_H
