#include "avocado_collectionview.h"
#include <QPainter>
#include <QTimer>
#include <QDebug>
#include <QScrollBar>


//
// CollectionItem
// =============================================================================

CollectionItemDelegate::CollectionItemDelegate(QObject *parent)
    :QObject(parent)
{

}

//
// CollectionItem
// =============================================================================



CollectionItem::CollectionItem(CollectionScene *scene, QGraphicsItem *parent):
    QGraphicsObject(parent),
    m_scene(scene)
{
    setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsFocusable);
    //this->text = "Test";
}

void CollectionItem::paint(QPainter *painter,
                           const QStyleOptionGraphicsItem *option,
                           QWidget *widget)
{
    Q_UNUSED(widget)

    if(isVisible())
    {
        painter->fillRect(boundingRect(),Qt::white);

        if(this->isSelected())
        {
            painter->save();
            QPen pen;
            pen.setColor(QColor(123,190,247));
            pen.setWidth(2);
            painter->setPen(pen);
            painter->drawRect(this->boundingRect());
            painter->restore();
        }else{
            painter->save();
            QPen pen;
            pen.setColor(QColor(231,231,231));
            pen.setWidth(1);
            painter->setPen(pen);
            painter->drawRect(this->boundingRect());
            painter->restore();
        }

        painter->drawText(boundingRect(),this->text());
    }
}

QRectF CollectionItem::boundingRect() const
{
    return QRectF(0,0,m_scene->cellWidth(),m_scene->cellHeight());
}


//
// CollectionGroupItem
// =============================================================================

CollectionGroupItem::CollectionGroupItem(CollectionScene *scene,
                                         QGraphicsItem *parent):
    QGraphicsObject(parent),
    m_scene(scene)
{

}

void CollectionGroupItem::paint(QPainter *painter,
                                const QStyleOptionGraphicsItem *option,
                                QWidget *widget)
{
    Q_UNUSED(widget)

    if(isVisible())
    {
    //painter->fillRect(boundingRect(),Qt::red);
    painter->drawText(boundingRect(), Qt::AlignLeft|Qt::AlignVCenter,"2015年12月03日");
    }
}

QRectF CollectionGroupItem::boundingRect() const
{
    return QRectF(0,0,
                  m_scene->viewportWidth() -
                  (m_scene->marginLeft()+m_scene->marginRight())
                  ,m_scene->verticalSpace() * 2);
}


//
// CollectionScene
// =============================================================================

CollectionScene::CollectionScene(CollectionView *parent):
    QGraphicsScene(parent),
    m_view(parent),
    m_itemDelegate(new CollectionItemDelegate(this))
{
    setItemIndexMethod(QGraphicsScene::NoIndex);
    setBackgroundBrush(QBrush(QColor(245,245,245)));

    // TEST
    QTimer::singleShot(0,[this](){

        for (int i = 0; i < 5; ++i) {
            CollectionObject *object = new CollectionObject(this);
            CollectionGroupItem *group = new CollectionGroupItem(this);
            object->groupItem = group;
            addItem(group);
            for (int j = 0; j < 5; ++j) {
                CollectionItem *item = new CollectionItem(this);
                item->setText(QString("Test %1 %2").arg(i).arg(j));
                object->items.append(item);
                addItem(item);
            }
            m_objects.append(object);
        }

        reLayout();
    });

}

int CollectionScene::viewportWidth() const
{
    return m_view->viewport()->width();
}


void CollectionScene::reLayout()
{

    int viewWidth = m_view->viewport()->width() - (marginLeft()+marginRight())/2;
    int availableColumns = viewWidth / (cellWidth() + horizontalSpace());

    int groupItemTop = 0;
    for (int i = 0; i < m_objects.count(); ++i) {

        CollectionObject *object = m_objects.at(i);

        // 如果没有子节点跳过
        if(object->items.count() == 0) continue;

        CollectionGroupItem *groupItem = object->groupItem;
        QRectF groupItemRect = groupItem->boundingRect();
        int groupItemHeight = groupItemRect.height();
        groupItem->setPos(0,groupItemTop);
        groupItemTop += groupItemHeight + verticalSpace() /2;

        int itemCount = object->items.count();

        int totalRow = itemCount > 0?1:0;
        if(availableColumns == 0) availableColumns = 1;
        totalRow = itemCount /  availableColumns;
        int itemsMod = itemCount % availableColumns;
        if(itemsMod >0) totalRow++;

        int row = 0;

        for (int j = 0; j < itemCount; ++j) {

            CollectionItem *item = object->items.at(j);

            int fixWidth = 0;

            if( totalRow > 1 )
            {
                fixWidth = (viewWidth % (cellWidth() + horizontalSpace())) / availableColumns;
            }

            int itemPosX = (j % availableColumns) * (cellWidth() + horizontalSpace() + fixWidth) + fixWidth / 2;

            if(availableColumns == 1)
            {
                itemPosX = (viewWidth -marginLeft() - cellWidth()) /2;
            }

            if(j % availableColumns == 0 and j > 0) row++;

            int itemPosY = row * (cellHeight() + verticalSpace()) + groupItemTop;

//            item->setText(QString("%1").arg(j));
            item->setPos(itemPosX,itemPosY);
        }

        groupItemTop += totalRow * (cellHeight() + verticalSpace()) - verticalSpace() /2;
    }

    QRectF itemBoundingRect = itemsBoundingRect();
    itemBoundingRect.adjust(-marginLeft(),-marginTop(),0,+ m_view->viewport()->height()/4);
    setSceneRect(itemBoundingRect);
}


//
// CollectionView
// =============================================================================

CollectionView::CollectionView(QWidget *parent) :
    QGraphicsView(parent),
    m_scene(new CollectionScene(this))
{
    setAlignment(Qt::AlignTop | Qt::AlignLeft);
    setDragMode(RubberBandDrag);
    setScene(m_scene);
}

CollectionView::~CollectionView()
{
}

void CollectionView::setScene(QGraphicsScene *scene)
{
    QGraphicsView::setScene(scene);
}

void CollectionView::resizeEvent(QResizeEvent *event)
{
//    QRect viewOfSceneRect = viewport()->rect();
//    QList<QGraphicsItem *>  items = this->items(viewOfSceneRect);

//    foreach (QGraphicsItem *item, items) {
//        CollectionItem *collectionItem = dynamic_cast<CollectionItem*>(item);
//        if(collectionItem)
//        {
//            qDebug() << collectionItem->text();
//        }
//    }

    m_scene->reLayout();
    QList<QGraphicsItem *> list = m_scene->selectedItems();

    if(list.count() > 0)
    {
        this->ensureVisible(list.at(0));
//    }else{
//        if(items.count() > 0)
//        {
//            this->ensureVisible(items.last());
//        }
    }

    QGraphicsView::resizeEvent(event);
}
