#ifndef COLLECTIONVIEW_H
#define COLLECTIONVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsObject>

class CollectionView;
class CollectionScene;
class CollectionItem;
class CollectionItemDelegate;
//
// CollectionItemDelegate
// =============================================================================

class CollectionItemDelegate : public QObject
{
    Q_OBJECT
public:
    explicit CollectionItemDelegate(QObject * parent = 0);
    virtual ~CollectionItemDelegate() {}
};


//
// CollectionItem
// =============================================================================

class CollectionItem : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit CollectionItem(CollectionScene *scene,QGraphicsItem * parent = 0);
    virtual ~CollectionItem() {}
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);
    QRectF boundingRect() const;
    QString text() const{return m_text;}
    void setText(const QString &text){this->m_text = text;}
private:
    CollectionScene *m_scene;
    QString m_text;

};




//
// CollectionGroupItem
// =============================================================================

class CollectionGroupItem : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit CollectionGroupItem(CollectionScene *scene,QGraphicsItem * parent = 0);
    virtual ~CollectionGroupItem() {}
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = 0);
    QRectF boundingRect() const;
private:
    CollectionScene *m_scene;
};



//
// CollectionObject
// =============================================================================

class CollectionObject : public QObject
{
    Q_OBJECT
public:
    explicit CollectionObject(QObject * parent = 0):QObject(parent){}
    virtual ~CollectionObject() {}
    CollectionGroupItem *groupItem;
    QList<CollectionItem*> items;
};

//
// CollectionScene
// =============================================================================

class CollectionScene : public QGraphicsScene
{
    Q_OBJECT
public:
    friend class CollectionView;
    friend class CollectionItem;
    friend class CollectionGroupItem;
    explicit CollectionScene(CollectionView * parent = 0);
    virtual ~CollectionScene() {}

    inline void setItemDelegate(CollectionItemDelegate *itemDelegate) {m_itemDelegate = itemDelegate;}
    inline CollectionItemDelegate * itemDelegate(){return m_itemDelegate;}

protected:

    inline int cellWidth() const {return 180;}
    inline int cellHeight() const {return 180;}

    inline int marginLeft() const {return 20;}
    inline int marginRight() const {return 20;}
    inline int marginTop() const {return 20;}
    inline int marginBottom() const {return 20;}
    inline int horizontalSpace() const {return 20;}
    inline int verticalSpace() const{ return 20;}

protected:
    int viewportWidth() const;
    void reLayout();
private:
    CollectionView *m_view;
    QList<CollectionObject*> m_objects;
    CollectionItemDelegate * m_itemDelegate;
};



//
// CollectionView
// =============================================================================


class CollectionView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit CollectionView(QWidget *parent = 0);
    virtual ~CollectionView();

    inline void setItemDelegate(CollectionItemDelegate *itemDelegate)
    {m_scene->setItemDelegate(itemDelegate);}
    inline CollectionItemDelegate * itemDelegate()
    {return m_scene->itemDelegate();}

protected:
    void setScene(QGraphicsScene *scene);
    void resizeEvent(QResizeEvent *event);
private:
    CollectionScene *m_scene;
signals:

public slots:
};

#endif // COLLECTIONVIEW_H
