#include "avocado_mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setApplicationName("Avocado");
    QApplication::setApplicationDisplayName("Avocado");
    QApplication::setApplicationVersion("1.0");
    MainWindow w;
    w.show();

    return a.exec();
}
