#include "avocado_mainwindow.h"
#include "ui_avocado_mainwindow.h"
#include <QApplication>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setUnifiedTitleAndToolBarOnMac(true);
    setWindowTitle(QString("%1 %2")
                   .arg(QApplication::applicationDisplayName())
                   .arg(QApplication::applicationVersion()));
    appRestoreState();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::appSaveState()
{
    QSettings settings;
    settings.setValue("mainWindow/geometry", saveGeometry());
    settings.setValue("mainWindow/windowState", saveState());
    ui->centralWidget->appSaveState();
}

void MainWindow::appRestoreState()
{
    QSettings settings;
    restoreGeometry(settings.value("mainWindow/geometry").toByteArray());
    restoreState(settings.value("mainWindow/windowState").toByteArray());
    ui->centralWidget->appRestoreState();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    appSaveState();
    QMainWindow::closeEvent(event);
}
