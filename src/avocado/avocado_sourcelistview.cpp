#include "avocado_sourcelistview.h"
#include <QTimer>

SourceListModel::SourceListModel(QObject *parent)
    :QStandardItemModel(parent)
{

    /* Init Items
       ==================== */
    {
    // LIBRARY
    // --------------------
    QStandardItem *itemLibrary = new QStandardItem(tr("LIBRARY"));
    {
    itemLibrary->setEditable(false);
    itemLibrary->setData(NodeRoleLibrary,NodeRole);
    }
    {
    QStandardItem *item = new QStandardItem(tr("All Notes"));
    item->setEditable(false);
    item->setData(NodeRoleLooseNotes,NodeRole);
    m_itemLooseNotes = item;
    itemLibrary->appendRow(item);
    }
    {
    QStandardItem *item = new QStandardItem(tr("Due for Study"));
    item->setEditable(false);
    item->setData(NodeRoleDueForStudy,NodeRole);
    m_itemDueForStudy = item;
    itemLibrary->appendRow(item);
    }
    {
    QStandardItem *item = new QStandardItem(tr("Flagged"));
    item->setEditable(false);
    item->setData(NodeRoleFlagged,NodeRole);
    m_itemFlagged = item;
    itemLibrary->appendRow(item);
    }
    {
    QStandardItem *item = new QStandardItem(tr("Trash"));
    item->setEditable(false);
    item->setData(NodeRoleTrash,NodeRole);
    m_itemTrash = item;
    itemLibrary->appendRow(item);
    }
    appendRow(itemLibrary);

    // RECENT
    // --------------------
    QStandardItem *itemRecent = new QStandardItem(tr("RECENT"));
    {
    itemRecent->setEditable(false);
    itemRecent->setData(NodeRoleRecent,NodeRole);
    }
    {
    QStandardItem *item = new QStandardItem(tr("Recently Studled"));
    item->setEditable(false);
    item->setData(NodeRoleRecentlyStudied,NodeRole);
    m_itemRecentlyStudied = item;
    itemRecent->appendRow(item);
    }
    {
    QStandardItem *item = new QStandardItem(tr("Recently Incorrect"));
    item->setEditable(false);
    item->setData(NodeRoleRecentlyIncorrect,NodeRole);
    m_itemRecentlyIncorrect = item;
    itemRecent->appendRow(item);
    }
    {
    QStandardItem *item = new QStandardItem(tr("Last Import"));
    item->setEditable(false);
    item->setData(NodeRoleRecentlyLastImport,NodeRole);
    m_itemRecentlyLastImport = item;
    itemRecent->appendRow(item);
    }
    {
    QStandardItem *item = new QStandardItem(tr("Recently Added"));
    item->setEditable(false);
    item->setData(NodeRoleRecentlyAdded,NodeRole);
    m_itemRecentlyAdded = item;
    itemRecent->appendRow(item);
    }
    appendRow(itemRecent);

    // STACKS
    // --------------------
    {
    QStandardItem *itemStacks = new QStandardItem(tr("STACKS"));
    itemStacks->setEditable(false);
    itemStacks->setData(NodeRoleStacks,NodeRole);
    m_itemStacks = itemStacks;
    appendRow(itemStacks);
    }
    } // End init Items

}

void SourceListModel::updateLibraryData(const QVariantList &list)
{

}

void SourceListModel::updateStacksData(const QVariantList &list)
{

}

// -----------------------------------------------------------------------------

SourceListDelegate::SourceListDelegate(QObject *parent):QItemDelegate(parent)
{

}

// -----------------------------------------------------------------------------

SourceListView::SourceListView(QWidget *parent) :
    QTreeView(parent),
    m_model(new SourceListModel(this))
{
    setAttribute(Qt::WA_MacShowFocusRect,false);
    setHeaderHidden(true);
    setItemDelegate(new SourceListDelegate(this));
    setModel(m_model);

    QTimer::singleShot(0,[=](){
        expandAll();
    });
}

