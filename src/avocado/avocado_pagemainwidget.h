#ifndef AVOCADO_PAGEMAINWIDGET_H
#define AVOCADO_PAGEMAINWIDGET_H

#include <QWidget>

namespace Ui {
class PageMainWidget;
}

class PageMainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PageMainWidget(QWidget *parent = 0);
    ~PageMainWidget();
    void appSaveState();
    void appRestoreState();
private:
    Ui::PageMainWidget *ui;
};

#endif // AVOCADO_PAGEMAINWIDGET_H
