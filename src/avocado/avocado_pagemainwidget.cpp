#include "avocado_pagemainwidget.h"
#include "ui_avocado_pagemainwidget.h"
#include <QSettings>

#include <QDebug>

PageMainWidget::PageMainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PageMainWidget)
{
    ui->setupUi(this);
    ui->splitter->setStretchFactor(0,0);
    ui->splitter->setStretchFactor(1,0);
    ui->splitter->setStretchFactor(2,1);
    ui->splitter->setCollapsible(0,false);
    ui->splitter->setCollapsible(1,false);
    ui->splitter->setCollapsible(2,false);

    connect(ui->sourceView,&SourceListView::activated,
            [this](const QModelIndex & index){
        Q_UNUSED(index)
    });
}

PageMainWidget::~PageMainWidget()
{
    delete ui;
}

void PageMainWidget::appSaveState()
{
    QSettings settings;
    settings.setValue("pageMainWidget/splitter", ui->splitter->saveState());
}

void PageMainWidget::appRestoreState()
{
    QSettings settings;
    ui->splitter->restoreState(settings.value("pageMainWidget/splitter").toByteArray());
}
