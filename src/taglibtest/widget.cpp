#include "widget.h"
#include "ui_widget.h"

#include <fileref.h>
#include <tag.h>
#include <mp4tag.h>
#include <mp4file.h>
#include <mp4coverart.h>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    //TagLib::FileRef f("/Users/kiko/Desktop/video/audio/01 2015-01-02 Kendra.m4a");
    TagLib::FileRef f("/Users/kiko/Desktop/2015-04-29 Grace Kelly.mp4");



    if(!f.isNull() && f.tag() && f.audioProperties()) {
//        TagLib::MP4::Tag *tag = static_cast<TagLib::MP4::Tag *>(f.tag());
        TagLib::Tag *tag = f.tag();
        qDebug() << TStringToQString(tag->title());
        qDebug() << TStringToQString(tag->album());
        qDebug() << TStringToQString(tag->artist());
        qDebug() << TStringToQString(tag->comment());
        qDebug() << TStringToQString(tag->genre());
        qDebug() << tag->track();
        qDebug() << tag->year();

//        TagLib::MP4::PropertyMap itemsListMap = tag->PropertyMap();

//        for (TagLib::MP4::ItemListMap::Iterator it=itemsListMap.begin(); it!=itemsListMap.end(); ++it)
//        {
//            qDebug() << TStringToQString(it->first);
//        }

//        QImage image;
//        if (itemsListMap.contains("covr"))
//        {
//            TagLib::MP4::Item coverItem = itemsListMap["covr"];
//            TagLib::MP4::CoverArtList coverArtList = coverItem.toCoverArtList();
//            TagLib::MP4::CoverArt coverArt = coverArtList.front();
//            image.loadFromData((const uchar *) coverArt.data().data(),coverArt.data().size());
//        }
//        ui->label->setPixmap(QPixmap::fromImage(image));
        TagLib::AudioProperties *audio = f.audioProperties();

        qDebug() << audio->length();
        qDebug() << audio->sampleRate();
        qDebug() << audio->channels();
    }
}
