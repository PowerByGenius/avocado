#-------------------------------------------------
#
# Project created by QtCreator 2015-10-05T13:34:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = taglibtest
TEMPLATE = app

include(taglib/taglib.pri)
#include(../taglib/taglib.pri)

SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui

DISTFILES +=
