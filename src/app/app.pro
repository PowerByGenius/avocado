#-------------------------------------------------
#
# Project created by QtCreator 2015-09-29T11:54:29
#
#-------------------------------------------------

QT       += core gui multimedia webkit webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = app
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
