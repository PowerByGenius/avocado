#include "searchresultspanel.h"
#include "ui_searchresultspanel.h"

#include "avocado_searchengine.h"

#include <QStandardItemModel>
#include <QItemSelectionModel>
#include <QSortFilterProxyModel>
#include <QPalette>
#include <QPainter>
#include <QDebug>



namespace NodeRole {
    enum NodeRoles{
        FilterFixedString = Qt::UserRole + 100,
        FilterRole,
        NodeType,
        NodeTypeAll,
        NodeTypeTitle,
        NodeTypeTitleAll,
        NodeTypeArtist,
        NodeTypeArtistAll,
        CaptionText,CaptionIndex,CaptionStart,CaptionEnd,
        UniqueID,MetaTitle,MetaArtist,MetaAlbum,MetaGenre,MetaDate,MetaCoverart,FileHash,FilePath,FileType
    };
}

QSize SearchResultsCaptionListDelegate::SearchResultsCaptionListDelegate::iconSize = QSize(64, 64);
int SearchResultsCaptionListDelegate::SearchResultsCaptionListDelegate::padding = 5;

//
// SearchResultsCaptionListDelegate
// =============================================================================

SearchResultsCaptionListDelegate::SearchResultsCaptionListDelegate(QObject *parent):
    QItemDelegate(parent)
{
    m_headerFont = QApplication::font();
    m_headerFont.setPointSize(11);

    m_subheaderFont = QApplication::font();
    m_subheaderFont.setPointSize(16);
}

void SearchResultsCaptionListDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(!index.isValid())
        return;

    painter->save();
    QString mediaCoverFolder = "/Users/kiko/Desktop/media/cover";
    QString imagepath = QString("%1/40/%2.png").arg(mediaCoverFolder).arg(index.data(NodeRole::MetaCoverart).toString());
    painter->drawPixmap(QRect(QPoint(13,option.rect.top()+8),QSize(40,40)),QPixmap(imagepath));


    QString subheaderText = index.data(Qt::DisplayRole).toString().replace("\n"," ");

    QString captionIndex = index.data(NodeRole::CaptionIndex).toString();
    QString captionStart = index.data(NodeRole::CaptionStart).toString().split(',').at(0);
    QString metaTitle = index.data(NodeRole::MetaTitle).toString();
    QString headerText = QString("%1 %2 %3").arg(captionIndex).arg(captionStart).arg(metaTitle);

    QFontMetrics headerFm(m_headerFont);
    QFontMetrics subheaderFm(m_subheaderFont);

    QRect headerRect =
            headerFm.boundingRect(option.rect.left() + iconSize.width(), option.rect.top() + padding,
                                  option.rect.width() - iconSize.width()- 2* padding, 0,
                                  Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                  headerText);
    QRect subheaderRect =
            subheaderFm.boundingRect(headerRect.left(), headerRect.bottom()+padding,
                                     option.rect.width() - iconSize.width()- 2* padding, 0,
                                     Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                     subheaderText);

    QColor headerColor(Qt::gray);
    QColor subHeaderColor(Qt::black);

    if (option.state & QStyle::State_Selected){
        QRect selectedRect = option.rect;

        selectedRect.setWidth(4);
        painter->fillRect(selectedRect,Qt::darkRed);
        subHeaderColor = QColor(Qt::darkRed);
    }

    painter->setPen(headerColor);
    painter->setFont(m_headerFont);
    painter->drawText(headerRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, headerText);

    painter->setPen(subHeaderColor);

    painter->setFont(m_subheaderFont);
    painter->drawText(subheaderRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, subheaderText);

    painter->restore();
}

QSize SearchResultsCaptionListDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(!index.isValid())
        return QSize();

    QString subheaderText = index.data(Qt::DisplayRole).toString().replace("\n"," ");

    QString captionIndex = index.data(NodeRole::CaptionIndex).toString();
    QString captionStart = index.data(NodeRole::CaptionStart).toString().split(',').at(0);
    QString metaTitle = index.data(NodeRole::MetaTitle).toString();
    QString headerText = QString("%1 %2 %3").arg(captionIndex).arg(captionStart).arg(metaTitle);

    QFontMetrics headerFm(m_headerFont);
    QFontMetrics subheaderFm(m_subheaderFont);

    QRect headerRect = headerFm.boundingRect(0, 0,
                                             option.rect.width() - iconSize.width() - 2* padding, 0,
                                             Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                             headerText);

    QRect subheaderRect = subheaderFm.boundingRect(0, 0,
                                                   option.rect.width() - iconSize.width() - 2* padding, 0,
                                                   Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                                   subheaderText);

    QSize size(option.rect.width(), headerRect.height() + subheaderRect.height() +  3*padding);

    /* Keep the minimum height needed in mind. */
    if(size.height()<iconSize.height())
        size.setHeight(iconSize.height());

    return size;
}

//
// SearchResultsCaptionTreeDelegate
// =============================================================================

SearchResultsCaptionTreeDelegate::SearchResultsCaptionTreeDelegate(QObject *parent):
    QItemDelegate(parent)
{

}

void SearchResultsCaptionTreeDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QItemDelegate::paint(painter,option,index);
}

QSize SearchResultsCaptionTreeDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QItemDelegate::sizeHint(option,index);
}


//
// SearchResultsPanel
// =============================================================================

SearchResultsPanel::SearchResultsPanel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SearchResultsPanel)
{
    ui->setupUi(this);

    setAutoFillBackground(true);
    QPalette palette = this->palette();
    palette.setColor(QPalette::Background, QColor(255,255,255));
    setPalette(palette);

    ui->searchField->setAttribute(Qt::WA_MacShowFocusRect,false);

    ui->captionResultsListView->setAttribute(Qt::WA_MacShowFocusRect,false);
    ui->captionResultsListView->setLayoutMode(QListView::Batched);
    ui->captionResultsListView->setItemDelegate(new SearchResultsCaptionListDelegate(this));

    ui->captionResultsTreeView->setAttribute(Qt::WA_MacShowFocusRect,false);
    ui->captionResultsTreeView->setItemDelegate(new SearchResultsCaptionTreeDelegate(this));

    ui->captionResultsSplitter->setStretchFactor(0,0);
    ui->captionResultsSplitter->setStretchFactor(1,1);
    ui->captionResultsSplitter->setSizes(QList<int>() <<100<<200);
    ui->captionResultsSplitter->setCollapsible(0,false);


    QSortFilterProxyModel *filterModel = new QSortFilterProxyModel(this);
    filterModel->setFilterRole(Qt::UserRole + 100);
    filterModel->setSourceModel(new QStandardItemModel(this));
    ui->captionResultsListView->setModel(filterModel);
    ui->captionResultsTreeView->setModel(new QStandardItemModel(this));

    m_searchEngine = new Avocado::SearchEngine();
    m_searchEngine->moveToThread(&m_thread);
    m_thread.start();


    connect(this,&SearchResultsPanel::requestSearch,
            m_searchEngine,&Avocado::SearchEngine::search);

//    connect(ui->searchField,&QLineEdit::textChanged,
//            [this](const QString &text){
//        QString query = this->escapeQueries(text);
//        emit requestSearch(query);
//    });

//    connect(ui->searchField,&QLineEdit::textChanged,
//            [this](const QString &text){
//        if(text.isEmpty())
//        {
//            QSortFilterProxyModel *filterModel = new QSortFilterProxyModel(this);
//            filterModel->setFilterRole(Qt::UserRole + 100);
//            filterModel->setSourceModel(new QStandardItemModel(this));
//            ui->captionResultsListView->setModel(filterModel);
//            ui->captionResultsTreeView->setModel(new QStandardItemModel(this));
//            return;
//        }
//    });

    connect(ui->searchField,&QLineEdit::returnPressed,
            [this](){
        QString text = ui->searchField->text();
        if(text.isEmpty())
        {
            QSortFilterProxyModel *filterModel = new QSortFilterProxyModel(this);
            filterModel->setFilterRole(Qt::UserRole + 100);
            filterModel->setSourceModel(new QStandardItemModel(this));
            ui->captionResultsListView->setModel(filterModel);
            ui->captionResultsTreeView->setModel(new QStandardItemModel(this));
            return;
        }
        QString query = this->escapeQueries(text);
        emit requestSearch(query);
    });


    connect(m_searchEngine,&Avocado::SearchEngine::searchResult,
            this,&SearchResultsPanel::searchResult);



}

SearchResultsPanel::~SearchResultsPanel()
{
    m_thread.quit();
    m_thread.wait();
    delete ui;
}

QString SearchResultsPanel::escapeQueries(const QString &text)
{
    static QStringList charsToEscapeList;
    if (charsToEscapeList.isEmpty()) {
        charsToEscapeList << QLatin1String("\\") << QLatin1String("+") << QLatin1String("-")
                          << QLatin1String("!") << QLatin1String("(") << QLatin1String(")") << QLatin1String(":")
                          << QLatin1String("^") << QLatin1String("[") << QLatin1String("]") << QLatin1String("{")
                          << QLatin1String("}") << QLatin1String("~");
    }

    static QString escapeChar(QLatin1String("\\"));
    static QRegExp regExp(QLatin1String("[\\+\\-\\!\\(\\)\\^\\[\\]\\{\\}~:]"));
    QString word = text;
    if (word.contains(regExp)) {
        foreach (const QString &charToEscape, charsToEscapeList)
            word.replace(charToEscape, escapeChar + charToEscape);
    }
    return word;
}

void SearchResultsPanel::searchResult(const QVariantList &result)
{
    QMap<QString, int> titles;
    QMap<QString, int> artists;


    QStandardItemModel *listModel = new QStandardItemModel(this);
    foreach (QVariant variant , result) {
        QVariantMap map = variant.toMap();

        QString metaTitle = map.value("meta_title").toString();
        QString metaArtist = map.value("meta_artist").toString();
        QString captionText = map.value("caption_text").toString();

        artists[metaArtist]=artists.value(metaArtist,0)+1;
        titles[metaTitle]=titles.value(metaTitle,0)+1;

        QStandardItem *item = new QStandardItem(captionText);

        //caption_text,caption_index,caption_start,caption_end
        //unique_id,meta_title,meta_artist,meta_album,meta_genre,meta_date,meta_coverart,file_hash,file_path,file_type

        item->setData(map.value("unique_id").toString(),NodeRole::UniqueID);
        item->setData(metaTitle,NodeRole::MetaTitle);
        item->setData(metaArtist,NodeRole::MetaArtist);
        item->setData(map.value("meta_album").toString(),NodeRole::MetaAlbum);
        item->setData(map.value("meta_genre").toString(),NodeRole::MetaGenre);
        item->setData(map.value("meta_date").toString(),NodeRole::MetaDate);
        item->setData(map.value("meta_coverart").toString(),NodeRole::MetaCoverart);
        item->setData(map.value("file_hash").toString(),NodeRole::FileHash);
        item->setData(map.value("file_path").toString(),NodeRole::FilePath);
        item->setData(map.value("file_type").toString(),NodeRole::FileType);
        item->setData(captionText,NodeRole::CaptionText);
        item->setData(map.value("caption_index").toString(),NodeRole::CaptionIndex);
        item->setData(map.value("caption_start").toString(),NodeRole::CaptionStart);
        item->setData(map.value("caption_end").toString(),NodeRole::CaptionEnd);

        listModel->appendRow(item);
    }

    QStandardItemModel *treeModel = new QStandardItemModel(this);
    QString textRoot = QString(tr("All (%1)")).arg(result.count());
    QStandardItem *rootItem = new QStandardItem(textRoot);
    rootItem->setData(NodeRole::NodeTypeAll,NodeRole::NodeType);
    rootItem->setData(NodeRole::MetaTitle,NodeRole::FilterRole);
    rootItem->setData(QString(),NodeRole::FilterFixedString);
    treeModel->appendRow(rootItem);


    QStandardItem *artistsRootItem = new QStandardItem(tr("Artists"));
    artistsRootItem->setData(NodeRole::NodeTypeArtistAll,NodeRole::NodeType);
    artistsRootItem->setData(NodeRole::MetaArtist,NodeRole::FilterRole);
    artistsRootItem->setData(QString(),NodeRole::FilterFixedString);
    QMapIterator<QString, int> artistsIter(artists);
    while (artistsIter.hasNext()) {
        artistsIter.next();
        QString text = QString("%1 (%2)").arg(artistsIter.key()).arg(artistsIter.value());
        QStandardItem *item = new QStandardItem(text);
        item->setData(NodeRole::NodeTypeArtist,NodeRole::NodeType);
        item->setData(NodeRole::MetaArtist,NodeRole::FilterRole);
        item->setData(artistsIter.key(),NodeRole::FilterFixedString);
        artistsRootItem->appendRow(item);
    }
    treeModel->appendRow(artistsRootItem);


    QStandardItem *titlesRootItem = new QStandardItem(tr("Titles"));
    titlesRootItem->setData(NodeRole::NodeTypeTitleAll,NodeRole::NodeType);
    titlesRootItem->setData(NodeRole::MetaTitle,NodeRole::FilterRole);
    titlesRootItem->setData(QString(),NodeRole::FilterFixedString);

    QMapIterator<QString, int> titlesIter(titles);
    while (titlesIter.hasNext()) {
        titlesIter.next();
        QString text = QString("%1 (%2)").arg(titlesIter.key()).arg(titlesIter.value());
        QStandardItem *item = new QStandardItem(text);
        item->setData(NodeRole::NodeTypeTitle,NodeRole::NodeType);
        item->setData(NodeRole::MetaTitle,NodeRole::FilterRole);
        item->setData(titlesIter.key(),NodeRole::FilterFixedString);
        titlesRootItem->appendRow(item);
    }

    treeModel->appendRow(titlesRootItem);

    ui->captionResultsTreeView->setModel(treeModel);
    ui->captionResultsTreeView->expandAll();



    QSortFilterProxyModel *filterModel = new QSortFilterProxyModel(this);
    filterModel->setSourceModel(listModel);
    ui->captionResultsListView->setModel(filterModel);


    connect(ui->captionResultsTreeView->selectionModel(),&QItemSelectionModel::currentChanged,
            [this](const QModelIndex & current, const QModelIndex & previous){

        QSortFilterProxyModel *filterModel = qobject_cast<QSortFilterProxyModel*>(ui->captionResultsListView->model());
        if(filterModel)
        {
            filterModel->setFilterFixedString(current.data(NodeRole::FilterFixedString).toString());
            filterModel->setFilterRole(current.data(NodeRole::FilterRole).toInt());
            if(current.data(NodeRole::NodeType) == NodeRole::NodeTypeTitle)
            {

                filterModel->setSortLocaleAware(true);
                filterModel->setSortRole(NodeRole::CaptionIndex);
                filterModel->sort(1,Qt::AscendingOrder);
            }
        }
    });


    connect(ui->captionResultsListView,&QListView::doubleClicked,
            [this](const QModelIndex & current){

        QVariantMap map;
        map["unique_id"] = current.data(NodeRole::UniqueID);
        map["meta_title"] = current.data(NodeRole::MetaTitle);
        map["meta_artist"] = current.data(NodeRole::MetaArtist);
        map["meta_album"] = current.data(NodeRole::MetaAlbum);
        map["meta_genre"] = current.data(NodeRole::MetaGenre);
        map["meta_date"] = current.data(NodeRole::MetaDate);
        map["meta_coverart"] = current.data(NodeRole::MetaCoverart);
        map["file_hash"] = current.data(NodeRole::FileHash);
        map["file_path"] = current.data(NodeRole::FilePath);
        map["file_type"] = current.data(NodeRole::FileType);
        map["caption_index"] = current.data(NodeRole::CaptionIndex);
        map["caption_start"] = current.data(NodeRole::CaptionStart);
        map["caption_end"] = current.data(NodeRole::CaptionEnd);
        map["caption_text"] = current.data(NodeRole::CaptionText);
        map["model_index"] = current;
        emit captionItemDoubleClicked(map);
    });


}


