#include "avocado_searchengine.h"
#include "qsearchable_p.h"
#include "qqueryparser_p.h"
#include "error.h"

#include <QElapsedTimer>
#include <QDebug>


namespace Avocado {

SearchEngine::SearchEngine(QObject *parent) :
    QObject(parent)
{
}

SearchEngine::~SearchEngine()
{
//    mutex.lock();
//    this->m_cancel = true;
//    waitCondition.wakeOne();
//    mutex.unlock();

//    this->thread()->wait();
}


void SearchEngine::search(const QString &text)
{
    QElapsedTimer time;
    time.start();
    qDebug() << "search" << text;

    try{

        //caption_text,caption_index,caption_start,caption_end
        //unique_id,meta_title,meta_artist,meta_album,meta_genre,meta_date,meta_coverart,file_hash,file_path,file_type

        QString indexStoreFolder = "/Users/kiko/Desktop/media/index/caption";
        QCLuceneIndexSearcher *indexSearcher = new QCLuceneIndexSearcher(indexStoreFolder);

        QStringList field = QStringList() << "caption_text"<<"meta_title";
        QCLuceneStandardAnalyzer analyzer;

        QCLuceneQuery *query = QCLuceneMultiFieldQueryParser::parse(text.toUtf8(),field,
                                                                    analyzer);
        QVariantList list;
        if(query)
        {
            QCLuceneHits hits = indexSearcher->search(*query);
            qDebug() << hits.length();
            for (int i = 0; i < hits.length(); ++i) {
                QCLuceneDocument doc = hits.document(i);
                qreal score = hits.score(i);
                QVariantMap map;
                map["score"] = score;
                map["unique_id"] = doc.get("unique_id");
                map["meta_title"] = doc.get("meta_title");
                map["meta_artist"] = doc.get("meta_artist");
                map["meta_album"] = doc.get("meta_album");
                map["meta_genre"] = doc.get("meta_genre");
                map["meta_date"] = doc.get("meta_date");
                map["meta_coverart"] = doc.get("meta_coverart");
                map["file_hash"] = doc.get("file_hash");
                map["file_path"] = doc.get("file_path");
                map["file_type"] = doc.get("file_type");
                map["caption_index"] = doc.get("caption_index");
                map["caption_text"] = doc.get("caption_text");
                map["caption_start"] = doc.get("caption_start");
                map["caption_end"] = doc.get("caption_end");
                list.append(map);
            }
        }
        indexSearcher->close();
        emit searchResult(list);


    }catch(CLuceneError error){
        qDebug() << error.what();
    }

    qDebug() << "search finished" << time.elapsed();
}


} // namespace Avocado

