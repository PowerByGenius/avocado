#-------------------------------------------------
#
# Project created by QtCreator 2015-10-04T05:14:00
#
#-------------------------------------------------

QT       += core gui multimedia
CONFIG +=C++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include(clucene/fulltextsearch.pri)

#include(../clucene/clucene.pri)

TARGET = searchview
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    searchresultspanel.cpp \
    avocado_searchengine.cpp

HEADERS  += mainwindow.h \
    searchresultspanel.h \
    avocado_searchengine.h

FORMS    += mainwindow.ui \
    searchresultspanel.ui
