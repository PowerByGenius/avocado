DEFINES += _BUILD_FOR_QT_ LUCENE_DISABLE_MEMTRACKING
DEFINES += QT_STATIC
CONFIG  += hide_symbols
CONFIG += warn_off

win32:DEFINES += _CRT_SECURE_NO_DEPRECATE _MT

CLUCENEDIR = $${PWD}/clucene/src/CLucene

INCLUDEPATH += $${PWD}/. $${PWD}/.. \
               $$CLUCENEDIR \
               $$CLUCENEDIR/../ \
               $$CLUCENEDIR/analysis \
               $$CLUCENEDIR/analysis/standard \
               $$CLUCENEDIR/config \
               $$CLUCENEDIR/debug \
               $$CLUCENEDIR/document \
               $$CLUCENEDIR/index \
               $$CLUCENEDIR/queryParser \
               $$CLUCENEDIR/search \
               $$CLUCENEDIR/store \
               $$CLUCENEDIR/util


SOURCES += $$CLUCENEDIR/StdHeader.cpp \
           $$CLUCENEDIR/analysis/AnalysisHeader.cpp \
           $$CLUCENEDIR/analysis/Analyzers.cpp \
           $$CLUCENEDIR/config/gunichartables.cpp \
           $$CLUCENEDIR/config/repl_lltot.cpp \
           $$CLUCENEDIR/config/repl_tcscasecmp.cpp \
           $$CLUCENEDIR/config/repl_tcslwr.cpp \
           $$CLUCENEDIR/config/repl_tcstod.cpp \
           $$CLUCENEDIR/config/repl_tcstoll.cpp \
           $$CLUCENEDIR/config/repl_tprintf.cpp \
           $$CLUCENEDIR/config/threads.cpp \
           $$CLUCENEDIR/config/utf8.cpp \
           $$CLUCENEDIR/debug/condition.cpp \
           $$CLUCENEDIR/debug/error.cpp \
           $$CLUCENEDIR/debug/memtracking.cpp \
           $$CLUCENEDIR/document/DateField.cpp \
           $$CLUCENEDIR/document/Document.cpp \
           $$CLUCENEDIR/document/Field.cpp \
           $$CLUCENEDIR/index/CompoundFile.cpp \
           $$CLUCENEDIR/index/DocumentWriter.cpp \
           $$CLUCENEDIR/index/FieldInfos.cpp \
           $$CLUCENEDIR/index/FieldsReader.cpp \
           $$CLUCENEDIR/index/FieldsWriter.cpp \
           $$CLUCENEDIR/index/IndexModifier.cpp \
           $$CLUCENEDIR/index/IndexReader.cpp \
           $$CLUCENEDIR/index/IndexWriter.cpp \
           $$CLUCENEDIR/index/MultiReader.cpp \
           $$CLUCENEDIR/index/SegmentInfos.cpp \
           $$CLUCENEDIR/index/SegmentMergeInfo.cpp \
           $$CLUCENEDIR/index/SegmentMergeQueue.cpp \
           $$CLUCENEDIR/index/SegmentMerger.cpp \
           $$CLUCENEDIR/index/SegmentReader.cpp \
           $$CLUCENEDIR/index/SegmentTermDocs.cpp \
           $$CLUCENEDIR/index/SegmentTermEnum.cpp \
           $$CLUCENEDIR/index/SegmentTermPositions.cpp \
           $$CLUCENEDIR/index/SegmentTermVector.cpp \
           $$CLUCENEDIR/index/Term.cpp \
           $$CLUCENEDIR/index/TermInfo.cpp \
           $$CLUCENEDIR/index/TermInfosReader.cpp \
           $$CLUCENEDIR/index/TermInfosWriter.cpp \
           $$CLUCENEDIR/index/TermVectorReader.cpp \
           $$CLUCENEDIR/index/TermVectorWriter.cpp \
           $$CLUCENEDIR/queryParser/Lexer.cpp \
           $$CLUCENEDIR/queryParser/MultiFieldQueryParser.cpp \
           $$CLUCENEDIR/queryParser/QueryParser.cpp \
           $$CLUCENEDIR/queryParser/QueryParserBase.cpp \
           $$CLUCENEDIR/queryParser/QueryToken.cpp \
           $$CLUCENEDIR/queryParser/TokenList.cpp \
           $$CLUCENEDIR/search/BooleanQuery.cpp \
           $$CLUCENEDIR/search/BooleanScorer.cpp \
           $$CLUCENEDIR/search/CachingWrapperFilter.cpp \
           $$CLUCENEDIR/search/ChainedFilter.cpp \
           $$CLUCENEDIR/search/ConjunctionScorer.cpp \
           $$CLUCENEDIR/search/DateFilter.cpp \
           $$CLUCENEDIR/search/ExactPhraseScorer.cpp \
           $$CLUCENEDIR/search/Explanation.cpp \
           $$CLUCENEDIR/search/FieldCache.cpp \
           $$CLUCENEDIR/search/FieldCacheImpl.cpp \
           $$CLUCENEDIR/search/FieldDocSortedHitQueue.cpp \
           $$CLUCENEDIR/search/FieldSortedHitQueue.cpp \
           $$CLUCENEDIR/search/FilteredTermEnum.cpp \
           $$CLUCENEDIR/search/FuzzyQuery.cpp \
           $$CLUCENEDIR/search/HitQueue.cpp \
           $$CLUCENEDIR/search/Hits.cpp \
           $$CLUCENEDIR/search/IndexSearcher.cpp \
           $$CLUCENEDIR/search/MultiSearcher.cpp \
           $$CLUCENEDIR/search/MultiTermQuery.cpp \
           $$CLUCENEDIR/search/PhrasePositions.cpp \
           $$CLUCENEDIR/search/PhraseQuery.cpp \
           $$CLUCENEDIR/search/PhraseScorer.cpp \
           $$CLUCENEDIR/search/PrefixQuery.cpp \
           $$CLUCENEDIR/search/QueryFilter.cpp \
           $$CLUCENEDIR/search/RangeFilter.cpp \
           $$CLUCENEDIR/search/RangeQuery.cpp \
           $$CLUCENEDIR/search/SearchHeader.cpp \
           $$CLUCENEDIR/search/Similarity.cpp \
           $$CLUCENEDIR/search/SloppyPhraseScorer.cpp \
           $$CLUCENEDIR/search/Sort.cpp \
           $$CLUCENEDIR/search/TermQuery.cpp \
           $$CLUCENEDIR/search/TermScorer.cpp \
           $$CLUCENEDIR/search/WildcardQuery.cpp \
           $$CLUCENEDIR/search/WildcardTermEnum.cpp \
           $$CLUCENEDIR/store/FSDirectory.cpp \
           $$CLUCENEDIR/store/IndexInput.cpp \
           $$CLUCENEDIR/store/IndexOutput.cpp \
           $$CLUCENEDIR/store/Lock.cpp \
           $$CLUCENEDIR/store/MMapInput.cpp \
           $$CLUCENEDIR/store/RAMDirectory.cpp \
           $$CLUCENEDIR/store/TransactionalRAMDirectory.cpp \
           $$CLUCENEDIR/util/BitSet.cpp \
           $$CLUCENEDIR/util/Equators.cpp \
           $$CLUCENEDIR/util/FastCharStream.cpp \
           $$CLUCENEDIR/util/fileinputstream.cpp \
           $$CLUCENEDIR/util/Misc.cpp \
           $$CLUCENEDIR/util/Reader.cpp \
           $$CLUCENEDIR/util/StringBuffer.cpp \
           $$CLUCENEDIR/util/StringIntern.cpp \
           $$CLUCENEDIR/util/ThreadLocal.cpp \
           $$CLUCENEDIR/analysis/standard/StandardAnalyzer.cpp \
           $$CLUCENEDIR/analysis/standard/StandardFilter.cpp \
           $$CLUCENEDIR/analysis/standard/StandardTokenizer.cpp


#Header files
HEADERS += $${PWD}/qclucene_global_p.h \
           $${PWD}/qclucene-config_p.h \
           $${PWD}/qanalyzer_p.h \
           $${PWD}/qtokenizer_p.h \
           $${PWD}/qtoken_p.h \
           $${PWD}/qtokenstream_p.h \
           $${PWD}/qdocument_p.h \
           $${PWD}/qfield_p.h \
           $${PWD}/qindexreader_p.h \
           $${PWD}/qindexwriter_p.h \
           $${PWD}/qterm_p.h \
           $${PWD}/qqueryparser_p.h \
           $${PWD}/qfilter_p.h \
           $${PWD}/qhits_p.h \
           $${PWD}/qsearchable_p.h \
           $${PWD}/qsort_p.h \
           $${PWD}/qquery_p.h \
           $${PWD}/qreader_p.h


#Source files
SOURCES += $${PWD}/qanalyzer.cpp \
           $${PWD}/qtokenizer.cpp \
           $${PWD}/qtoken.cpp \
           $${PWD}/qtokenstream.cpp \
           $${PWD}/qdocument.cpp \
           $${PWD}/qfield.cpp \
           $${PWD}/qindexreader.cpp \
           $${PWD}/qindexwriter.cpp \
           $${PWD}/qterm.cpp \
           $${PWD}/qqueryparser.cpp \
           $${PWD}/qfilter.cpp \
           $${PWD}/qhits.cpp \
           $${PWD}/qsearchable.cpp \
           $${PWD}/qsort.cpp \
           $${PWD}/qquery.cpp \
           $${PWD}/qreader.cpp
