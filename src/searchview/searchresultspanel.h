#ifndef SEARCHRESULTSPANEL_H
#define SEARCHRESULTSPANEL_H

#include <QWidget>

#include <QThread>
#include <QItemDelegate>

namespace Ui {
class SearchResultsPanel;
}
namespace Avocado{
class SearchEngine;
}

//
// SearchResultsCaptionListDelegate
// =============================================================================

class SearchResultsCaptionListDelegate : public QItemDelegate
{
    Q_OBJECT
public:

    explicit SearchResultsCaptionListDelegate(QObject * parent = 0);
    void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
private:
    static QSize iconSize;
    static int padding;
    QFont m_headerFont;
    QFont m_subheaderFont;
};

//
// SearchResultsCaptionTreeDelegate
// =============================================================================

class SearchResultsCaptionTreeDelegate : public QItemDelegate
{
    Q_OBJECT
public:

    explicit SearchResultsCaptionTreeDelegate(QObject * parent = 0);
    void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
private:
    static QSize iconSize;
    static int padding;
    QFont m_headerFont;
    QFont m_subheaderFont;
};


//
// SearchResultsPanel
// =============================================================================

class SearchResultsPanel : public QWidget
{
    Q_OBJECT

public:
    explicit SearchResultsPanel(QWidget *parent = 0);
    ~SearchResultsPanel();
protected:
    QString escapeQueries(const QString &text);
private:
    Ui::SearchResultsPanel *ui;
    QThread m_thread;
    Avocado::SearchEngine *m_searchEngine;
signals:
    void requestSearch(const QString &query);
    void captionItemDoubleClicked(const QVariantMap &data);
public slots:
    void searchResult(const QVariantList &result);

};

#endif // SEARCHRESULTSPANEL_H
