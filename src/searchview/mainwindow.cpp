#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "avocado_searchengine.h"


using namespace Avocado;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_player(new QMediaPlayer(this))
{
    ui->setupUi(this);
    ui->dockWidget->setTitleBarWidget(new QWidget(this));




    connect(m_player,&QMediaPlayer::stateChanged,[=](QMediaPlayer::State state){
        qint64 start = m_player->property("play_start").toReal();
        qint64 end = m_player->property("play_end").toReal();
        if(state == QMediaPlayer::PlayingState)
            m_player->setPosition(start);
    });

    connect(m_player,&QMediaPlayer::durationChanged,[=](qint64 duration){
        qint64 start = m_player->property("play_start").toReal();
        qint64 end = m_player->property("play_end").toReal();
        m_player->setPosition(start);
    });

    connect(m_player,&QMediaPlayer::positionChanged,[=](qint64 position){
        qint64 start = m_player->property("play_start").toReal();
        qint64 end = m_player->property("play_end").toReal();
        if(position>=end){
            m_player->stop();
            m_player->setMedia(QMediaContent());
        }
    });

    connect(ui->dockWidgetContents,&SearchResultsPanel::captionItemDoubleClicked,
            [=](const QVariantMap &data){
        QString mediaLibrary = "/Users/kiko/Desktop/media/media";
        QString path = QString("%1/%2").arg(mediaLibrary).arg(data["file_path"].toString());
        QUrl url = QUrl::fromLocalFile(path);
//lottery

        qint64 start = textToPosition(data["caption_start"].toString());
        qint64 end = textToPosition(data["caption_end"].toString());

        m_player->setProperty("play_start",start);
        m_player->setProperty("play_end",end);

        if(m_player->currentMedia().canonicalUrl() == url )
        {
            m_player->pause();
        }else{
            m_player->setMedia(url);

        }
        m_player->setPosition(start);
        m_player->play();
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

qint64 MainWindow::textToPosition(const QString &text)
{
    qreal time = 0;
    QRegExp rx("(\\d{2}):(\\d{2}):(\\d{2}),(\\d{3})");
    if(rx.exactMatch(text))
    {
         time = rx.cap(4).toInt()
                +rx.cap(3).toInt() * 1000
                +rx.cap(2).toInt() * 60  * 1000
                +rx.cap(1).toInt() * 60 * 60 * 1000;
    }
    return time;
}
