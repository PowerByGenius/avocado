LIBS += -L../taglib -ltaglib

TAGLIBDIR = $${PWD}/taglib

INCLUDEPATH += $${PWD}/. $${PWD}/.. \
               $$TAGLIBDIR \
               $$TAGLIBDIR/../ \
               $$TAGLIBDIR/ape \
               $$TAGLIBDIR/asf \
               $$TAGLIBDIR/flac \
               $$TAGLIBDIR/it \
               $$TAGLIBDIR/mod \
               $$TAGLIBDIR/mp4 \
               $$TAGLIBDIR/mpc \
               $$TAGLIBDIR/mpeg \
               $$TAGLIBDIR/mpeg/id3v1 \
               $$TAGLIBDIR/mpeg/id3v2 \
               $$TAGLIBDIR/mpeg/id3v2/frames \
               $$TAGLIBDIR/ogg \
               $$TAGLIBDIR/ogg/flac \
               $$TAGLIBDIR/ogg/opus \
               $$TAGLIBDIR/ogg/speex \
               $$TAGLIBDIR/ogg/vorbis \
               $$TAGLIBDIR/riff \
               $$TAGLIBDIR/riff/aiff \
               $$TAGLIBDIR/riff/wav \
               $$TAGLIBDIR/s3m \
               $$TAGLIBDIR/toolkit \
               $$TAGLIBDIR/trueaudio \
               $$TAGLIBDIR/wavpack \
               $$TAGLIBDIR/xm
