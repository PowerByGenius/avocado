#-------------------------------------------------
#
# Project created by QtCreator 2015-10-06T13:09:22
#
#-------------------------------------------------

QT       -= core gui

TARGET = taglib
TEMPLATE = lib
CONFIG += staticlib c11++

include(taglib/taglib.pri)

#unix {
#    target.path = /usr/lib
#    INSTALLS += target
#}
