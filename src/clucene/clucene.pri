LIBS += -L../clucene -lclucene

CLUCENEDIR = $${PWD}/clucene/clucene/src/CLucene

INCLUDEPATH += $${PWD}/. $${PWD}/.. $${PWD}/clucene \
               $$CLUCENEDIR \
               $$CLUCENEDIR/../ \
               $$CLUCENEDIR/analysis \
               $$CLUCENEDIR/analysis/standard \
               $$CLUCENEDIR/config \
               $$CLUCENEDIR/debug \
               $$CLUCENEDIR/document \
               $$CLUCENEDIR/index \
               $$CLUCENEDIR/queryParser \
               $$CLUCENEDIR/search \
               $$CLUCENEDIR/store \
               $$CLUCENEDIR/util


