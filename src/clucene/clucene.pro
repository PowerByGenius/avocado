#-------------------------------------------------
#
# Project created by QtCreator 2015-10-06T13:09:22
#
#-------------------------------------------------

QT       -= gui

TARGET = clucene
TEMPLATE = lib
CONFIG += staticlib c11++

include(clucene/fulltextsearch.pri)

#unix {
#    target.path = /usr/lib
#    INSTALLS += target
#}
