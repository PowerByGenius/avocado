#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "avocado_captionindexer.h"
#include <QTimer>
namespace Ui {
class MainWindow;
}
namespace Avocado {
class SearchEngineIndexer;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnIndex_clicked();

    void on_btnSearch_clicked();

    void updateStatus(int document,int documentCount,
                     int caption,int captionCount,
                     const QString &itemText,const QString &captionText);

private:
    Ui::MainWindow *ui;
    Avocado::SearchEngineIndexer *m_indexer;
    QThread m_captionIndexThread;
    Avocado::CaptionIndexer *m_captionIndexer;
    QTimer *timer;
};

#endif // MAINWINDOW_H
