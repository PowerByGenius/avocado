#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "avocado_searchengineindexer.h"
#include <QApplication>
#include <QDebug>

using namespace Avocado;
struct IndexStatus
{
int document;
int documentCount;
int caption;
int captionCount;
QString itemText;
QString captionText;
};
IndexStatus status;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_indexer(new SearchEngineIndexer(this)),
    timer(new QTimer(this))
{
    ui->setupUi(this);
    m_captionIndexer = new Avocado::CaptionIndexer();
    m_captionIndexer->moveToThread(&m_captionIndexThread);

    connect(ui->btnCaptionIndexerStart,&QPushButton::clicked,
            m_captionIndexer,&CaptionIndexer::rebuildIndex);

    connect(ui->btnCaptionIndexerCancel,&QPushButton::clicked,
            m_captionIndexer,&CaptionIndexer::cancel);

    m_captionIndexThread.start();
    timer->setInterval(10);

        connect(m_captionIndexer,
                SIGNAL(indexStarted()),
                timer,SLOT(start()));

        connect(m_captionIndexer,&CaptionIndexer::indexFinished,
                timer,&QTimer::stop);

//    connect(m_captionIndexer,&CaptionIndexer::indexStarted,
//            [this](){
//        updateStatus(0,0,0,0,QString(),QString());
//        timer->start();
//    });
//    connect(m_captionIndexer,&CaptionIndexer::indexFinished,
//            [this](){
//        updateStatus(0,0,0,0,QString(),QString());
//        timer->stop();
//    });

    connect(m_captionIndexer,&CaptionIndexer::indexStatus,
            this,&MainWindow::updateStatus,Qt::QueuedConnection);

    connect(timer,&QTimer::timeout,[=]{

        ui->progressBar_1->setMinimum(0);
        ui->progressBar_1->setMaximum(status.documentCount);
        ui->progressBar_1->setValue(status.document);


        ui->progressBar_2->setMinimum(0);
        ui->progressBar_2->setMaximum(status.captionCount);
        ui->progressBar_2->setValue(status.caption);


        ui->label_1->setText(status.itemText);
        ui->label_2->setText(status.captionText);
        ui->barLabel_1->setText(QString("%1/%2").arg(status.document).arg(status.documentCount));
        ui->barLabel_2->setText(QString("%1/%2").arg(status.caption).arg(status.captionCount));
        qApp->processEvents();
    });
}

MainWindow::~MainWindow()
{

    m_captionIndexer->cancel();
    m_captionIndexThread.quit();
    m_captionIndexThread.wait();
    delete ui;
}


void MainWindow::on_btnIndex_clicked()
{
    m_indexer->index();
}

void MainWindow::on_btnSearch_clicked()
{
    m_indexer->search(ui->lineEdit->text());
}

void MainWindow::updateStatus(int document, int documentCount, int caption, int captionCount, const QString &itemText, const QString &captionText)
{
    status.document = document;
    status.documentCount = documentCount;
    status.caption = caption;
    status.captionCount = captionCount;
    status.itemText = itemText;
    status.captionText = captionText;
}
