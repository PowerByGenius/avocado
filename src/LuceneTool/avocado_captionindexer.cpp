#include "avocado_captionindexer.h"
#include <QElapsedTimer>
#include <QSqlQuery>
#include <QDebug>
#include <QDateTime>

#include "qindexwriter_p.h"
#include "qsearchable_p.h"
#include "qqueryparser_p.h"
#include "error.h"

namespace Avocado {

CaptionIndexer::CaptionIndexer(QObject *parent) : QObject(parent)
{
    m_status = Stoped;
    m_cancel = false;
}


QList<QVariantMap> CaptionIndexer::parser(const QString &text) const
{
    QRegExp rx("(\\d+)\\s*(\\d{2}:\\d{2}:\\d{2},\\d{3}) --> (\\d{2}:\\d{2}:\\d{2},\\d{3})");

    QList<QVariantMap> list;
    int pos = 0;
    int posNext = 0;
    while ((pos = rx.indexIn(text, pos)) != -1)
    {
        QVariantMap map;
        map.insert("index",rx.cap(1).trimmed());
        map.insert("start",rx.cap(2).trimmed());
        map.insert("end",rx.cap(3).trimmed());

        pos += rx.matchedLength();
        posNext = text.indexOf(rx,pos);
        QString temp;
        if(posNext > 0){
            temp = text.mid(pos,posNext-pos);
        }else{
            temp = text.mid(pos);
        }
        map.insert("caption",temp.trimmed());

        list.append(map);
    }

    return list;
}

void CaptionIndexer::rebuildIndex()
{
    if(m_status == Running)
    {
        return;
    }

    m_mutex.lock();
    m_cancel = false;
    m_status = Running;
    m_mutex.unlock();

    emit indexStarted();

    QElapsedTimer timer;
    timer.start();

    QString connectionName = "CaptionIndexer";
    QSqlDatabase db = QSqlDatabase::database(connectionName);

    if(!db.isValid())
    {
        db = QSqlDatabase::addDatabase("QSQLITE",connectionName);
        db.setDatabaseName("/Users/kiko/Desktop/Library/Avocado/Qt/Avocado/database/avocado.sqlite");
    }
    bool ok = db.open();


    QString indexStoreFolder = "/Users/kiko/Desktop/media/index/caption";
    QCLuceneStandardAnalyzer analyzer;
    QCLuceneIndexWriter *writer = new QCLuceneIndexWriter(indexStoreFolder,analyzer,true);
    writer->setMergeFactor(100);
    writer->setMinMergeDocs(1000);

    int docCount = 0;
    int docCurrent = 0;

    QSqlQuery qc("SELECT count(*) FROM avocado_media",db);
    if(qc.next())
    {
        docCount = qc.value(0).toInt();
    }


    QSqlQuery query("SELECT * FROM avocado_media",db);

    while (query.next()) {

        if(m_cancel){
            m_mutex.lock();
            m_status = Stoped;
            m_mutex.unlock();
            emit indexCanceled();
            break;
        }

        QString captionText = query.value("meta_caption_en").toString();

        QList<QVariantMap> captionList = parser(captionText);

        int captionCount = captionList.count();
        int captionCurrent = 1;
        foreach (QVariant var, captionList) {

            if(m_cancel){
                m_mutex.lock();
                m_status = Stoped;
                m_mutex.unlock();
                emit indexCanceled();
                break;
            }

            QVariantMap map = var.toMap();

            emit indexStatus(docCurrent,docCount,
                             captionCurrent,captionCount,
                             query.value("meta_title").toString(),
                             map.value("caption").toString());


            QCLuceneDocument doc;
            //caption_text,caption_index,caption_start,caption_end
            //unique_id,meta_title,meta_artist,meta_album,meta_genre,meta_date,meta_coverart,file_hash,file_path,file_type
            doc.add(new QCLuceneField("caption_text",map.value("caption").toString(),
                                      QCLuceneField::STORE_YES | QCLuceneField::INDEX_TOKENIZED));

            doc.add(new QCLuceneField("caption_index",map.value("index").toString(),
                                      QCLuceneField::STORE_YES| QCLuceneField::INDEX_UNTOKENIZED));

            doc.add(new QCLuceneField("caption_start",map.value("start").toString(),
                                      QCLuceneField::STORE_YES| QCLuceneField::INDEX_UNTOKENIZED));

            doc.add(new QCLuceneField("caption_end",map.value("end").toString(),
                                      QCLuceneField::STORE_YES| QCLuceneField::INDEX_UNTOKENIZED));

            foreach (QString val, QString("unique_id,meta_title,meta_artist,meta_album,meta_genre,meta_date,meta_coverart,file_hash,file_path,file_type").split(",")) {
                doc.add(new QCLuceneField(val,query.value(val).toString(),
                                          QCLuceneField::STORE_YES| QCLuceneField::INDEX_UNTOKENIZED));
            }

            writer->addDocument(doc,analyzer);
            captionCurrent++;
        }
        docCurrent++;
    }
    writer->optimize();
    writer->close();
    delete writer;
    db.close();

    m_mutex.lock();
    m_status = Stoped;
    emit indexFinished();
    m_mutex.unlock();
    qDebug() << timer.elapsed()/1000;
}

void CaptionIndexer::cancel()
{
    if(m_status == Running)
    {
        m_mutex.lock();
        m_cancel = true;
        m_mutex.unlock();
    }
    return;
}


} // namespace Avocado

