#include "avocado_searchengineindexer.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QElapsedTimer>

#include "qindexwriter_p.h"
#include "qsearchable_p.h"
#include "qqueryparser_p.h"
#include "error.h"

namespace Avocado {

SearchEngineIndexer::SearchEngineIndexer(QObject *parent) : QObject(parent)
{

}

void SearchEngineIndexer::index()
{

    QElapsedTimer timer;
    timer.start();

    QString connectionName = "SearchEnglineIndexer";
    QSqlDatabase db = QSqlDatabase::database("connectionName");

    if(!db.isValid())
    {
        db = QSqlDatabase::addDatabase("QSQLITE",connectionName);
        db.setDatabaseName("/Users/kiko/Desktop/Library/Avocado/Qt/Avocado/database/avocado.sqlite");
    }
    bool ok = db.open();


    QString indexStoreFolder = "/Users/kiko/Desktop/Library/Avocado/Qt/Avocado/index";
    QCLuceneStandardAnalyzer analyzer;
    QCLuceneIndexWriter *writer = new QCLuceneIndexWriter(indexStoreFolder,analyzer,true);




    QSqlQuery query("SELECT * FROM tracks",db);
    while (query.next()) {
        QString id = query.value("id").toString();
        QString name = query.value("name").toString();
        QString index = query.value("index").toString();
        QString start = query.value("start").toString();
        QString end = query.value("end").toString();
        QString text = query.value("text").toString();

        QCLuceneDocument doc;
        doc.add(new QCLuceneField("text",query.value("text").toString(),
                                  QCLuceneField::STORE_YES | QCLuceneField::INDEX_TOKENIZED));
        doc.add(new QCLuceneField("id",query.value("id").toString(),
                                  QCLuceneField::STORE_YES| QCLuceneField::INDEX_UNTOKENIZED));
        doc.add(new QCLuceneField("name",query.value("name").toString(),
                                  QCLuceneField::STORE_YES| QCLuceneField::INDEX_TOKENIZED));
        doc.add(new QCLuceneField("index",query.value("index").toString(),
                                  QCLuceneField::STORE_YES| QCLuceneField::INDEX_UNTOKENIZED));
        doc.add(new QCLuceneField("start",query.value("start").toString(),
                                  QCLuceneField::STORE_YES| QCLuceneField::INDEX_UNTOKENIZED));
        doc.add(new QCLuceneField("end",query.value("end").toString(),
                                  QCLuceneField::STORE_YES| QCLuceneField::INDEX_UNTOKENIZED));
        writer->addDocument(doc,analyzer);

        qDebug() << id << name << text;
    }
    writer->close();
    delete writer;
    db.close();
    qDebug() << timer.elapsed()/1000;
}

void SearchEngineIndexer::search(const QString text)
{
    QCLuceneIndexSearcher *indexSearcher = 0;
    try{
    QString indexStoreFolder = "/Users/kiko/Desktop/Library/Avocado/Qt/Avocado/index";
    QCLuceneStandardAnalyzer analyzer;

//    QCLuceneIndexReader indexReader = QCLuceneIndexReader::open(indexStoreFolder);
    QCLuceneIndexSearcher *indexSearcher = new QCLuceneIndexSearcher(indexStoreFolder);

    QStringList field = QStringList() << "text"<<"name";
    QCLuceneQuery *query = QCLuceneMultiFieldQueryParser::parse(text,field,analyzer);

    if(query)
    {
        QCLuceneHits hits = indexSearcher->search(*query);
        qDebug() << hits.length();
//        for (int i = 0; i < hits.length(); ++i) {
//            qDebug() << hits.document(i).get("name") << hits.document(i).get("text");
//        }
    }

    }catch(CLuceneError error){
        qDebug() << error.what();
    }

    if(indexSearcher)
        indexSearcher->close();


//    indexReader.close();
}

} // namespace Avocado

