#ifndef AVOCADO_SEARCHENGINEINDEXER_H
#define AVOCADO_SEARCHENGINEINDEXER_H

#include <QObject>

namespace Avocado {

class SearchEngineIndexer : public QObject
{
    Q_OBJECT
public:
    explicit SearchEngineIndexer(QObject *parent = 0);
    void index();
    void search(const QString text);
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_SEARCHENGINEINDEXER_H
