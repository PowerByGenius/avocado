#-------------------------------------------------
#
# Project created by QtCreator 2015-10-04T06:41:59
#
#-------------------------------------------------

QT       += core gui sql


include(clucene/fulltextsearch.pri)
CONFIG += C++11
#include(../clucene/clucene.pri)
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LuceneTool
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    avocado_searchengineindexer.cpp \
    avocado_captionindexer.cpp

HEADERS  += mainwindow.h \
    avocado_searchengineindexer.h \
    avocado_captionindexer.h

FORMS    += mainwindow.ui
