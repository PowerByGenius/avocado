#ifndef AVOCADO_CAPTIONINDEXER_H
#define AVOCADO_CAPTIONINDEXER_H

#include <QObject>
#include <QMutex>

namespace Avocado {

class CaptionIndexer : public QObject
{
    Q_OBJECT
public:
    explicit CaptionIndexer(QObject *parent = 0);
private:
    QList<QVariantMap> parser(const QString &text) const;
    enum Status{Running,Stoped};
    Status m_status;
    bool m_cancel;
    QMutex m_mutex;
signals:
    void indexStarted();
    void indexFinished();
    void indexStatus(int document,int documentCount,
                     int caption,int captionCount,
                     const QString &itemText,const QString &captionText);
    void indexCanceled();
public slots:
    void rebuildIndex();
    void cancel();
};

} // namespace Avocado

#endif // AVOCADO_CAPTIONINDEXER_H
