#-------------------------------------------------
#
# Project created by QtCreator 2015-10-04T03:59:32
#
#-------------------------------------------------

QT       += core gui webkit webkitwidgets
CONFIG += c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = richeditor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
