#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QWebFrame>
#include <QWebPage>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    connect(ui->webView->page(),&QWebPage::contentsChanged,[this](){
        QWebPage *page = ui->webView->page();
        ui->textEdit->setPlainText(page->currentFrame()->toHtml());

    });
    QWebPage *page = ui->webView->page();
    page->setContentEditable(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}
