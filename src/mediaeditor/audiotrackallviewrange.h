#ifndef AVOCADO_AUDIOTRACKALLVIEWRANGE_H
#define AVOCADO_AUDIOTRACKALLVIEWRANGE_H

#include <QObject>

namespace Avocado {

class AudioTrackAllViewRange : public QObject
{
    Q_OBJECT
public:
    explicit AudioTrackAllViewRange(QObject *parent = 0);

signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKALLVIEWRANGE_H
