#ifndef AVOCADO_AUDIOTRACKALLVIEW_H
#define AVOCADO_AUDIOTRACKALLVIEW_H

#include <QGraphicsView>
#include <QPointer>
#include <QWeakPointer>

#include "audiotrack.h"
#include "audiotrackview.h"

namespace Avocado {
//class AudioTrack;
//class AudioTrackView;
class AudioTrackAllView;
class AudioTrackAllViewTrack;

class AudioTrackAllView : public QGraphicsView
{
    Q_OBJECT
public:
    friend class AudioTrackAllViewTrack;
    friend class AudioTrackAllViewMarginLeft;
    friend class AudioTrackAllViewMarginRight;
    friend class AudioTrackAllViewWaveform;
    friend class AudioTrackAllViewIndicator;
    friend class AudioTrackAllViewRangeAB;
    friend class AudioTrackAllViewMark;

    explicit AudioTrackAllView(QWidget *parent = 0);
    void setAudioTrackView(AudioTrackView *audioTrackView);

    qreal marginLeft() const;
    qreal marginRight() const;
    qreal viewportWidth() const;
    qreal viewportHeight() const;
    qint64 length() const;
    qint64 currentPosition() const;
    qreal waveformLength() const;

private:
    AudioTrackAllViewTrack *m_allViewTrack;
    QPointer<AudioTrackView> m_audioTrackView;
    QPointer<AudioTrack> m_audioTrack;
protected:
    void drawBackground(QPainter * painter, const QRectF & rect);
    void resizeEvent(QResizeEvent *event);
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKALLVIEW_H
