#include "audiotrackmarklistview.h"
#include "QStandardItemModel"
#include "audiotrackview.h"
#include "audiotrack.h"
#include "audiotrackmark.h"
#include <QPainter>
#include <QTextDocument>
#include <QDebug>
#include <QMimeData>
#include <QDataStream>
#include <QDrag>

namespace Avocado {


QString positionToString(qint64 position)
{
    QString text;
    if(position/100>3600)
    {
        text = QString("%1:%2:%3.%4").arg(position/100/3600,2,10,QChar('0'))
                .arg(position/100/60%60,2,10,QChar('0'))
                .arg(position/100%60,2,10,QChar('0'))
                .arg(position*10%1000,3,10,QChar('0'));
    }else{
        text = QString("%1:%2.%3")
                .arg(position/100/60%60,2,10,QChar('0'))
                .arg(position/100%60,2,10,QChar('0'))
                .arg(position*10%1000,3,10,QChar('0'));
    }
    return text;
}

QString trackPositionToString(qint64 position)
{
    QString text;
        text = QString("%1:%2:%3.%4").arg(position/100/3600,2,10,QChar('0'))
                .arg(position/100/60%60,2,10,QChar('0'))
                .arg(position/100%60,2,10,QChar('0'))
                .arg(position*10%100,2,10,QChar('0'));
    return text;
}



// AudioTrackMarkListItemDelegate
// =============================================================================

AudioTrackMarkListItemDelegate::AudioTrackMarkListItemDelegate(QObject *parent):
    QItemDelegate(parent)
{

}

void AudioTrackMarkListItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    QRect rect = option.rect;


    if (option.state & QStyle::State_Selected){
        QRect selectedRect = option.rect;
        painter->fillRect(selectedRect,option.palette.highlight());
    }

    QRect rectContent = rect.adjusted(5,2,-5,-2);
    QRect rectTextContent = rectContent.adjusted(24,2,0,0);
    QRect rectTextTitle = rectTextContent;
    rectTextTitle.setHeight(32);

    QRect rectNumber = rectContent;
    rectNumber.setWidth(19);
    rectNumber.adjust(0,22,0,0);
    //painter->fillRect(rectNumber,Qt::red);

    QRect rectTimeContent = rectTextContent.adjusted(0,rectTextTitle.height(),0,0);
    QRect rectTimeA = rectTimeContent.adjusted(0,0,-rectTimeContent.width()/2,0);
    QRect rectTimeB = rectTimeContent.adjusted(rectTimeContent.width()/2,0,0,0);


    {
    painter->save();
    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(Qt::NoPen);
    painter->setBrush(QColor(255, 102, 0,200));
    QPoint iconTopLeft(rectContent.left()+2,rectContent.top()+2);
    int markIconHight = 23;
    int markIconWidth = 15;
    QPoint markIconPoints[5]={
        iconTopLeft,
        QPoint(iconTopLeft.x(),iconTopLeft.y()+markIconHight),
        QPoint(iconTopLeft.x()+markIconWidth/2,iconTopLeft.y()+markIconHight-5),
        QPoint(iconTopLeft.x()+markIconWidth,iconTopLeft.y()+markIconHight),
        QPoint(iconTopLeft.x()+markIconWidth,iconTopLeft.ry()),
    };
    painter->drawPolygon(markIconPoints,5);
    painter->restore();
    }

    if (option.state & QStyle::State_Selected){
        painter->setPen(Qt::white);
    }else{
        painter->setPen(Qt::black);
    }

    QString text = index.data(Qt::UserRole+200).toString();

    QString newText = painter->fontMetrics().elidedText(text, Qt::ElideRight,rectTextTitle.width());
    painter->drawText(rectTextTitle, Qt::AlignLeft | Qt::AlignTop | Qt::TextWordWrap|Qt::ElideLeft, newText);


//    QFontMetrics fm(painter->font());
//    text = fm.elidedText(text,Qt::ElideRight,rectTextTitle.width());

//    painter->drawText(rectTextTitle,Qt::AlignLeft|Qt::AlignVCenter|Qt::TextWordWrap,text);

    QVariant variant = index.data(Qt::UserRole+100);
    if(variant.isValid())
    {
        AudioTrackMark *mark = qvariant_cast<AudioTrackMark*>(variant);
        if(mark)
        {
            painter->drawText(rectNumber,Qt::AlignCenter,QString("%1").arg(mark->index()+1));

            painter->drawText(rectTimeA,Qt::AlignLeft|Qt::AlignVCenter,QString("A | %1")
                              .arg(positionToString(mark->markPosStart())));

            painter->drawText(rectTimeB,Qt::AlignLeft|Qt::AlignVCenter,QString("B | %1")
                              .arg(positionToString(mark->markPosEnd())));

        }
    }


}

QSize AudioTrackMarkListItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QSize size = QItemDelegate::sizeHint(option,index);
    size.setHeight(60);
    return size;
}


// AudioTrackMarkListModel
// =============================================================================
AudioTrackMarkListModel::AudioTrackMarkListModel(QObject *parent):
    QStandardItemModel(parent)
{

}

QMimeData *AudioTrackMarkListModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();

    QVariantList list;
    foreach (QModelIndex index, indexes) {
      if (index.isValid()) {
          QVariantMap map;
              map["index"] = index.row()+1;
              map["start"] = trackPositionToString(index.data(Qt::UserRole+110).toInt());
              map["end"]   = trackPositionToString(index.data(Qt::UserRole+120).toInt());
              map["length"]  = trackPositionToString(index.data(Qt::UserRole+120).toInt()-index.data(Qt::UserRole+110).toInt());
              map["text"]  = index.data(Qt::UserRole+200).toString();
              map["file"]  = index.data(Qt::UserRole+300).toString();
              list.append(map);
          }
      }

    QByteArray data;
    QDataStream stream(&data,QIODevice::WriteOnly);
    stream << list;
    mimeData->setProperty("count",list.count());
    mimeData->setData("application/x-track-mark",data);
    return mimeData;
}





// AudioTrackMarkListView
// =============================================================================

AudioTrackMarkListView::AudioTrackMarkListView(QWidget *parent) :
    QListView(parent),
    m_model(new AudioTrackMarkListModel(this)),
    m_sortModel(new QSortFilterProxyModel(this))
{
    setAttribute(Qt::WA_MacShowFocusRect,false);
    setUniformItemSizes(true);
    setDragEnabled(true);
    setDragDropMode(DragOnly);


    m_sortModel->setSourceModel(m_model);
    m_sortModel->setSortRole(Qt::UserRole+110);
    setModel(m_sortModel);
    setEditTriggers(QListView::NoEditTriggers);
    setItemDelegate(new AudioTrackMarkListItemDelegate(this));
}

void AudioTrackMarkListView::setAudioTrackView(AudioTrackView *trackView)
{
    m_trackView = trackView;
    connect(trackView,&AudioTrackView::markInserted,
            this,&AudioTrackMarkListView::markInsert);

    //    connect(trackView,&AudioTrackView::markAboutToBeMoved,
    //            this,&AudioTrackMarkListView::markMoved);

    connect(trackView,&AudioTrackView::markAboutToBeRemoved,
            this,&AudioTrackMarkListView::markRemoved);

    connect(trackView,&AudioTrackView::markDataChanged,
            this,&AudioTrackMarkListView::markDataChanged);


    connect(selectionModel(),&QItemSelectionModel::currentChanged,
            this,&AudioTrackMarkListView::currentChanged);


    connect(this,&AudioTrackMarkListView::activated,
            this,&AudioTrackMarkListView::currentActivated);



}

void AudioTrackMarkListView::markInsert(Avocado::AudioTrackMark *mark)
{
    QStandardItem *item = new QStandardItem();

    item->setText(QString("%1 %2-%3")
                  .arg(mark->index()+1)
                  .arg(positionToString(mark->markPosStart()))
                  .arg(positionToString(mark->markPosEnd()))
                  );

    item->setData(QVariant::fromValue(mark),Qt::UserRole+100);
    item->setData(mark->markPosStart(),Qt::UserRole+110);
    item->setData(mark->markPosEnd(),Qt::UserRole+120);
    item->setData(mark->textDocument()->toPlainText(),Qt::UserRole+200);
    item->setData(mark->mediaFilePath(),Qt::UserRole+300);
    m_model->appendRow(item);
    m_sortModel->sort(0);

}

void AudioTrackMarkListView::markRemoved(AudioTrackMark *mark)
{
    for (int i = 0; i < m_model->rowCount(); ++i) {
        QStandardItem *item = m_model->item(i);
        QVariant variant = item->data(Qt::UserRole+100);
        if(variant.isValid())
        {
            AudioTrackMark *itemMark = qvariant_cast<AudioTrackMark*>(variant);
            if(itemMark == mark)
            {
                m_model->removeRow(i);
                return;
            }
        }
    }
}

//void AudioTrackMarkListView::markMoved(AudioTrackMark *mark, int oldIndex, int newIndex)
//{
//    Q_UNUSED(oldIndex)
//    m_sortModel->sort(0);
//    update();
//}

void AudioTrackMarkListView::markDataChanged(AudioTrackMark *mark)
{
    for (int i = 0; i < m_model->rowCount(); ++i) {
        QStandardItem *item = m_model->item(i);
        QVariant variant = item->data(Qt::UserRole+100);
        if(variant.isValid())
        {
            AudioTrackMark *itemMark = qvariant_cast<AudioTrackMark*>(variant);
            if(itemMark == mark)
            {
                item->setData(mark->markPosStart(),Qt::UserRole+110);
                item->setData(mark->markPosEnd(),Qt::UserRole+120);
                item->setData(mark->textDocument()->toPlainText(),Qt::UserRole+200);
                item->setData(mark->mediaFilePath(),Qt::UserRole+300);
                item->setText(QString("%1 %2-%3")
                              .arg(mark->index()+1)
                              .arg(positionToString(mark->markPosStart()))
                              .arg(positionToString(mark->markPosEnd()))
                              );
                m_sortModel->sort(0);
                update();
                return;
            }
        }
    }
}

void AudioTrackMarkListView::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
    Q_UNUSED(current)
    Q_UNUSED(previous)

    //    QVariant variant = current.data(Qt::UserRole+100);
    //    if(variant.isValid())
    //    {
    //        AudioTrackMark *mark = qvariant_cast<AudioTrackMark*>(variant);
    //        if(mark)
    //        {
    //            m_trackView.data()->markFromListViewSelected(mark);
    //        }
    //    }
}

void AudioTrackMarkListView::currentActivated(const QModelIndex &index)
{
    QVariant variant = index.data(Qt::UserRole+100);
    if(variant.isValid())
    {
        AudioTrackMark *mark = qvariant_cast<AudioTrackMark*>(variant);
        if(mark)
        {
            m_trackView.data()->markFromListViewActivated(mark);
        }
    }
}

void AudioTrackMarkListView::startDrag(Qt::DropActions supportedActions)
{
    Q_UNUSED(supportedActions)

    QDrag *drag = new QDrag(this);
    QMimeData *mimeData = model()->mimeData(selectionModel()->selectedIndexes());
    drag->setMimeData(mimeData);
    QPixmap img = QPixmap(":/audiotrack/images/mark-drag-icon.png");

    int number = mimeData->property("count").toInt() > 99 ? 99:mimeData->property("count").toInt();
    if(number > 1){
        QRect count = QRect(0,0,20,20);
        QPainter p(&img);
        p.setRenderHints(QPainter::Antialiasing|QPainter::TextAntialiasing);
        p.setBrush(QColor(255,0,0));
        p.setPen(Qt::NoPen);
        p.drawEllipse(count);
        QFont font;
        font.setFamily("Arial");
        font.setPointSize(12);
        font.setBold(true);
        p.setFont(font);
        p.setPen(Qt::white);
        p.drawText(count,Qt::AlignCenter,QString("%1").arg(number));
    }
    drag->setPixmap(img);
    drag->setHotSpot(QPoint(32,37));
    Qt::DropAction result = drag->exec(supportedActions);
    emit dragIgnoreAction();
}






} // namespace Avocado

