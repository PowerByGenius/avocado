#include "exportdroppresetwidget.h"
#include "ui_exportdroppresetwidget.h"
#include <QDragEnterEvent>
#include <QFileInfo>
#include <QMimeData>
#include <QPainter>
#include <QDebug>
namespace Avocado {

ExportDropPresetWidget::ExportDropPresetWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ExportDropPresetWidget),
    m_isDragEnter(false)
{
    ui->setupUi(this);
    setAcceptDrops(true);
    QPalette pe;pe.setColor(QPalette::WindowText,Qt::white);ui->label_text->setPalette(pe);

}

ExportDropPresetWidget::~ExportDropPresetWidget()
{
    delete ui;
}

void ExportDropPresetWidget::setPixmap(const QPixmap &pixmap)
{
    ui->label_icon->setPixmap(pixmap.scaled(64,64,Qt::KeepAspectRatio,Qt::SmoothTransformation));
}

const QPixmap *ExportDropPresetWidget::pixmap()
{
    return ui->label_icon->pixmap();
}

void ExportDropPresetWidget::setText(const QString &text)
{
    ui->label_text->setText(text);
}

void ExportDropPresetWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasFormat("application/x-track-mark"))
    {
        m_isDragEnter = true;
        update();
        event->accept();
    }
    QWidget::dragEnterEvent(event);
}

void ExportDropPresetWidget::dragLeaveEvent(QDragLeaveEvent *event)
{
    m_isDragEnter = false;
    update();
    QWidget::dragLeaveEvent(event);
}

void ExportDropPresetWidget::dropEvent(QDropEvent *event)
{
    if(event->mimeData()->hasFormat("application/x-track-mark"))
    {
        //unpack
        QVariantList list;
        QByteArray data = event->mimeData()->data("application/x-track-mark");
        QDataStream stream(&data,QIODevice::ReadOnly);
        stream >> list;

        QVariantMap map;
//        map["argument"] = QStringList();
        map["files"] = list;
        setPresetMap(&map);
        emit dropAccepted(map);

        event->acceptProposedAction();
        m_isDragEnter = false;
        update();
    }
}

void ExportDropPresetWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    if(m_isDragEnter)
    {
        QPainter p (this);
        p.setRenderHint(QPainter::Antialiasing,true);
        QRect rect = this->rect();
        rect.adjust(5,5,-5,-5);
        p.setPen(QColor(255,255,255,64));
        p.setBrush(QColor(21,21,21,128));
        p.drawRoundedRect(rect,7,7);
    }
}

ExportDropPresetWidgetAudio::ExportDropPresetWidgetAudio(QWidget *parent):
    ExportDropPresetWidget(parent)
{
    setPixmap(QPixmap(":/audiotrack/images/preset-icon-audio.png"));
    setText(tr("Audio (Copy)"));
}

void ExportDropPresetWidgetAudio::setPresetMap(QVariantMap *presetMap)
{

    QVariantList list = presetMap->value("files").toList();
    for(int i=0;i<list.count();i++) {
        QVariantMap map = list.at(i).toMap();
        map["icon"] = pixmap()->scaled(20,20,Qt::KeepAspectRatio,Qt::SmoothTransformation);
        map["preset_name"] = QString("audiocopy");
        map["file_target_suff"] = QString("m4a");
        map["argument"] = QString("-vn -c:a copy");
        list[i] = map;
    }
    presetMap->insert("files",list);
}

ExportDropPresetWidgetVideo::ExportDropPresetWidgetVideo(QWidget *parent):
    ExportDropPresetWidget(parent)
{
    setPixmap(QPixmap(":/audiotrack/images/preset-icon-video.png"));
    setText(tr("Video (Copy)"));
}

void ExportDropPresetWidgetVideo::setPresetMap(QVariantMap *presetMap)
{
    QVariantList list = presetMap->value("files").toList();
    for(int i=0;i<list.count();i++) {
        QVariantMap map = list.at(i).toMap();
        map["icon"] = pixmap()->scaled(20,20,Qt::KeepAspectRatio,Qt::SmoothTransformation);
        map["preset_name"] = QString("videocopy");
        map["file_target_suff"] = QFileInfo(map["file"].toString()).suffix();
        map["argument"] = QString("-c copy");
        list[i] = map;
    }
    presetMap->insert("files",list);
}


ExportDropPresetWidgetYoutube::ExportDropPresetWidgetYoutube(QWidget *parent):
    ExportDropPresetWidget(parent)
{
    setPixmap(QPixmap(":/audiotrack/images/preset-icon-youtube.png"));
    setText(tr("Youtube (Mp4/720p)"));
}

void ExportDropPresetWidgetYoutube::setPresetMap(QVariantMap *presetMap)
{

    //https://trac.ffmpeg.org/wiki/Encode/H.264
    //ffmpeg -i <input file> -codec:v libx264 -crf 21 -bf 2 -flags +cgop -pix_fmt yuv420p -codec:a aac -strict -2 -b:a 384k -r:a 48000 -movflags faststart <output_name>.mp4
    // 480p
    //ffmpeg -i input.mkv -vcodec libx264 -vpre medium -crf 15 -s 640x272 -aspect 640:272 -r 23.976 -threads 4 -acodec libvo_aacenc -ab 128k -ar 48000 -async 48000 -ac 2 -scodec copy output.mkv

    QVariantList list = presetMap->value("files").toList();
    for(int i=0;i<list.count();i++) {
        QVariantMap map = list.at(i).toMap();
        map["icon"] = pixmap()->scaled(20,20,Qt::KeepAspectRatio,Qt::SmoothTransformation);
        map["preset_name"] = QString("youtube720p");
        map["file_target_suff"] = QString("mp4");
        map["argument"] = QString("-codec:v libx264 -crf 21 -bf 2 -flags +cgop -pix_fmt yuv420p -codec:a aac -strict -2 -b:a 384k -r:a 48000 -movflags faststart");
        list[i] = map;
    }

    presetMap->insert("files",list);

}

ExportDropPresetWidgetITunes::ExportDropPresetWidgetITunes(QWidget *parent):
    ExportDropPresetWidget(parent)
{
    setPixmap(QPixmap(":/audiotrack/images/preset-icon-itunes.png"));
    setText(tr("iTunes Plus"));
}

void ExportDropPresetWidgetITunes::setPresetMap(QVariantMap *presetMap)
{

    //https://trac.ffmpeg.org/wiki/Encode/H.264
    //ffmpeg -i <input file> -codec:v libx264 -crf 21 -bf 2 -flags +cgop -pix_fmt yuv420p -codec:a aac -strict -2 -b:a 384k -r:a 48000 -movflags faststart <output_name>.mp4
    // 480p
    //ffmpeg -i input.mkv -vcodec libx264 -vpre medium -crf 15 -s 640x272 -aspect 640:272 -r 23.976 -threads 4 -acodec libvo_aacenc -ab 128k -ar 48000 -async 48000 -ac 2 -scodec copy output.mkv

    QVariantList list = presetMap->value("files").toList();
    for(int i=0;i<list.count();i++) {
        QVariantMap map = list.at(i).toMap();
        map["icon"] = pixmap()->scaled(20,20,Qt::KeepAspectRatio,Qt::SmoothTransformation);
        map["preset_name"] = QString("itunesplus");
        map["file_target_suff"] = QString("m4a");
        map["argument"] = QString("-vn -codec:a libfdk_aac -b:a 256k -r:a 44100");
        list[i] = map;
    }

    presetMap->insert("files",list);

}

} // namespace Avocado
