#ifndef AVOCADO_AUDIOTRACKALLVIEWRANGEAB_H
#define AVOCADO_AUDIOTRACKALLVIEWRANGEAB_H

#include <QObject>

#include <QGraphicsObject>

namespace Avocado {

class AudioTrackAllView;
class AudioTrackAllViewTrack;
class AudioTrackAllViewRangeAB;

class AudioTrackAllViewRangeABHandle : public QGraphicsItem
{
public:
    enum HandleTypes{HandleStart,HandleEnd};
    explicit AudioTrackAllViewRangeABHandle(const HandleTypes types,AudioTrackAllViewRangeAB *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
private:
    AudioTrackAllViewRangeAB *m_trackRange;
    HandleTypes m_handleType;
    QPointF m_buttonDownPos;
    int m_size;
signals:

public slots:
};


class AudioTrackAllViewRangeAB: public QGraphicsObject
{
    Q_OBJECT
public:
    friend class AudioTrackAllViewRangeABHandle;
    explicit AudioTrackAllViewRangeAB(AudioTrackAllView *view, QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;

    inline void setRangePosStart(qreal value){m_posStart=value;prepareGeometryChange();}
    inline void setRangePosEnd(qreal value){m_posEnd = value;prepareGeometryChange();}

    qreal rangePosStart() {return m_posStart;}
    qreal rangePosEnd() {return m_posEnd;}

    void setRangePosition(const qint64 &start,const qint64 &end);
    void rangeDragChanged();
    void reLayout();
    void setRangeABLoop(bool value);
    bool isRangeABLoop()const {return m_isRangeABLoop;}
protected:

    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);

private:
    AudioTrackAllView *m_view;
    AudioTrackAllViewRangeABHandle *m_handleStart;
    AudioTrackAllViewRangeABHandle *m_handleEnd;
    qreal m_posStart;
    qreal m_posEnd;

    bool isMoving;
    QPointF m_buttonDownPos;

    qint64 m_positionStart;
    qint64 m_positionEnd;
    bool m_isRangeABLoop;
signals:
    void rangePositionChanged(const qint64 &start,const qint64 &end);
public slots:
};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKALLVIEWRANGEAB_H
