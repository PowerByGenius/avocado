#include "exportprocesslistview.h"
#include "exportprocessitemwidget.h"
#include <QPainter>
#include <QDrag>
#include <QFileInfo>
#include <QMimeData>
#include <QUrl>
#include <QDebug>

namespace Avocado {

ExportProcessListDelgate::ExportProcessListDelgate(QWidget *parent):
    QItemDelegate(parent)
{

}

void ExportProcessListDelgate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(option.state & QStyle::State_Selected)
    {
        QRect rect = option.rect;
        painter->fillRect(rect,QColor(27,27,27,32));
    }
}


ExportProcessListView::ExportProcessListView(QWidget *parent)
    : QListWidget(parent)
{
    setAttribute(Qt::WA_MacShowFocusRect,false);
    setItemDelegate(new ExportProcessListDelgate(this));
    setSelectionMode(QListWidget::ExtendedSelection);
    setDragEnabled(true);
}

void ExportProcessListView::onItemWidgetFinished(ExportProcessItemWidget *itemWidget)
{
    // TODO: 所有item完成后会call这个函数
    //qDebug() << "itemWidget finished!";
}

void ExportProcessListView::addTask(const QVariantMap &data)
{

    QList<QVariant> list = data.value("files").toList();
    foreach (QVariant v, list) {
        QVariantMap map = v.toMap();
        QListWidgetItem *item = new QListWidgetItem();
        ExportProcessItemWidget *widget = new ExportProcessItemWidget(item,map,this);
        connect(widget,&ExportProcessItemWidget::finished,
                this,&ExportProcessListView::onItemWidgetFinished);
        item->setData(Qt::UserRole+100,QVariant::fromValue(widget));
        insertItem(0,item);
        setItemWidget(item,widget);
        widget->start();
    }

}

QMimeData *ExportProcessListView::mimeData(const QList<QListWidgetItem *> items) const
{
    QMimeData *mimeData = new QMimeData();
    QList<QUrl> urls;
    foreach (QListWidgetItem *item, items) {
        QString path = item->data(Qt::UserRole+1000).toString();
        if(QFileInfo(path).exists())
        {
            urls.append(QUrl::fromLocalFile(path));
        }
    }
    mimeData->setUrls(urls);
    return mimeData;
}

void ExportProcessListView::startDrag(Qt::DropActions supportedActions)
{
    QMimeData *mimeData = model()->mimeData(selectionModel()->selectedIndexes());
    if(mimeData->urls().count() <1) return;

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);
    QPixmap img = QPixmap(":/audiotrack/images/process-drag-icon.png");
    int number = drag->mimeData()->urls().count() > 99 ?99:drag->mimeData()->urls().count();
    if(number > 1){
        QRect count = QRect(0,0,20,20);
        QPainter p(&img);
        p.setRenderHints(QPainter::Antialiasing|QPainter::TextAntialiasing);
        p.setBrush(QColor(255,0,0));
        p.setPen(Qt::NoPen);
        p.drawEllipse(count);
        QFont font;
        font.setFamily("Arial");
        font.setPointSize(12);
        font.setBold(true);
        p.setFont(font);
        p.setPen(Qt::white);
        p.drawText(count,Qt::AlignCenter,QString("%1").arg(number));
    }
    drag->setPixmap(img);
    drag->setHotSpot(QPoint(32,37));
    /*Qt::DropActions action = */
    drag->exec(supportedActions,Qt::CopyAction);
}

} // namespace Avocado

