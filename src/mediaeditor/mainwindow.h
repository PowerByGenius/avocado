#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QtAV>

namespace Ui {
class MainWindow;
}
namespace Avocado {
    class ExportDropWidget;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:


private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_btnA_clicked();
    void on_btnB_clicked();
    void on_btnC_clicked();
    void on_checkBoxLoop_stateChanged(int arg1);

    void on_rateUp_clicked();
    void on_rateDown_clicked();

    void on_rateNormal_clicked();

private:
    Ui::MainWindow *ui;
    Avocado::ExportDropWidget *m_exportDropWidget;
    QMediaPlayer *m_player;
    QMediaPlayer *m_player_video;

    QtAV::AVPlayer *m_avplayer;
    QtAV::VideoOutput *m_avout;

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
};

#endif // MAINWINDOW_H
