#ifndef AUDIOTRACKMARK_H
#define AUDIOTRACKMARK_H

#include <QGraphicsObject>
#include <QMimeType>
#include <QFont>

class QMenu;
class QAction;

namespace Avocado {
class AudioTrack;
class AudioTrackMark;
class AudioTrackMarkText;
class AudioTrackMarkHandle;


class AudioTrackMarkHandle : public QGraphicsItem
{
public:
    enum HandleTypes{HandleStart,HandleEnd};
    explicit AudioTrackMarkHandle(const HandleTypes types,AudioTrackMark *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
private:
    AudioTrackMark *m_trackMark;
    HandleTypes m_handleType;
    QPointF m_buttonDownPos;
    QFont m_font;
    int m_size;
signals:

public slots:
};


class AudioTrackMarkText : public QGraphicsTextItem
{
public:
    explicit AudioTrackMarkText(AudioTrackMark *parent = 0);
    QRectF boundingRect() const;
    void reLayout();
private:
    AudioTrackMark *m_trackMark;
signals:

public slots:
};


class AudioTrackMark : public QGraphicsObject
{
    Q_OBJECT
public:
    friend class AudioTrackMarkHandle;
    explicit AudioTrackMark(AudioTrack *audioTrack,QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;

    void setMarkPosStart(qreal value);
    void setMarkPosEnd(qreal value);

    qreal markPosStart() {return m_posStart;}
    qreal markPosEnd() {return m_posEnd;}
    QString mediaFilePath() const;
    int index();

    QTextDocument * textDocument() {return m_textItem->document();}

    static bool toAssending( AudioTrackMark * s1 , AudioTrackMark * s2 );

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
private:
    AudioTrack *m_audioTrack;
    AudioTrackMarkHandle *m_handleStart;
    AudioTrackMarkHandle *m_handleEnd;
    AudioTrackMarkText * m_textItem;
    qreal m_posStart;
    qreal m_posEnd;
    bool isMoving;
    QPointF m_buttonDownPos;
    QAction *m_removeAction;

//    QRectF m_rectIconLock;
signals:

public slots:
};

}
Q_DECLARE_METATYPE(Avocado::AudioTrackMark*)
#endif // AUDIOTRACKMARK_H
