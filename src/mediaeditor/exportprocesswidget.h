#ifndef AVOCADO_EXPORTPROCESSWIDGET_H
#define AVOCADO_EXPORTPROCESSWIDGET_H

#include <QWidget>

namespace Avocado {

namespace Ui {
class ExportProcessWidget;
}

class ExportProcessWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ExportProcessWidget(QWidget *parent = 0);
    ~ExportProcessWidget();

private:
    Ui::ExportProcessWidget *ui;

public slots:
    void addTask(const QVariantMap &map);
};


} // namespace Avocado
#endif // AVOCADO_EXPORTPROCESSWIDGET_H
