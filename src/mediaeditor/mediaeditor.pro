#-------------------------------------------------
#
# Project created by QtCreator 2015-10-10T07:02:33
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets opengl
CONFIG +=c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.11
INCLUDEPATH += $$PWD/../../thirdparty/build/osx/include
LIBS += -L$$PWD/../../thirdparty/build/osx/lib
include(QtAV/QtAv.pri)


TARGET = mediaeditor
TEMPLATE = app

SOURCES += main.cpp\
    mainwindow.cpp \
    audiotrackview.cpp \
    audiotrack.cpp \
    audiotrackwaveform.cpp \
    audiotrackcaption.cpp \
    audiotrackselection.cpp \
    audiotrackrange.cpp \
    audiotrackrangeab.cpp \
    audiotrackallview.cpp \
    audiotrackallviewtrack.cpp \
    audiotrackallviewwaveform.cpp \
    audiotrackallviewrangeab.cpp \
    audiotrackallviewrange.cpp \
    audiotrackallviewindicator.cpp \
    audiotrackmark.cpp \
    audiotrackmarklistview.cpp \
    audiotrackallviewmark.cpp \
    exportprocesslistview.cpp \
    exportprocessitemwidget.cpp \
    exportprocesswidget.cpp \
    exportdropwidget.cpp \
    exportdroppresetwidget.cpp

HEADERS  += mainwindow.h \
    audiotrackview.h \
    audiotrack.h \
    audiotrackwaveform.h \
    audiotrackcaption.h \
    audiotrackselection.h \
    audiotrackrange.h \
    audiotrackrangeab.h \
    audiotrackallview.h \
    audiotrackallviewtrack.h \
    audiotrackallviewwaveform.h \
    audiotrackallviewrangeab.h \
    audiotrackallviewrange.h \
    audiotrackallviewindicator.h \
    audiotrackmark.h \
    audiotrackmarklistview.h \
    audiotrackallviewmark.h \
    exportprocesslistview.h \
    exportprocessitemwidget.h \
    exportprocesswidget.h \
    exportdropwidget.h \
    exportdroppresetwidget.h

FORMS    += mainwindow.ui \
    exportprocessitemwidget.ui \
    exportprocesswidget.ui \
    exportdropwidget.ui \
    exportdroppresetwidget.ui

RESOURCES += \
    audiotrack.qrc
