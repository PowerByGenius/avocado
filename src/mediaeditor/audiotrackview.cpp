#include "audiotrackview.h"
#include "audiotrack.h"
#include "audiotrackmark.h"

#include <QTimer>
#include <QGraphicsItem>
#include <QPainter>
#include <QScrollBar>
#include <QWheelEvent>
#include <QApplication>
#include <QDebug>

namespace Avocado {

// AudioTrackViewIndicator
// =============================================================================
AudioTrackViewIndicator::AudioTrackViewIndicator(QWidget *parent):
    QWidget(parent)
{
    //    this->setMinimumWidth(20);
    //    this->setMaximumWidth(20);
    //    setAttribute(Qt::WA_NoSystemBackground,true);
    //    setAttribute(Qt::WA_TranslucentBackground,true);
    setAttribute(Qt::WA_TransparentForMouseEvents,true);
}

void AudioTrackViewIndicator::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter p(this);
    //    p.setCompositionMode(QPainter::CompositionMode_Overlay);
    p.fillRect(rect(),Qt::red);
}



// AudioTrackView
// =============================================================================

AudioTrackView::AudioTrackView(QWidget *parent) :
    QGraphicsView(parent),
    m_track(new AudioTrack(this,this)),
    m_indicator(new AudioTrackViewIndicator(this)),
    m_isHandDragging(false)
{
    //m_track = new AudioTrack(this,this);
    //    setDragMode(ScrollHandDrag);
    //    setInteractive(false);



    setScene(m_track);
    setResizeAnchor(QGraphicsView::NoAnchor);
    setAlignment(Qt::AlignTop|Qt::AlignLeft);
    ensureVisible(0,0,0,0);
    setBackgroundBrush(QColor(51,51,51));
    //    setOptimizationFlags(QGraphicsView::DontSavePainterState|QGraphicsView::DontAdjustForAntialiasing);
    setCacheMode(QGraphicsView::CacheBackground);
    setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);


    connect(horizontalScrollBar(),&QScrollBar::sliderMoved,
            [=](int position){
        QRect rect = viewport()->rect();
        int padding = rect.width()/2;
        m_currrentPosition = position+padding;
        emit handDragPosition(m_currrentPosition);
    });

}

void AudioTrackView::resizeEvent(QResizeEvent *event)
{
    m_track->relayout();
    m_indicator->setGeometry(viewport()->width()/2,0,1,this->viewport()->height());

    QRect rect = viewport()->rect();
    int padding = rect.width()/2;
    horizontalScrollBar()->setValue(m_currrentPosition - padding);
    QGraphicsView::resizeEvent(event);
}

// Mouse Event
// -----------------------------------------------------------------------------

void AudioTrackView::wheelEvent(QWheelEvent *event)
{
    QPoint degree = event->angleDelta() / 2;

    qint64 value = this->horizontalScrollBar()->value()+degree.ry();
    this->horizontalScrollBar()->setValue(value);

    QRect rect = viewport()->rect();
    int padding = rect.width()/2;
    m_currrentPosition = value+padding;
    emit handDragPosition(m_currrentPosition);

    event->accept();

    QGraphicsView::wheelEvent(event);
}

void AudioTrackView::mouseMoveEvent(QMouseEvent *event)
{
    if(dragMode() == QGraphicsView::ScrollHandDrag)
    {
        QRect rect = viewport()->rect();
        int padding = rect.width()/2;
        m_currrentPosition = horizontalScrollBar()->value()+padding;
        emit handDragPosition(m_currrentPosition);
    }

    QGraphicsView::mouseMoveEvent(event);
}

void AudioTrackView::mousePressEvent(QMouseEvent *event)
{

    QGraphicsItem *item = itemAt(event->pos());
    if(item->data(Qt::UserRole+100) == true)
    {
        QGraphicsView::mousePressEvent(event);
        return;
    }
//    if(qgraphicsitem_cast<AudioTrackMarkText*>(item))
//    {
//        QGraphicsView::mousePressEvent(event);
//        return;
//    }


    if(event->button() == Qt::LeftButton)
    {
        m_isHandDragging = true;
        setDragMode(ScrollHandDrag);
        emit handDragStared();
    }


    QGraphicsView::mousePressEvent(event);
}

void AudioTrackView::mouseReleaseEvent(QMouseEvent *event)
{
    m_isHandDragging = false;
    setDragMode(NoDrag);

    topLevelWidget()->setCursor(Qt::ArrowCursor);
    emit handDragDone();
    QGraphicsView::mouseReleaseEvent(event);
}

// Position
// -----------------------------------------------------------------------------

qint64 AudioTrackView::currrentPosition() const
{
    QRect rect = viewport()->rect();
    int padding = rect.width()/2;
    if(m_track->length()<=0)
        return 0;
    return horizontalScrollBar()->value()+padding;
}

void AudioTrackView::setPosition(qint64 position)
{
    if(dragMode() == QGraphicsView::ScrollHandDrag) return;

    QRect rect = viewport()->rect();
    int padding = rect.width()/2;
    m_currrentPosition = position/10;
    this->horizontalScrollBar()->setValue(position/10 - padding);
    emit positionChanged(position/10);
}

void AudioTrackView::modifyPosition(qint64 position)
{
    QRect rect = viewport()->rect();
    int padding = rect.width()/2;
    m_currrentPosition = position;
    this->horizontalScrollBar()->setValue(position - padding);
    emit handDragPosition(m_currrentPosition);
}

// Caption
// -----------------------------------------------------------------------------

void AudioTrackView::showCaption(bool value)
{
    m_track->showCaption(value);
}

void AudioTrackView::setCaptionTextBrowserInteraction(bool value)
{
    m_track->setCaptionTextBrowserInteraction(value);
}


// Hand Drag
// -----------------------------------------------------------------------------



// Range AB Position
// -----------------------------------------------------------------------------


qint64 AudioTrackView::rangeABPositionStart() const
{
    return m_track->rangeABPositionStart();
}

qint64 AudioTrackView::rangeABPositionEnd() const
{
    return m_track->rangeABPositionEnd();
}

void AudioTrackView::rangePositionHandDragChanged(qint64 start,qint64 end)
{
    emit rangeABValueHasChanged(start,end);
}

void AudioTrackView::setCurrentPositionForRangeA()
{
    m_track->setCurrentPositionForRangeA();
    emit rangeABValueHasChanged(rangeABPositionStart(),rangeABPositionEnd());
}

void AudioTrackView::setCurrentPositionForRangeB()
{
    m_track->setCurrentPositionForRangeB();
    emit rangeABValueHasChanged(rangeABPositionStart(),rangeABPositionEnd());
}

void AudioTrackView::setRangeAB(qint64 start, qint64 end)
{
    m_track->setRangeAB(start,end);
    emit rangeABValueHasChanged(start,end);
}

void AudioTrackView::cleanRangeAB()
{
    m_track->cleanRangeAB();
    emit RangeABHasCleaned();
}

bool AudioTrackView::isRangeABLoop()
{
    return m_track->isRangeABLoop();
}

void AudioTrackView::setRangeABLoop(bool value)
{
    m_track->setRangeABLoop(value);
    emit setRangeABLoopHasChanged(value);
}

// Mark
// -----------------------------------------------------------------------------


void AudioTrackView::addMarkOnRangeABPosition()
{
    m_track->addMarkOnRangeABPosition();
}

void AudioTrackView::markFromListViewActivated(AudioTrackMark *mark)
{
    setRangeAB(mark->markPosStart(),mark->markPosEnd());
    modifyPosition(mark->markPosStart());
}

// Files load
// -----------------------------------------------------------------------------

void AudioTrackView::loadWaveformFile(const QString &filepath, const QString &mediapath)
{
    m_mediapath = mediapath;

    QTimer::singleShot(0,[=](){
        m_track->loadWaveformFile(filepath);
        m_currrentPosition=0;
        ensureVisible(0,0,0,0);
        emit waveformFileHasChanged();
    });

}

void AudioTrackView::loadSrtFile(const QString &filepath)
{
    QTimer::singleShot(0,[=](){
        m_track->loadSrtFile(filepath);
        ensureVisible(0,0,0,0);
        emit srtFileHasChanged();
    });
}

void AudioTrackView::loadSrtText(const QString &text)
{
    QTimer::singleShot(0,[=](){
        m_track->loadSrtText(text);
        ensureVisible(0,0,0,0);
        emit srtFileHasChanged();
    });
}

} // namespace Avocado

