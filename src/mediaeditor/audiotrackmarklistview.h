#ifndef AVOCADO_AUDIOTRACKMARKLISTVIEW_H
#define AVOCADO_AUDIOTRACKMARKLISTVIEW_H

#include <QListView>
#include <QPointer>
#include <QStandardItemModel>
#include <QItemDelegate>
#include <QSortFilterProxyModel>
#include <QMimeData>

class QStandardItemModel;
class QMimeData;

namespace Avocado {
class AudioTrackView;
class AudioTrack;
class AudioTrackMark;

class AudioTrackMarkListItemDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit AudioTrackMarkListItemDelegate(QObject *parent = 0);
protected:
    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;
    QSize sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const;
public slots:
};

class AudioTrackMarkListModel : public QStandardItemModel
{
    Q_OBJECT
public:
    explicit AudioTrackMarkListModel(QObject *parent = 0);
protected:
    QMimeData *mimeData(const QModelIndexList & indexes) const;
public slots:
};



class AudioTrackMarkListView : public QListView
{
    Q_OBJECT
public:
    explicit AudioTrackMarkListView(QWidget *parent = 0);
    void setAudioTrackView(AudioTrackView *trackView);
private:
    QPointer<AudioTrackView> m_trackView;
    AudioTrackMarkListModel *m_model;
    QSortFilterProxyModel *m_sortModel;
signals:
    void dragIgnoreAction();
private slots:
    void markInsert(Avocado::AudioTrackMark *mark);
    void markRemoved(Avocado::AudioTrackMark *mark);
//    void markMoved(Avocado::AudioTrackMark *mark,int oldIndex,int newIndex);
    void markDataChanged(Avocado::AudioTrackMark *mark);
    void currentChanged(const QModelIndex & current, const QModelIndex & previous);
    void currentActivated(const QModelIndex & index);
public slots:

    // QAbstractItemView interface
protected:
    void startDrag(Qt::DropActions supportedActions);
};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKMARKLISTVIEW_H
