#ifndef AUDIOTRACKRANGEAB_H
#define AUDIOTRACKRANGEAB_H

#include <QGraphicsObject>

namespace Avocado {


class AudioTrack;
class AudioTrackRangeAB;

class AudioTrackRangeABHandle : public QGraphicsItem
{
public:
    enum HandleTypes{HandleStart,HandleEnd};
    explicit AudioTrackRangeABHandle(const HandleTypes types,AudioTrackRangeAB *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
private:
    AudioTrackRangeAB *m_trackRange;
    HandleTypes m_handleType;
    QPointF m_buttonDownPos;
    int m_size;
signals:

public slots:
};


class AudioTrackRangeAB : public QGraphicsObject
{
    Q_OBJECT
public:
    friend class AudioTrackRangeABHandle;
    explicit AudioTrackRangeAB(AudioTrack *audioTrack,QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;

    void setRangePosStart(qreal value);
    void setRangePosEnd(qreal value);

    qreal rangePosStart() {return m_posStart;}
    qreal rangePosEnd() {return m_posEnd;}

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);

private:
    AudioTrack *m_audioTrack;
    AudioTrackRangeABHandle *m_handleStart;
    AudioTrackRangeABHandle *m_handleEnd;
    qreal m_posStart;
    qreal m_posEnd;
    bool isMoving;
    QPointF m_buttonDownPos;
signals:

public slots:
};

}

#endif // AUDIOTRACKRANGEAB_H
