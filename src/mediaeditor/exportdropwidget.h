#ifndef AVOCADO_EXPORTDROPWIDGET_H
#define AVOCADO_EXPORTDROPWIDGET_H

#include <QWidget>

namespace Avocado {

class FlowLayout;

namespace Ui {
class ExportDropWidget;
}

class ExportDropWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ExportDropWidget(QWidget *parent = 0);
    ~ExportDropWidget();

private:
    Ui::ExportDropWidget *ui;
    FlowLayout *m_layout;
private slots:
    void presetDropAccepted(const QVariantMap &map);
signals:
    void dropAccepted(const QVariantMap &map);
};


} // namespace Avocado
#endif // AVOCADO_EXPORTDROPWIDGET_H
