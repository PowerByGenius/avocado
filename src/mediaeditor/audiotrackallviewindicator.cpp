#include "audiotrackallviewindicator.h"
#include "audiotrackallview.h"
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QDebug>

namespace Avocado {

AudioTrackAllViewIndicator::AudioTrackAllViewIndicator(AudioTrackAllView *view, QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_view(view),
    m_position(0),
    m_polygon(QPolygonF(5)),
    m_isDraggable(false)
{
    setFlag(QGraphicsItem::ItemIsMovable,true);
    setZValue(1000);
}

QRectF AudioTrackAllViewIndicator::boundingRect() const
{
    QRectF rect;
    if(parentItem()) rect = parentItem()->boundingRect();
    return QRectF(rect.left(),0,20,m_view->viewportHeight());
}

void AudioTrackAllViewIndicator::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    //    painter->fillRect(boundingRect(),QColor(255,255,0,128));

    painter->setRenderHint(QPainter::Antialiasing);

    QRectF rect = boundingRect();
    qreal top = 20;

    //    QPointF points[5] = {
    //        QPointF(rect.left()+1, top),
    //        QPointF(rect.left(), top+14),
    //        QPointF(rect.left()+rect.width()/2, (top+14)*1.25),
    //        QPointF(rect.right(),top+14),
    //        QPointF(rect.right()-1, top)
    //    };

    m_polygon[0]= QPointF(rect.left()+1, top);
    m_polygon[1]=QPointF(rect.left(), top+14);
    m_polygon[2]=QPointF(rect.left()+rect.width()/2, (top+14)*1.25);
    m_polygon[3]=QPointF(rect.right(),top+14);
    m_polygon[4]=QPointF(rect.right()-1, top);

    painter->setPen(QColor(255, 102, 0));
    painter->drawLine((rect.left()+rect.width()/2),0,(rect.left()+rect.width()/2),top);
    painter->drawLine((rect.left()+rect.width()/2),(top+14)*1.25-1,(rect.left()+rect.width()/2),rect.height());

    //    painter->setBrush(QColor(255, 102, 0));
    //    painter->setPen(Qt::NoPen);

    QPen pen;
    pen.setWidthF(2);
    pen.setColor(QColor(255, 102, 0));
    painter->setPen(pen);
    painter->setBrush(QColor(255, 102, 0,128));
    painter->drawPolygon(m_polygon);

}

void AudioTrackAllViewIndicator::setPosition(const qint64 &position)
{
    QRectF rect;
    if(parentItem()) rect = parentItem()->boundingRect();
    qreal widthWaveform = rect.width();
    qreal x = 0;
    if(m_view->length()>0){
    qreal q =  widthWaveform/m_view->length();
        x = (position * q);
        if(x<0 || x !=x || x > std::numeric_limits<qreal>::max() || x < -std::numeric_limits<qreal>::max()) x = 0;
    }
    this->setX(x-this->boundingRect().width()/2);
    m_position = position;
}

void AudioTrackAllViewIndicator::reLayout()
{
    QRectF rect;
    if(parentItem()) rect = parentItem()->boundingRect();
    qreal widthWaveform = rect.width();
    qreal x = 0;
    if(m_view->length() > 0 )
    {
        qreal q =  widthWaveform/m_view->length();
        x = (m_position * q)-this->boundingRect().width()/2;
    }

    this->setX(x);
}

void AudioTrackAllViewIndicator::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    event->accept();
    if(m_isDraggable)
    {
        QRectF rect;
        if(parentItem()) rect = parentItem()->boundingRect();
        qreal x = event->scenePos().rx()-m_buttonDownPos.rx();
        if(x >= 0 - this->boundingRect().width()/2  && x <= rect.width()-this->boundingRect().width()/2)
        {
            qreal widthWaveform = rect.width();
            qreal q =  0;
            if(m_view->length()>0)
            {
                q =  widthWaveform/m_view->length();
                m_position =qRound((x+this->boundingRect().width()/2)/q);
            }else{
                m_position =qRound(x);
            }

            this->setX(x);
            emit positionChanged(m_position);
        }
    }
}

void AudioTrackAllViewIndicator::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(m_polygon.boundingRect().contains(event->pos()) && event->button() == Qt::LeftButton)
    {
        m_buttonDownPos = event->buttonDownPos(Qt::LeftButton);
        m_isDraggable = true;
        event->accept();
    }
    QGraphicsObject::mousePressEvent(event);
}

void AudioTrackAllViewIndicator::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    m_isDraggable = false;
    event->accept();
    QGraphicsObject::mouseReleaseEvent(event);
}

} // namespace Avocado

