#include "audiotrackwaveform.h"
#include "audiotrackview.h"
#include <QPainter>
#include <QDebug>

namespace Avocado {

AudioTrackWaveform::AudioTrackWaveform(AudioTrack *audioTrack,QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_audioTrack(audioTrack)
{

}

QRectF AudioTrackWaveform::boundingRect() const
{
    return QRectF(0,0,m_audioTrack->audioWaveformData().count(),m_audioTrack->view()->viewport()->height());
}

void AudioTrackWaveform::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    Q_UNUSED(option)
    Q_UNUSED(widget)

    QRectF viewPortRect = m_audioTrack->view()->mapToScene(m_audioTrack->view()->viewport()->rect()).boundingRect();

    int start = 0;
    int end = 0;
    if(viewPortRect.left() < 0)
    {
        start = 0;
        end = qAbs(viewPortRect.width()+viewPortRect.left());

    }else{

        start = viewPortRect.left();
        end  = start + viewPortRect.width();
    }

    int length = m_audioTrack->audioWaveformData().count();

    if(end >= length)
    {
        end = length;
    }


    QFont rulerFont;
    rulerFont.setPointSize(10);

    QRectF rectRuler = viewPortRect;
    rectRuler.setHeight(20);
    painter->fillRect(rectRuler,QColor(255,255,255,8));

    int scale = 100;
    for (int i = start - viewPortRect.width()/2; i < end + viewPortRect.width()/2; ++i) {
        int ms = i;
        if(ms % (scale/10) == 0)
        {
            QPen rulerPen;
            rulerPen.setColor(QColor(255,255,255,64));
            rulerPen.setWidthF(1);
            painter->setPen(rulerPen);

            if(ms % scale == 0)
            {
                int t = (ms) / scale;

                if(i >= start - scale && i <= end && t >=0)
                {
                QString timeText = t > 3600 ? QString("%1:%2:%3")
                                              .arg(t/3600,2,10,QChar('0'))
                                              .arg(t/60%60,2,10,QChar('0'))
                                              .arg(t%60,2,10,QChar('0'))
                                            :QString("%1:%2")
                                              .arg(t/60%60,2,10,QChar('0'))
                                              .arg(t%60,2,10,QChar('0'));
                painter->setFont(rulerFont);
                painter->drawText(ms+3,12,timeText);
                }

                for(int x=5;x<18;x++)
                {
                    painter->drawPoint(ms,x);
                }

            }

            painter->drawPoint(ms,18);
            painter->drawPoint(ms,19);
            painter->drawPoint(ms,20);
        }
    }

    qreal height = boundingRect().height();

//    painter->setRenderHint(QPainter::Antialiasing,true);

    qreal q = height/255;

    for (int i = start; i < end; i++) {

        int ms = i;
        int data = m_audioTrack->audioWaveformData().at(ms);
        int y_min = (data >> 8) & 0xFF;
        int y_max = data & 0xFF;



        painter->setPen(QColor(255,204,0,172));

        if(m_audioTrack->isRangeABLoop())
        {
            if(i>=m_audioTrack->rangeABPositionStart() && i<= m_audioTrack->rangeABPositionEnd())
            {
                painter->setPen(QColor(255, 102, 0,172));
            }
        }


        for (int p = y_min*q; p < y_max*q+1; ++p) {
            painter->drawPoint(ms,p);
        }

//        painter->drawLine(ms,y_min*q,ms,y_max*q);
    }

}
} // namespace Avocado

