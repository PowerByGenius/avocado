#include "audiotrackallview.h"
#include "audiotrackallviewtrack.h"
#include "audiotrackallviewwaveform.h"
#include "audiotrackallviewindicator.h"
#include "audiotrackallviewrangeab.h"
#include "audiotrackallviewmark.h"
#include "audiotrackmark.h"
#include "audiotrack.h"
#include <QDebug>

namespace Avocado {

// AudioTrackMarginItem
// =============================================================================

AudioTrackAllViewMarginRangeText::AudioTrackAllViewMarginRangeText(AudioTrackAllView *view,Ranges range,QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_view(view),
    m_range(range)
{
    m_font.setPointSize(12);
}

QRectF AudioTrackAllViewMarginRangeText::boundingRect() const
{
    QRectF rect = parentItem()->boundingRect();
    return QRectF(rect.left(),rect.height()/2,rect.width(),rect.height()/2);
}

void AudioTrackAllViewMarginRangeText::setPosition(qint64 position)
{
    if(position/100>3600)
    {
    m_text = QString("%1:%2:%3.%4").arg(position/100/3600,2,10,QChar('0'))
            .arg(position/100/60%60,2,10,QChar('0'))
            .arg(position/100%60,2,10,QChar('0'))
            .arg(position*10%1000,3,10,QChar('0'));
    }else{
        m_text = QString("%1:%2.%3")
                .arg(position/100/60%60,2,10,QChar('0'))
                .arg(position/100%60,2,10,QChar('0'))
                .arg(position*10%1000,3,10,QChar('0'));
    }

    update();
}

void AudioTrackAllViewMarginRangeText::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->setFont(m_font);
    painter->setPen(QColor(255,255,255,128));
    painter->setRenderHint(QPainter::TextAntialiasing,true);
    QRectF rect = boundingRect();
;

    QString text;
    switch(m_range)
    {
    case AudioTrackAllViewMarginRangeText::Start:
        rect.adjust(12,12,0,0);
        text = QString("A | %1").arg(m_text);
        painter->drawText(rect,Qt::AlignLeft,text);
        break;
    case AudioTrackAllViewMarginRangeText::End:
        rect.adjust(0,12,-12,0);
        text = QString("%1 | B").arg(m_text);
        painter->drawText(rect,Qt::AlignRight,text);
        break;
    }

}


// AudioTrackMarginItem
// =============================================================================

AudioTrackAllViewMarginLeft::AudioTrackAllViewMarginLeft(AudioTrackAllView *view,QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_view(view),
    m_rangeText(new AudioTrackAllViewMarginRangeText(m_view,AudioTrackAllViewMarginRangeText::Start,this))
{
    m_font.setPointSize(14);
}

QRectF AudioTrackAllViewMarginLeft::boundingRect() const
{
    return QRectF(0,0,m_view->marginLeft(),m_view->viewportHeight());
}

void AudioTrackAllViewMarginLeft::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    QRectF rect = boundingRect();
    rect.setHeight(rect.height()/2);
    rect.adjust(0,20,0,0);
    painter->setFont(m_font);
    painter->setPen(QColor(255,255,255));
    painter->drawText(rect,Qt::AlignCenter,m_text);
    //painter->fillRect(rect,QColor(0,255,0,128));
}

void AudioTrackAllViewMarginLeft::setRangePosition(qint64 position)
{
    m_rangeText->setPosition(position);
    m_rangeText->update();
}

void AudioTrackAllViewMarginLeft::setPosition(qint64 position)
{
    if(position/100>3600)
    {
    m_text = QString("%1:%2:%3.%4").arg(position/100/3600,2,10,QChar('0'))
            .arg(position/100/60%60,2,10,QChar('0'))
            .arg(position/100%60,2,10,QChar('0'))
            .arg(position*10%1000,3,10,QChar('0'));
    }else{
        m_text = QString("%1:%2.%3")
                .arg(position/100/60%60,2,10,QChar('0'))
                .arg(position/100%60,2,10,QChar('0'))
                .arg(position*10%1000,3,10,QChar('0'));
    }
    QRectF rect = boundingRect();
    rect.setHeight(rect.height()/2);
    rect.adjust(0,20,0,0);
    update(rect);
}


// AudioTrackAllViewMarginRight
// =============================================================================

AudioTrackAllViewMarginRight::AudioTrackAllViewMarginRight(AudioTrackAllView *view,QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_view(view),
    m_rangeText(new AudioTrackAllViewMarginRangeText(m_view,AudioTrackAllViewMarginRangeText::End,this))
{
    m_font.setPointSize(14);
}

QRectF AudioTrackAllViewMarginRight::boundingRect() const
{
    return QRectF(m_view->viewportWidth()-m_view->marginRight(),0,m_view->marginRight(),m_view->viewportHeight());
}

void AudioTrackAllViewMarginRight::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    QRectF rect = boundingRect();
    rect.setHeight(rect.height()/2);
    rect.adjust(0,20,0,0);
    painter->setFont(m_font);
    painter->setPen(QColor(255,255,255));
    painter->drawText(rect,Qt::AlignCenter,m_text);
    //painter->fillRect(rect,QColor(0,255,0,128));
}

void AudioTrackAllViewMarginRight::setRangePosition(qint64 position)
{
    m_rangeText->setPosition(position);
    m_rangeText->update();
}

void AudioTrackAllViewMarginRight::setPosition(qint64 position)
{
    qint64 p = m_view->length() - position;
    if(p<0) p=0;
    if(p/100>3600)
    {
    m_text = QString("-%1:%2:%3.%4").arg(p/100/3600,2,10,QChar('0'))
            .arg(p/100/60%60,2,10,QChar('0'))
            .arg(p/100%60,2,10,QChar('0'))
            .arg(p*10%1000,3,10,QChar('0'));
    }else{
        m_text = QString("-%1:%2.%3")
                .arg(p/100/60%60,2,10,QChar('0'))
                .arg(p/100%60,2,10,QChar('0'))
                .arg(p*10%1000,3,10,QChar('0'));
    }

    QRectF rect = boundingRect();
    rect.setHeight(rect.height()/2);
    rect.adjust(0,20,0,0);
    update(rect);
}








// AudioTrackAllViewTrack
// =============================================================================


AudioTrackAllViewTrack::AudioTrackAllViewTrack(AudioTrackAllView *view, QObject *parent) :
    QGraphicsScene(parent),
    m_view(view),
    m_waveform(new AudioTrackAllViewWaveform(m_view)),
    m_marginLeft(new AudioTrackAllViewMarginLeft(m_view)),
    m_marginRight(new AudioTrackAllViewMarginRight(m_view)),
    m_indicator(new AudioTrackAllViewIndicator(m_view,m_waveform)),
    m_rangeAB(new AudioTrackAllViewRangeAB(m_view,m_waveform))
{
    addItem(m_marginLeft);
    addItem(m_waveform);
    addItem(m_marginRight);
//    setPosition(0);
//    modifyRageABPosition(0,0);
}

void AudioTrackAllViewTrack::reLayout()
{

    m_indicator->reLayout();
    m_rangeAB->reLayout();

    foreach (AudioTrackAllViewMark *mark, m_markItems) {
        mark->reLayout();
    }

    QRectF itemBoundingRect = this->itemsBoundingRect();
    this->setSceneRect(itemBoundingRect);
}

AudioTrackAllView *AudioTrackAllViewTrack::view()
{
    return m_view;
}

qreal AudioTrackAllViewTrack::waveformLength() const
{
    return m_waveform->boundingRect().width();
}

void AudioTrackAllViewTrack::waveformFileHasChanged()
{
    m_waveform->updateWaveform();
    m_waveform->update();
}

void AudioTrackAllViewTrack::setPosition(const qint64 &position)
{
    m_marginLeft->setPosition(position);
    m_marginRight->setPosition(position);
    m_indicator->setPosition(position);
}


void AudioTrackAllViewTrack::modifyRageABPosition(const qint64 &start, const qint64 &end)
{
    m_marginLeft->setRangePosition(start);
    m_marginRight->setRangePosition(end);
    m_rangeAB->setRangePosition(start,end);

}

void AudioTrackAllViewTrack::setRangeABLoop(bool value)
{
    m_rangeAB->setRangeABLoop(value);
    m_rangeAB->update();
}

void AudioTrackAllViewTrack::audioTrackMarkInsert(AudioTrackMark *audioTrackMark)
{
    AudioTrackAllViewMark *mark = new AudioTrackAllViewMark(m_view,audioTrackMark,m_waveform);
    m_markItems.append(mark);
    mark->setPosition(audioTrackMark->markPosStart(),audioTrackMark->markPosEnd());
    mark->reLayout();
}

void AudioTrackAllViewTrack::audioTrackMarkRemoved(AudioTrackMark *audioTrackMark)
{
    for (int i = 0; i < m_markItems.count(); ++i) {
        AudioTrackAllViewMark *mark = m_markItems.at(i);
        if(mark->audioTrackMark() == audioTrackMark)
        {
            m_markItems.takeAt(i);
            removeItem(mark);
        }
    }
}

void AudioTrackAllViewTrack::audioTrackDataChanged(AudioTrackMark *audioTrackMark)
{
    for (int i = 0; i < m_markItems.count(); ++i) {
        AudioTrackAllViewMark *mark = m_markItems.at(i);
        if(mark->audioTrackMark() == audioTrackMark)
        {
            mark->setPosition(audioTrackMark->markPosStart(),
                              audioTrackMark->markPosEnd());
        }
    }
}

} // namespace Avocado

