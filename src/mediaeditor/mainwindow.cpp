#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "exportdropwidget.h"
#include "audiotrack.h"
#include <QVideoWidget>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDebug>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_exportDropWidget(new Avocado::ExportDropWidget(this)),
    m_player(new QMediaPlayer(this)),
    m_player_video(new QMediaPlayer(this))
{
    ui->setupUi(this);
    setAcceptDrops(true);
    m_exportDropWidget->hide();
    m_exportDropWidget->raise();
    resize(800,500);



    ui->trackAllview->setAudioTrackView(ui->graphicsView);
    ui->listView->setAudioTrackView(ui->graphicsView);


    m_avplayer = new QtAV::AVPlayer(this);
    m_avout = new QtAV::VideoOutput();
    m_avplayer->setRenderer(m_avout);
    ui->centralWidget->layout()->replaceWidget(ui->widgetView,m_avout->widget());

    //m_avplayer->setInterruptTimeout(10);



    m_avplayer->setNotifyInterval(10);

    connect(ui->graphicsView,&Avocado::AudioTrackView::handDragPosition,[=](qint64 position){
        m_avplayer->pause(true);
        m_avplayer->setPosition(position*10);
        m_avplayer->pause(false);
    });

    connect(m_avplayer,&QtAV::AVPlayer::positionChanged,
            ui->graphicsView,&Avocado::AudioTrackView::setPosition,Qt::QueuedConnection);

    m_avplayer->setSeekType(QtAV::AnyFrameSeek);

/*
    QVideoWidget *videoWidget = new QVideoWidget(this);
    ui->centralWidget->layout()->replaceWidget(ui->widgetView,videoWidget);

    m_player->setNotifyInterval(10);
    m_player_video->setVolume(0);
    m_player_video->setMuted(true);
    m_player_video->setVideoOutput(videoWidget);

    connect(m_player,&QMediaPlayer::playbackRateChanged,
            m_player_video,&QMediaPlayer::setPlaybackRate);

    connect(m_player,&QMediaPlayer::stateChanged,
            [=](QMediaPlayer::State state){
        switch(state){
        case QMediaPlayer::StoppedState:
            m_player_video->stop();
            break;
        case QMediaPlayer::PlayingState:
            m_player_video->play();
            break;
        case QMediaPlayer::PausedState:
            m_player_video->pause();
            break;
        }
    });

    connect(m_player,&QMediaPlayer::positionChanged,
            ui->graphicsView,&Avocado::AudioTrackView::setPosition,Qt::QueuedConnection);

    connect(m_player,&QMediaPlayer::positionChanged,[=](qint64 position){

        if(ui->graphicsView->isHandDragging())
        {
            if(position > ui->graphicsView->currrentPosition()*10)
            {
                m_player->pause();
                m_player_video->pause();
            }
        }

        if(ui->graphicsView->isRangeABLoop())
        {
            if(position > ui->graphicsView->rangeABPositionEnd()*10)
            {
                m_player->setPosition(ui->graphicsView->rangeABPositionStart()*10);
                m_player_video->setPosition(ui->graphicsView->rangeABPositionStart()*10);
            }
        }
    });

    connect(ui->graphicsView,&Avocado::AudioTrackView::handDragPosition,[=](qint64 position){

        m_player_video->setVolume(0);
        m_player_video->setMuted(true);

        m_player->pause();
        m_player->setPosition(position*10);
        m_player->play();


        m_player_video->pause();
        m_player_video->setPosition(position*10);
        m_player_video->play();
    });

    connect(m_player,&QMediaPlayer::positionChanged,
            ui->graphicsView,&Avocado::AudioTrackView::setPosition,Qt::QueuedConnection);

*/





    connect(m_exportDropWidget,&Avocado::ExportDropWidget::dropAccepted,
            ui->widgetExportProcess,&Avocado::ExportProcessWidget::addTask);

    connect(ui->listView,&Avocado::AudioTrackMarkListView::dragIgnoreAction,
            m_exportDropWidget,&Avocado::ExportDropWidget::hide);

}

MainWindow::~MainWindow()
{
    delete m_avout;
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    switch(m_player->state()){
    case QMediaPlayer::StoppedState:
        m_player->play();
        m_player_video->play();
        break;
    case QMediaPlayer::PlayingState:
        m_player->pause();
        m_player_video->pause();
        break;
    case QMediaPlayer::PausedState:
        m_player->play();
        m_player_video->play();
        break;
    }
     m_avplayer->play();
}

void MainWindow::on_pushButton_2_clicked()
{
    m_avplayer->pause();
}

void MainWindow::on_pushButton_3_clicked()
{
//    m_player->play();
//    m_player->pause();
//    m_player->setPosition(ui->graphicsView->selectedPositionStart()*10);
//    m_player->play();

//    ui->graphicsView->loadWaveformFile("/Users/kiko/Desktop/abc/test2.data");
//    ui->graphicsView->loadSrtFile("/Users/kiko/Desktop/abc/abc.srt");

//    m_player_video->setMedia(QUrl::fromLocalFile("/Users/kiko/Desktop/abc/Friends.with.Benefits.2011.BluRay.720p.x264.AC3-WOFEI.mp4"));
//    m_player->setMedia(QUrl::fromLocalFile("/Users/kiko/Desktop/abc/Friends.with.Benefits.2011.BluRay.720p.x264.AC3-WOFEI.m4a"));


    m_avplayer->setFile("/Users/kiko/Desktop/abc/Friends.with.Benefits.2011.BluRay.720p.x264.AC3-WOFEI.mp4");
    ui->graphicsView->loadWaveformFile("/Users/kiko/Desktop/abc/Friends.with.Benefits.2011.BluRay.720p.x264.AC3-WOFEI.data","/Users/kiko/Desktop/abc/Friends.with.Benefits.2011.BluRay.720p.x264.AC3-WOFEI.mp4");
    ui->graphicsView->loadSrtFile("/Users/kiko/Desktop/abc/Friends.with.Benefits.2011.BluRay.720p.x264.AC3-WOFEI.srt");
    m_avplayer->play();
}

void MainWindow::on_btnA_clicked()
{
    ui->graphicsView->setCurrentPositionForRangeA();
}

void MainWindow::on_btnB_clicked()
{
    ui->graphicsView->setCurrentPositionForRangeB();
}

void MainWindow::on_btnC_clicked()
{
    ui->graphicsView->addMarkOnRangeABPosition();
}

void MainWindow::on_checkBoxLoop_stateChanged(int arg1)
{
    if(arg1 > 0){
        ui->graphicsView->setRangeABLoop(true);
    }else{
        ui->graphicsView->setRangeABLoop(false);
    }
}

void MainWindow::on_rateUp_clicked()
{
    m_player->setPlaybackRate(m_player->playbackRate()+0.05);
    ui->rateNormal->setText(QString("%1").arg(m_player->playbackRate()));
}

void MainWindow::on_rateDown_clicked()
{
    m_player->setPlaybackRate(m_player->playbackRate()-0.05);
    ui->rateNormal->setText(QString("%1").arg(m_player->playbackRate()));
}

void MainWindow::on_rateNormal_clicked()
{
    m_player->setPlaybackRate(1);
    ui->rateNormal->setText(QString("%1").arg(m_player->playbackRate()));
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    QRect rect = this->rect();
    m_exportDropWidget->move(rect.width()-m_exportDropWidget->width()-width()*0.05,rect.height()/2-m_exportDropWidget->height()/2);
    QMainWindow::resizeEvent(event);
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasFormat("application/x-track-mark"))
    {
        m_exportDropWidget->show();
    }

    QMainWindow::dragEnterEvent(event);
}
