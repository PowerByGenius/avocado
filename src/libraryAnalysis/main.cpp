#include "mediaanalysiswidget.h"
#include <QApplication>
#include <QSqlDatabase>
#include <QSettings>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QApplication::setApplicationName("Aguacate");
    QApplication::setApplicationDisplayName("Aguacate");
    QApplication::setApplicationVersion("1.0");

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/Users/kiko/Desktop/Library/Avocado/Qt/Avocado/database/avocado.sqlite");
    bool ok = db.open();
    QSettings().setValue("library_path","/Users/kiko/Desktop/media");


    MediaAnalysisWidget w;
    w.show();

    return a.exec();
}
