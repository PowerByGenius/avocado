#-------------------------------------------------
#
# Project created by QtCreator 2015-10-24T22:40:00
#
#-------------------------------------------------

QT       += core gui sql concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = libraryAnalysis
TEMPLATE = app


SOURCES += main.cpp\
        mediaanalysiswidget.cpp

HEADERS  += mediaanalysiswidget.h

FORMS    += mediaanalysiswidget.ui
