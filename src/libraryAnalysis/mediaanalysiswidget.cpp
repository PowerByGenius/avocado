#include "mediaanalysiswidget.h"
#include "ui_mediaanalysiswidget.h"
#include <QThread>
#include <QtConcurrent>
#include <QFutureWatcher>
#include <QDebug>



const int iterations = 20;

void spin(int &iteration)
{
    const int work = 1000 * 1000 * 40;
    volatile int v = 0;
    for (int j = 0; j < work; ++j)
        ++v;

    qDebug() << "iteration" << iteration << "in thread" << QThread::currentThreadId();
}

MediaAnalysisWidget::MediaAnalysisWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MediaAnalysisWidget)
{
    ui->setupUi(this);

    QVector<int> vector;
    for (int i = 0; i < iterations; ++i)
        vector.append(i);

    QFutureWatcher<void> futureWatcher;
    futureWatcher.setFuture(QtConcurrent::map(vector, spin));
}

MediaAnalysisWidget::~MediaAnalysisWidget()
{
    delete ui;
}
