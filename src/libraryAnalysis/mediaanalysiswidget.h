#ifndef MEDIAANALYSISWIDGET_H
#define MEDIAANALYSISWIDGET_H

#include <QWidget>

namespace Ui {
class MediaAnalysisWidget;
}

class MediaAnalysisWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MediaAnalysisWidget(QWidget *parent = 0);
    ~MediaAnalysisWidget();

private:
    Ui::MediaAnalysisWidget *ui;
};

#endif // MEDIAANALYSISWIDGET_H
