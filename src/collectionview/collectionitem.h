#ifndef AVOCADO_COLLECTIONITEM_H
#define AVOCADO_COLLECTIONITEM_H

#include <QGraphicsItem>
#include <QPropertyAnimation>
namespace Avocado {

class CollectionItem : public  QGraphicsObject
{
public:
    explicit CollectionItem( QGraphicsItem *parent = 0);
    ~CollectionItem();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void setItemPos(const QPointF &pos);

private:
//    QPropertyAnimation *m_posAnimation;
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_COLLECTIONITEM_H
