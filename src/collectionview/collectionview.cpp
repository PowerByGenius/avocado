#include "collectionview.h"
#include "collectionscene.h"
#include "collectionitem.h"


namespace Avocado {

CollectionView::CollectionView(QWidget *parent) :
    QGraphicsView(parent),
    m_scene(new CollectionScene(this))
{
    setScene(m_scene);
//    this->setRubberBandSelectionMode(Qt::ContainsItemBoundingRect);

    for (int i = 0; i < 100000; ++i) {
        CollectionItem *x = new CollectionItem();
//        x->setPos(0,i*180);
        m_scene->addItem(x);
    }
}

void CollectionView::resizeEvent(QResizeEvent *event)
{
    m_scene->reLayout();
    QGraphicsView::resizeEvent(event);
}

} // namespace Avocado

