#include "testwidget.h"
#include "ui_testwidget.h"
#include "collectionitem.h"

using namespace Avocado;

TestWidget::TestWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestWidget)
{
    ui->setupUi(this);
//    m_scene->setItemIndexMethod(QGraphicsScene::NoIndex);
//    ui->graphicsView->setScene(m_scene);

//    for (int i = 0; i < 100000; ++i) {
//        CollectionItem *x = new CollectionItem();
//        x->setPos(0,i*180);
//        m_scene->addItem(x);
//    }
}

TestWidget::~TestWidget()
{
    delete ui;
}
