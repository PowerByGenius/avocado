#-------------------------------------------------
#
# Project created by QtCreator 2015-10-01T16:31:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = collectionview
TEMPLATE = app


SOURCES += main.cpp\
        testwidget.cpp \
    collectionscene.cpp \
    collectionitem.cpp \
    collectionview.cpp

HEADERS  += testwidget.h \
    collectionscene.h \
    collectionitem.h \
    collectionview.h

FORMS    += testwidget.ui
