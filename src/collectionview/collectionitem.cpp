#include "collectionitem.h"
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QDebug>

namespace Avocado {

CollectionItem::CollectionItem(QGraphicsItem *parent) :
    QGraphicsObject(parent)
//    m_posAnimation(new QPropertyAnimation(this,"pos",this))
{
    this->setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsFocusable);
//    m_posAnimation->setDuration(500);
//    m_posAnimation->setEasingCurve(QEasingCurve::InQuad);
}

CollectionItem::~CollectionItem()
{
//    m_posAnimation->deleteLater();
}

QRectF CollectionItem::boundingRect() const
{
    //return QRect(QPoint(0,0),QSize(view->scene()->cellWidth(),view->scene()->cellHeight()));
    return QRectF(0,0,180,180);
}

void CollectionItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if(this->isVisible())
    {
        // TODO: 所有的画面全部在这画,效率非常高
        // 如果需要动画,然后添加property属性

        //painter->fillRect(this->boundingRect(),Qt::green);
        QRectF imageRectF = this->boundingRect();//.adjusted(10,10,-10,-10);

        imageRectF.setHeight(imageRectF.width());

        painter->fillRect(imageRectF,Qt::white);
        painter->drawText(20,20,QString("%1").arg(this->data(0).toInt()));

//        painter->drawPixmap(0,0,this->boundingRect().width(),32,QPixmap("/Users/kiko/Desktop/a.png"));

        if(this->isSelected())
        {
            painter->save();
            QPen pen;
            pen.setColor(QColor(123,190,247));
            pen.setWidth(2);
            painter->setPen(pen);
            painter->drawRect(this->boundingRect());
            painter->restore();
        }else{
            painter->save();
            QPen pen;
            pen.setColor(QColor(231,231,231));
            pen.setWidth(1);
            painter->setPen(pen);
            painter->drawRect(this->boundingRect());
            painter->restore();
        }
    }
}

void CollectionItem::setItemPos(const QPointF &pos)
{
//    if(this->isVisible())
//    {
//        m_posAnimation->setEndValue(pos);
//        m_posAnimation->start();
//    }else{
//        setPos(pos);
    //    }
}


} // namespace Avocado

