#ifndef AVOCADO_COLLECTIONVIEW_H
#define AVOCADO_COLLECTIONVIEW_H

#include <QGraphicsView>

namespace Avocado {

class CollectionScene;

class CollectionView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit CollectionView(QWidget *parent = 0);
protected:
    void resizeEvent(QResizeEvent * event);
protected:
    CollectionScene *m_scene;
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_COLLECTIONVIEW_H
