#include "collectionscene.h"
#include "collectionview.h"
#include "collectionitem.h"

#include <QGraphicsItem>
#include <QDebug>

namespace Avocado {

CollectionScene::CollectionScene(CollectionView *parent) :
    QGraphicsScene(parent),
    m_marginLeft(20),
    m_marginRight(20),
    m_marginTop(20),
    m_marginBottom(20),
    m_spacing(10),
    m_cellNormalSize(180),
    m_columMinimumCell(3),
    m_view(parent)

{
    setItemIndexMethod(QGraphicsScene::NoIndex);
    setBackgroundBrush(QBrush(QColor(245,245,245)));
}

void CollectionScene::reLayout()
{
//    d->marginTopLeftItem->setPos(0,0);
//    d->marginTopRightItem->setPos(d->view->viewport()->width() - d->marginRight,0);

//    if(d->extendingItem->isVisible())
//    {
//        int cellHeight = this->cellHeight();
//        int row = this->itemRow(d->extendingItem->item()) + 1;
//        int y = row * (cellHeight + d->spacing) + d->marginTop;
//        d->extendingItem->setPos(0,y);
//    }

    for (int i = 0; i < items().count(); ++i) {

        CollectionItem *item = static_cast<CollectionItem *>(items().at(i));
        items().at(i)->setPos(this->itemPos(i));
//        item->setItemPos(this->itemPos(i));
    }

    QRectF itemBoundingRect = this->itemsBoundingRect();
    itemBoundingRect.adjust(-m_marginLeft,-m_marginTop,0,+m_marginBottom);
    this->setSceneRect(itemBoundingRect);

}

int CollectionScene::spacing()
{
    return m_spacing;
}

int CollectionScene::columSize()
{
    int viewWidth = m_view->viewport()->width() - (m_marginLeft + m_marginRight);
    int size = (viewWidth + m_spacing*2) / (m_cellNormalSize + m_spacing *2);
    return size == 0?1:size;
}

int CollectionScene::rowSize()
{
    return 0;
}

int CollectionScene::cellNormalSize()
{
    return m_cellNormalSize;
}

int CollectionScene::cellWidth()
{

    int viewWidth = m_view->viewport()->width() - (m_marginLeft + m_marginRight);
    int m = (viewWidth + m_spacing*2) % (m_cellNormalSize + m_spacing*2);
    int cellSize = this->columSize();
    int fitWidth = 0;

    if(items().count() > cellSize)
    {
        fitWidth = m / cellSize;
    }

    return m_cellNormalSize + fitWidth;
}

int CollectionScene::cellHeight()
{
    //return cellWidth();可以变化
    return cellNormalSize(); //固定
}

QPointF CollectionScene::itemPos(int index)
{
    if(index > -1)
    {
        int cellWidth = this->cellWidth();
        int cellHeight = this->cellHeight();
        int columSize = this->columSize();
        int spacingX = this->spacing() * 2;
        int spacingY = this->spacing() * 2;

        if(index < columSize ) // 这个index小于列的数量,在第一行
        {
            int y = m_marginTop;
            int spacingItem = index > 0 ? spacingX : 0;
            int x = index * (cellWidth +  spacingItem)+m_marginLeft;
            return QPointF(x,y);

        }else{

            int columnCount = 0;
            int row = 0;

            columnCount  = index % columSize;
            row = index / columSize;

            int y = row * (cellHeight + spacingY) + m_marginTop; //因为不在第一行,可以直接加spacing

//            if(d->extendingItem->isVisible())
//            {
//                int extendingItemHeight = d->extendingItem->boundingRect().height();
//                int extendingRow = this->itemRow(d->extendingItem->item());

//                if(row > extendingRow)
//                {
//                    y += extendingItemHeight+d->spacing;
//                }
//            }

            int spacingItem = columnCount > 0 ? spacingX : 0;
            int x = columnCount * (cellWidth + spacingItem)+m_marginLeft;
            return QPointF(x,y);
        }
    }

    return QPointF(0,0);
}

} // namespace Avocado

