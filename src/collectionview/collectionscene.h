#ifndef AVOCADO_COLLECTIONSCENE_H
#define AVOCADO_COLLECTIONSCENE_H

#include <QGraphicsScene>

namespace Avocado {
class CollectionView;
class CollectionScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit CollectionScene(CollectionView *parent = 0);
    void reLayout();

    int spacing();
    int columSize();
    int rowSize();
    int cellNormalSize();
    int cellWidth();
    int cellHeight();

protected:
    int m_marginLeft;
    int m_marginRight;
    int m_marginTop;
    int m_marginBottom;
    int m_spacing;
    int m_cellNormalSize;
    int m_columMinimumCell;
    QPointF itemPos(int index);
protected:
    CollectionView *m_view;
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_COLLECTIONSCENE_H
