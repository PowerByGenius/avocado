#include "waveformwidget.h"
#include "ui_waveformwidget.h"
#include "avocado_waveformgenerator.h"
#include <QWheelEvent>
#include <QDebug>
#include <QTimer>
#include <QDebug>
#include <QScrollBar>
#include <QApplication>

WaveFormWidget::WaveFormWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WaveFormWidget),
    m_generator(new Avocado::WaveformGenerator(this))

{
    ui->setupUi(this);
        m_scroll = 0;
        QTimer *timer = new QTimer(this);
        connect(timer,&QTimer::timeout,[=](){
           //ui->waveform->update(ui->waveform->visibleRegion());
           //ui->waveform->update(ui->waveform->visibleRegion());
            ui->waveform->repaint();
        });

        timer->start(40);

        QTimer *timer2 = new QTimer(this);
        connect(timer2,&QTimer::timeout,[=](){
           ui->scrollArea->horizontalScrollBar()->setValue(ui->scrollArea->horizontalScrollBar()->value()+2);
           QApplication::processEvents();
           //ui->waveform->repaint();//ui->waveform->visibleRegion());
        });

        timer2->start(10);


}

WaveFormWidget::~WaveFormWidget()
{
    delete ui;
}

void WaveFormWidget::resizeEvent(QResizeEvent *event)
{
    ui->widget_2->setMinimumWidth(ui->scrollArea->width()/2);
    ui->widget_3->setMinimumWidth(ui->scrollArea->width()/2);
    QWidget::resizeEvent(event);
}


void WaveFormWidget::on_pushButton_2_clicked()
{
    //m_generator->loadFile("/Users/kiko/Desktop/video/audio/01 2015-01-02 Kendra.m4a");
    //m_generator->loadFile("/Users/kiko/Desktop/Skipping.mp3");
    m_generator->loadFile("/Users/kiko/Desktop/abc/abc.m4a");
    //m_generator->loadFile("/Users/kiko/Desktop/abc/10sec2.mp3");

    //m_generator->loadFile("/Users/kiko/Desktop/abc/1sec.m4a");
    ui->waveform->waveform = m_generator->m_pixmap;
    ui->waveform->update();
}
