#include "avocado_waveformreader.h"
#include <QDebug>
#include <QFile>
#include <QDataStream>



namespace Avocado {

WaveformReader::WaveformReader(QObject *parent) :
    QObject(parent),
    m_format_context(NULL),
    m_decoder_context(NULL)
{
    class AVInitializer {
    public:
        AVInitializer() {
            qDebug("av_register_all");
            //av_log_set_level(AV_LOG_QUIET);
            av_register_all();
        }
        ~AVInitializer() {
            qDebug("avformat_network_deinit");
        }
    };
    static AVInitializer sAVInit;
    Q_UNUSED(sAVInit);
}

WaveformReader::~WaveformReader()
{
    if(m_decoder_context){
        avcodec_close(m_decoder_context);
        m_decoder_context = 0;
    }

    if(m_format_context){
        avformat_close_input(&m_format_context);
        m_format_context = 0;
    }

}

void get_format_range(enum AVSampleFormat format, int *min, int *max) {

    // figure out the range of sample values we're dealing with
    switch (format) {
    case AV_SAMPLE_FMT_FLT:
    case AV_SAMPLE_FMT_FLTP:
    case AV_SAMPLE_FMT_DBL:
    case AV_SAMPLE_FMT_DBLP:
        *min = -1;
        *max = 1;
        break;
    case AV_SAMPLE_FMT_U8:
    case AV_SAMPLE_FMT_U8P:
        *min = 0;
        *max = 255;
        break;
    case AV_SAMPLE_FMT_S16:
    case AV_SAMPLE_FMT_S16P:
        *min = pow(2, 2 * 8) / -2;
        *max = pow(2, 2 * 8) / 2 - 1;
        break;
    case AV_SAMPLE_FMT_S32:
    case AV_SAMPLE_FMT_S32P:
        *min = pow(2, 4 * 8) / -2;
        *max = pow(2, 4 * 8) / 2 - 1;
        break;
    }
}


double get_sample(enum AVSampleFormat format,uint8_t *data, int index) {
    double value = 0.0;

    switch (format) {
    case AV_SAMPLE_FMT_U8:
    case AV_SAMPLE_FMT_U8P:
        value += ((int8_t *) data)[index];
        break;
    case AV_SAMPLE_FMT_S16:
    case AV_SAMPLE_FMT_S16P:
        value += ((int16_t *) data)[index];
        break;
    case AV_SAMPLE_FMT_S32:
    case AV_SAMPLE_FMT_S32P:
        value += ((int32_t *) data)[index];
        break;
    case AV_SAMPLE_FMT_FLT:
    case AV_SAMPLE_FMT_FLTP:
        value += ((float *) data)[index];
        break;
    case AV_SAMPLE_FMT_DBL:
    case AV_SAMPLE_FMT_DBLP:
        value += ((double *) data)[index];
        break;
    }

    if (format == AV_SAMPLE_FMT_FLT ||
            format == AV_SAMPLE_FMT_FLTP ||
            format == AV_SAMPLE_FMT_DBL ||
            format == AV_SAMPLE_FMT_DBLP ) {
        if (value < -1.0) {
            value = -1.0;
        } else if (value > 1.0) {
            value = 1.0;
        }
    }
    return value;
}



void WaveformReader::loadFile(const QString &path)
{
    AVFormatContext *pFormatContext = NULL; // Container for the audio file
    AVCodecContext *pDecoderContext = NULL; // Container for the stream's codec
    AVCodec *pDecoder = NULL; // actual codec for the stream
    int stream_index = 0; // which audio stream should be looked at

    try{
        // open the audio file
        if (avformat_open_input(&pFormatContext, path.toLocal8Bit().data(), NULL, NULL) < 0) {
            fprintf(stderr, "Cannot open input file.\n");
            throw -1;
        }

        // Tell ffmpeg to read the file header and scan some of the data to determine
        // everything it can about the format of the file
        if (avformat_find_stream_info(pFormatContext, NULL) < 0) {
            fprintf(stderr, "Cannot find stream information.\n");
            throw -1;
        }

        // find the audio stream we probably care about.
        // For audio files, there will most likely be only one stream.
        stream_index = av_find_best_stream(pFormatContext, AVMEDIA_TYPE_AUDIO, -1, -1, &pDecoder, 0);

        if (stream_index < 0) {
            fprintf(stderr, "Unable to find audio stream in file.\n");
            throw -1;
        }

        // now that we have a stream, get the codec for the given stream
        pDecoderContext = pFormatContext->streams[stream_index]->codec;

        // open the decoder for this audio stream
        if (avcodec_open2(pDecoderContext, pDecoder, NULL) < 0) {
            fprintf(stderr, "Cannot open audio decoder.\n");
            throw -1;
        }

        m_format_context = pFormatContext;
        m_decoder_context = pDecoderContext;
        m_sampleSize = av_get_bytes_per_sample(pDecoderContext->sample_fmt);
        m_sampleRate = pDecoderContext->sample_rate;
        m_channels = pDecoderContext->channels;
        m_duration = pFormatContext->duration / (double) AV_TIME_BASE;
        m_sample_fmt = pDecoderContext->sample_fmt;
        m_size = 0;
        //av_dump_format(pFormatContext, 0, 0, 0);

        readRawData2();
        qDebug() << m_size  << m_duration << m_sampleRate <<  m_channels << m_sampleSize;

        //readRawData();
        genData();

        if (m_samples != NULL) {
            free(m_samples);
        }


    }catch (int e){

        if(pDecoderContext){
            avcodec_close(pDecoderContext);
            pDecoderContext = 0;
        }

        if(pFormatContext){
            avformat_close_input(&pFormatContext);
            pFormatContext = 0;
        }
    }

}


void WaveformReader::Gen()
{
    AVPacket packet;
    AVFrame *pFrame = NULL;
    double duration = m_format_context->duration / (double) AV_TIME_BASE;
    bool is_planar = av_sample_fmt_is_planar(m_decoder_context->sample_fmt);

    av_init_packet(&packet);

    if (!(pFrame = avcodec_alloc_frame())) {
        qDebug() <<"Could not allocate AVFrame\n";
        //        free_audio_data(data);
        return;
    }

    int total_size = 0;
    auto reframe = [=](AVPacket packet,int total_size){
    if(av_read_frame(m_format_context, &packet) == 0) return false;
    int frame_finished = 0;
    if (avcodec_decode_audio4(m_decoder_context, pFrame, &frame_finished, &packet) < 0) {
        qDebug() << " unable to decode this packet. continue on to the next packet";
    }
    if (frame_finished) {
        int data_size = av_samples_get_buffer_size(
                    is_planar ? &pFrame->linesize[0] : NULL,
                    m_channels,
                    pFrame->nb_samples,
                    m_decoder_context->sample_fmt,
                    1
                    );
        if (is_planar) {
            int i = 0;
            int c = 0;

            for (; i < data_size / m_decoder_context->channels; i += m_sampleSize) {
                for (c = 0; c < m_decoder_context->channels; c++) {
                    memcpy(m_samples + total_size, pFrame->extended_data[c] + i, m_sampleSize);
                    total_size += m_sampleSize;
                }
            }
        }else{
            qDebug() << "aa";
        }
    }
    av_free_packet(&packet);
        return true;
    };




}





void WaveformReader::readRawData()
{
    AVPacket packet;
    AVFrame *pFrame = NULL;
    double duration = m_format_context->duration / (double) AV_TIME_BASE;
    int raw_sample_rate = 0;
    bool is_planar = av_sample_fmt_is_planar(m_decoder_context->sample_fmt);
    av_init_packet(&packet);

    if (!(pFrame = avcodec_alloc_frame())) {
        qDebug() <<"Could not allocate AVFrame\n";
        //        free_audio_data(data);
        return;
    }

    QList<qint16> list;

    int pngHeight = 255;
    int pngWidth = m_duration * 100;
    int track_height = pngHeight;

    int sample_min;
    int sample_max;
    get_format_range(m_sample_fmt, &sample_min, &sample_max);
    uint32_t sample_range = sample_max - sample_min;

    double channel_average_multiplier = 1.0 / m_channels;

    m_size = 1111547392;

    int sample_count = m_size / m_sampleSize / m_channels;
    int samples_per_pixel = sample_count / pngWidth; // how many samples fit in a column of pixels?
    qDebug() << samples_per_pixel;
    int total_size = 0;

    double min = sample_max;
    double max = sample_min;

    while (av_read_frame(m_format_context, &packet) == 0) {

        int frame_finished = 0;

        if (avcodec_decode_audio4(m_decoder_context, pFrame, &frame_finished, &packet) < 0) {
             qDebug() << " unable to decode this packet. continue on to the next packet";
            continue;
        }

        if (frame_finished) {

            int data_size = av_samples_get_buffer_size(
                        is_planar ? &pFrame->linesize[0] : NULL,
                        m_channels,
                        pFrame->nb_samples,
                        m_decoder_context->sample_fmt,
                        1
                        );
            qDebug() << data_size;
            if (raw_sample_rate == 0) {
                raw_sample_rate = pFrame->sample_rate;
            }

            if (is_planar) {

                int i = 0;
                int c = 0;
                for (; i < data_size / m_channels; i += m_sampleSize) {

                    double value = 0;
                    for (c = 0; c < m_channels; c++) {
                        value += get_sample(m_sample_fmt,pFrame->extended_data[c],i) * channel_average_multiplier;
                        total_size ++;
                    }

                    if (value < min) {
                        min = value;
                    }
                    if (value > max) {
                        max = value;
                    }

                    if(total_size % 959 == 0)
                    {
                        int y_max = track_height - ((min - sample_min) * track_height / sample_range);
                        int y_min = track_height - ((max - sample_min) * track_height / sample_range);
                        qint16 d = (y_min << 8) | (y_max & 0xFF);
                        list.append(d);
                        min = sample_max;
                        max = sample_min;
                    }
                }

            }else{
                qDebug() << "aa";
            }
        }
        av_free_packet(&packet);
    }

    QFile file("/Users/kiko/Desktop/abc/test2.data");
    file.remove();
    file.open(QIODevice::WriteOnly);
    QDataStream stream(&file);
    stream << list;
    qDebug() << list.size();
}










































































void WaveformReader::readRawData2()
{
    AVPacket packet;
    AVFrame *pFrame = NULL;
    int raw_sample_rate = 0;
    double duration = m_format_context->duration / (double) AV_TIME_BASE;
    int is_planar = av_sample_fmt_is_planar(m_decoder_context->sample_fmt);
    int total_size = 0;
    bool populate_sample_buffer = true;
    av_init_packet(&packet);

    if (!(pFrame = avcodec_alloc_frame())) {
        //free_audio_data(data);
        if (m_samples != NULL) {
            free(m_samples);
        }
        return;
    }

    int allocated_buffer_size = 0;

    // guess how much memory we'll need for samples.
    if (populate_sample_buffer) {
        allocated_buffer_size = (m_format_context->bit_rate / 8) * duration;
        m_samples = (uint8_t*) malloc(sizeof(uint8_t) * allocated_buffer_size);
    }


    while (av_read_frame(m_format_context, &packet) == 0) {
        int frame_finished = 0;
        if (avcodec_decode_audio4(m_decoder_context, pFrame, &frame_finished, &packet) < 0) {
            // unable to decode this packet. continue on to the next packet
            continue;
        }

        // did we get an entire raw frame from the packet?
        if (frame_finished) {
            // Find the size of all pFrame->extended_data in bytes. Remember, this will be:
            // data_size = pFrame->nb_samples * pFrame->channels * bytes_per_sample
            int data_size = av_samples_get_buffer_size(
                        is_planar ? &pFrame->linesize[0] : NULL,
                        m_decoder_context->channels,
                        pFrame->nb_samples,
                        m_decoder_context->sample_fmt,
                        1
                        );

            if (raw_sample_rate == 0) {
                raw_sample_rate = pFrame->sample_rate;
            }

            // if we don't have enough space in our copy buffer, expand it
            if (populate_sample_buffer && total_size + data_size > allocated_buffer_size) {
                allocated_buffer_size = allocated_buffer_size * 1.25;
                m_samples = (uint8_t*)realloc(m_samples, allocated_buffer_size);
            }

            if (is_planar) {

                int i = 0;
                int c = 0;

                for (; i < data_size / m_decoder_context->channels; i += m_sampleSize) {
                    for (c = 0; c < m_decoder_context->channels; c++) {
                        if (populate_sample_buffer) {
                            memcpy(m_samples + total_size, pFrame->extended_data[c] + i, m_sampleSize);
                        }
                        total_size += m_sampleSize;
                    }
                }
            } else {

                if (populate_sample_buffer) {
                    memcpy(m_samples + total_size, pFrame->extended_data[0], data_size);
                }

                total_size += data_size;
            }
        }

        av_free_packet(&packet);
    }

    m_size = total_size;
    if (total_size == 0) {
        // not a single packet could be read.
        return;
    }
}

void WaveformReader::genData()
{
    int pngHeight = 255;
    int pngWidth = m_duration *100;

    int sample_min;
    int sample_max;

    QList<qint16> list;
    get_format_range(m_decoder_context->sample_fmt, &sample_min, &sample_max);

    uint32_t sample_range = sample_max - sample_min; // total range of values a sample can have
    int sample_count = m_size / m_sampleSize; // how many samples are there total?
    int samples_per_pixel = sample_count / pngWidth; // how many samples fit in a column of pixels?
    qDebug() << "samples_per_pixel" << samples_per_pixel;

    // multipliers used to produce averages while iterating through samples.
    double channel_average_multiplier = 1.0 / m_decoder_context->channels;


    // 10% padding
    int padding = (int) (pngHeight * 0.05);

    padding = 0;

    int track_height = pngHeight - (padding * 2);


    // for each column of pixels in the final output image
    int x;
    for (x = 0; x < pngWidth; ++x) {

        double min = sample_max;
        double max = sample_min;

        int i;
        for (i = 0; i < samples_per_pixel; i += m_decoder_context->channels) {
            double value = 0;

            int c;
            for (c = 0; c < m_decoder_context->channels; ++c) {
                int index = x * samples_per_pixel + i + c;

                value += get_sample(m_decoder_context->sample_fmt,m_samples, index) * channel_average_multiplier;
            }

            if (value < min) {
                min = value;
            }

            if (value > max) {
                max = value;
            }
        }
        int y_max = track_height - ((min - sample_min) * track_height / sample_range) + padding;
        int y_min = track_height - ((max - sample_min) * track_height / sample_range) + padding;
        qint16 d = (y_min << 8) | (y_max & 0xFF);

        list.append(d);

    }

    QFile file("/Users/kiko/Desktop/abc/test2.data");
    file.open(QIODevice::WriteOnly);
    QDataStream stream(&file);
    stream << list;
    qDebug() << "END";

}

} // namespace Avocado

