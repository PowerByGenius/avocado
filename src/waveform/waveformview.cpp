#include "waveformview.h"
#include <QPainter>
#include <QPaintEvent>
#include <QBrush>
#include <QDebug>
#include <QTimer>


QList<QVariantMap> captionParser(const QString &text)
{
    QRegExp rx("(\\d+)\\s*(\\d{2}:\\d{2}:\\d{2},\\d{3}) --> (\\d{2}:\\d{2}:\\d{2},\\d{3})");

    QList<QVariantMap> list;
    int pos = 0;
    int posNext = 0;
    while ((pos = rx.indexIn(text, pos)) != -1)
    {
        QVariantMap map;
        map.insert("index",rx.cap(1).trimmed());
        map.insert("start",rx.cap(2).trimmed());
        map.insert("end",rx.cap(3).trimmed());

        pos += rx.matchedLength();
        posNext = text.indexOf(rx,pos);
        QString temp;
        if(posNext > 0){
            temp = text.mid(pos,posNext-pos);
        }else{
            temp = text.mid(pos);
        }
        map.insert("caption",temp.trimmed());

        list.append(map);
    }

    return list;
}


qint64 textToPosition(const QString &text)
{
    qreal time = 0;
    QRegExp rx("(\\d{2}):(\\d{2}):(\\d{2}),(\\d{3})");
    if(rx.exactMatch(text))
    {
         time = rx.cap(4).toInt()
                +rx.cap(3).toInt() * 1000
                +rx.cap(2).toInt() * 60  * 1000
                +rx.cap(1).toInt() * 60 * 60 * 1000;
    }
    return time;
}




WaveformView::WaveformView(QWidget *parent) :
    QWidget(parent)
{
    //    this->setMinimumHeight(200);
    //    this->setMinimumWidth(1000);

    {
    QFile file("/Users/kiko/Desktop/abc/test.data");
    file.open(QIODevice::ReadOnly);
    QDataStream stream(&file);
    stream >> m_data;
    }

    {
    QFile file("/Users/kiko/Desktop/abc/abc.srt");
    file.open(QIODevice::ReadOnly);
    QString text = file.readAll();
    foreach (QVariantMap map, captionParser(text)) {
        qint64 start = textToPosition(map.value("start").toString())/10;
        m_caption[start] = map;
    }

    }


}

void WaveformView::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    //    p.drawPixmap(0,0,waveform);
    //    this->setMinimumWidth(waveform.width());


    QRect rect = event->rect();

    m_viewPortWidth = rect.width();

    int padding = m_viewPortWidth/2;

    p.fillRect(rect,QColor(27,27,27));
    QRect ruler = rect;
    ruler.setBottom(20);
    p.fillRect(ruler,Qt::white);



    QFont rulerFont;
    rulerFont.setPointSize(10);

    for(int i=rect.left();i<rect.right();i++)
    {
        int ms = i;

        if(ms % 11 == 0)
        {
            QPen rulerPen;
            rulerPen.setColor(Qt::black);
            rulerPen.setWidthF(1);
            p.setPen(rulerPen);
            if(ms % 110 == 0)
            {
                int t = (ms) / 110;
                QString timeText = t > 3600 ? QString("%1:%2:%3")
                                              .arg(t/3600,2,10,QChar('0'))
                                              .arg(t/60%60,2,10,QChar('0'))
                                              .arg(t%60,2,10,QChar('0'))
                                            :QString("%1:%2")
                                              .arg(t/60%60,2,10,QChar('0'))
                                              .arg(t%60,2,10,QChar('0'));
                p.setFont(rulerFont);
                p.drawText(ms+3,12,timeText);
                for(int x=5;x<18;x++)
                {
                    p.drawPoint(ms,x);
                }
            }
            p.drawPoint(ms,18);
            p.drawPoint(ms,19);
            p.drawPoint(ms,20);
        }


        //218/186/48
        //238/206/61

        if((ms) >= m_data.count()) break;
        int data = m_data.at(ms);


        int v = rect.height()/2-127+10;
        int y = 127+v;
        int y_min = (data >> 8) & 0xFF;
        int y_max = data & 0xFF;

        p.setRenderHint(QPainter::Antialiasing,true);
        p.setPen(QColor(255,204,0));

//        p.drawLine(i,y,i,y_min+v);
//        p.drawLine(i,y,i,y_max+v);

//        if(i%2==0)
//        {
//            if(y_max-y_min > 1)
//                p.setPen(QColor(27,27,27));
//        }

        p.drawLine(ms,y_min+v,ms,y_max+v);


        if(m_caption.contains(ms))
        {
            QVariantMap map = m_caption.value(ms);

            QFont fontCaption = p.font();
            fontCaption.setPointSize(24);
            p.setPen(Qt::white);
            p.setBrush(Qt::white);
            p.setFont(fontCaption);
            int endPos = textToPosition(map.value("end").toString())/10;
            QString text = map.value("caption").toString();
            QFontMetrics fmCaption(fontCaption);


            QRect rectCaption = fmCaption.boundingRect(ms,0,endPos-ms,0,Qt::AlignLeft|Qt::AlignTop,text);

            int top = ((rect.height()-20)/2) /2 - rectCaption.height();

            if(map.value("index").toString().toInt() % 2 == 0)
            {
                top = ((rect.height()-20)/2) /2 + ((rect.height()-20)/2) + rectCaption.height();
            }

            QRect r(ms,top,rectCaption.width(),rectCaption.height());

            p.drawText(r,Qt::AlignLeft|Qt::AlignTop,text);
        }

    }

//    int redlineX = rect.left() + rect.width() / 2;
//    p.setPen(Qt::red);
//    p.drawLine(redlineX,20,redlineX,rect.height());

    QWidget::paintEvent(event);
}

void WaveformView::resizeEvent(QResizeEvent *event)
{
    this->setMinimumWidth(m_data.count());
    QWidget::resizeEvent(event);
}

