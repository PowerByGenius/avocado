#ifndef AUDIOWIDGET_H
#define AUDIOWIDGET_H

#include <QWidget>

namespace Ui {
class AudioWidget;
}

class AudioWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AudioWidget(QWidget *parent = 0);
    ~AudioWidget();

private slots:
    void on_btnGen_clicked();

private:
    Ui::AudioWidget *ui;
};

#endif // AUDIOWIDGET_H
