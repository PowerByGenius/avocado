#include "waveformreaderwidget.h"
#include "ui_waveformreaderwidget.h"
#include "avocado_waveformreader.h"

WaveFormReaderWidget::WaveFormReaderWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WaveFormReaderWidget)
{
    ui->setupUi(this);
}

WaveFormReaderWidget::~WaveFormReaderWidget()
{
    delete ui;
}

void WaveFormReaderWidget::on_btnReader_clicked()
{
    Avocado::WaveformReader *reader = new Avocado::WaveformReader(this);
    reader->loadFile("/Users/kiko/Desktop/abc/c.wav");
}
