#ifndef WAVEFORMREADERWIDGET_H
#define WAVEFORMREADERWIDGET_H

#include <QWidget>

namespace Ui {
class WaveFormReaderWidget;
}

class WaveFormReaderWidget : public QWidget
{
    Q_OBJECT

public:
    explicit WaveFormReaderWidget(QWidget *parent = 0);
    ~WaveFormReaderWidget();

private slots:
    void on_btnReader_clicked();

private:
    Ui::WaveFormReaderWidget *ui;
};

#endif // WAVEFORMREADERWIDGET_H
