#include "waveformtracktestwidget.h"
#include "ui_waveformtracktestwidget.h"
#include "waveformtrack.h"
#include <QApplication>
#include <QPainter>
#include <QTimer>
#include <QBackingStore>
#include <QResizeEvent>
#include <QOpenGLContext>

WaveFormTrackTestWidget::WaveFormTrackTestWidget(QWindow *parent) :
    QWindow(parent),
    ui(new Ui::WaveFormTrackTestWidget),
    m_track(new WaveformTrack(this))
{

    setSurfaceType(QWindow::OpenGLSurface);

    m_backingStore = new QBackingStore(this);
    create();


    QTimer *timer = new QTimer(this);
    connect(timer,&QTimer::timeout,[=](){
        m_track->setPositon(m_track->position()+1);
    });

    timer->start(10);

    QTimer *timer2 = new QTimer(this);
    connect(timer2,&QTimer::timeout,[=](){
        this->renderNow();
    });

    timer2->start(20);
}

WaveFormTrackTestWidget::~WaveFormTrackTestWidget()
{
    delete ui;
}

void WaveFormTrackTestWidget::render(QPainter *painter)
{
    QRect rect(0, 0, width(), height());
    m_track->render(painter,rect);
}

void WaveFormTrackTestWidget::renderLater()
{
    if (!m_update_pending) {
        m_update_pending = true;
        QCoreApplication::postEvent(this, new QEvent(QEvent::UpdateRequest));
    }
}

void WaveFormTrackTestWidget::renderNow()
{
    if (!isExposed())
        return;

    QRect rect(0, 0, width(), height());


    m_backingStore->beginPaint(rect);

    QPaintDevice *device = m_backingStore->paintDevice();
    QPainter painter(device);

    painter.fillRect(0, 0, width(), height(), Qt::white);
    render(&painter);

    m_backingStore->endPaint();
    m_backingStore->flush(rect);
}

bool WaveFormTrackTestWidget::event(QEvent *event)
{
    if (event->type() == QEvent::UpdateRequest) {
        m_update_pending = false;
        renderNow();
        return true;
    }
    return QWindow::event(event);
}

void WaveFormTrackTestWidget::resizeEvent(QResizeEvent *event)
{
    m_backingStore->resize(event->size());
    if (isExposed())
        renderNow();
}

void WaveFormTrackTestWidget::exposeEvent(QExposeEvent *event)
{
    if (isExposed()) {
        renderNow();
    }
}
