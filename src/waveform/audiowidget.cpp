#include "audiowidget.h"
#include "ui_audiowidget.h"
#include <QFile>
#include <QDataStream>
#include <QCryptographicHash>
#ifdef __cplusplus
extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>

#include <libavutil/samplefmt.h>
#include <libswresample/swresample.h>

}
#endif /*__cplusplus*/

#include <QDebug>

void get_format_range(enum AVSampleFormat format, int *min, int *max) {

    // figure out the range of sample values we're dealing with
    switch (format) {
    case AV_SAMPLE_FMT_FLT:
    case AV_SAMPLE_FMT_FLTP:
    case AV_SAMPLE_FMT_DBL:
    case AV_SAMPLE_FMT_DBLP:
        *min = -1;
        *max = 1;
        break;
    case AV_SAMPLE_FMT_U8:
    case AV_SAMPLE_FMT_U8P:
        *min = 0;
        *max = 255;
        break;
    case AV_SAMPLE_FMT_S16:
    case AV_SAMPLE_FMT_S16P:
        *min = pow(2, 2 * 8) / -2;
        *max = pow(2, 2 * 8) / 2 - 1;
        break;
    case AV_SAMPLE_FMT_S32:
    case AV_SAMPLE_FMT_S32P:
        *min = pow(2, 4 * 8) / -2;
        *max = pow(2, 4 * 8) / 2 - 1;
        break;
    case AV_SAMPLE_FMT_NONE:
    case AV_SAMPLE_FMT_NB:
        break;
    }
}

double get_sample(enum AVSampleFormat format,uint8_t *data) {
    double value = 0.0;

    switch (format) {
    case AV_SAMPLE_FMT_U8:
    case AV_SAMPLE_FMT_U8P:
        value += *((int8_t *) data);
        break;
    case AV_SAMPLE_FMT_S16:
    case AV_SAMPLE_FMT_S16P:
        value += *((int16_t *) data);
        break;
    case AV_SAMPLE_FMT_S32:
    case AV_SAMPLE_FMT_S32P:
        value += *((int32_t *) data);
        break;
    case AV_SAMPLE_FMT_FLT:
    case AV_SAMPLE_FMT_FLTP:
        value += *((float *) data);
        break;
    case AV_SAMPLE_FMT_DBL:
    case AV_SAMPLE_FMT_DBLP:
        value += *((double *) data);
        break;
    case AV_SAMPLE_FMT_NONE:
    case AV_SAMPLE_FMT_NB:
        break;
    }

    if (format == AV_SAMPLE_FMT_FLT ||
            format == AV_SAMPLE_FMT_FLTP ||
            format == AV_SAMPLE_FMT_DBL ||
            format == AV_SAMPLE_FMT_DBLP ) {
        if (value < -1.0) {
            value = -1.0;
        } else if (value > 1.0) {
            value = 1.0;
        }
    }
    return value;
}

AudioWidget::AudioWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AudioWidget)
{
    ui->setupUi(this);
    av_register_all();
}

AudioWidget::~AudioWidget()
{
    delete ui;
}

void AudioWidget::on_btnGen_clicked()
{
    AVFormatContext *pFormatContext = NULL;
    AVCodecContext *pDecoderContext = NULL;
    AVCodec *pDecoder = NULL;
    int stream_index = 0;
    QString path = "/Users/kiko/Desktop/abc/q.mp4";

    path = "/Users/kiko/Desktop/abc/Friends.with.Benefits.2011.BluRay.720p.x264.AC3-WOFEI.mp4";
    path = "/Users/kiko/Downloads/Friends.with.Benefits.2011.BluRay.720p.DTS.x264-CHD/Friends.with.Benefits.2011.BluRay.720p.x264.AC3-WOFEI.mkv";
    try{

        if (avformat_open_input(&pFormatContext, path.toLocal8Bit().data(), NULL, NULL) < 0) throw -1;
        if (avformat_find_stream_info(pFormatContext, NULL) < 0) throw -2;
        stream_index = av_find_best_stream(pFormatContext, AVMEDIA_TYPE_AUDIO, -1, -1, &pDecoder, 0);
        if (stream_index < 0) throw -3;
        pDecoderContext = pFormatContext->streams[stream_index]->codec;
        if (avcodec_open2(pDecoderContext, pDecoder, NULL) < 0) throw -4;

        av_dump_format(pFormatContext, 0, 0, 0);

        int sample_size = av_get_bytes_per_sample(pDecoderContext->sample_fmt);
        int sample_rate = pDecoderContext->sample_rate;
        int channels =   pDecoderContext->channels;
        AVSampleFormat sample_fmt = pDecoderContext->sample_fmt;

        int bit_rate    = pFormatContext->bit_rate;
        double duration = pFormatContext->duration / (double) AV_TIME_BASE;


//        qDebug() << "sample_size:" << sample_size
//                 << "sample_rate:" << sample_rate
//                 << "channels:" << channels
//                 << "bit_rate:" << bit_rate
//                 << "duration:" << duration;


        bool        convert = false;

        SwrContext *swr = NULL;

        qDebug() << pDecoderContext->channel_layout;
        if(channels > 2)
        {
            sample_fmt = AV_SAMPLE_FMT_FLTP;
            channels = 2;
            sample_size = av_get_bytes_per_sample(AV_SAMPLE_FMT_FLTP);
            sample_rate = 48000;

            swr = swr_alloc();
            av_opt_set_int(swr, "in_channel_layout",  pDecoderContext->channel_layout, 0);
            av_opt_set_int(swr, "out_channel_layout", AV_CH_LAYOUT_STEREO,  0);
            av_opt_set_int(swr, "in_channel_count", pDecoderContext->channels,  0);
            av_opt_set_int(swr, "out_channel_count", channels,  0);
            av_opt_set_int(swr, "in_sample_rate",     pDecoderContext->sample_rate, 0);
            av_opt_set_int(swr, "out_sample_rate",    sample_rate, 0);
            av_opt_set_sample_fmt(swr, "in_sample_fmt",  pDecoderContext->sample_fmt, 0);
            av_opt_set_sample_fmt(swr, "out_sample_fmt", sample_fmt,  0);
            swr_init(swr);
        }



        av_log_set_level(0);

        QList<qint16> list;

        int         testCount = 0;

        int         sample_min;
        int         sample_max;
        get_format_range(sample_fmt, &sample_min, &sample_max);

        uint32_t    sample_range = sample_max - sample_min;
        double      channel_average_multiplier = 1.0 / channels;
        int         scale = sample_rate/1000 * 10;
        int         track_height = 255;
        int         padding = 0;

        QVector<double> buff;
        int         buffer_size = 8192; //每读取达到这个数量的Frame执行一次数据处理
        int         frame_size = 0;






        int total_size = 0;

        int is_planar = av_sample_fmt_is_planar(sample_fmt);

        AVFrame *pFrame = NULL;
        AVFrame *pFrameOut = NULL;
        if (!(pFrame = av_frame_alloc())) throw -11;
        if (!(pFrameOut = av_frame_alloc())) throw -11;

        AVPacket packet;
        av_init_packet(&packet);

        while (av_read_frame(pFormatContext, &packet) == 0) {

            int frame_finished = 0;
            if (avcodec_decode_audio4(pDecoderContext, pFrame, &frame_finished, &packet) < 0) {
                // 偶尔会发生读不来，让它去吧
                continue;
            }


            pFrameOut->channels = channels;
            pFrameOut->channel_layout = AV_CH_LAYOUT_STEREO;
            pFrameOut->nb_samples = pFrame->nb_samples;
            pFrameOut->format = sample_fmt;
            pFrameOut->sample_rate = sample_rate;
            pFrameOut->pts = pFrame->pts;

            av_samples_alloc(pFrameOut->data,&pFrameOut->linesize[0],
                    pFrameOut->channels,pFrameOut->nb_samples,AV_SAMPLE_FMT_FLTP,0);


            swr_convert(swr,pFrameOut->data,pFrameOut->nb_samples,
                        (const uint8_t**)pFrame->data,pFrame->nb_samples);


            if (frame_finished) {

//                int data_size = av_samples_get_buffer_size(
//                            is_planar ? &pFrame->linesize[0] : NULL,
//                            channels,
//                            pFrame->nb_samples,
//                            sample_fmt,
//                            1
//                            );

                int data_size = av_samples_get_buffer_size(
                            is_planar ? &pFrameOut->linesize[0] : NULL,
                            channels,
                            pFrameOut->nb_samples,
                            sample_fmt,
                            1
                            );


                if (is_planar) {
                    int i = 0;
                    int c = 0;
                    for (; i < data_size / channels; i += sample_size) {
                        double value = 0;
                        for (c = 0; c < channels; c++) {
                            //value += get_sample(sample_fmt,&pFrame->extended_data[c][i]) * channel_average_multiplier;
                            value += get_sample(sample_fmt,&pFrameOut->extended_data[c][i]) * channel_average_multiplier;
                        }
                        buff.append(value);
                        total_size ++;
                    }
                } else {
                    int i = 0;
                    for (; i < data_size; i += sample_size) {
                        double value = 0;
                        //value = get_sample(sample_fmt,&pFrame->extended_data[0][i]);
                        value = get_sample(sample_fmt,&pFrameOut->extended_data[0][i]);
                        buff.append(value);
                        total_size ++;
                    }

                }
            }

            av_free_packet(&packet);


            if(frame_size % buffer_size == 0)
            {
                if(buff.size() > scale)
                {
                    int t = buff.size() / scale;
                    for (int i = 0; i < t ; ++i) {
                        double min = sample_max;
                        double max = sample_min;
                        double value = 0;
                        for (int x = 0; x < scale; ++x) {
                            value = buff.at((i*scale)+x);
                            if (value < min) min = value;
                            if (value > max) max = value;
                        }
                        int y_max = track_height - ((min - sample_min) * track_height / sample_range) + padding;
                        int y_min = track_height - ((max - sample_min) * track_height / sample_range) + padding;
                        qint16 d = (y_min << 8) | (y_max & 0xFF);
                        list.append(d);
                        testCount++;
                    }
                    QVector<double> temp;
                    for (int i = t*scale; i < buff.size(); ++i) {
                        temp.append(buff.at(i));
                    }
                    buff.clear();

                    for (int i = 0; i < temp.size(); ++i) {
                        buff.append(temp.at(i));
                    }
                    temp.clear();
                }
            }

            frame_size++;
        } // end while

        if(swr){
          swr_free(&swr);
          swr=0;
        }

        av_frame_free(&pFrame);
        av_frame_free(&pFrameOut);
        av_free_packet(&packet);


        // 如果长度小于buffer_size，前面没有执行，这里补多次
        if(buff.size() > scale)
        {
            int t = buff.size() / scale;
            for (int i = 0; i < t ; ++i) {
                double min = sample_max;
                double max = sample_min;
                double value = 0;
                for (int x = 0; x < scale; ++x) {
                    value = buff.at((i*scale)+x);
                    if (value < min) min = value;
                    if (value > max) max = value;
                }
                int y_max = track_height - ((min - sample_min) * track_height / sample_range) + padding;
                int y_min = track_height - ((max - sample_min) * track_height / sample_range) + padding;
                qint16 d = (y_min << 8) | (y_max & 0xFF);
                list.append(d);
                testCount++;
            }
            QVector<double> temp;
            for (int i = t*scale; i < buff.size(); ++i) {
                temp.append(buff.at(i));
            }
            buff.clear();

            for (int i = 0; i < temp.size(); ++i) {
                buff.append(temp.at(i));
            }
            temp.clear();
        }


        // Buff剩下的采样

        if(buff.size() > 0)
        {
            double min = sample_max;
            double max = sample_min;
            double value = 0;
            for (int x = 0; x < buff.size(); ++x) {
                value = buff.takeFirst();
                if (value < min) min = value;
                if (value > max) max = value;
            }
            int y_max = track_height - ((min - sample_min) * track_height / sample_range) + padding;
            int y_min = track_height - ((max - sample_min) * track_height / sample_range) + padding;
            qint16 d = (y_min << 8) | (y_max & 0xFF);
            list.append(d);
            testCount++;
        }

        qDebug() << scale << testCount << buff.size();


        QFile file("/Users/kiko/Desktop/abc/test2.data");
        file.remove();
        file.open(QIODevice::WriteOnly);
        QDataStream stream(&file);
        stream << list;
        qDebug() << list.size();
        list.clear();


        //        if (total_size == 0) {
        //            // not a single packet could be read.
        //            return;
        //        }


    }catch (int e){

        switch (e) {
        case -1:
            qDebug("Cannot open input file.");
            break;
        case -2:
            qDebug("Cannot find stream information.\n");
            break;
        case -3:
            qDebug("Unable to find audio stream in file.\n");
            break;
        case -4:
            qDebug("Cannot open audio decoder.\n");
            break;
        case -11:
            //free_audio_data(data);
            qDebug("Cannot alloc frame.\n");
            break;
        default:
            break;
        }

        if(pDecoderContext){
            avcodec_close(pDecoderContext);
            pDecoderContext = 0;
        }
        if(pFormatContext){
            avformat_close_input(&pFormatContext);
            pFormatContext = 0;
        }
    }

    if(pDecoderContext){
        avcodec_close(pDecoderContext);
        pDecoderContext = 0;
    }
    if(pFormatContext){
        avformat_close_input(&pFormatContext);
        pFormatContext = 0;
    }

    //    if (m_samples != NULL) {
    //        free(m_samples);
    //    }




}
