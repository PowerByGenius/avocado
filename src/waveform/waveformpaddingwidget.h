#ifndef WAVEFORMPADDINGWIDGET_H
#define WAVEFORMPADDINGWIDGET_H

#include <QWidget>

class WaveformPaddingWidget : public QWidget
{
    Q_OBJECT
public:
    explicit WaveformPaddingWidget(QWidget *parent = 0);
protected:
    void paintEvent(QPaintEvent *event);

signals:

public slots:
};

#endif // WAVEFORMPADDINGWIDGET_H
