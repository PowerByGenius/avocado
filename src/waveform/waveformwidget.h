#ifndef WAVEFORMWIDGET_H
#define WAVEFORMWIDGET_H

#include <QWidget>

namespace Ui {
class WaveFormWidget;
}

namespace Avocado{
class WaveformGenerator;
}

class WaveFormWidget : public QWidget
{
    Q_OBJECT

public:
    explicit WaveFormWidget(QWidget *parent = 0);
    ~WaveFormWidget();
protected:
    void resizeEvent(QResizeEvent *event);
private slots:
    void on_pushButton_2_clicked();

private:
    Ui::WaveFormWidget *ui;
    Avocado::WaveformGenerator *m_generator;
    int m_scroll;

};

#endif // WAVEFORMWIDGET_H
