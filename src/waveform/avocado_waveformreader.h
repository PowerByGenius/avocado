#ifndef AVOCADO_WAVEFORMREADER_H
#define AVOCADO_WAVEFORMREADER_H

#ifdef __cplusplus
extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
}
#endif /*__cplusplus*/

#include <QObject>

class AVFormatContext;
class AVCodecContext;

namespace Avocado {

class WaveformReader : public QObject
{
    Q_OBJECT
public:
    explicit WaveformReader(QObject *parent = 0);
    ~WaveformReader();
    void loadFile(const QString &path);
    void Gen();
    void readRawData();
    void readRawData2();
    void genData();
private:
    AVFormatContext *m_format_context;
    AVCodecContext *m_decoder_context;
    int m_channels;
    int m_sampleRate;
    int m_sampleSize;
    int m_size;
    double m_duration;
    uint8_t *m_samples;
    AVSampleFormat m_sample_fmt;
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_WAVEFORMREADER_H
