#ifndef WAVEFORMTRACK_H
#define WAVEFORMTRACK_H

#include <QObject>

class QPainter;

class WaveformTrack : public QObject
{
    Q_OBJECT
public:
    explicit WaveformTrack(QObject *parent = 0);
    void render(QPainter *painter, const QRect &rect);
    qint64 position() const {return m_position;}
private:
    QList<qint16> m_waveformData;
    qint64 m_position;
signals:

public slots:
    void setPositon(qint64 value);
};

#endif // WAVEFORMTRACK_H
