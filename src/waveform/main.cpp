#include "waveformwidget.h"
#include "waveformtracktestwidget.h"
#include "waveformreaderwidget.h"
#include "audiowidget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AudioWidget wa;
    wa.show();

//    WaveFormWidget w2;
//    w2.show();

//    QSurfaceFormat fmt;
//    fmt.setSamples(4);
//    QSurfaceFormat::setDefaultFormat(fmt);
//    WaveFormTrackTestWidget x;
//    x.show();

//    WaveFormReaderWidget w;
//    w.show();
    return a.exec();
}
