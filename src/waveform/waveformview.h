#ifndef WAVEFORMVIEW_H
#define WAVEFORMVIEW_H

#include <QWidget>
#include <QVariantMap>
#include <QOpenGLWidget>

class WaveformView : public QWidget
{
    Q_OBJECT
public:
    explicit WaveformView(QWidget *parent = 0);
    QPixmap waveform;
protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);
private:
    QList<qint16> m_data;
    int m_viewPortWidth;
    QMap<qint64,QVariantMap> m_caption;
signals:

public slots:
};

#endif // WAVEFORMVIEW_H
