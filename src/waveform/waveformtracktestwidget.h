#ifndef WAVEFORMTRACKTESTWIDGET_H
#define WAVEFORMTRACKTESTWIDGET_H

#include <QWindow>
#include <QOpenGLPaintDevice>

namespace Ui {
class WaveFormTrackTestWidget;
}

class WaveformTrack;
class BackingStore;
class WaveFormTrackTestWidget : public QWindow
{
    Q_OBJECT

public:
    explicit WaveFormTrackTestWidget(QWindow *parent = 0);
    ~WaveFormTrackTestWidget();

    virtual void render(QPainter *painter);

public slots:
    void renderLater();
    void renderNow();

protected:
    bool event(QEvent *event) Q_DECL_OVERRIDE;

    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
    void exposeEvent(QExposeEvent *event) Q_DECL_OVERRIDE;

//protected:
//    void paintEvent(QPaintEvent *event);
private:
    Ui::WaveFormTrackTestWidget *ui;
    WaveformTrack *m_track;
    QBackingStore *m_backingStore;
    bool m_update_pending;
    QOpenGLContext *m_context;
    QOpenGLPaintDevice *m_device;
};

#endif // WAVEFORMTRACKTESTWIDGET_H
