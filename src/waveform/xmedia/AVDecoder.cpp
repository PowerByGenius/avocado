#include <XMedia/AVDecoder.h>
#include <XMedia/private/AVDecoder_p.h>

namespace XMedia {
AVDecoder::AVDecoder()
{
}

AVDecoder::AVDecoder(AVDecoderPrivate &d)
    :DPTR_INIT(&d)
{

}

AVDecoder::~AVDecoder()
{
}

void AVDecoder::flush()
{
    if (isAvailable())
        avcodec_flush_buffers(d_func().codec_ctx);
}

void AVDecoder::setCodecContext(AVCodecContext *codecCtx)
{
    DPTR_D(AVDecoder);
    d.codec_ctx = codecCtx;
}

AVCodecContext* AVDecoder::codecContext() const
{
    return d_func().codec_ctx;
}

bool AVDecoder::isAvailable() const
{
    return d_func().codec_ctx != 0;
}

bool AVDecoder::prepare()
{
    return true;
}

bool AVDecoder::decode(const QByteArray &encoded)
{
    Q_UNUSED(encoded);
    return true;
}

QByteArray AVDecoder::data() const
{
    return d_func().decoded;
}

} //namespace XMedia
