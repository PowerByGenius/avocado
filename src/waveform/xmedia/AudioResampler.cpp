#include <XMedia/AudioResampler.h>
#include <XMedia/AudioFormat.h>
#include <XMedia/private/AudioResampler_p.h>
#include <XMedia/factory.h>

namespace XMedia {

FACTORY_DEFINE(AudioResampler)

extern void RegisterAudioResamplerFF_Man();
extern void RegisterAudioResamplerLibav_Man();

void AudioResampler_RegisterAll()
{
#if XMEDIA_HAVE(SWRESAMPLE)
    RegisterAudioResamplerFF_Man();
#endif //XMEDIA_HAVE(SWRESAMPLE)
#if XMEDIA_HAVE(AVRESAMPLE)
    RegisterAudioResamplerLibav_Man();
#endif //XMEDIA_HAVE(AVRESAMPLE)
}

AudioResampler::AudioResampler()
{
}

AudioResampler::AudioResampler(AudioResamplerPrivate& d):DPTR_INIT(&d)
{
}

AudioResampler::~AudioResampler()
{
}

QByteArray AudioResampler::outData() const
{
    return d_func().data_out;
}

bool AudioResampler::prepare()
{
    if (!inAudioFormat().isValid()) {
        qWarning("src audio parameters 'channel layout(or channels), sample rate and sample format must be set before initialize resampler");
        return false;
    }
    return true;
}

bool AudioResampler::convert(const quint8 **data)
{
    Q_UNUSED(data);
    return false;
}

void AudioResampler::setSpeed(qreal speed)
{
    d_func().speed = speed;
}

qreal AudioResampler::speed() const
{
    return d_func().speed;
}


void AudioResampler::setInAudioFormat(const AudioFormat& format)
{
    d_func().in_format = format;
}

AudioFormat& AudioResampler::inAudioFormat()
{
    return d_func().in_format;
}

const AudioFormat& AudioResampler::inAudioFormat() const
{
    return d_func().in_format;
}

void AudioResampler::setOutAudioFormat(const AudioFormat& format)
{
    d_func().out_format = format;
}

AudioFormat& AudioResampler::outAudioFormat()
{
    return d_func().out_format;
}

const AudioFormat& AudioResampler::outAudioFormat() const
{
    return d_func().out_format;
}

void AudioResampler::setInSampesPerChannel(int samples)
{
    d_func().in_samples_per_channel = samples;
}

//channel count can be computed by av_get_channel_layout_nb_channels(chl)
void AudioResampler::setInSampleRate(int isr)
{
    d_func().in_format.setSampleRate(isr);
}

void AudioResampler::setOutSampleRate(int osr)
{
    d_func().out_format.setSampleRate(osr);
}

void AudioResampler::setInSampleFormat(int isf)
{
    d_func().in_format.setSampleFormatFFmpeg(isf);
}

void AudioResampler::setOutSampleFormat(int osf)
{
    d_func().out_format.setSampleFormatFFmpeg(osf);
}

void AudioResampler::setInChannelLayout(qint64 icl)
{
    d_func().in_format.setChannelLayoutFFmpeg(icl);
}

void AudioResampler::setOutChannelLayout(qint64 ocl)
{
    d_func().out_format.setChannelLayoutFFmpeg(ocl);
}

void AudioResampler::setInChannels(int channels)
{
    d_func().in_format.setChannels(channels);
}

void AudioResampler::setOutChannels(int channels)
{
    d_func().out_format.setChannels(channels);
}

} //namespace XMedia
