#include <cstdio>
#include <cstdlib>
#include <XMedia/VideoRendererTypes.h>
#include <XMedia/prepost.h>
#include <XMedia/WidgetRenderer.h>
#include <XMedia/GraphicsItemRenderer.h>
#if XMEDIA_HAVE(GL)
#include <XMedia/GLWidgetRenderer.h>
#endif //XMEDIA_HAVE(GL)
#if XMEDIA_HAVE(GDIPLUS)
#include <XMedia/GDIRenderer.h>
#endif //XMEDIA_HAVE(GDIPLUS)
#if XMEDIA_HAVE(DIRECT2D)
#include <XMedia/Direct2DRenderer.h>
#endif //XMEDIA_HAVE(DIRECT2D)
#if XMEDIA_HAVE(XV)
#include <XMedia/XVRenderer.h>
#endif //XMEDIA_HAVE(XV)
#include <XMedia/factory.h>

namespace XMedia {

FACTORY_DEFINE(VideoRenderer)

VideoRendererId VideoRendererId_QPainter = 0;
VideoRendererId VideoRendererId_Widget = 1;
VideoRendererId VideoRendererId_GraphicsItem = 2;
VideoRendererId VideoRendererId_GLWidget = 3;
VideoRendererId VideoRendererId_GDI = 4;
VideoRendererId VideoRendererId_Direct2D = 5;
VideoRendererId VideoRendererId_XV = 6;

//QPainterRenderer is abstract. So can not register(operator new will needed)
FACTORY_REGISTER_ID_AUTO(VideoRenderer, Widget, "QWidegt")

void RegisterVideoRendererWidget_Man()
{
    FACTORY_REGISTER_ID_MAN(VideoRenderer, Widget, "QWidegt")
}

FACTORY_REGISTER_ID_AUTO(VideoRenderer, GraphicsItem, "QGraphicsItem")

void RegisterVideoRendererGraphicsItem_Man()
{
    FACTORY_REGISTER_ID_MAN(VideoRenderer, GraphicsItem, "QGraphicsItem")
}

VideoRendererId WidgetRenderer::id() const
{
    return VideoRendererId_Widget;
}

VideoRendererId GraphicsItemRenderer::id() const
{
    return VideoRendererId_GraphicsItem;
}

VideoRendererId QPainterRenderer::id() const
{
    return VideoRendererId_QPainter;
}

#if XMEDIA_HAVE(GL)
FACTORY_REGISTER_ID_AUTO(VideoRenderer, GLWidget, "QGLWidegt")

void RegisterVideoRendererGLWidget_Man()
{
    FACTORY_REGISTER_ID_MAN(VideoRenderer, GLWidget, "QGLWidegt")
}

VideoRendererId GLWidgetRenderer::id() const
{
    return VideoRendererId_GLWidget;
}
#endif //XMEDIA_HAVE(GL)
#if XMEDIA_HAVE(GDIPLUS)
FACTORY_REGISTER_ID_AUTO(VideoRenderer, GDI, "GDI")

void RegisterVideoRendererGDI_Man()
{
    FACTORY_REGISTER_ID_MAN(VideoRenderer, GDI, "GDI")
}

VideoRendererId GDIRenderer::id() const
{
    return VideoRendererId_GDI;
}
#endif //XMEDIA_HAVE(GDIPLUS)
#if XMEDIA_HAVE(DIRECT2D)
FACTORY_REGISTER_ID_AUTO(VideoRenderer, Direct2D, "Direct2D")

void RegisterVideoRendererDirect2D_Man()
{
    FACTORY_REGISTER_ID_MAN(VideoRenderer, Direct2D, "Direct2D")
}

VideoRendererId Direct2DRenderer::id() const
{
    return VideoRendererId_Direct2D;
}
#endif //XMEDIA_HAVE(DIRECT2D)
#if XMEDIA_HAVE(XV)
FACTORY_REGISTER_ID_AUTO(VideoRenderer, XV, "XVideo")

void RegisterVideoRendererXV_Man()
{
    FACTORY_REGISTER_ID_MAN(VideoRenderer, XV, "XVideo")
}

VideoRendererId XVRenderer::id() const
{
    return VideoRendererId_XV;
}
#endif //XMEDIA_HAVE(XV)

void VideoRenderer_RegisterAll()
{
    RegisterVideoRendererWidget_Man();
#if XMEDIA_HAVE(GL)
    RegisterVideoRendererGLWidget_Man();
#endif //XMEDIA_HAVE(GL)
#if XMEDIA_HAVE(GDIPLUS)
    RegisterVideoRendererGDI_Man();
#endif //XMEDIA_HAVE(GDIPLUS)
#if XMEDIA_HAVE(DIRECT2D)
    RegisterVideoRendererDirect2D_Man();
#endif //XMEDIA_HAVE(DIRECT2D)
#if XMEDIA_HAVE(XV)
    RegisterVideoRendererXV_Man();
#endif //XMEDIA_HAVE(XV)
}

#if ID_STATIC
namespace {
static void FixUnusedCompileWarning()
{
    FixUnusedCompileWarning(); //avoid warning about this function may not be used
    Q_UNUSED(VideoRendererId_GDI);
    Q_UNUSED(VideoRendererId_GLWidget);
    Q_UNUSED(VideoRendererId_Direct2D);
    Q_UNUSED(VideoRendererId_XV);
}
}//namespace
#endif //ID_STATIC
}//namespace XMedia
