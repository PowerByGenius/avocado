#include <XMedia/Statistics.h>

namespace XMedia {

Statistics::Common::Common():
    available(false)
  , fps_guess(0)
  , fps(0)
  , bit_rate(0)
  , avg_frame_rate(0)
  , frames(0)
  , size(0)
{
}

Statistics::AudioOnly::AudioOnly():
    sample_rate(0)
  , channels(0)
  , frame_size(0)
  , frame_number(0)
  , block_align(0)
{
}

Statistics::VideoOnly::VideoOnly():
    width(0)
  , height(0)
  , coded_width(0)
  , coded_height(0)
  , gop_size(0)
{
}

Statistics::Statistics()
{
}

Statistics::~Statistics()
{
}

void Statistics::reset()
{
    url = "";
    audio = Common();
    video = Common();
    audio_only = AudioOnly();
    video_only = VideoOnly();
}

} //namespace XMedia
