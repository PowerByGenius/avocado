#include <QWidget>
#include <XMedia/AVPlayer.h>
#include <XMedia/OutputSet.h>

namespace XMedia {

OutputSet::OutputSet(AVPlayer *player):
    QObject(player)
  , mCanPauseThread(false)
  , mpPlayer(player)
  , mPauseCount(0)
{
}

OutputSet::~OutputSet()
{
    mCond.wakeAll();
    //delete? may be deleted by vo's parent
    clearOutputs();
}

void OutputSet::lock()
{
    mMutex.lock();
}

void OutputSet::unlock()
{
    mMutex.unlock();
}

QList<AVOutput *> OutputSet::outputs()
{
    return mOutputs;
}

void OutputSet::sendData(const QByteArray &data)
{
    QMutexLocker lock(&mMutex);
    Q_UNUSED(lock);
    foreach(AVOutput *output, mOutputs) {
        if (!output->isAvailable())
            continue;
        output->writeData(data);
    }
}

void OutputSet::clearOutputs()
{
    QMutexLocker lock(&mMutex);
    Q_UNUSED(lock);
    foreach(AVOutput *output, mOutputs) {
        output->removeOutputSet(this);
    }
    mOutputs.clear();
}

void OutputSet::addOutput(AVOutput *output)
{
    QMutexLocker lock(&mMutex);
    Q_UNUSED(lock);
    mOutputs.append(output);
    output->addOutputSet(this);
    emit updateParametersRequired(output);
}

void OutputSet::removeOutput(AVOutput *output)
{
    QMutexLocker lock(&mMutex);
    Q_UNUSED(lock);
    mOutputs.removeAll(output);
    output->removeOutputSet(this);
}

void OutputSet::notifyPauseChange(AVOutput *output)
{
    if (output->isPaused()) {
        mPauseCount++;
        if (mPauseCount == mOutputs.size()) {
            mCanPauseThread = true;
        }
        //DO NOT pause here because it must be paused in AVThread
    } else {
        mPauseCount--;
        mCanPauseThread = false;
        if (mPauseCount == mOutputs.size() - 1) {
            resumeThread();
        }
    }
}

bool OutputSet::canPauseThread() const
{
    return mCanPauseThread;
}

void OutputSet::pauseThread()
{
    QMutexLocker lock(&mMutex);
    Q_UNUSED(lock);
    mCond.wait(&mMutex);
}

void OutputSet::resumeThread()
{
    mCond.wakeAll();
}

} //namespace XMedia
