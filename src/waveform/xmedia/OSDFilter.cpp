#include <XMedia/OSDFilter.h>
#include <XMedia/Statistics.h>
#include <XMedia/private/Filter_p.h>
#include <QtGui/QPainter>
#if XMEDIA_HAVE(GL)
#include <QtOpenGL/QGLWidget>
#endif //XMEDIA_HAVE(GL)


namespace XMedia {

class OSDFilterPrivate : public FilterPrivate
{
public:
};

OSDFilter::OSDFilter(OSDFilterPrivate &d):
    Filter(d)
{
}

OSDFilterQPainter::OSDFilterQPainter():
    OSDFilter(*new OSDFilterPrivate())
{
}

void OSDFilterQPainter::process()
{
    if (mShowType == ShowNone)
        return;
    DPTR_D(Filter);
    QPainterFilterContext* ctx = static_cast<QPainterFilterContext*>(d.context);

    //qDebug() << ctx->rect << ctx->video_height << ctx->video_width;
    //qDebug("ctx=%p tid=%p main tid=%p", ctx, QThread::currentThread(), qApp->thread());

//    QRect rect;
//    rect.setLeft(ctx->rect.x());
//    rect.setTop(ctx->rect.y());
//    rect.setWidth(ctx->video_width);
//    rect.setHeight(ctx->video_height);
    QRect rect  = ctx->rect;
    rect.adjust(0,20,-20,0);
    ctx->drawPlainText(rect, Qt::AlignTop|Qt::AlignRight, text(d.statistics));
}

OSDFilterGL::OSDFilterGL():
    OSDFilter(*new OSDFilterPrivate())
{
}

void OSDFilterGL::process()
{
    if (mShowType == ShowNone)
        return;
    DPTR_D(Filter);
    GLFilterContext *ctx = static_cast<GLFilterContext*>(d.context);
    //TODO: render off screen
#if XMEDIA_HAVE(GL)
    QGLWidget *glw = static_cast<QGLWidget*>(ctx->paint_device);
    if (!glw)
        return;
    glw->renderText(ctx->rect.x(), ctx->rect.y(), text(d.statistics), ctx->font);
#endif //XMEDIA_HAVE(GL)
}
} //namespace XMedia
