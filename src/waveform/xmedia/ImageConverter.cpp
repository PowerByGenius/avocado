#include <XMedia/ImageConverter.h>
#include <XMedia/private/ImageConverter_p.h>
#include <XMedia/XMedia_Compat.h>
#include <XMedia/factory.h>

namespace XMedia {

FACTORY_DEFINE(ImageConverter)

extern void RegisterImageConverterFF_Man();
extern void RegisterImageConverterIPP_Man();

void ImageConverter_RegisterAll()
{
    RegisterImageConverterFF_Man();
    RegisterImageConverterIPP_Man();
}


ImageConverter::ImageConverter()
{
}

ImageConverter::ImageConverter(ImageConverterPrivate& d):DPTR_INIT(&d)
{
}

ImageConverter::~ImageConverter()
{
}

QByteArray ImageConverter::outData() const
{
    return d_func().data_out;
}

void ImageConverter::setInSize(int width, int height)
{
    DPTR_D(ImageConverter);
    if (d.w_in == width && d.h_in == height)
        return;
    d.w_in = width;
    d.h_in = height;
    prepareData();
}

void ImageConverter::setOutSize(int width, int height)
{
    DPTR_D(ImageConverter);
    if (d.w_out == width && d.h_out == height)
        return;
    d.w_out = width;
    d.h_out = height;
    prepareData();
}

void ImageConverter::setInFormat(int format)
{
    d_func().fmt_in = format;
}

void ImageConverter::setOutFormat(int format)
{
    DPTR_D(ImageConverter);
    if (d.fmt_out == format)
        return;
    d.fmt_out = format;
    prepareData();
}

void ImageConverter::setInterlaced(bool interlaced)
{
    d_func().interlaced = interlaced;
}

bool ImageConverter::isInterlaced() const
{
    return d_func().interlaced;
}

bool ImageConverter::prepareData()
{
    return false;
}

} //namespace XMedia
