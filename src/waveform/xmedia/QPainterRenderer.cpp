#include <XMedia/QPainterRenderer.h>
#include <XMedia/private/QPainterRenderer_p.h>
#include <XMedia/FilterContext.h>
#include <XMedia/OSDFilter.h>

namespace XMedia {

QPainterRenderer::QPainterRenderer()
    :VideoRenderer(*new QPainterRendererPrivate())
{
    DPTR_D(QPainterRenderer);
    d.filter_context = FilterContext::create(FilterContext::QtPainter);
    setOSDFilter(new OSDFilterQPainter());
}

QPainterRenderer::QPainterRenderer(QPainterRendererPrivate &d)
    :VideoRenderer(d)
{
    d.filter_context = FilterContext::create(FilterContext::QtPainter);
    setOSDFilter(new OSDFilterQPainter());
}

QPainterRenderer::~QPainterRenderer()
{
}

int QPainterRenderer::filterContextType() const
{
    return FilterContext::QtPainter;
}

/*
QImage QPainterRenderer::currentFrameImage() const
{
    return d_func().image;
}
*/
//FIXME: why crash if QImage use widget size?
void QPainterRenderer::convertData(const QByteArray &data)
{
    DPTR_D(QPainterRenderer);
    //int ss = 4*d.src_width*d.src_height*sizeof(char);
    //if (ss != data.size())
    //    qDebug("src size=%d, data size=%d", ss, data.size());
    //if (d.src_width != d.renderer_width || d.src_height != d.renderer_height)
    //    return;
    /*
     * QImage constructed from memory do not deep copy the data, data should be available throughout
     * image's lifetime and not be modified. painting image happens in main thread, we must ensure the
     * image data is not changed if not use the original frame size, so we need the lock.
     * In addition, the data passed by ref, so even if we lock, the original data may changed in decoder
     * (ImageConverter), so we need a copy of this data(not deep copy) to ensure the image's data not changes before it
     * is painted.
     * But if we use the fixed original frame size, the data address and size always the same, so we can
     * avoid the lock and use the ref data directly and safely
     */
    //if (!d.scale_in_renderer) {
        /*if lock is required, do not use locker in if() scope, it will unlock outside the scope*/
    QMutexLocker locker(&d.img_mutex);
    Q_UNUSED(locker);
        d.data = data; //TODO: why need this line? Then use data.data() is OK? If use d.data.data() it will eat more cpu, why?
        //qDebug("data address = %p, %p", data.data(), d.data.data());
    #if QT_VERSION >= QT_VERSION_CHECK(4, 0, 0)
        d.image = QImage((uchar*)data.data(), d.src_width, d.src_height, QImage::Format_RGB32);
    #else
        d.image = QImage((uchar*)data.data(), d.src_width, d.src_height, 16, NULL, 0, QImage::IgnoreEndian);
    #endif
    //} else {
        //qDebug("data address = %p", data.data());
        //Format_RGB32 is fast. see document
#if QT_VERSION >= QT_VERSION_CHECK(4, 0, 0)
        //d.image = QImage((uchar*)data.data(), d.src_width, d.src_height, QImage::Format_RGB32);
#else
    d.image = QImage((uchar*)data.data(), d.src_width, d.src_height, 16, NULL, 0, QImage::IgnoreEndian);
#endif
    //}
}

} //namespace XMedia
