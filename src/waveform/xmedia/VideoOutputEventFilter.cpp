#include <XMedia/VideoOutputEventFilter.h>
#include <XMedia/VideoRenderer.h>
#include <QtCore/QEvent>
#include <QKeyEvent>
#include <QWidget>

namespace XMedia {

VideoOutputEventFilter::VideoOutputEventFilter(VideoRenderer *renderer):
    QObject(renderer->widget())
  , mRendererIsQObj(!!renderer->widget())
  , mpRenderer(renderer)
{
    if (renderer->widget()) {
        connect(renderer->widget(), SIGNAL(destroyed()), SLOT(stopFiltering()), Qt::DirectConnection);
    }
}

bool VideoOutputEventFilter::eventFilter(QObject *watched, QEvent *event)
{
    // a widget shown means it's available and not deleted
    if (event->type() == QEvent::Show || event->type() == QEvent::ShowToParent) {
        mRendererIsQObj = !!mpRenderer->widget();
        return false;
    }
    if (!mRendererIsQObj)
        return false;
    /*
     * deleted renderer (after destroyed()) can not access it's member, so we must mark it as invalid.
     * hide event is sent when close. what about QEvent::Close?
     */
    if (event->type() == QEvent::Close || event->type() == QEvent::Hide) {
        mRendererIsQObj = false;
        return true;
    }
    if (!mpRenderer)
        return false;
    if (watched != mpRenderer->widget()
            /* && watched != mpRenderer->graphicsWidget()*/ //no showFullScreen() etc.
            )
        return false;
    switch (event->type()) {
    case QEvent::KeyPress: {
        QKeyEvent *key_event = static_cast<QKeyEvent*>(event);
        int key = key_event->key();
//        Qt::KeyboardModifiers modifiers = key_event->modifiers();
        switch (key) {
        case Qt::Key_F: { //TODO: move to gui
            //TODO: active window
            QWidget *w = mpRenderer->widget();
            if (!w)
                return false;
            if (w->isFullScreen())
                w->showNormal();
            else
                w->showFullScreen();
        }
            break;
        }
    }
        break;
    default:
        break;
    }
    return false;
}

void VideoOutputEventFilter::stopFiltering()
{
    mpRenderer = 0;
}

} //namespace XMedia
