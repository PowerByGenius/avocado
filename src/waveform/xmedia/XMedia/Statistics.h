#ifndef XMEDIA_STATISTICS_H
#define XMEDIA_STATISTICS_H

#include <XMedia/XMedia_Global.h>
#include <QtCore/QTime>

/*
 * time unit is s
 * TODO: frame counter, frame droped. see VLC
 */

namespace XMedia {

class StatisticsPrivate;
class Q_EXPORT Statistics
{
public:
    Statistics();
    ~Statistics();
    void reset();

    QString url;
    int bit_rate;
    QString format;
    QTime start_time, duration;
    //TODO: filter, decoder, resampler info etc.
    class Common {
    public:
        Common();
        bool available;
        QString codec, codec_long;
        //common audio/video info that may be used(visualize) by filters
        QTime current_time, total_time, start_time; //TODO: in AVFormatContext and AVStream, what's the difference?
        //AVStream.avg_frame_rate may be 0, then use AVStream.r_frame_rate
        //http://libav-users.943685.n4.nabble.com/Libav-user-Reading-correct-frame-rate-fps-of-input-video-td4657666.html
        qreal fps_guess;
        qreal fps; //playing fps
        qreal bit_rate;
        qreal avg_frame_rate; //AVStream.avg_frame_rate Kps
        qint64 frames; //AVStream.nb_frames. AVCodecContext.frame_number?
        qint64 size; //audio/video stream size. AVCodecContext.frame_size?

        //union member with ctor, dtor, copy ctor only works in c++11
        /*union {
            audio_only audio;
            video_only video;
        } only;*/
    } audio, video; //init them

    //from AVCodecContext
    class AudioOnly {
    public:
        AudioOnly();
        int sample_rate; ///< samples per second
        int channels;    ///< number of audio channels
        QString channel_layout;
        QString sample_fmt;  ///< sample format
        /**
         * Number of samples per channel in an audio frame.
         * - decoding: may be set by some decoders to indicate constant frame size
         */
        int frame_size;
        /**
         * Frame counter, set by libavcodec.
         *   @note the counter is not incremented if encoding/decoding resulted in an error.
         */
        int frame_number;
        /**
         * number of bytes per packet if constant and known or 0
         * Used by some WAV based audio codecs.
         */
        int block_align;
        //int cutoff; //Audio cutoff bandwidth (0 means "automatic")
    } audio_only;
    //from AVCodecContext
    class VideoOnly {
    public:
        //union member with ctor, dtor, copy ctor only works in c++11
        VideoOnly();
        int width, height;
        /**
         * Bitstream width / height, may be different from width/height if lowres enabled.
         * - encoding: unused
         * - decoding: Set by user before init if known. Codec should override / dynamically change if needed.
         */
        int coded_width, coded_height;
        /**
         * the number of pictures in a group of pictures, or 0 for intra_only
         */
        int gop_size;
        QString pix_fmt; //TODO: new enum in QtAV
        /**
         * Motion estimation algorithm used for video coding.
         * 1 (zero), 2 (full), 3 (log), 4 (phods), 5 (epzs), 6 (x1), 7 (hex),
         * 8 (umh), 9 (iter), 10 (tesa) [7, 8, 10 are x264 specific, 9 is snow specific]
         */
        //int me_method;
    } video_only;
};

} //namespace XMedia

#endif // XMEDIA_STATISTICS_H
