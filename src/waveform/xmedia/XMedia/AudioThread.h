#ifndef QAV_AUDIOTHREAD_H
#define QAV_AUDIOTHREAD_H

#include <XMedia/AVThread.h>

namespace XMedia {

class AudioDecoder;
class AudioThreadPrivate;
class Q_EXPORT AudioThread : public AVThread
{
    Q_OBJECT
    DPTR_DECLARE_PRIVATE(AudioThread)
public:
    explicit AudioThread(QObject *parent = 0);

protected:
    virtual void run();
};

} //namespace XMedia
#endif // QAV_AUDIOTHREAD_H
