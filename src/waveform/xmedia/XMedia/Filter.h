#ifndef XMEDIA_FILTER_H
#define XMEDIA_FILTER_H

#include <XMedia/XMedia_Global.h>
#include <XMedia/FilterContext.h>
/*
 * QPainterFilter, D2DFilter, ...
 *
 *TODO: force apply. e.g. an animation filter on vo, update vo and apply filter even not video is
 * playing.
 */

class QByteArray;
namespace XMedia {

class FilterPrivate;
class Statistics;
class Q_EXPORT Filter
{
    DPTR_DECLARE_PRIVATE(Filter)
public:
    virtual ~Filter();
    //isEnabled() then setContext
    //TODO: parameter FrameContext
    void setEnabled(bool enabled); //AVComponent.enabled
    bool isEnabled() const;

    virtual FilterContext::Type contextType() const = 0;

    /*!
     * check context and apply the filter
     * if context is null, or contextType() != context->type(), then create a right one and assign it to context.
     */
    void process(FilterContext *&context, Statistics* statistics, QByteArray* data = 0);

protected:
    /*
     * If the filter is in AVThread, it's safe to operate on ref.
     */
    Filter(FilterPrivate& d);
    virtual void process() = 0;

    DPTR_DECLARE(Filter)
};

} //namespace XMedia

#endif // XMEDIA_FILTER_H
