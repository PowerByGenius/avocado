#ifndef QAV_AUDIODECODER_H
#define QAV_AUDIODECODER_H

#include <XMedia/AVDecoder.h>

//TODO: decoder.in/outAudioFormat()?
namespace XMedia {

class AudioResampler;
class AudioDecoderPrivate;
class Q_EXPORT AudioDecoder : public AVDecoder
{
    DPTR_DECLARE_PRIVATE(AudioDecoder)
public:
    AudioDecoder();
    virtual bool prepare();
    virtual bool decode(const QByteArray &encoded);
    AudioResampler *resampler();
    int undecodedSize() const;
};

} //namespace XMedia
#endif // QAV_AUDIODECODER_H
