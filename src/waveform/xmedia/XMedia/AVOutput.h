#ifndef QAV_WRITER_H
#define QAV_WRITER_H

#include <QtCore/QByteArray>
#include <XMedia/XMedia_Global.h>

/*!
 * TODO: add api id(), name(), detail()
 */

namespace XMedia {

class AVDecoder;
class AVOutputPrivate;
class Filter;
class FilterContext;
class Statistics;
class OutputSet;
class Q_EXPORT AVOutput
{
    DPTR_DECLARE_PRIVATE(AVOutput)
public:
    AVOutput();
    virtual ~AVOutput();
    /* store the data ref, then call convertData() and write(). tryPause() will be called*/
    bool writeData(const QByteArray& data);
    bool isAvailable() const;

    //void addSource(AVPlayer* player); //call player.addVideoRenderer(this)
    //void removeSource(AVPlayer* player);

    Q_DECL_DEPRECATED virtual bool open() = 0;
    Q_DECL_DEPRECATED virtual bool close() = 0;

//    virtual bool prepare() {}
//    virtual bool finish() {}
    //Demuxer thread automatically paused because packets will be full
    //only pause the renderering, the thread going on. If all outputs are paused, then pause the thread(OutputSet.tryPause)
    void pause(bool p); //processEvents when waiting?
    bool isPaused() const;

    /* check context.type, if not compatible(e.g. type is QtPainter but vo is d2d)
     * but type is not same is also ok if we just use render engine in vo but not in context.
     * TODO:
     * what if multiple vo(different render engines) share 1 player?
     * private?: set in AVThread, context is used by this class internally
     */
    virtual int filterContextType() const;
    //No filters() api, they are used internally?
    //for add/remove/clear on list. avo.add/remove/clear?
    QList<Filter*>& filters();
protected:
    AVOutput(AVOutputPrivate& d);
    /*
     * Reimplement this. You should convert and save the decoded data, e.g. QImage,
     * which will be used in write() or some other functions. Do nothing by default.
     */
    virtual void convertData(const QByteArray& data);
    virtual bool write() = 0; //TODO: why pure may case "pure virtual method called"
    /*
     * If the pause state is true setted by pause(true), then block the thread and wait for pause state changed, i.e. pause(false)
     * and return true. Otherwise, return false immediatly.
     */
    Q_DECL_DEPRECATED bool tryPause(); //move to OutputSet
    //TODO: we need an active set
    void addOutputSet(OutputSet *set);
    void removeOutputSet(OutputSet *set);
    void attach(OutputSet *set); //add this to set
    void detach(OutputSet *set = 0); //detatch from (all, if 0) output set(s)

    DPTR_DECLARE(AVOutput)

private:
    void setStatistics(Statistics* statistics);
    friend class AVPlayer;
    friend class OutputSet;
};

} //namespace XMedia
#endif //QAV_WRITER_H
