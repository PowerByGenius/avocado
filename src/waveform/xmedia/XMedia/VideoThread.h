#ifndef XMEDIA_VIDEOTHREAD_H
#define XMEDIA_VIDEOTHREAD_H

#include <XMedia/AVThread.h>
#include <QtCore/QSize>

namespace XMedia {

class ImageConverter;
class OSDFilter;
class VideoCapture;
class VideoThreadPrivate;
class VideoThread : public AVThread
{
    Q_OBJECT
    DPTR_DECLARE_PRIVATE(VideoThread)
public:
    explicit VideoThread(QObject *parent = 0);
    //return the old
    ImageConverter* setImageConverter(ImageConverter *converter);
    ImageConverter* imageConverter() const;
    double currentPts() const;
    VideoCapture *setVideoCapture(VideoCapture* cap); //ensure thread safe
protected:
    virtual void run();
};

} //namespace XMedia
#endif // XMEDIA_VIDEOTHREAD_H
