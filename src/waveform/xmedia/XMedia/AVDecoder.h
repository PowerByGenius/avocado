#ifndef QAV_DECODER_H
#define QAV_DECODER_H

#include <XMedia/XMedia_Global.h>

class QByteArray;
struct AVCodecContext;
struct AVFrame;

namespace XMedia {

class AVDecoderPrivate;
class Q_EXPORT AVDecoder
{
    DPTR_DECLARE_PRIVATE(AVDecoder)
public:
    AVDecoder();
    virtual ~AVDecoder();
    void flush();
    void setCodecContext(AVCodecContext* codecCtx); //protected
    AVCodecContext* codecContext() const;
    /*not available if AVCodecContext == 0*/
    bool isAvailable() const;
    virtual bool prepare(); //if resampler or image converter set, call it
    virtual bool decode(const QByteArray& encoded) = 0; //decode AVPacket?
    QByteArray data() const; //decoded data

protected:
    AVDecoder(AVDecoderPrivate& d);

    DPTR_DECLARE(AVDecoder)
};

} //namespace XMedia
#endif // QAV_DECODER_H
