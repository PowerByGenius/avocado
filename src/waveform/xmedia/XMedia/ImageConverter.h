#ifndef XMEDIA_IMAGECONVERTER_H
#define XMEDIA_IMAGECONVERTER_H

#include <XMedia/XMedia_Global.h>
#include <XMedia/FactoryDefine.h>

namespace XMedia {

typedef int ImageConverterId;
class ImageConverter;
FACTORY_DECLARE(ImageConverter)

class ImageConverterPrivate;
class Q_EXPORT ImageConverter //export is not needed
{
    DPTR_DECLARE_PRIVATE(ImageConverter)
public:
    ImageConverter();
    virtual ~ImageConverter();

    QByteArray outData() const;
    void setInSize(int width, int height);
    void setOutSize(int width, int height);
    //TODO: new enum. Now using FFmpeg's enum
    void setInFormat(int format);
    void setOutFormat(int format);
    void setInterlaced(bool interlaced);
    bool isInterlaced() const;
    virtual bool convert(const quint8 *const srcSlice[], const int srcStride[]) = 0;
    //virtual bool convertColor(const quint8 *const srcSlice[], const int srcStride[]) = 0;
    //virtual bool resize(const quint8 *const srcSlice[], const int srcStride[]) = 0;
protected:
    ImageConverter(ImageConverterPrivate& d);
    //Allocate memory for out data. Called in setOutFormat()
    virtual bool prepareData();
    DPTR_DECLARE(ImageConverter)
};

} //namespace XMedia
#endif // XMEDIA_IMAGECONVERTER_H
