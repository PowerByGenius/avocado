#ifndef XMEDIA_H
#define XMEDIA_H

#include <XMedia/XMedia_Global.h>
#include <XMedia/version.h>

#include <XMedia/AVClock.h>
#include <XMedia/AVDecoder.h>
#include <XMedia/AVDemuxer.h>
#include <XMedia/AVOutput.h>
#include <XMedia/AVPlayer.h>

#include <XMedia/AudioDecoder.h>
#include <XMedia/AudioFormat.h>
#include <XMedia/AudioOutput.h>
#include <XMedia/AudioResampler.h>
#include <XMedia/AudioResamplerTypes.h>

#include <XMedia/Filter.h>
#include <XMedia/FilterContext.h>
#include <XMedia/OSD.h>
#include <XMedia/OSDFilter.h>

#include <XMedia/ImageConverter.h>
#include <XMedia/ImageConverterTypes.h>

#include <XMedia/VideoCapture.h>
#include <XMedia/VideoDecoder.h>

#include <XMedia/VideoRenderer.h>
#include <XMedia/VideoRendererTypes.h>
//The following renderer headers can be removed
#include <XMedia/Direct2DRenderer.h>
#include <XMedia/GDIRenderer.h>
#include <XMedia/GLWidgetRenderer.h>
#include <XMedia/QPainterRenderer.h>
#include <XMedia/GraphicsItemRenderer.h>
#include <XMedia/WidgetRenderer.h>
#include <XMedia/XVRenderer.h>

#endif // XMEDIA_H
