#ifndef XMEDIA_VIDEODECODER_H
#define XMEDIA_VIDEODECODER_H

#include <XMedia/AVDecoder.h>

class QSize;
struct SwsContext;
namespace XMedia {
class VideoDecoderPrivate;
class Q_EXPORT VideoDecoder : public AVDecoder
{
    DPTR_DECLARE_PRIVATE(VideoDecoder)
public:
    VideoDecoder();
    //virtual bool prepare();
    virtual bool decode(const QByteArray &encoded);
    //TODO: new api: originalVideoSize()(inSize()), decodedVideoSize()(outSize())
    //size: the decoded(actually then resized in ImageConverter) frame size
    void resizeVideoFrame(const QSize& size);
    void resizeVideoFrame(int width, int height);
    //TODO: decodedSize()
    int width() const;
    int height() const;
};

} //namespace XMedia
#endif // XMEDIA_VIDEODECODER_H
