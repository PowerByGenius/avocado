//#ifndef XMEDIA_EVENTFILTER_H
//#define XMEDIA_EVENTFILTER_H

///*
// * This class is used interally as QtAV's default event filter. It is suite for single player object
// */
//#include <QtCore/QObject>
//#include <QtCore/QPoint>

//class QMenu;
//class QPoint;
//namespace XMedia {

////for internal use
//class EventFilter : public QObject
//{
//    Q_OBJECT
//public:
//    explicit EventFilter(QObject *parent = 0);
//    virtual ~EventFilter();

//public slots:
//    void openLocalFile();
//    void openUrl();
//    void about();
//    void aboutFFmpeg();
//    void help();

//protected:
//    virtual bool eventFilter(QObject *, QEvent *);
//    void showMenu(const QPoint& p);

//private:
//    QMenu *menu;
//    bool isWindowMovable;
//    QPoint gMousePos, iMousePos;
//};

//} //namespace XMedia
//#endif // XMEDIA_EVENTFILTER_H
