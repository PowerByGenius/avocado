#ifndef XMEDIA_AVDECODER_P_H
#define XMEDIA_AVDECODER_P_H

#include <QtCore/QByteArray>
#include <QtCore/QMutex>
#include <XMedia/XMedia_Compat.h>

namespace XMedia {

class Q_EXPORT AVDecoderPrivate : public DPtrPrivate<AVDecoder>
{
public:
    AVDecoderPrivate():
        codec_ctx(0)
      , frame(0)
      , got_frame_ptr(0)
    {
        frame = avcodec_alloc_frame();
    }
    virtual ~AVDecoderPrivate() {
        if (frame) {
            av_free(frame);
            frame = 0;
        }
    }

    AVCodecContext *codec_ctx; //set once and not change
    AVFrame *frame; //set once and not change
    QByteArray decoded;
    int got_frame_ptr;
    QMutex mutex;
};

} //namespace XMedia
#endif // XMEDIA_AVDECODER_P_H
