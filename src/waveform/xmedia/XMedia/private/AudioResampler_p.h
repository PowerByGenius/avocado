#ifndef XMEDIA_AUDIORESAMPLER_P_H
#define XMEDIA_AUDIORESAMPLER_P_H

#include <XMedia/AudioFormat.h>
#include <XMedia/XMedia_Compat.h>
#include <QtCore/QByteArray>

namespace XMedia {

class AudioResampler;
class AudioResamplerPrivate : public DPtrPrivate<AudioResampler>
{
public:
    AudioResamplerPrivate():
        in_samples_per_channel(0)
      , out_samples_per_channel(0)
      , speed(1.0)
    {
        in_format.setSampleFormat(AudioFormat::SampleFormat_Unknown);
        out_format.setSampleFormat(AudioFormat::SampleFormat_Float);
    }

    int in_samples_per_channel, out_samples_per_channel;
    qreal speed;
    AudioFormat in_format, out_format;
    QByteArray data_out;
};

} //namespace XMedia

#endif // XMEDIA_AUDIORESAMPLER_P_H
