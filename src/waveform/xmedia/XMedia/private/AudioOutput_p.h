#ifndef XMEDIA_AUDIOOUTPUT_P_H
#define XMEDIA_AUDIOOUTPUT_P_H

#include <XMedia/private/AVOutput_p.h>
#include <XMedia/AudioFormat.h>

namespace XMedia {

class Q_EXPORT AudioOutputPrivate : public AVOutputPrivate
{
public:
    AudioOutputPrivate():
        mute(false)
      , vol(1)
      , speed(1.0)
      , max_channels(1)
    {
    }
    virtual ~AudioOutputPrivate(){}
    bool mute;
    qreal vol;
    qreal speed;
    int max_channels;
    AudioFormat format;
};

} //namespace XMedia
#endif // XMEDIA_AUDIOOUTPUT_P_H
