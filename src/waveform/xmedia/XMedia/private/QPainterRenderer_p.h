#ifndef XMEDIA_QPAINTERRENDERER_P_H
#define XMEDIA_QPAINTERRENDERER_P_H

#include <QtGui/QImage>
#include <QtGui/QPainter>
#include <XMedia/private/VideoRenderer_p.h>

namespace XMedia {

class Q_EXPORT QPainterRendererPrivate : public VideoRendererPrivate
{
public:
    QPainterRendererPrivate():
        painter(0)
    {}
    virtual ~QPainterRendererPrivate(){
        if (painter) {
            delete painter;
            painter = 0;
        }
    }
    void setupQuality() {
        switch (quality) {
        case VideoRenderer::QualityFastest:
            painter->setRenderHint(QPainter::Antialiasing, false);
            painter->setRenderHint(QPainter::TextAntialiasing, false);
            painter->setRenderHint(QPainter::SmoothPixmapTransform, false);
            painter->setRenderHint(QPainter::HighQualityAntialiasing, false);
            break;
        case VideoRenderer::QualityBest:
        default:
            painter->setRenderHint(QPainter::Antialiasing, true);
            painter->setRenderHint(QPainter::TextAntialiasing, true);
            painter->setRenderHint(QPainter::SmoothPixmapTransform, true);
            painter->setRenderHint(QPainter::HighQualityAntialiasing, true);
            break;
        }
    }

    QImage image;
    QPainter *painter;
};

} //namespace XMedia
#endif // XMEDIA_QPAINTERRENDERER_P_H
