#ifndef XMEDIA_AVOUTPUT_P_H
#define XMEDIA_AVOUTPUT_P_H

#include <QtCore/QList>
#include <QtCore/QMutex>
#include <QtCore/QWaitCondition>
#include <XMedia/XMedia_Global.h>

namespace XMedia {

class AVOutput;
class AVDecoder;
class Filter;
class FilterContext;
class Statistics;
class OutputSet;
class Q_EXPORT AVOutputPrivate : public DPtrPrivate<AVOutput>
{
public:
    AVOutputPrivate():
        paused(false)
      , available(true)
      , statistics(0)
      , filter_context(0)
    {}
    virtual ~AVOutputPrivate();

    bool paused;
    bool available;
    QMutex mutex; //pause
    QWaitCondition cond; //pause
    QByteArray data;

    //paintEvent is in main thread, copy it(only dynamic information) is better.
    //the static data are copied from AVPlayer when open
    Statistics *statistics; //do not own the ptr. just use AVPlayer's statistics ptr
    FilterContext *filter_context; //create internally by the renderer with correct type
    QList<Filter*> filters;
    QList<OutputSet*> output_sets;
};

} //namespace XMedia
#endif // XMEDIA_AVOUTPUT_P_H
