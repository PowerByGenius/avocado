#ifndef XMEDIA_GRAPHICSITEMRENDERER_P_H
#define XMEDIA_GRAPHICSITEMRENDERER_P_H

#include <XMedia/private/QPainterRenderer_p.h>

namespace XMedia {

class Q_EXPORT GraphicsItemRendererPrivate : public QPainterRendererPrivate
{
public:
    virtual ~GraphicsItemRendererPrivate(){}
};

} //namespace XMedia
#endif // XMEDIA_GRAPHICSITEMRENDERER_P_H
