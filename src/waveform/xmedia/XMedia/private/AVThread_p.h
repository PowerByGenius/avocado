#ifndef XMEDIA_AVTHREAD_P_H
#define XMEDIA_AVTHREAD_P_H

#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtCore/QWaitCondition>

#include <XMedia/Packet.h>
#include <XMedia/XMedia_Global.h>

namespace XMedia {

const double kSyncThreshold = 0.005; // 5 ms

class AVDecoder;
class AVOutput;
class AVClock;
class Filter;
class FilterContext;
class Statistics;
class OutputSet;
class Q_EXPORT AVThreadPrivate : public DPtrPrivate<AVThread>
{
public:
    AVThreadPrivate():
        paused(false)
      , next_pause(false)
      , demux_end(false)
      , stop(false)
      , clock(0)
      , dec(0)
      , outputSet(0)
      , writer(0)
      , delay(0)
      , filter_context(0)
      , statistics(0)
    {
    }
    //DO NOT delete dec and writer. We do not own them
    virtual ~AVThreadPrivate();

    bool paused, next_pause;
    bool demux_end;
    volatile bool stop; //true when packets is empty and demux is end.
    AVClock *clock;
    PacketQueue packets;
    AVDecoder *dec;
    OutputSet *outputSet;
    AVOutput *writer;
    QMutex mutex;
    QWaitCondition cond; //pause
    qreal delay;
    FilterContext *filter_context;//TODO: use own smart ptr. QSharedPointer "=" is ugly
    QList<Filter*> filters;
    Statistics *statistics; //not obj. Statistics is unique for the player, which is in AVPlayer
    QList<AVOutput*> update_outputs;
};

} //namespace XMedia
#endif // XMEDIA_AVTHREAD_P_H
