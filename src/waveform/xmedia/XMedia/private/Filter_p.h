#ifndef XMEDIA_FILTER_P_H
#define XMEDIA_FILTER_P_H

#include <XMedia/XMedia_Global.h>

namespace XMedia {

class Filter;
class FilterContext;
class Statistics;
class FilterPrivate : public DPtrPrivate<Filter>
{
public:
    FilterPrivate():
        enabled(true)
      , context(0)
      , statistics(0)
    {}
    virtual ~FilterPrivate() {}

    bool enabled;
    FilterContext *context; //used only when is necessary
    Statistics *statistics;
};

} //namespace XMedia

#endif // XMEDIA_FILTER_P_H
