#ifndef XMEDIA_WIDGETRENDERER_P_H
#define XMEDIA_WIDGETRENDERER_P_H

#include <XMedia/private/QPainterRenderer_p.h>
#include <XMedia/WidgetRenderer.h>

namespace XMedia {

class Q_EXPORT WidgetRendererPrivate : public QPainterRendererPrivate
{
public:
    virtual ~WidgetRendererPrivate(){}
};

} //namespace XMedia
#endif // XMEDIA_WIDGETRENDERER_P_H
