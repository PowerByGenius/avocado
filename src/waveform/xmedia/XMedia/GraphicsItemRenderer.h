#ifndef QAV_GRAPHICSITEMRENDERER_H
#define QAV_GRAPHICSITEMRENDERER_H

#include <XMedia/QPainterRenderer.h>
#include <QGraphicsWidget>

//QGraphicsWidget will lose focus forever if TextItem inserted text. Why?
#define CONFIG_GRAPHICSWIDGET 0
#if CONFIG_GRAPHICSWIDGET
#define GraphicsWidget QGraphicsWidget
#else
#define GraphicsWidget QGraphicsObject
#endif

namespace XMedia {

class GraphicsItemRendererPrivate;
class Q_EXPORT GraphicsItemRenderer : public GraphicsWidget, public QPainterRenderer
{
    DPTR_DECLARE_PRIVATE(GraphicsItemRenderer)
public:
    GraphicsItemRenderer(QGraphicsItem * parent = 0);
    virtual ~GraphicsItemRenderer();
    virtual VideoRendererId id() const;

    QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
    GraphicsItemRenderer(GraphicsItemRendererPrivate& d, QGraphicsItem *parent);

    virtual bool write();
    virtual bool needUpdateBackground() const;
    //called in paintEvent before drawFrame() when required
    virtual void drawBackground();
    //draw the current frame using the current paint engine. called by paintEvent()
    virtual void drawFrame();
#if CONFIG_GRAPHICSWIDGET
    virtual bool event(QEvent *event);
#else
    //virtual bool sceneEvent(QEvent *event);
#endif //CONFIG_GRAPHICSWIDGET
};
typedef GraphicsItemRenderer VideoRendererGraphicsItem;
}

#endif // QAV_GRAPHICSITEMRENDERER_H
