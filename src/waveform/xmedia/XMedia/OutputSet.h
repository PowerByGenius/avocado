#ifndef XMEDIA_OUTPUTSET_H
#define XMEDIA_OUTPUTSET_H

#include <QtCore/QObject>
#include <QtCore/QMutex>
#include <QtCore/QWaitCondition>
#include <XMedia/XMedia_Global.h>
#include <XMedia/AVOutput.h>

namespace XMedia {

class AVPlayer;
class Q_EXPORT OutputSet : public QObject
{
    Q_OBJECT
public:
    OutputSet(AVPlayer *player);
    virtual ~OutputSet();

    //required when accessing renderers
    void lock();
    void unlock();

    //implicity shared
    //QList<AVOutput*> outputs();
    QList<AVOutput*> outputs();

    //each(OutputOperation(data))
    //
    void sendData(const QByteArray& data);

    void clearOutputs();
    void addOutput(AVOutput* output);

    void notifyPauseChange(AVOutput *output);
    bool canPauseThread() const;
    //in AVThread
    void pauseThread(); //There are 2 ways to pause AVThread: 1. pause thread directly. 2. pause all outputs
    /*
     * in user thread when pause count < set size.
     * 1. AVPlayer.pause(false) in player thread then call each output pause(false)
     * 2. shortcut for AVOutput.pause(false)
     */
    void resumeThread();

signals:
    void updateParametersRequired(AVOutput* output);

public slots:
    //connect to renderer->aboutToClose(). test whether delete on close
    void removeOutput(AVOutput *output);

private:
    bool mCanPauseThread;
    AVPlayer *mpPlayer;
    int mPauseCount; //pause AVThread if equals to mOutputs.size()
    QList<AVOutput*> mOutputs;
    QMutex mMutex;
    QWaitCondition mCond; //pause
};

} //namespace XMedia

#endif // XMEDIA_OUTPUTSET_H
