#ifndef XMEDIA_AOPORTAUDIO_H
#define XMEDIA_AOPORTAUDIO_H

#include <XMedia/AudioOutput.h>

namespace XMedia {

class AOPortAudioPrivate;
class Q_EXPORT AOPortAudio : public AudioOutput
{
    DPTR_DECLARE_PRIVATE(AOPortAudio)
public:
    AOPortAudio();
    ~AOPortAudio();

    bool open();
    bool close();

protected:
    bool write();
};

} //namespace XMedia
#endif // XMEDIA_AOPORTAUDIO_H
