#ifndef XMEDIA_AVTHREAD_H
#define XMEDIA_AVTHREAD_H

#include <QtCore/QThread>
#include <QtCore/QScopedPointer>
#include <XMedia/Packet.h>
//TODO: pause functions. AVOutput may be null, use AVThread's pause state
namespace XMedia {

class AVDecoder;
class AVThreadPrivate;
class AVOutput;
class AVClock;
class Statistics;
class OutputSet;
class Q_EXPORT AVThread : public QThread
{
    Q_OBJECT
    DPTR_DECLARE_PRIVATE(AVThread)
public:
	explicit AVThread(QObject *parent = 0);
    virtual ~AVThread();

    //used for changing some components when running
    Q_DECL_DEPRECATED void lock();
    Q_DECL_DEPRECATED void unlock();

    void setClock(AVClock *clock);
    AVClock* clock() const;

    //void setPacketQueue(PacketQueue *queue);
    PacketQueue* packetQueue() const;

    void setDecoder(AVDecoder *decoder);
    AVDecoder *decoder() const;

    Q_DECL_DEPRECATED void setOutput(AVOutput *out);
    Q_DECL_DEPRECATED AVOutput* output() const;

    void setOutputSet(OutputSet *set);
    OutputSet* outputSet() const;

    void setDemuxEnded(bool ended);

    bool isPaused() const;
public slots:
    virtual void stop();
    /*change pause state. the pause/continue action will do in the next loop*/
    void pause(bool p); //processEvents when waiting?
    void nextAndPause(); //process 1 frame and pause

private slots:
    void addOutputToBeUpdated(AVOutput *output);

protected:
    AVThread(AVThreadPrivate& d, QObject *parent = 0);
    void resetState();
    /*
     * If the pause state is true setted by pause(true), then block the thread and wait for pause state changed, i.e. pause(false)
     * and return true. Otherwise, return false immediatly.
     */
    bool tryPause();

    DPTR_DECLARE(AVThread)

private:
    void setStatistics(Statistics* statistics);
    friend class AVPlayer;
};
}

#endif // XMEDIA_AVTHREAD_H
