#ifndef QAV_PACKET_H
#define QAV_PACKET_H

#include <queue>
#include <QtCore/QByteArray>
#include <QtCore/QQueue>
#include <QtCore/QMutex>
#include <XMedia/BlockingQueue.h>
//#include <XMedia/BlockingRing.h>
#include <XMedia/XMedia_Global.h>

namespace XMedia {
class Q_EXPORT Packet
{
public:
    Packet();
    inline bool isValid() const;
    inline bool isEnd() const;
    void markEnd();

    QByteArray data;
    qreal pts, duration;
private:
    static const qreal kEndPts;
};

bool Packet::isValid() const
{
    return !data.isNull() && pts >= 0 && duration >= 0; //!data.isEmpty()?
}

bool Packet::isEnd() const
{
    return pts == kEndPts;
}

template <typename T> class StdQueue : public std::queue<T>
{
public:
	bool isEmpty() const { return this->empty(); }
	void clear() { while (!this->empty()) { this->pop(); } }
	T dequeue() { this->pop(); return this->front(); }
	void enqueue(const T& t) { this->push(t); }
};
typedef BlockingQueue<Packet, QQueue> PacketQueue;
//typedef BlockingQueue<Packet, StdQueue> PacketQueue;
//typedef BlockingRing<Packet> PacketQueue;
} //namespace XMedia

#endif // QAV_PACKET_H
