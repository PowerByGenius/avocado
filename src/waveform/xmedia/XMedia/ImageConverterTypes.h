#ifndef XMEDIA_IMAGECONVERTERTYPES_H
#define XMEDIA_IMAGECONVERTERTYPES_H

/*
 * This file can not be included by ImageConverterXXX.{h,cpp}
 * Usually you just include this and then you can use factory
 * e.g.
 *      ImageConverter* c = ImageConverter::create(ImageConverterId_FF);
 */

#include <XMedia/ImageConverter.h>

namespace XMedia {

/*
 * When a new type is created, declare the type here, define the value in cpp
 */
//why can not be const for msvc?
extern Q_EXPORT ImageConverterId ImageConverterId_FF;
extern Q_EXPORT ImageConverterId ImageConverterId_IPP;

/*
 * This must be called manually in your program(outside this library) if your compiler does
 * not support automatically call a function before main(). And you should add functions there
 * to register a new type.
 * See prepost.h. Currently supports c++ compilers, and C compilers such as gnu compiler
 * (including llvm based compilers) and msvc
 * Another case is link this library as static library. Not all data will be pulled to you program
 * especially the automatically called functions.
 * Steps:
 *   1. add RegisterImageConverterXXX_Man() in ImageConverter_RegisterAll() in ImageConverter.cpp;
 *   2. define the RegisterImageConverterXXX_Man() In ImageConverterXXX.cpp
 */
Q_EXPORT void ImageConverter_RegisterAll();

} //namespace XMedia
#endif // XMEDIA_IMAGECONVERTERTYPES_H
