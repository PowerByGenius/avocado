#ifndef XMEDIA_VERSION_H
#define XMEDIA_VERSION_H

#define XMEDIA_MAJOR 1    //((XMEDIA_VERSION&0xff0000)>>16)
#define XMEDIA_MINOR 2    //((XMEDIA_VERSION&0xff00)>>8)
#define XMEDIA_PATCH 4    //(XMEDIA_VERSION&0xff)


#define XMEDIA_VERSION_MAJOR(V) ((V & 0xff0000) >> 16)
#define XMEDIA_VERSION_MINOR(V) ((V & 0xff00) >> 8)
#define XMEDIA_VERSION_PATCH(V) (V & 0xff)

#define XMEDIA_VERSION_CHK(major, minor, patch) \
    (((major&0xff)<<16) | ((minor&0xff)<<8) | (patch&0xff))

#define XMEDIA_VERSION XMEDIA_VERSION_CHK(XMEDIA_MAJOR, XMEDIA_MINOR, XMEDIA_PATCH)

/*! Stringify \a x. */
#define _TOSTR(x)   #x
/*! Stringify \a x, perform macro expansion. */
#define TOSTR(x)  _TOSTR(x)


/* the following are compile time version */
/* C++11 requires a space between literal and identifier */
static const char* const xmedia_version_string = TOSTR(XMEDIA_MAJOR) "." TOSTR(XMEDIA_MINOR) "." TOSTR(XMEDIA_PATCH) "(" __DATE__ ", " __TIME__ ")";
#define XMEDIA_VERSION_STR        TOSTR(XMEDIA_MAJOR) "." TOSTR(XMEDIA_MINOR) "." TOSTR(XMEDIA_PATCH)
#define XMEDIA_VERSION_STR_LONG   XMEDIA_VERSION_STR "(" __DATE__ ", " __TIME__ ")"

#endif // XMEDIA_VERSION_H
