#ifndef QAV_QPAINTERRENDERER_H
#define QAV_QPAINTERRENDERER_H

#include <XMedia/VideoRenderer.h>
#include <qimage.h>
//TODO: not abstract.
namespace XMedia {

class QPainterRendererPrivate;
class Q_EXPORT QPainterRenderer : public VideoRenderer
{
    DPTR_DECLARE_PRIVATE(QPainterRenderer)
public:
    QPainterRenderer();
    virtual ~QPainterRenderer();
    virtual VideoRendererId id () const;
    virtual int filterContextType() const;
    //virtual QImage currentFrameImage() const;
protected:
    virtual void convertData(const QByteArray &data);
    QPainterRenderer(QPainterRendererPrivate& d);
};

} //namespace XMedia
#endif // QAV_QPAINTERRENDERER_H
