#ifndef XMEDIA_GLWIDGETRENDERER_H
#define XMEDIA_GLWIDGETRENDERER_H

#include <XMedia/VideoRenderer.h>
#include <QtOpenGL/QGLWidget>

namespace XMedia {

class GLWidgetRendererPrivate;
class Q_EXPORT GLWidgetRenderer : public QGLWidget, public VideoRenderer
{
    Q_OBJECT
    DPTR_DECLARE_PRIVATE(GLWidgetRenderer)
public:
    GLWidgetRenderer(QWidget* parent = 0, const QGLWidget* shareWidget = 0, Qt::WindowFlags f = 0);
    virtual ~GLWidgetRenderer();
    virtual VideoRendererId id() const;

protected:
    virtual void convertData(const QByteArray &data);
    virtual bool needUpdateBackground() const;
    //called in paintEvent before drawFrame() when required
    virtual void drawBackground();
    //draw the current frame using the current paint engine. called by paintEvent()
    virtual void drawFrame();
    virtual void initializeGL();
    virtual void paintGL();
    virtual void resizeGL(int w, int h);
    virtual void resizeEvent(QResizeEvent *);
    virtual void showEvent(QShowEvent *);
    virtual bool write();
};
typedef GLWidgetRenderer VideoRendererGLWidget;

} //namespace XMedia

#endif // XMEDIA_GLWidgetRenderer_H
