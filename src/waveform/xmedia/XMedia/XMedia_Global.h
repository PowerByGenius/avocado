#ifndef XMEDIA_GLOBAL_H
#define XMEDIA_GLOBAL_H

#include <qglobal.h>
#include <XMedia/dptr.h>

#if defined(Q_DLL_LIBRARY)
#  undef Q_EXPORT
#  define Q_EXPORT Q_DECL_EXPORT
#else
#  undef Q_EXPORT
//#  define Q_EXPORT Q_DECL_IMPORT //only for vc?
#  define Q_EXPORT
#endif

/* runtime version. used to compare with compile time version */
Q_EXPORT unsigned XMedia_Version();
Q_EXPORT QString XMedia_Version_String();
Q_EXPORT QString XMedia_Version_String_Long();
namespace XMedia {
Q_EXPORT void about(); //popup a dialog
Q_EXPORT void aboutFFmpeg();
Q_EXPORT QString aboutFFmpeg_PlainText();
Q_EXPORT QString aboutFFmpeg_HTML();
Q_EXPORT void aboutQtAV();
Q_EXPORT QString aboutXMedia_PlainText();
Q_EXPORT QString aboutXMedia_HTML();
}

/*
 * msvc sucks! can not deal with (defined XMEDIA_HAVE_##FEATURE && XMEDIA_HAVE_##FEATURE)
 */
#define XMEDIA_HAVE(FEATURE) (defined XMEDIA_HAVE_##FEATURE && XMEDIA_HAVE_##FEATURE)

//TODO: always inline
/* --gnu option of the RVCT compiler also defines __GNUC__ */
#if defined(Q_CC_GNU) && !defined(Q_CC_RVCT)
#define GCC_VERSION_AT_LEAST(major, minor, patch) \
    (__GNUC__ > major || (__GNUC__ == major && (__GNUC_MINOR__ > minor \
    || (__GNUC_MINOR__ == minor && __GNUC_PATCHLEVEL__ >= patch))))
#else
/* Define this for !GCC compilers, just so we can write things like GCC_VERSION_AT_LEAST(4, 1, 0). */
#define GCC_VERSION_AT_LEAST(major, minor, patch) 0
#endif

#endif // XMEDIA_GLOBAL_H

