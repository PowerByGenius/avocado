#ifndef XMEDIA_OSD_H
#define XMEDIA_OSD_H

#include <XMedia/XMedia_Global.h>
#include <QtCore/QPoint>
#include <QtGui/QFont>

namespace XMedia {

class Statistics;
class Q_EXPORT OSD
{
public:
    enum ShowType {
        ShowCurrentTime = 1,
        ShowCurrentAndTotalTime = 1<<1,
        ShowRemainTime = 1<<2,
        ShowPercent = 1<<3,
        ShowNone
    };

    OSD();
    virtual ~OSD();
    void setShowType(ShowType type);
    ShowType showType() const;
    void useNextShowType();
    bool hasShowType(ShowType t) const;
    QString text(Statistics* statistics);
protected:
    ShowType mShowType;
    int mSecsTotal;
};

}//namespace XMedia
#endif // XMEDIA_OSD_H
