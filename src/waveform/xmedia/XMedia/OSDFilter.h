#ifndef XMEDIA_OSDFILTER_H
#define XMEDIA_OSDFILTER_H

#include <XMedia/Filter.h>
#include <XMedia/OSD.h>
#include <XMedia/FilterContext.h>
namespace XMedia {

class OSDFilterPrivate;
//TODO: not template. OSDFilter : public Filter, public OSD
class OSDFilter : public Filter, public OSD
{
protected:
    DPTR_DECLARE_PRIVATE(OSDFilter)
    OSDFilter(OSDFilterPrivate& d);
};

class OSDFilterQPainter : public OSDFilter
{
public:
    OSDFilterQPainter();
    FilterContext::Type contextType() const {
        return FilterContext::QtPainter;
    }
protected:
    void process();
};

class OSDFilterGL : public OSDFilter
{
public:
    OSDFilterGL();
    FilterContext::Type contextType() const {
        return FilterContext::OpenGL;
    }
protected:
    void process();
};

} //namespace XMedia

#endif // XMEDIA_OSDFILTER_H
