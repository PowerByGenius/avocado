#ifndef XMEDIA_AUDIORESAMPLERTYPES_H
#define XMEDIA_AUDIORESAMPLERTYPES_H

#include <XMedia/AudioResampler.h>

namespace XMedia {

extern Q_EXPORT AudioResamplerId AudioResamplerId_FF;
extern Q_EXPORT AudioResamplerId AudioResamplerId_Libav;

Q_EXPORT void AudioResampler_RegisterAll();

} //namespace XMedia

#endif // XMEDIA_AUDIORESAMPLERTYPES_H
