#ifndef QAV_AUDIOOUTPUT_H
#define QAV_AUDIOOUTPUT_H

#include <XMedia/AVOutput.h>
//TODO: audio device class

namespace XMedia {

class AudioFormat;
class AudioOutputPrivate;
class Q_EXPORT AudioOutput : public AVOutput
{
    DPTR_DECLARE_PRIVATE(AudioOutput)
public:
    AudioOutput();
    virtual ~AudioOutput() = 0;

    int maxChannels() const;
    //virtual bool isSupported(const AudioFormat& format);
    void setAudioFormat(const AudioFormat& format);
    AudioFormat& audioFormat();
    const AudioFormat& audioFormat() const;

    void setSampleRate(int rate); //deprecated
    int sampleRate() const; //deprecated

    void setChannels(int channels); //deprecated
    int channels() const; //deprecated

    void setVolume(qreal volume);
    qreal volume() const;
    void setMute(bool yes);
    bool isMute() const;
    /*!
     * \brief setSpeed  set audio playing speed
     *
     * The speed affects the playing only if audio is available and clock type is
     * audio clock. For example, play a video contains audio without special configurations.
     * To change the playing speed in other cases, use AVPlayer::setSpeed(qreal)
     * \param speed
     */
    void setSpeed(qreal speed);
    qreal speed() const;

protected:
    AudioOutput(AudioOutputPrivate& d);
};

} //namespace XMedia
#endif // QAV_AUDIOOUTPUT_H
