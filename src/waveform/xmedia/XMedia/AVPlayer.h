#ifndef XMEDIA_AVPLAYER_H
#define XMEDIA_AVPLAYER_H

#include <XMedia/AVClock.h>
#include <XMedia/AVDemuxer.h>
#include <XMedia/Statistics.h>

namespace XMedia {

class AVOutput;
class AudioOutput;
class AVThread;
class AudioThread;
class VideoThread;
class AudioDecoder;
class VideoDecoder;
class VideoRenderer;
class AVClock;
class AVDemuxThread;
class VideoCapture;
class OutputSet;
class Q_EXPORT AVPlayer : public QObject
{
    Q_OBJECT
public:
    explicit AVPlayer(QObject *parent = 0);
    ~AVPlayer();

    //NOT const. This is the only way to access the clock.
    AVClock* masterClock();
    void setFile(const QString& path);
	QString file() const;
    void setKey(const QString& key);

    bool load(const QString& path);
    bool load();
    bool isLoaded() const;
    qreal duration() const; //unit: s, This function may be removed in the future.
    /*!
     * \brief capture and save current frame to "appdir/filename_pts.png".
     * To capture with custom configurations, such as name and dir, use
     * VideoCapture api through AVPlayer::videoCapture()
     * \return
     */
    bool captureVideo();
    VideoCapture *videoCapture();
    bool play(const QString& path);
	bool isPlaying() const;
    bool isPaused() const;
    //this will install the default EventFilter. To use customized filter, register after this
    void addVideoRenderer(VideoRenderer *renderer);
    void removeVideoRenderer(VideoRenderer *renderer);
    void clearVideoRenderers();
    void setRenderer(VideoRenderer* renderer);
    VideoRenderer* renderer();
    QList<VideoRenderer*> videoOutputs();
    void setAudioOutput(AudioOutput* ao);

    /*!
     * To change audio format, you should set both AudioOutput's format and AudioResampler's format
     * So signals/slots is a better solution.
     * TODO: AudioOutput.audioFormatChanged (signal)---AudioResampler.setOutAudioFormat (slot)
     */
    AudioOutput* audio();
    void enableAudio(bool enable = true);
    void disableAudio(bool disable = true);
    void setMute(bool mute);
    bool isMute() const;
    /*!
     * \brief setSpeed set playing speed.
     * \param speed  speed > 0. 1.0: normal speed
     */
    void setSpeed(qreal speed);
    qreal speed() const;
    /*only 1 event filter is available. the previous one will be removed. setPlayerEventFilter(0) will remove the event filter*/
    void setPlayerEventFilter(QObject *obj);

    Statistics& statistics();
    const Statistics& statistics() const;
signals:
    void paused(bool p);
    void started();
    void stopped();
    void speedChanged(qreal speed);

public slots:
    void togglePause();
    void pause(bool p);
    void play(); //replay
    void stop();
    void playNextFrame();
    void seek(qreal pos);
    void seekForward();
    void seekBackward();
    void updateClock(qint64 msecs); //update AVClock's external clock

private:
    void initStatistics();
    void setupAudioThread();
    void setupVideoThread();
    void setupAVThread(AVThread*& thread, AVCodecContext* ctx);
    template<class Out>
    void setAVOutput(Out*& pOut, Out* pNew, AVThread* thread);
    //TODO: addAVOutput()

    bool loaded;
    AVFormatContext	*formatCtx; //changed when reading a packet
    AVCodecContext *aCodecCtx, *vCodecCtx; //set once and not change
    QString path;
    QString key;

    //the following things are required and must be setted not null
    AVDemuxer demuxer;
    AVDemuxThread *demuxer_thread;
    AVClock *clock;
    VideoRenderer *_renderer; //list?
    AudioOutput *_audio;
    AudioDecoder *audio_dec;
    VideoDecoder *video_dec;
    AudioThread *audio_thread;
    VideoThread *video_thread;

    //tODO: (un)register api
    QObject *event_filter;
    VideoCapture *video_capture;
    Statistics mStatistics;
    qreal mSpeed;
    bool ao_enable;
    OutputSet *mpVOSet, *mpAOSet;
};

} //namespace XMedia
#endif // XMEDIA_AVPLAYER_H
