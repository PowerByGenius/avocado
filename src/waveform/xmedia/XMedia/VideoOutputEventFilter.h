#ifndef XMEDIA_VIDEOOUTPUTEVENTFILTER_H
#define XMEDIA_VIDEOOUTPUTEVENTFILTER_H

#include <QtCore/QObject>
#include <XMedia/XMedia_Global.h>

namespace XMedia {

class VideoRenderer;
class Q_EXPORT VideoOutputEventFilter : public QObject
{
    Q_OBJECT
public:
    VideoOutputEventFilter(VideoRenderer *renderer = 0);
    virtual bool eventFilter(QObject *watched, QEvent *event);

private slots:
    void stopFiltering();
private:
    bool mRendererIsQObj;
    VideoRenderer *mpRenderer;
};

} //namespace XMedia

#endif // XMEDIA_VIDEOOUTPUTEVENTFILTER_H
