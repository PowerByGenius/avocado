#ifndef QAV_DEMUXTHREAD_H
#define QAV_DEMUXTHREAD_H

#include <QtCore/QMutex>
#include <QtCore/QThread>
#include <QtCore/QWaitCondition>
#include <XMedia/XMedia_Global.h>

namespace XMedia {

class AVDemuxer;
class AVThread;
class Q_EXPORT AVDemuxThread : public QThread
{
    Q_OBJECT
public:
    explicit AVDemuxThread(QObject *parent = 0);
    explicit AVDemuxThread(AVDemuxer *dmx, QObject *parent = 0);
    void setDemuxer(AVDemuxer *dmx);
    void setAudioThread(AVThread *thread);
    AVThread* audioThread();
    void setVideoThread(AVThread *thread);
    AVThread* videoThread();
    //flag: -1 backward, 0 seek to 0<=pos<=1, 1 seek forward
    //TODO: demuxer(or thread).seek(qreal delta, Flag Cur|Begin|End|Time|Size|Norm|KeyFrame|AnyFrame)
    void seek(qreal pos, int flag = 0);
    void seekForward();
    void seekBackward();
    //AVDemuxer* demuxer
    bool isPaused() const;
    bool isEnd() const;
public slots:
    void stop();
    void pause(bool p);
private slots:
    void notifyEnd();

protected:
    virtual void run();
    /*
     * If the pause state is true setted by pause(true), then block the thread and wait for pause state changed, i.e. pause(false)
     * and return true. Otherwise, return false immediatly.
     */
    bool tryPause();

private:
    void setAVThread(AVThread *&pOld, AVThread* pNew);
    bool paused, seeking;
    volatile bool end;
    AVDemuxer *demuxer;
    AVThread *audio_thread, *video_thread;
    int audio_stream, video_stream;
    QMutex buffer_mutex;
    QWaitCondition cond, seek_cond;
};

} //namespace XMedia
#endif // QAV_DEMUXTHREAD_H
