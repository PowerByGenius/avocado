#include <XMedia/OSD.h>
#include <XMedia/Statistics.h>

namespace XMedia {

OSD::OSD():
    mShowType(ShowCurrentAndTotalTime)
  , mSecsTotal(-1)
{
}

OSD::~OSD()
{
}

void OSD::setShowType(ShowType type)
{
    mShowType = type;
}

OSD::ShowType OSD::showType() const
{
    return mShowType;
}

void OSD::useNextShowType()
{
    if (mShowType == ShowNone) {
        mShowType = (ShowType)1;
        return;
    }
    if (mShowType + 1 == ShowNone) {
        mShowType = ShowNone;
        return;
    }
    mShowType = (ShowType)(mShowType << 1);
}

bool OSD::hasShowType(ShowType t) const
{
    return (t&mShowType) == t;
}

QString OSD::text(Statistics *statistics)
{
    QString text;
    Statistics::Common *av = &statistics->video;
    if (!av->available)
        av = &statistics->audio;
    if (hasShowType(ShowCurrentTime) || hasShowType(ShowCurrentAndTotalTime)) {
        text = av->current_time.toString("HH:mm:ss");
    }
    //how to compute mSecsTotal only once?
    if (hasShowType(ShowCurrentAndTotalTime) || hasShowType(ShowPercent) /*mSecsTotal < 0*/) {
        if (statistics->duration.isNull())
            return text;
        mSecsTotal = QTime(0, 0, 0).secsTo(statistics->duration); //why video.total_time may be wrong(mkv)
    }
    if (hasShowType(ShowCurrentAndTotalTime)) {
        text += "/" + statistics->duration.toString("HH:mm:ss");
    }
    if (hasShowType(ShowRemainTime)) {
        text += "-" + QTime(0, 0, 0).addSecs(av->current_time.secsTo(statistics->duration)).toString("HH:mm:ss");
    }
    if (hasShowType(ShowPercent))
        text += QString::number(qreal(QTime(0, 0, 0).secsTo(av->current_time))
                                /qreal(mSecsTotal)*100.0, 'f', 1) + "%";
    return text;
}


} //namespace XMedia

