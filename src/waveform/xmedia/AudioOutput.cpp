#include <XMedia/AudioOutput.h>
#include <XMedia/private/AudioOutput_p.h>

namespace XMedia {
AudioOutput::AudioOutput()
    :AVOutput(*new AudioOutputPrivate())
{
}

AudioOutput::AudioOutput(AudioOutputPrivate &d)
    :AVOutput(d)
{
}

AudioOutput::~AudioOutput()
{
}

int AudioOutput::maxChannels() const
{
    return d_func().max_channels;
}

void AudioOutput::setAudioFormat(const AudioFormat& format)
{
    d_func().format = format;
}

AudioFormat& AudioOutput::audioFormat()
{
    return d_func().format;
}

const AudioFormat& AudioOutput::audioFormat() const
{
    return d_func().format;
}

void AudioOutput::setSampleRate(int rate)
{
    d_func().format.setSampleRate(rate);
}

int AudioOutput::sampleRate() const
{
    return d_func().format.sampleRate();
}

void AudioOutput::setChannels(int channels)
{
    DPTR_D(AudioOutput);
    if (channels > d.max_channels) {
        qWarning("not support. max channels is %d", d.max_channels);
        return;
    }
    d.format.setChannels(channels);
}

int AudioOutput::channels() const
{
    return d_func().format.channels();
}

void AudioOutput::setVolume(qreal volume)
{
    DPTR_D(AudioOutput);
    d.vol = qMax<qreal>(volume, 0);
    d.mute = d.vol == 0;
}

qreal AudioOutput::volume() const
{
    return qMax<qreal>(d_func().vol, 0);
}

void AudioOutput::setMute(bool yes)
{
    d_func().mute = yes;
}

bool AudioOutput::isMute() const
{
    return !isAvailable() || d_func().mute;
}

void AudioOutput::setSpeed(qreal speed)
{
    d_func().speed = speed;
}

qreal AudioOutput::speed() const
{
    return d_func().speed;
}

} //namespace XMedia
