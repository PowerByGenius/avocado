#include <XMedia/AVOutput.h>
#include <XMedia/private/AVOutput_p.h>
#include <XMedia/Filter.h>
#include <XMedia/FilterContext.h>
#include <XMedia/OutputSet.h>

namespace XMedia {

AVOutputPrivate::~AVOutputPrivate() {
    cond.wakeAll(); //WHY: failed to wake up
    if (filter_context) {
        delete filter_context;
        filter_context = 0;
    }
    qDeleteAll(filters);
    filters.clear();
}

AVOutput::AVOutput()
{
}

AVOutput::AVOutput(AVOutputPrivate &d)
    :DPTR_INIT(&d)
{
}

AVOutput::~AVOutput()
{
    DPTR_D(AVOutput);
    pause(false); //Does not work. cond may still waiting when destroyed
    detach();
}

bool AVOutput::writeData(const QByteArray &data)
{
    Q_UNUSED(data);
    //DPTR_D(AVOutput);
    //locker(&mutex)
    //TODO: make sure d.data thread safe. lock around here? for audio and video(main thread problem)?
    /* you can use d.data directly in AVThread. In other thread, it's not safe, you must do something
     * to make sure the data is not be modified in AVThread when using it*/
    //d_func().data = data;
    if (d_func().paused)
        return false;
    convertData(data); //TODO: only once for all outputs
	bool result = write();
	//write then pause: if capture when pausing, the displayed picture is captured
    //tryPause(); ///DO NOT pause the thread so that other outputs can still run
	return result;
}

bool AVOutput::isAvailable() const
{
    return d_func().available;
}

void AVOutput::pause(bool p)
{
    DPTR_D(AVOutput);
    if (d.paused == p)
        return;
    d.paused = p;
#if V1_2
    if (!d.paused)
        d.cond.wakeAll();
#endif //V1_2
}

bool AVOutput::isPaused() const
{
    return d_func().paused;
}

//TODO: how to call this automatically before write()?
bool AVOutput::tryPause()
{
    DPTR_D(AVOutput);
    if (!d.paused)
        return false;
    QMutexLocker lock(&d.mutex);
    Q_UNUSED(lock);
    d.cond.wait(&d.mutex);
    return true;
}

void AVOutput::addOutputSet(OutputSet *set)
{
    d_func().output_sets.append(set);
}

void AVOutput::removeOutputSet(OutputSet *set)
{
    d_func().output_sets.removeAll(set);
}

void AVOutput::attach(OutputSet *set)
{
    set->addOutput(this);
}

void AVOutput::detach(OutputSet *set)
{
    DPTR_D(AVOutput);
    if (set) {
        set->removeOutput(this);
        return;
    }
    foreach(OutputSet *set, d.output_sets) {
        set->removeOutput(this);
    }
}

void AVOutput::convertData(const QByteArray &data)
{
    //TODO: make sure d.data thread safe. lock here?
    DPTR_D(AVOutput);
    d.data = data;
}

int AVOutput::filterContextType() const
{
    return FilterContext::None;
}

QList<Filter*>& AVOutput::filters()
{
    return d_func().filters;
}

void AVOutput::setStatistics(Statistics *statistics)
{
    DPTR_D(AVOutput);
    d.statistics = statistics;
}

} //namespace XMedia
