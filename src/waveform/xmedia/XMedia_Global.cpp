#include <XMedia/XMedia_Global.h>
#include <QtCore/QObject>
#include <QtCore/QRegExp>
#include <QBoxLayout>
#include <QMessageBox>
#include <QPushButton>
#include <QTableWidget>
#include <QTextBrowser>
#include <XMedia/version.h>
#include <XMedia/XMedia_Compat.h>

unsigned XMedia_Version()
{
    return XMEDIA_VERSION;
}

QString XMedia_Version_String()
{
    return XMEDIA_VERSION_STR;
}

QString XMedia_Version_String_Long()
{
    return XMEDIA_VERSION_STR_LONG;
}

namespace XMedia {

//TODO: auto add new depend libraries information
void about()
{
    //we should use new because a qobject will delete it's children
//    QTextBrowser *viewQtAV = new QTextBrowser;
//    QTextBrowser *viewFFmpeg = new QTextBrowser;
//    viewQtAV->setOpenExternalLinks(true);
//    viewFFmpeg->setOpenExternalLinks(true);
//    viewQtAV->setHtml(aboutXMedia_HTML());
//    viewFFmpeg->setHtml(aboutFFmpeg_HTML());
//    QTabWidget *tab = new QTabWidget;
//    tab->addTab(viewQtAV, "QtAV");
//    tab->addTab(viewFFmpeg, "FFmpeg");
//    QPushButton *btn = new QPushButton(QObject::tr("Ok"));
//    QHBoxLayout *btnLayout = new QHBoxLayout;
//    btnLayout->addStretch();
//    btnLayout->addWidget(btn);
//    QDialog dialog;
//    dialog.setWindowTitle(QObject::tr("About") + "  QtAV");
//    QVBoxLayout *layout = new QVBoxLayout;
//    dialog.setLayout(layout);
//    layout->addWidget(tab);
//    layout->addLayout(btnLayout);
//    QObject::connect(btn, SIGNAL(clicked()), &dialog, SLOT(accept()));
//    dialog.exec();
}

void aboutFFmpeg()
{
//    QMessageBox::about(0, QObject::tr("About FFmpeg"), aboutFFmpeg_HTML());
}

QString aboutFFmpeg_PlainText()
{
//    return aboutFFmpeg_HTML().remove(QRegExp("<[^>]*>"));
    return QString();
}

QString aboutFFmpeg_HTML()
{
//    QString text = "<h3>FFmpeg/Libav</h3>\n";
//    struct ff_component {
//        const char* lib;
//        unsigned build_version;
//        unsigned rt_version;
//        const char *config;
//        const char *license;
//    } components[] = {
////TODO: auto check loaded libraries
//#define FF_COMPONENT(name, NAME) #name, LIB##NAME##_VERSION_INT, name##_version(), name##_configuration(), name##_license()
//        { FF_COMPONENT(avcodec, AVCODEC) },
//        { FF_COMPONENT(avformat, AVFORMAT) },
//        { FF_COMPONENT(avutil, AVUTIL) },
//        { FF_COMPONENT(swscale, SWSCALE) },
//    #if XMEDIA_HAVE(SWRESAMPLE)
//        { FF_COMPONENT(swresample, SWRESAMPLE) },
//    #endif //XMEDIA_HAVE(SWRESAMPLE)
//    #if XMEDIA_HAVE(AVRESAMPLE)
//        { FF_COMPONENT(avresample, AVRESAMPLE) },
//    #endif //XMEDIA_HAVE(AVRESAMPLE)
//#undef FF_COMPONENT
//        { 0, 0, 0, 0, 0 }
//    };
//    for (int i = 0; components[i].lib != 0; ++i) {
//        text += "<h4>" + QObject::tr("Build version")
//                + QString(": lib%1-%2.%3.%4</h4>\n")
//                .arg(components[i].lib)
//                .arg(XMEDIA_VERSION_MAJOR(components[i].build_version))
//                .arg(XMEDIA_VERSION_MINOR(components[i].build_version))
//                .arg(XMEDIA_VERSION_PATCH(components[i].build_version))
//                ;
//        unsigned rt_version = components[i].rt_version;
//        if (components[i].build_version != rt_version) {
//            text += "<h4 style='color:#ff0000;'>" + QString(QObject::tr("Runtime version"))
//                    + QString(": %1.%2.%3</h4>\n")
//                    .arg(XMEDIA_VERSION_MAJOR(rt_version))
//                    .arg(XMEDIA_VERSION_MINOR(rt_version))
//                    .arg(XMEDIA_VERSION_PATCH(rt_version))
//                    ;
//        }
//        text += "<p>" + QString(components[i].config) + "</p>\n"
//                "<p>" + QString(components[i].license) + "</p>\n";
//    }
//    return text;
    return QString();
}

void aboutQtAV()
{
    //QMessageBox::about(0, QObject::tr("About QtAV"), aboutXMedia_HTML());
}

QString aboutXMedia_PlainText()
{
    return aboutXMedia_HTML().remove(QRegExp("<[^>]*>"));
}

QString aboutXMedia_HTML()
{
//    static QString about = "<h3>QtAV " XMEDIA_VERSION_STR_LONG "</h3>\n"
//            "<p>" + QObject::tr("A media playing library base on Qt and FFmpeg.\n") + "</p>"
//            "<p>" + QObject::tr("Distributed under the terms of LGPLv2.1 or later.\n") + "</p>"
//            "<p>Copyright (C) 2012-2013 Wang Bin (aka. Lucas Wang) <a href='mailto:wbsecg1@gmail.com'>wbsecg1@gmail.com</a></p>\n"
//            "<p>" + QObject::tr("Shanghai University, Shanghai, China\n") + "</p>"
//            "<p>" + QObject::tr("Source") + ": <a href='https://github.com/wang-bin/QtAV'>https://github.com/wang-bin/QtAV</a></p>\n"
//            "<p>" + QObject::tr("Downloads") + ": <a href='https://sourceforge.net/projects/qtav'>https://sourceforge.net/projects/qtav</a></p>";
//    return about;
    return QString();
}

}
