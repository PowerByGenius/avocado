LIBS += -lavformat -lavcodec -lavutil -lswscale

macx {
LIBS += -lz -lbz2
}

win32{
LIBS += -lws2_32  #-lksguid #-luuid
}

INCLUDEPATH += $$PWD \
               $$PWD/XMedia

DEPENDPATH += $$PWD \
               $$PWD/XMedia

    DEFINES += XMEDIA_HAVE_SWRESAMPLE=1
    SOURCES += $$PWD/AudioResamplerFF.cpp
    LIBS += -lswresample

    SOURCES += $$PWD/AOPortAudio.cpp
    HEADERS += $$PWD/XMedia/AOPortAudio.h
    DEFINES += XMEDIA_HAVE_PORTAUDIO=1

macx {
LIBS += -lportaudio -framework CoreAudio -framework AudioToolbox -framework AudioUnit -framework Carbon
}

win32{
    LIBS +=-lportaudio -lwinmm
}


    QT += opengl
    DEFINES += XMEDIA_HAVE_GL=1
    SOURCES += $$PWD/GLWidgetRenderer.cpp
    HEADERS += $$PWD/XMedia/private/GLWidgetRenderer_p.h
    HEADERS += $$PWD/XMedia/GLWidgetRenderer.h

SOURCES += \
    $$PWD/XMedia_Compat.cpp \
    $$PWD/XMedia_Global.cpp \
    $$PWD/AudioThread.cpp \
    $$PWD/AVThread.cpp \
    $$PWD/AudioDecoder.cpp \
    $$PWD/AudioFormat.cpp \
    $$PWD/AudioOutput.cpp \
    $$PWD/AudioResampler.cpp \
    $$PWD/AudioResamplerTypes.cpp \
    $$PWD/AVDecoder.cpp \
    $$PWD/AVDemuxer.cpp \
    $$PWD/AVDemuxThread.cpp \
    $$PWD/EventFilter.cpp \
    $$PWD/Filter.cpp \
    $$PWD/FilterContext.cpp \
    $$PWD/GraphicsItemRenderer.cpp \
    $$PWD/ImageConverter.cpp \
    $$PWD/ImageConverterFF.cpp \
    $$PWD/ImageConverterIPP.cpp \
    $$PWD/QPainterRenderer.cpp \
    $$PWD/OSD.cpp \
    $$PWD/OSDFilter.cpp \
    $$PWD/Packet.cpp \
    $$PWD/AVPlayer.cpp \
    $$PWD/VideoCapture.cpp \
    $$PWD/VideoRenderer.cpp \
    $$PWD/VideoRendererTypes.cpp \
    $$PWD/VideoOutputEventFilter.cpp \
    $$PWD/WidgetRenderer.cpp \
    $$PWD/AVOutput.cpp \
    $$PWD/OutputSet.cpp \
    $$PWD/AVClock.cpp \
    $$PWD/Statistics.cpp \
    $$PWD/VideoDecoder.cpp \
    $$PWD/VideoThread.cpp

HEADERS += \
    $$PWD/XMedia/XMedia.h \
    $$PWD/XMedia/dptr.h \
    $$PWD/XMedia/XMedia_Global.h \
    $$PWD/XMedia/AudioResampler.h \
    $$PWD/XMedia/AudioResamplerTypes.h \
    $$PWD/XMedia/AudioDecoder.h \
    $$PWD/XMedia/AudioFormat.h \
    $$PWD/XMedia/AudioOutput.h \
    $$PWD/XMedia/AVDecoder.h \
    $$PWD/XMedia/AVDemuxer.h \
    $$PWD/XMedia/BlockingQueue.h \
    $$PWD/XMedia/Filter.h \
    $$PWD/XMedia/FilterContext.h \
    $$PWD/XMedia/GraphicsItemRenderer.h \
    $$PWD/XMedia/ImageConverter.h \
    $$PWD/XMedia/ImageConverterTypes.h \
    $$PWD/XMedia/QPainterRenderer.h \
    $$PWD/XMedia/OSD.h \
    $$PWD/XMedia/OSDFilter.h \
    $$PWD/XMedia/Packet.h \
    $$PWD/XMedia/AVPlayer.h \
    $$PWD/XMedia/VideoCapture.h \
    $$PWD/XMedia/VideoRenderer.h \
    $$PWD/XMedia/VideoRendererTypes.h \
    $$PWD/XMedia/WidgetRenderer.h \
    $$PWD/XMedia/AVOutput.h \
    $$PWD/XMedia/AVClock.h \
    $$PWD/XMedia/VideoDecoder.h \
    $$PWD/XMedia/FactoryDefine.h \
    $$PWD/XMedia/Statistics.h \
    $$PWD/XMedia/version.h

HEADERS += \
    $$PWD/XMedia/prepost.h \
    $$PWD/XMedia/AVDemuxThread.h \
    $$PWD/XMedia/AVThread.h \
    $$PWD/XMedia/AudioThread.h \
    $$PWD/XMedia/VideoThread.h \
    $$PWD/XMedia/VideoOutputEventFilter.h \
    $$PWD/XMedia/OutputSet.h \
    $$PWD/XMedia/XMedia_Compat.h \
    $$PWD/XMedia/EventFilter.h \
    $$PWD/XMedia/singleton.h \
    $$PWD/XMedia/factory.h \
    $$PWD/XMedia/private/AudioOutput_p.h \
    $$PWD/XMedia/private/AudioResampler_p.h \
    $$PWD/XMedia/private/AVThread_p.h \
    $$PWD/XMedia/private/AVDecoder_p.h \
    $$PWD/XMedia/private/AVOutput_p.h \
    $$PWD/XMedia/private/Filter_p.h \
    $$PWD/XMedia/private/GraphicsItemRenderer_p.h \
    $$PWD/XMedia/private/ImageConverter_p.h \
    $$PWD/XMedia/private/VideoRenderer_p.h \
    $$PWD/XMedia/private/QPainterRenderer_p.h \
    $$PWD/XMedia/private/WidgetRenderer_p.h

