#include <XMedia/Filter.h>
#include <XMedia/private/Filter_p.h>
#include <XMedia/Statistics.h>

namespace XMedia {

Filter::Filter(FilterPrivate &d)
    :DPTR_INIT(&d)
{

}

Filter::~Filter()
{
}

void Filter::process(FilterContext *&context, Statistics *statistics, QByteArray* data)
{
    DPTR_D(Filter);
    if (!context || context->type() != contextType()) {
        if (context) {
            qDebug("incompatible context type");
            //we don't delete it because the context MUST be used by other filters
            //TODO: How to release them at the end? refcount?
        } else {
            qDebug("null context");
        }
        //use internal context
        if (d.context) {
            qDebug("replace context with internal one");
            context = d.context;
        } else {
            context = FilterContext::create(contextType());
            qDebug("new filter context=%p, type=%d", context, contextType());
            context->video_width = statistics->video_only.width;
            context->video_height = statistics->video_only.height;
        }
    }
    //if (data)
        context->initializeOnData(data);
    d.context = context;
    d.statistics = statistics;
    process();
}

void Filter::setEnabled(bool enabled)
{
    DPTR_D(Filter);
    d.enabled = enabled;
}

bool Filter::isEnabled() const
{
    DPTR_D(const Filter);
    return d.enabled;
}

FilterContext::Type Filter::contextType() const
{
    return FilterContext::None;
}

} //namespace XMedia
