#include <XMedia/ImageConverter.h>
#include <XMedia/private/ImageConverter_p.h>
#include <XMedia/XMedia_Compat.h>
#include <XMedia/prepost.h>

#ifdef IPP_LINK
#include <ipp.h>
#else

#endif

namespace XMedia {

class ImageConverterIPPPrivate;
class ImageConverterIPP : public ImageConverter //Q_EXPORT is not needed
{
    DPTR_DECLARE_PRIVATE(ImageConverterIPP)
public:
    ImageConverterIPP();
    virtual bool convert(const quint8 *const srcSlice[], const int srcStride[]);
protected:
    virtual bool prepareData(); //Allocate memory for out data
};

ImageConverterId ImageConverterId_IPP = 1;
FACTORY_REGISTER_ID_AUTO(ImageConverter, IPP, "IPP")

void RegisterImageConverterIPP_Man()
{
    FACTORY_REGISTER_ID_MAN(ImageConverter, IPP, "IPP")
}

class ImageConverterIPPPrivate : public ImageConverterPrivate
{
public:
    ImageConverterIPPPrivate():need_scale(true) {}
    bool need_scale;
    QByteArray orig_ori_rgb;
};

ImageConverterIPP::ImageConverterIPP()
    :ImageConverter(*new ImageConverterIPPPrivate())
{
}

bool ImageConverterIPP::convert(const quint8 *const srcSlice[], const int srcStride[])
{
    DPTR_D(ImageConverterIPP);
    //color convertion, no scale
#ifdef IPP_LINK
    ippiYUV420ToRGB_8u_P3AC4R(const_cast<const quint8 **>(srcSlice), const_cast<int*>(srcStride), (Ipp8u*)(d.orig_ori_rgb.data())
                           , 4*sizeof(quint8)*d.w_in, (IppiSize){d.w_in, d.h_in});
    d.data_out = d.orig_ori_rgb;
    return true;
    if (d.need_scale) {
        qDebug("rs");
        ippiResize_8u_AC4R((const Ipp8u*)d.orig_ori_rgb.data(), (IppiSize){d.w_in, d.h_in}, 4*sizeof(quint8)*d.w_in, (IppiRect){0, 0, d.w_in, d.h_in}
                  , (Ipp8u*)d.data_out.data(), 4*sizeof(quint8)*d.w_in, (IppiSize){d.w_out, d.h_out}
                  , (double)d.w_out/(double)d.w_in, (double)d.h_out/(double)d.h_in, IPPI_INTER_CUBIC);
    } else {
        d.data_out = d.orig_ori_rgb;
    }
#else
    Q_UNUSED(d)
    Q_UNUSED(srcSlice)
    Q_UNUSED(srcStride)
#endif
    return true;
}

//TODO: call it when out format is setted. and avoid too much calls
bool ImageConverterIPP::prepareData()
{
    DPTR_D(ImageConverterIPP);
    //for color convertion
    if (d.w_in > 0 && d.h_in > 0) {
        qDebug("in size=%d x %d", d.w_in, d.h_in);
        int bytes = avpicture_get_size((PixelFormat)d.fmt_out, d.w_in, d.h_in);
        //if(d.orig_ori_rgb.size() < bytes) {
            d.orig_ori_rgb.resize(bytes);
        //}
    }
    //for resize
    d.need_scale = d.w_in != d.w_out || d.h_in != d.h_out;
    if (d.w_out > 0 && d.h_out > 0) {
        qDebug("out size=%d x %d", d.w_out, d.h_out);
        if (d.need_scale) {
            int bytes = avpicture_get_size((PixelFormat)d.fmt_out, d.w_out, d.h_out);
            //if(d.data_out.size() < bytes) {
                d.data_out.resize(bytes);
            //}
        }
    }
    return true;
}

} //namespace XMedia
