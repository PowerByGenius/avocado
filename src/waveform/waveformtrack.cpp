#include "waveformtrack.h"
#include <QFile>
#include <QDataStream>
#include <QPainter>

WaveformTrack::WaveformTrack(QObject *parent) :
    QObject(parent),
    m_position(0)
{

    {
    QFile file("/Users/kiko/Desktop/abc/test.data");
    file.open(QIODevice::ReadOnly);
    QDataStream stream(&file);
    stream >> m_waveformData;
    }

}

void WaveformTrack::render(QPainter *painter, const QRect &rect)
{

    qreal height = rect.height();
    int width = rect.width();

    int start = m_position;
    int end = m_position+width;

    if(end>m_waveformData.count()) end = m_waveformData.count();


    //painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QColor(255,204,0,172));
    qreal q = height/255;

    int ms = rect.left();

    for (int i = start; i < end; i++) {
        int data = m_waveformData.at(i);
        int y_min = (data >> 8) & 0xFF;
        int y_max = data & 0xFF;

        for (int p = y_min*q; p < y_max*q+1; ++p) {
            painter->drawPoint(ms,p);
        }

        ms++;
//        painter->drawLine(ms,y_min*q,ms,y_max*q);
    }

}

void WaveformTrack::setPositon(qint64 value)
{
    if(value < 0){
        m_position = 0;
        return;
    }

    if(value > m_waveformData.count())
    {
        m_position = m_waveformData.count();
        return;
    }

    m_position = value;

}

