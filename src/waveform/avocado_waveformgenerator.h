#ifndef AVOCADO_WAVEFORMGENERATOR_H
#define AVOCADO_WAVEFORMGENERATOR_H

#include <QObject>
#include <QPixmap>

namespace Avocado {

class WaveformGenerator : public QObject
{
    Q_OBJECT
public:
    explicit WaveformGenerator(QObject *parent = 0);
    void loadFile(const QString &path);
    QPixmap m_pixmap;
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_WAVEFORMGENERATOR_H
