#-------------------------------------------------
#
# Project created by QtCreator 2015-10-07T17:26:47
#
#-------------------------------------------------

QT       += core gui opengl
CONFIG += c++11

#include (QtAV/QtAV.pri)

LIBS += -lavformat -lavcodec -lavutil -lswscale -lswresample

macx {
LIBS += -lz -lbz2 -lssl
}

macx {
#QMAKE_CXXFLAGS += -mmacosx-version-min=10.11

QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.11
}

macx {
QMAKE_LFLAGS_RELEASE   += -Wl,-s -Wl,-x -g -fno-rtti -fvisibility=hidden -dead_strip
QMAKE_CXXFLAGS_RELEASE = -O4 -Wl,-s -g -fno-rtti -fvisibility=hidden -dead_strip -dead_strip_dylibs
INCLUDEPATH += $$PWD/../../thirdparty/build/osx/include
LIBS += -L$$PWD/../../thirdparty/build/osx/lib
}

macx {
LIBS += -framework Security \
        -framework CoreMedia \
        -framework CoreAudio \
        -framework CoreVideo \
        -framework VideoDecodeAcceleration \
        -framework AudioToolbox \
        -framework VideoToolbox \
        -framework AudioUnit \
        -framework Carbon
}


include (xmedia/xmedia.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = waveform
TEMPLATE = app


SOURCES += main.cpp\
        waveformwidget.cpp \
    waveformview.cpp \
    avocado_waveformgenerator.cpp \
    waveformpaddingwidget.cpp \
    waveformtrack.cpp \
    waveformtracktestwidget.cpp \
    avocado_waveformreader.cpp \
    waveformreaderwidget.cpp \
    audiowidget.cpp

HEADERS  += waveformwidget.h \
    waveformview.h \
    avocado_waveformgenerator.h \
    waveformpaddingwidget.h \
    waveformtrack.h \
    waveformtracktestwidget.h \
    avocado_waveformreader.h \
    waveformreaderwidget.h \
    audiowidget.h

FORMS    += waveformwidget.ui \
    waveformtracktestwidget.ui \
    waveformreaderwidget.ui \
    audiowidget.ui
