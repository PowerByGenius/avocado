#include "mainwindow.h"
#include <QApplication>
#include <QSqlDatabase>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    // db.setDatabaseName("/Users/kiko/Desktop/Library/Avocado/Qt/Avocado/src/data/mnemosyne.sqlite");
    db.setDatabaseName("/Users/kiko/Desktop/Library/Avocado/Qt/Avocado/database/avocado.sqlite");
    bool ok = db.open();
    qDebug() << ok << db.databaseName();


    MainWindow w;
    w.show();

    return a.exec();
}
