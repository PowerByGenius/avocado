#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QSqlQuery>
#include <QSqlRecord>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString sql = ui->plainTextEdit->toPlainText();
    sql = "select * from avocado_media";
    QSqlQuery query;
    query.exec(sql);

    QSqlRecord records= query.record();


    QStringList name;
    QStringList nameVar;
    for (int i = 0; i < records.count(); ++i) {
        name.append(records.fieldName(i));
        nameVar.append(":"+records.fieldName(i));
    }

    QString out;

    out.append(QString("query.prepare(\"INSERT INTO person (%1) VALUES (%2)\");\n")
            .arg(name.join(",")).arg(nameVar.join(",")));

    foreach (QString value, name) {
        out.append(QString("query.bindValue(\":%1\",map[\"%1\"]);\n").arg(value));
    }

    ui->plainTextEdit_2->setPlainText(out);
//    QSqlQuery query;
//    query.prepare("INSERT INTO person (id, forename, surname) "
//                  "VALUES (:id, :forename, :surname)");
//    query.bindValue(0, 1001);
//    query.bindValue(1, "Bart");
//    query.bindValue(2, "Simpson");
//    query.exec();


}
