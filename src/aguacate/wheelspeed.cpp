#include "wheelspeed.h"
#include <QPainter>
#include <QWheelEvent>
#include <QDebug>
namespace Avocado {

WheelSpeed::WheelSpeed(QWidget *parent) : QWidget(parent),
    m_wheelStep(0),
    m_default(1.00),m_value(1.00),
    m_max(2),m_min(0.05),m_step(0.01)
{
}

qreal WheelSpeed::value() const
{
    return m_value;
}

void WheelSpeed::setValue(const qreal &value)
{
    m_value = value;
    update();
}

void WheelSpeed::valueUp()
{

}

void WheelSpeed::valueDown()
{

}

void WheelSpeed::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        m_mouseButtonDownPos = event->pos();
        m_isMoving = true;
        event->accept();
    }
    QWidget::mousePressEvent(event);
}

void WheelSpeed::mouseReleaseEvent(QMouseEvent *event)
{
    m_isMoving = false;
    QWidget::mouseReleaseEvent(event);
}

void WheelSpeed::mouseDoubleClickEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton){
    m_wheelStep = 0;
    m_value = m_default;
    update();
    event->accept();
    emit valueChanged(m_value);
    }
    QWidget::mouseDoubleClickEvent(event);
}

void WheelSpeed::mouseMoveEvent(QMouseEvent *event)
{

    if(m_isMoving)
    {

        QPoint point = event->pos() - m_mouseButtonDownPos;

        if(point.x() > 0)
        {
            if(m_value <= m_max){
                m_wheelStep++;
                m_value += m_step;
                update();
                event->accept();
                emit valueChanged(m_value);
            }
        }else{
            if(m_value >= m_min){
                m_wheelStep--;
                m_value -= m_step;
                update();
                event->accept();
                emit valueChanged(m_value);
            }
        }
        m_mouseButtonDownPos = event->pos();
    }

    QWidget::mouseMoveEvent(event);
}

void WheelSpeed::wheelEvent(QWheelEvent *event)
{

    if(event->angleDelta().y() < 0)
    {
        if(m_value <= m_max){
            m_wheelStep++;
            m_value += m_step;
            update();
            event->accept();
            emit valueChanged(m_value);
        }
    }else{
        if(m_value >= m_min){
            m_wheelStep--;
            m_value -= m_step;
            update();
            event->accept();
            emit valueChanged(m_value);
        }
    }
    QWidget::wheelEvent(event);
}

void WheelSpeed::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter p(this);
    //draw background
    QRect rect = this->rect();
    p.setRenderHint(QPainter::Antialiasing,true);

    p.fillRect(rect,QColor(51,51,51));

    QLinearGradient gradient(QPointF(0,0),QPointF(width(),0));
    gradient.setColorAt(0,QColor(14,14,14,207));
    gradient.setColorAt(0.5,QColor(0,0,0,0));
    gradient.setColorAt(1,QColor(14,14,14,207));

    QColor gap= QColor(255,255,255,16);
    QColor body = QColor(255,255,255,64);
    QColor border = QColor(255,255,255,32);
    QColor hightlight = QColor(255,255,255,128);

    QFont font;
    font.setFamily("Arial");
    font.setPixelSize(12);
    font.setWeight(25);
    font.setLetterSpacing(QFont::PercentageSpacing,110);


    QSize t(10,8);

    int q = m_wheelStep % t.width();

    for (int i = -t.width(); i < rect.width()/t.width()+t.width(); ++i) {

        QRect rt((i-1)*t.width()+q*4,1,t.width(),t.height());
        QRect rb = QRect((i-1)*t.width()+q*4,height()-t.height()-1,t.width(),t.height());
        p.fillRect(rt,gap);
        p.fillRect(rb,gap);
        rt.adjust(3,0,-3,0);
        rb.adjust(3,0,-3,0);
        p.setBrush(body);
        p.setPen(border);
        p.drawRect(rt);
        p.drawRect(rb);
        p.setPen(hightlight);
        p.drawLine(rt.topLeft().x()-2,rt.topLeft().y(),rt.topRight().x()+2,rt.topRight().y());
        p.drawLine(rb.topLeft().x()-2,rb.topLeft().y(),rb.topRight().x()+2,rb.topRight().y());

//        QRect rt((i-1)*t.width()+q*4,1,t.width(),t.height());
//        p.fillRect(rt,background);
//        rt.adjust(3,0,-3,0);
//        p.setBrush(body);
//        p.setPen(border);
//        p.drawRect(rt);
//        p.setPen(hightlight);
//        p.drawLine(rt.topLeft().x()-2,rt.topLeft().y(),rt.topRight().x()+2,rt.topRight().y());


//        QRect rb = QRect((i-1)*t.width()+q*4,height()-t.height()-1,t.width(),t.height());
//        p.fillRect(rb,background);
//        rb.adjust(3,0,-3,0);
//        p.setBrush(body);
//        p.setPen(border);
//        p.drawRect(rb);
//        p.setPen(hightlight);
//        p.drawLine(rb.topLeft().x()-2,rb.topLeft().y(),rb.topRight().x()+2,rb.topRight().y());
    }

    p.fillRect(this->rect(),QBrush(gradient));

    if(m_value == m_default){
        p.setPen(QColor(255,255,255,172));
    }else{
        //p.setPen(QColor(110,176,255));
        p.setPen(QColor(255,127,0,207));
    }

    p.setFont(font);
    p.drawText(this->rect(),Qt::AlignCenter,QString("%1x").arg(m_value, 0, ' ', 2));
}

} // namespace Avocado

