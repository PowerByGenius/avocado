#include "mainwindow.h"
#include <QApplication>
#include <QSqlDatabase>
#include <QSettings>

int main(int argc, char *argv[])
{
    QApplication::setApplicationName("Aguacate");
    QApplication::setApplicationDisplayName("Aguacate");
    QApplication::setApplicationVersion("1.0");

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/Users/kiko/Desktop/Library/Avocado/Qt/Avocado/database/avocado.sqlite");
    bool ok = db.open();
    QSettings().setValue("library_path","/Users/kiko/Desktop/media");

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
