#include "audiotrackcaption.h"
#include "audiotrack.h"
#include "audiotrackview.h"
#include <QPainter>
#include <QDebug>
#include <QStyleOptionGraphicsItem>

namespace Avocado {

AudioTrackCaption::AudioTrackCaption(AudioTrack *audioTrack, QGraphicsItem *parent) :
    QGraphicsTextItem(parent),
    m_audioTrack(audioTrack)
{
 //   this->setTextInteractionFlags(Qt::LinksAccessibleByKeyboard|Qt::LinksAccessibleByMouse);
}

AudioTrackCaption::AudioTrackCaption(AudioTrack *audioTrack,const QString &text, QGraphicsItem *parent):
    QGraphicsTextItem(text,parent),
    m_audioTrack(audioTrack)
{

}

QRectF AudioTrackCaption::boundingRect() const
{
    QRectF rect = QGraphicsTextItem::boundingRect();

    int length = data(Qt::UserRole+110).toInt() - data(Qt::UserRole+100).toInt();
    if(length>=rect.width())
    {
        rect.setWidth(length);
    }

    return rect;
}

void AudioTrackCaption::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QGraphicsTextItem::paint(painter,option,widget);

    int length = data(Qt::UserRole+110).toInt() - data(Qt::UserRole+100).toInt();
    int index = data(Qt::UserRole+120).toInt();

    if(index % 2 > 0)
    {
        painter->setPen(QColor(204, 0, 0,128));
        painter->drawLine(this->boundingRect().left(),
                          this->boundingRect().height(),
                          length,this->boundingRect().height());

    }else{
        painter->setPen(QColor(153, 255, 0,128));
        painter->drawLine(this->boundingRect().left(),
                          this->boundingRect().top(),
                          length,this->boundingRect().top());
    }
}

void AudioTrackCaption::setCaptionText(const QString &text)
{
//    QStringList buff = text.split(' ');
//    for (int i = 0; i < buff.length(); ++i) {
//        QString t = QString("<a href=\"#\" style=\"color: white;text-decoration:none;\">%1</a>").arg(buff.at(i));
//        buff[i] = t;
//    }
//    this->setHtml(buff.join(" "));
    this->setPlainText(text);
}


} // namespace Avocado

