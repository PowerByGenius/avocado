#include "audiotrackselection.h"
#include "audiotrack.h"
#include "audiotrackview.h"
#include <QDebug>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

namespace Avocado {


AudioTrackSelectionHandle::AudioTrackSelectionHandle (AudioTrackSelection *parent):
    QGraphicsItem(parent),
    m_trackSelection(parent)
{
    Q_UNUSED(m_trackSelection)
}

void AudioTrackSelectionHandle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->fillRect(this->boundingRect(),Qt::red);

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

QRectF AudioTrackSelectionHandle::boundingRect() const
{
    return QRectF();
}

void AudioTrackSelectionHandle::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseMoveEvent(event);
}

void AudioTrackSelectionHandle::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
}

void AudioTrackSelectionHandle::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
}


AudioTrackSelection::AudioTrackSelection(AudioTrack *audioTrack, QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_audioTrack(audioTrack),
    m_start(0),m_end(0),isMoving(false)
{
    this->setZValue(10000);
//    this->setFlag(QGraphicsItem::ItemIsMovable,true);
}

void AudioTrackSelection::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->fillRect(this->boundingRect(),QColor(255,0,0,64));
}

QRectF AudioTrackSelection::boundingRect() const
{
    qreal height = m_audioTrack->view()->viewport()->height();
    return QRectF(m_start,0,m_end,height);
}

void AudioTrackSelection::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseMoveEvent(event);
}

void AudioTrackSelection::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_buttonDownPos = event->buttonDownPos(Qt::LeftButton);
    QGraphicsObject::mousePressEvent(event);
}

void AudioTrackSelection::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseReleaseEvent(event);
}



} // namespace Avocado

