#include "audiotrackallviewwaveform.h"
#include "audiotrack.h"
#include <QPainter>
#include <QDebug>

namespace Avocado {

AudioTrackAllViewWaveform::AudioTrackAllViewWaveform(AudioTrackAllView *view,QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_view(view)
{
    m_waveform = QPixmap(QSize(boundingRect().width(),boundingRect().height()));
}

QRectF AudioTrackAllViewWaveform::boundingRect() const
{
    return QRectF(m_view->marginLeft(),0,m_view->viewportWidth()-m_view->marginRight()-m_view->marginLeft(),m_view->viewportHeight());
}

void AudioTrackAllViewWaveform::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->drawPixmap(boundingRect().toRect(),m_waveform);
}

void AudioTrackAllViewWaveform::updateWaveform()
{
    if(m_view->m_audioTrack.isNull()) return;

    qint64 count = m_view->m_audioTrack.data()->audioWaveformData().count();
    if(count <=0) return;

    qreal width = 2048;
    qreal height = this->boundingRect().height();

    qreal q = height/255;
    qreal s = count/width;

    m_waveform = QPixmap(QSize(width,height));
    m_waveform.fill(Qt::transparent);
    QPainter painter(&m_waveform);
    painter.setPen(QColor(255,204,0,16));

    for (int i = 0; i < count; ++i) {
        int data = m_view->m_audioTrack.data()->audioWaveformData().at(i);
        int y_min = (data >> 8) & 0xFF;
        int y_max = data & 0xFF;
        for (int p = y_min*q; p < y_max*q; ++p) {
            painter.drawPoint(QPointF(i/s,p));
        }
    }
}

} // namespace Avocado

