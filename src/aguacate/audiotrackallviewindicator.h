#ifndef AVOCADO_AUDIOTRACKALLVIEWINDICATOR_H
#define AVOCADO_AUDIOTRACKALLVIEWINDICATOR_H

#include <QGraphicsObject>

namespace Avocado {
class AudioTrackAllView;
class AudioTrackAllViewIndicator : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit AudioTrackAllViewIndicator(AudioTrackAllView *view, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    void setPosition(const qint64 &position);
    qint64 position() const {return m_position;}
    void reLayout();
protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
private:
    AudioTrackAllView *m_view;
    QPointF m_buttonDownPos;
    qint64 m_position;
    QPolygonF m_polygon;
    bool m_isDraggable;
signals:
    void positionChanged(const qint64 &position);
public slots:
};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKALLVIEWINDICATOR_H
