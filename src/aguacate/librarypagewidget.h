#ifndef LIBRARYPAGEWIDGET_H
#define LIBRARYPAGEWIDGET_H

#include <QWidget>
#include <QStandardItemModel>
#include <QIdentityProxyModel>
#include <QSortFilterProxyModel>
#include <QSqlQueryModel>
#include <QItemDelegate>
#include <QThread>

namespace Ui {
class LibraryPageWidget;
}

namespace Avocado {
class LibraryFilesImport;
}

class LibraryItemsDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit LibraryItemsDelegate(QObject *parent = 0);
protected:
//    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;
    QSize sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const;
public slots:
};



class LibraryPageWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LibraryPageWidget(QWidget *parent = 0);
    ~LibraryPageWidget();
    void appSaveState();
    void appResotreState();
protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
private:
    void init();
private:
    Ui::LibraryPageWidget *ui;
    QThread m_libraryFileImportThread;
    Avocado::LibraryFilesImport *m_libraryFileImport;

private:
    QStandardItemModel *m_libraryNavigationTreeModel;
    QStandardItemModel *m_libraryItemsModel;
    QSortFilterProxyModel *m_libraryItemsListModel;

    QIdentityProxyModel *m_libraryProxyModel;
    QStandardItemModel *m_artistsTreeModel;
    QSortFilterProxyModel *m_artistsListModel;
    QSortFilterProxyModel *m_artistsAlbumListModel;
    QStandardItemModel *m_albumsTreeModel;
    QSortFilterProxyModel *m_albumsListModel;

private slots:
    void onItemActivated(const QModelIndex &index);
signals:
    void itemActivated(const QVariantMap &data);

public slots:
    void reflash();
private slots:
    void importFilesFinished(const QVariantList &list);
signals:
    void requestImportFiles(const QStringList &urls);
};

#endif // LIBRARYPAGEWIDGET_H
