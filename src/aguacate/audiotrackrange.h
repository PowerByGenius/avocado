#ifndef AUDIOTRACKRANGE_H
#define AUDIOTRACKRANGE_H

#include <QGraphicsObject>

namespace Avocado {
class AudioTrack;
class AudioTrackRange;

class AudioTrackRangeHandle : public QGraphicsItem
{
public:
    enum HandleTypes{HandleStart,HandleEnd};
    explicit AudioTrackRangeHandle(const HandleTypes types,AudioTrackRange *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
private:
    AudioTrackRange *m_trackRange;
    HandleTypes m_handleType;
    QPointF m_buttonDownPos;
    int m_size;
signals:

public slots:
};


class AudioTrackRange : public QGraphicsObject
{
    Q_OBJECT
public:
    friend class AudioTrackRangeHandle;
    explicit AudioTrackRange(AudioTrack *audioTrack,QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;

    inline void setRangePosStart(qreal value){m_posStart=value;prepareGeometryChange();}
    inline void setRangePosEnd(qreal value){m_posEnd = value;prepareGeometryChange();}

    qreal rangePosStart() {return m_posStart;}
    qreal rangePosEnd() {return m_posEnd;}

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);

private:
    AudioTrack *m_audioTrack;
    AudioTrackRangeHandle *m_handleStart;
    AudioTrackRangeHandle *m_handleEnd;
    qreal m_posStart;
    qreal m_posEnd;
    bool isMoving;
    QPointF m_buttonDownPos;
signals:

public slots:
};

}

#endif // AUDIOTRACKRANGE_H
