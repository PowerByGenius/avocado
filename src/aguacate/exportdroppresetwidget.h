#ifndef AVOCADO_EXPORTDROPPRESETWIDGET_H
#define AVOCADO_EXPORTDROPPRESETWIDGET_H

#include <QWidget>

namespace Avocado {

namespace Ui {
class ExportDropPresetWidget;
}

class ExportDropPresetWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ExportDropPresetWidget(QWidget *parent = 0);
    ~ExportDropPresetWidget();
    inline void setArgument(const QStringList &argument){m_argument=argument;}
    inline QStringList argument()const{return m_argument;}

    void setPixmap(const QPixmap &pixmap);
    const QPixmap * pixmap();
    void setText(const QString &text);

protected:
    virtual void setPresetMap(QVariantMap *presetMap) = 0;

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dropEvent(QDropEvent *event);
    void paintEvent(QPaintEvent *event);

private:
    Ui::ExportDropPresetWidget *ui;

protected:
    QStringList m_argument;
    bool m_isDragEnter;

signals:
    void dropAccepted(const QVariantMap &map);
};


class ExportDropPresetWidgetAudio : public ExportDropPresetWidget
{
    Q_OBJECT
public:
    explicit ExportDropPresetWidgetAudio(QWidget *parent = 0);
protected:
    void setPresetMap(QVariantMap *presetMap);
};


class ExportDropPresetWidgetVideo : public ExportDropPresetWidget
{
    Q_OBJECT
public:
    explicit ExportDropPresetWidgetVideo(QWidget *parent = 0);
protected:
    void setPresetMap(QVariantMap *presetMap);
};


class ExportDropPresetWidgetYoutube : public ExportDropPresetWidget
{
    Q_OBJECT
public:
    explicit ExportDropPresetWidgetYoutube(QWidget *parent = 0);
protected:
    void setPresetMap(QVariantMap *presetMap);
};


class ExportDropPresetWidgetITunes : public ExportDropPresetWidget
{
    Q_OBJECT
public:
    explicit ExportDropPresetWidgetITunes(QWidget *parent = 0);
protected:
    void setPresetMap(QVariantMap *presetMap);
};



} // namespace Avocado
#endif // AVOCADO_EXPORTDROPPRESETWIDGET_H
