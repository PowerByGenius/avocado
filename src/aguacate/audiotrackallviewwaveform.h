#ifndef AVOCADO_AUDIOTRACKALLVIEWWAVEFORM_H
#define AVOCADO_AUDIOTRACKALLVIEWWAVEFORM_H

#include <QGraphicsObject>
#include "audiotrackallview.h"

namespace Avocado {
class AudioTrackAllView;

class AudioTrackAllViewWaveform : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit AudioTrackAllViewWaveform(AudioTrackAllView *view, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    void updateWaveform();
private:
    AudioTrackAllView *m_view;
    QPixmap m_waveform;
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKALLVIEWWAVEFORM_H
