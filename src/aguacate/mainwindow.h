#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
//#include <QtAV>

namespace Avocado{
class ExportDropWidget;
}

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:
    void resizeEvent(QResizeEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
private:
    Ui::MainWindow *ui;
    //QtAV::AVPlayer *m_player;
    QMediaPlayer *m_player;
    Avocado::ExportDropWidget *m_exportDropWidget;
private slots:
    void onLibraryItemActivated(const QVariantMap &data);
    void on_btnRangeSetA_clicked();
    void on_btnRangeSetB_clicked();
    void on_btnRangeClean_clicked();
    void on_btnMarkAddCurrent_clicked();
    void on_btnLoop_toggled(bool checked);
};

#endif // MAINWINDOW_H
