#include "audiotrackrange.h"
#include "audiotrack.h"
#include "audiotrackview.h"
#include "audiotrackwaveform.h"
#include <QDebug>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

namespace Avocado {


AudioTrackRangeHandle::AudioTrackRangeHandle (const HandleTypes types,AudioTrackRange *parent):
    QGraphicsItem(parent),
    m_trackRange(parent),
    m_handleType(types),
    m_size(20)
{
this->setFlag(QGraphicsItem::ItemIsMovable,true);
    this->setData(Qt::UserRole+100,true); // unhanddragable
}

void AudioTrackRangeHandle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->fillRect(this->boundingRect(),QColor(255, 102, 0,207));
}

QRectF AudioTrackRangeHandle::boundingRect() const
{
    switch(m_handleType){
    case HandleStart:
        return QRectF(m_trackRange->boundingRect().left()-m_size,0,m_size,m_size);
        break;
    case HandleEnd:
        return QRectF(m_trackRange->boundingRect().right(),0,m_size,m_size);
        break;
    }
    return QRectF(0,0,0,0);
}

void AudioTrackRangeHandle::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    event->accept();
    switch(m_handleType){
    case HandleStart:
    {
        qreal x = event->scenePos().rx()+m_buttonDownPos.rx();
        if(x<0) x =0;
        if(x>m_trackRange->rangePosEnd()) x = m_trackRange->rangePosEnd();
        m_trackRange->setRangePosStart(x);
        break;
    }

    case HandleEnd:
    {
        qreal x = event->scenePos().rx() - m_buttonDownPos.rx();

        if(x < m_trackRange->rangePosStart())
        {
            x = m_trackRange->rangePosStart();
        }
        if(x> m_trackRange->m_audioTrack->length())
        {
            x = m_trackRange->m_audioTrack->length();
        }
        m_trackRange->setRangePosEnd(x);
        break;
    }

    }
//    QGraphicsItem::mouseMoveEvent(event);
}

void AudioTrackRangeHandle::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_buttonDownPos = QPointF(event->scenePos().rx()
                              -mapToScene(this->boundingRect().topLeft()).rx(),0);

    QGraphicsItem::mousePressEvent(event);
}

void AudioTrackRangeHandle::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
}


AudioTrackRange::AudioTrackRange(AudioTrack *audioTrack, QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_audioTrack(audioTrack),
    m_handleStart(new AudioTrackRangeHandle(AudioTrackRangeHandle::HandleStart,this)),
    m_handleEnd(new AudioTrackRangeHandle(AudioTrackRangeHandle::HandleEnd,this)),
    m_posStart(0),m_posEnd(0),isMoving(false)
{
    this->setZValue(1000);
}

void AudioTrackRange::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->fillRect(this->boundingRect(),QColor(255, 102, 0,64));
}

QRectF AudioTrackRange::boundingRect() const
{
    qreal height = m_audioTrack->view()->viewport()->height();
    return QRectF(m_posStart,0,m_posEnd-m_posStart,height);
}

void AudioTrackRange::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseMoveEvent(event);
}

void AudioTrackRange::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_buttonDownPos = event->buttonDownPos(Qt::LeftButton);
    QGraphicsObject::mousePressEvent(event);
}

void AudioTrackRange::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseReleaseEvent(event);
}



} // namespace Avocado

