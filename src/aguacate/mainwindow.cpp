#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "exportdropwidget.h"
#include <QDragEnterEvent>
#include <QMimeData>
#include <QPushButton>
#include <QFileInfo>
#include <QSettings>
#include <QDebug>
#include <QSqlQuery>

const QString SqlLibraryTableName = "avocado_media";

qint64 textToPosition(const QString &text)
{
    qreal time = 0;
    QRegExp rx("(\\d{2}):(\\d{2}):(\\d{2}),(\\d{3})");
    if(rx.exactMatch(text))
    {
         time = rx.cap(4).toInt()
                +rx.cap(3).toInt() * 1000
                +rx.cap(2).toInt() * 60  * 1000
                +rx.cap(1).toInt() * 60 * 60 * 1000;
    }
    return time;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    //m_player(new QtAV::AVPlayer(this)),
    m_player(new QMediaPlayer(this)),
    m_exportDropWidget(new Avocado::ExportDropWidget(this))
{
    ui->setupUi(this);
    setMinimumSize(QSize(800,500));
    setWindowTitle(QApplication::applicationDisplayName());
    setAcceptDrops(true);

    ui->splitterMedia->setStretchFactor(0,1);
    ui->splitterMedia->setStretchFactor(1,0);
    ui->splitterMedia->setCollapsible(1,false);

    ui->splitterMain->setStretchFactor(0,0);
    ui->splitterMain->setStretchFactor(1,1);
    ui->splitterMain->setCollapsible(0,false);

    ui->splitterContent->setStretchFactor(0,1);
    ui->splitterContent->setStretchFactor(1,0);

    ui->audioTrackControl->hide();

    ui->toolBar->addWidget(ui->controllerWidget);

    m_exportDropWidget->raise();


    {
        QPushButton *button = new QPushButton(statusBar());
        button->setMaximumHeight(24);
        button->setFlat(true);
        button->setText(tr("Library"));
        statusBar()->addWidget(button,0);
        connect(button,&QPushButton::clicked,[=](){
            ui->stackedWidgetPage->setCurrentIndex(0);
        });
    }

    {
        QPushButton *button = new QPushButton(statusBar());
        button->setMaximumHeight(24);
        button->setFlat(true);
        button->setText(tr("Mark"));
        statusBar()->addWidget(button,0);
        connect(button,&QPushButton::clicked,[=](){
            ui->stackedWidgetPage->setCurrentIndex(1);
        });
    }

    {
        QPushButton *button = new QPushButton(statusBar());
        button->setMaximumHeight(24);
        button->setFlat(true);
        button->setText(tr("Search"));
        statusBar()->addWidget(button,0);
        connect(button,&QPushButton::clicked,[=](){
            ui->stackedWidgetPage->setCurrentIndex(2);
        });
    }


    {
        QPushButton *button = new QPushButton(statusBar());
        button->setMaximumHeight(24);
        button->setFlat(true);
        button->setText(tr("Export"));
        button->setCheckable(true);
        statusBar()->addPermanentWidget(button,0);
        connect(button,&QPushButton::toggled,[=](bool checked){
            if(checked){
                ui->stackedRightSide->show();
            }else{
                ui->stackedRightSide->hide();
            }
        });
        ui->stackedRightSide->hide();
    }

   ui->audioTrackAllView->setAudioTrackView(ui->audioTrackView);
   ui->markListView->setAudioTrackView(ui->audioTrackView);


    connect(ui->pageLibrary,&LibraryPageWidget::itemActivated,
            this,&MainWindow::onLibraryItemActivated);


    connect(m_exportDropWidget,&Avocado::ExportDropWidget::dropAccepted,
            ui->exportProcessWidget,&Avocado::ExportProcessWidget::addTask);

    connect(ui->markListView,&Avocado::AudioTrackMarkListView::dragIgnoreAction,
            m_exportDropWidget,&Avocado::ExportDropWidget::hide);


    m_player->setNotifyInterval(10);

    connect(ui->wheelSpeedWidget,&Avocado::WheelSpeed::valueChanged,
            m_player,&QMediaPlayer::setPlaybackRate);


    connect(m_player,&QMediaPlayer::positionChanged,[=](qint64 position){
        if(m_player->property("loadAndSetPosition").toBool())
        {
            m_player->setPosition(ui->audioTrackView->rangeABPositionStart()*10);
            m_player->setProperty("loadAndSetPosition",false);
            return;
        }

        if(ui->audioTrackView->isHandDragging())
        {
            if(position > ui->audioTrackView->currrentPosition()*10)
            {
                m_player->pause();
            }
        }

        if(ui->audioTrackView->isRangeABLoop())
        {
            if(position > ui->audioTrackView->rangeABPositionEnd()*10 || position < ui->audioTrackView->rangeABPositionStart()*10 )
            {
                m_player->setPosition(ui->audioTrackView->rangeABPositionStart()*10);
            }
        }

    });

    connect(ui->audioTrackView,&Avocado::AudioTrackView::handDragPosition,[=](qint64 position){

        m_player->pause();
        m_player->setPosition(position*10);
        m_player->play();
        m_player->setPlaybackRate(ui->wheelSpeedWidget->value());
    });


    connect(m_player,&QMediaPlayer::positionChanged,
            ui->audioTrackView,&Avocado::AudioTrackView::setPosition,Qt::QueuedConnection);


    connect(ui->btnPlay,&QPushButton::clicked,[=](bool checked){
        switch(m_player->state()){
        case QMediaPlayer::StoppedState:
            m_player->play();
            m_player->setPlaybackRate(ui->wheelSpeedWidget->value());
            break;
        case QMediaPlayer::PlayingState:
            m_player->pause();
            break;
        case QMediaPlayer::PausedState:
            m_player->play();
            m_player->setPlaybackRate(ui->wheelSpeedWidget->value());
            break;
        }
    });


    connect(m_player,&QMediaPlayer::positionChanged,ui->pageCaption,&Avocado::CaptionView::setPosition);
    connect(ui->pageCaption,&Avocado::CaptionView::positionChanged,m_player,&QMediaPlayer::setPosition);


    //search

    connect(ui->pageSearch,&SearchResultsPanel::captionItemDoubleClicked,[=](const QVariantMap &data){

        QStringList artist = QStringList() << data["meta_artist"].toString() << data["meta_album"].toString();
        ui->labelArtist->setText(artist.join(" - "));
        ui->labelTitle->setText(data["meta_title"].toString());

        QString coverPath = QString("%1/%2/40/%3.png")
                .arg(QSettings().value("library_path").toString())
                .arg("cover")
                .arg(data["meta_coverart"].toString());

        QString mediaPath = QString("%1/%2/%3")
                .arg(QSettings().value("library_path").toString())
                .arg("media")
                .arg(data["file_path"].toString());

        QString wavePath = QString("%1/%2/%3.data")
                .arg(QSettings().value("library_path").toString())
                .arg("waveform")
                .arg(data["file_hash"].toString());



        m_player->stop();
        ui->audioTrackView->setMediaUID(data["unique_id"].toString());
        ui->audioTrackView->setMediaFilePath(mediaPath);
        ui->audioTrackView->loadWaveformFile(wavePath);
        ui->audioTrackView->markStoreLoadAll();


        if(QFileInfo(coverPath).exists())
        {
            ui->labelCover->setPixmap(QPixmap(coverPath));
            //.scaled(40,40,Qt::KeepAspectRatio,Qt::SmoothTransformation)
        }

        QSqlQuery query(QString("SELECT meta_caption_en from %1 WHERE unique_id='%2'").arg(SqlLibraryTableName).arg(data["unique_id"].toString()));
        if(query.next())
        {
            ui->audioTrackView->loadSrtText(query.value("meta_caption_en").toString());
            ui->pageCaption->loadSrtText(query.value("meta_caption_en").toString());
        }

        ui->audioTrackView->setRangeAB(textToPosition(data["caption_start"].toString())/10,
                                       textToPosition(data["caption_end"].toString())/10);

        ui->audioTrackView->modifyPosition(ui->audioTrackView->rangeABPositionStart());
        m_player->stop();
        m_player->setMedia(QUrl::fromLocalFile(mediaPath));
        m_player->setProperty("loadAndSetPosition",true);
//        m_player->play();

//        m_player->setPosition(textToPosition(data["caption_start"].toString()));
//        emit ui->audioTrackView->handDragPosition(textToPosition(data["caption_start"].toString())/10);
//        ui->audioTrackView->modifyPosition(ui->audioTrackView->rangeABPositionStart());



    });

//    m_player->setNotifyInterval(10);
//    m_player->setSeekType(QtAV::AnyFrameSeek);

//    connect(ui->wheelSpeedWidget,&Avocado::WheelSpeed::valueChanged,
//            m_player,&QtAV::AVPlayer::setSpeed);

//    connect(m_player,&QtAV::AVPlayer::positionChanged,
//            ui->audioTrackView,&Avocado::AudioTrackView::setPosition,Qt::QueuedConnection);

//    connect(ui->audioTrackView,&Avocado::AudioTrackView::handDragPosition,[=](qint64 position){
//        m_player->seek(position);
//    });

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::resizeEvent(QResizeEvent *event)
{
    QRect rect = this->rect();
    m_exportDropWidget->move(rect.width()-m_exportDropWidget->width()-width()*0.05,rect.height()/2-m_exportDropWidget->height()/2);
    QMainWindow::resizeEvent(event);
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasFormat("application/x-track-mark"))
    {
        m_exportDropWidget->show();
    }

    QMainWindow::dragEnterEvent(event);
}

void MainWindow::onLibraryItemActivated(const QVariantMap &data)
{
    QStringList artist = QStringList() << data["meta_artist"].toString() << data["meta_album"].toString();
    ui->labelArtist->setText(artist.join(" - "));
    ui->labelTitle->setText(data["meta_title"].toString());

    QString coverPath = QString("%1/%2/40/%3.png")
            .arg(QSettings().value("library_path").toString())
            .arg("cover")
            .arg(data["meta_coverart"].toString());

    QString mediaPath = QString("%1/%2/%3")
            .arg(QSettings().value("library_path").toString())
            .arg("media")
            .arg(data["file_path"].toString());

    QString wavePath = QString("%1/%2/%3.data")
            .arg(QSettings().value("library_path").toString())
            .arg("waveform")
            .arg(data["file_hash"].toString());

    ui->audioTrackView->setMediaUID(data["unique_id"].toString());
    ui->audioTrackView->setMediaFilePath(mediaPath);
    ui->audioTrackView->loadWaveformFile(wavePath);
    ui->audioTrackView->loadSrtText(data["meta_caption_en"].toString());
    ui->audioTrackView->markStoreLoadAll();
    ui->pageCaption->loadSrtText(data["meta_caption_en"].toString());


    if(QFileInfo(coverPath).exists())
    {
        ui->labelCover->setPixmap(QPixmap(coverPath));
        //.scaled(40,40,Qt::KeepAspectRatio,Qt::SmoothTransformation)
    }

    m_player->setMedia(QUrl::fromLocalFile(mediaPath));
//    m_player->play(mediaPath);
//    m_player->setSpeed(ui->wheelSpeedWidget->value());

}

void MainWindow::on_btnRangeSetA_clicked()
{
    ui->audioTrackView->setCurrentPositionForRangeA();
}

void MainWindow::on_btnRangeSetB_clicked()
{
    ui->audioTrackView->setCurrentPositionForRangeB();
}

void MainWindow::on_btnRangeClean_clicked()
{
    ui->audioTrackView->cleanRangeAB();
}

void MainWindow::on_btnMarkAddCurrent_clicked()
{
    ui->audioTrackView->addMarkOnRangeABPosition();
}

void MainWindow::on_btnLoop_toggled(bool checked)
{
    ui->audioTrackView->setRangeABLoop(checked);
}
