#ifndef AVOCADO_AUDIOTRACK_H
#define AVOCADO_AUDIOTRACK_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include "audiotrackmark.h"

namespace Avocado {

class AudioTrack;
class AudioTrackView;
class AudioTrackWaveform;
class AudioTrackSelection;
class AudioTrackRange;
class AudioTrackMark;
class AudioTrackRangeAB;
class AudioTrackCaption;


class AudioTrackMarginItem : public QGraphicsItem
{
public:
    explicit AudioTrackMarginItem(AudioTrack *audioTrack,QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;
    void relayout();
private:
    AudioTrack *m_audioTrack;
signals:

public slots:
};



class AudioTrack : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit AudioTrack(AudioTrackView *audioTrackView,QObject *parent = 0);
    inline AudioTrackView* view(){return m_trackView;}
    void relayout();

private:
    AudioTrackView *m_trackView;
    AudioTrackMarginItem *m_marginItem;

    // Mouse Event
    // --------------------------------
protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * mouseEvent);
    void mousePressEvent(QGraphicsSceneMouseEvent * mouseEvent);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * mouseEvent);
private:
    QPointF m_buttonDownScenePos;

    // WaveformData
    // --------------------------------
public :
    inline AudioTrackWaveform *audioTrackWaveform(){return m_trackWaveform;}
    QList<qint16> audioWaveformData() const {return m_audioWaveformData;}
private:
    AudioTrackWaveform *m_trackWaveform;
    QList<qint16> m_audioWaveformData;

    // Position
    // --------------------------------
public:
    qint64 currentPosition() const;
    qint64 length() const;

    // Section
    // --------------------------------
    /*
public:
    qint64 selectedPositionStart();
    qint64 selectedPositionEnd();
private:
    AudioTrackSelection *m_trackSelection;
    bool isStartedStelect;
    */

    // Caption
    // --------------------------------
public:
    void showCaption(bool value);
    void setCaptionTextBrowserInteraction(bool value);
private:
    QList<AudioTrackCaption*> m_captionItems;

    // Range AB Position
    // --------------------------------
public:
    qint64 rangeABPositionStart() const;
    qint64 rangeABPositionEnd() const;

    void setCurrentPositionForRangeA();
    void setCurrentPositionForRangeB();
    void setRangeAB(qint64 start,qint64 end);
    void cleanRangeAB();
    bool isRangeABLoop();
    void setRangeABLoop(bool value);

private:
    AudioTrackRangeAB *m_rangeAB;
    bool m_isRangeABLoop;

    // Range Position
    // --------------------------------
private:
    QList<AudioTrackRange*> m_rangeItems;


    // Mark Position
    // --------------------------------
public:
     int indexOfMark(AudioTrackMark *mark) const;
     int findMarkIndexByPostion(const qint64 &start,const qint64 &end);
     bool addMarkOnRangeABPosition();
     void removeMark(AudioTrackMark *mark);
     void reIndexMark(AudioTrackMark *mark);
     void marksCleanAll();

     void markStoreCreate(AudioTrackMark *mark);
     void markStoreUpdate(AudioTrackMark *mark);
     void markStoreDelete(AudioTrackMark *mark);
     void markStoreLoadAll();

private:
    QList<AudioTrackMark*> m_markItems;

    // Files load
    // --------------------------------
public:
    void loadWaveformFile(const QString &filepath);
    void loadSrtFile(const QString &filepath);
    void loadSrtText(const QString &text);

signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACK_H
