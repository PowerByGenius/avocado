#include "exportprocesswidget.h"
#include "ui_exportprocesswidget.h"
#include <QDebug>

namespace Avocado {

ExportProcessWidget::ExportProcessWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ExportProcessWidget)
{
    ui->setupUi(this);
}

ExportProcessWidget::~ExportProcessWidget()
{
    delete ui;
}

void ExportProcessWidget::addTask(const QVariantMap &map)
{
    ui->listWidget->addTask(map);
}

} // namespace Avocado
