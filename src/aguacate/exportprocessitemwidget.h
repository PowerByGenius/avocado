#ifndef AVOCADO_EXPORTPROCESSITEMWIDGET_H
#define AVOCADO_EXPORTPROCESSITEMWIDGET_H

#include <QWidget>
#include <QListWidgetItem>


class QProcess;

namespace Avocado {

namespace Ui {
class ExportProcessItemWidget;
}

class ExportProcessItemWidget : public QWidget
{
    Q_OBJECT

public:
    enum States{Init,Processing,Done,Error};
    enum FileTypes{FileTypeAudio,FileTypeVideo};

    explicit ExportProcessItemWidget(QListWidgetItem *item,const QVariantMap &data,
                                     QWidget *parent = 0);
    ~ExportProcessItemWidget();
    void setState(States state);
    void start();
    void setSelected(bool check);
protected:
    void resizeEvent(QResizeEvent *event);

private:
    QMap<QString,QString> getMetadata(const QString &filepath);
    QString getTargetBasename(const QMap<QString,QString> &map) const;
private slots:
    void on_btnFinder_clicked();
    void on_btnCancel_clicked();
    void processingTime(const QString text);
signals:
    void finished(ExportProcessItemWidget *itemWidget);
private:
    Ui::ExportProcessItemWidget *ui;
    QListWidgetItem *m_item;
    QProcess *m_process;
    States m_state;
    FileTypes m_fileType;
    QString m_ffmpeg;
    QVariantMap m_data;
    QString m_targetPath;
    QString m_targetFolder;
};


} // namespace Avocado
#endif // AVOCADO_EXPORTPROCESSITEMWIDGET_H
