#ifndef AVOCADO_AUDIOTRACKSELECTION_H
#define AVOCADO_AUDIOTRACKSELECTION_H

#include <QGraphicsObject>

namespace Avocado {
class AudioTrack;
class AudioTrackSelection;

class AudioTrackSelectionHandle : public QGraphicsItem
{
public:
    explicit AudioTrackSelectionHandle(AudioTrackSelection *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
private:
    AudioTrackSelection *m_trackSelection;
signals:

public slots:
};


class AudioTrackSelection : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit AudioTrackSelection(AudioTrack *audioTrack,QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;
    inline void setSelectPosStart(qreal value){m_start=value;prepareGeometryChange();}
    inline void setSelectPosEnd(qreal value){m_end = value;prepareGeometryChange();}
    qreal selectPosStart() {return m_start;}
    qreal selectPosEnd() {return m_end;}

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);

private:
    AudioTrack *m_audioTrack;
    qreal m_start;
    qreal m_end;
    bool isMoving;
    QPointF m_buttonDownPos;
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKSELECTION_H
