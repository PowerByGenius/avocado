#ifndef MAINTOOLBARWIDGET_H
#define MAINTOOLBARWIDGET_H

#include <QWidget>

class MainToolbarWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainToolbarWidget(QWidget *parent = 0);
private:
    QPoint m_dragPosition;
    bool m_isDragging;

signals:

public slots:

    // QWidget interface
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
};

#endif // MAINTOOLBARWIDGET_H
