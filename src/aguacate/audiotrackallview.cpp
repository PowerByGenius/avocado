#include "audiotrackallview.h"
#include "audiotrackallviewtrack.h"
#include "audiotrackallviewindicator.h"
#include "audiotrackallviewrangeab.h"

namespace Avocado {



AudioTrackAllView::AudioTrackAllView(QWidget *parent) :
    QGraphicsView(parent),
    m_allViewTrack(new AudioTrackAllViewTrack(this,this)),
    m_audioTrackView(NULL),
    m_audioTrack(NULL)
{
    setResizeAnchor(QGraphicsView::NoAnchor);
    setAlignment(Qt::AlignTop|Qt::AlignLeft);
    setDragMode(QGraphicsView::NoDrag);
    ensureVisible(0,0,0,0);
    setBackgroundBrush(QColor(51,51,51));
    setViewportUpdateMode(FullViewportUpdate);
    setScene(m_allViewTrack);
}

void AudioTrackAllView::setAudioTrackView(AudioTrackView *audioTrackView)
{
    m_audioTrackView = audioTrackView;
    m_audioTrack = audioTrackView->track();

    connect(m_audioTrackView,&AudioTrackView::waveformFileHasChanged,
            m_allViewTrack,&AudioTrackAllViewTrack::waveformFileHasChanged);

    connect(m_audioTrackView,&AudioTrackView::handDragPosition,
            m_allViewTrack,&AudioTrackAllViewTrack::setPosition);

    connect(m_audioTrackView,&AudioTrackView::positionChanged,
            m_allViewTrack,&AudioTrackAllViewTrack::setPosition);


    connect(m_allViewTrack->indicator(),&AudioTrackAllViewIndicator::positionChanged,
            m_audioTrackView,&AudioTrackView::modifyPosition);

    connect(m_audioTrackView,&AudioTrackView::rangeABValueHasChanged,
            m_allViewTrack,&AudioTrackAllViewTrack::modifyRageABPosition);

    connect(m_allViewTrack->rangeAB(),&AudioTrackAllViewRangeAB::rangePositionChanged,
            m_audioTrackView,&AudioTrackView::setRangeAB);

    connect(m_audioTrackView,&AudioTrackView::setRangeABLoopHasChanged,
            m_allViewTrack,&AudioTrackAllViewTrack::setRangeABLoop);


    connect(m_audioTrackView,&AudioTrackView::markInserted,
            m_allViewTrack,&AudioTrackAllViewTrack::audioTrackMarkInsert);

    connect(m_audioTrackView,&AudioTrackView::markAboutToBeRemoved,
            m_allViewTrack,&AudioTrackAllViewTrack::audioTrackMarkRemoved);

    connect(m_audioTrackView,&AudioTrackView::markDataChanged,
            m_allViewTrack,&AudioTrackAllViewTrack::audioTrackDataChanged);


}

qreal AudioTrackAllView::marginLeft() const
{
    return 120;
}

qreal AudioTrackAllView::marginRight() const
{
    return 120;
}

qreal AudioTrackAllView::viewportWidth() const
{
    return viewport()->rect().width();
}

qreal AudioTrackAllView::viewportHeight() const
{
    return viewport()->rect().height();
}

qint64 AudioTrackAllView::length() const
{
    if(m_audioTrack.isNull()) return 0;
    return m_audioTrack.data()->length();
}

qint64 AudioTrackAllView::currentPosition() const
{
    if(m_audioTrack.isNull()) return 0;
    return m_audioTrack.data()->currentPosition();
}

qreal AudioTrackAllView::waveformLength() const
{
    return m_allViewTrack->waveformLength();
}


void AudioTrackAllView::drawBackground(QPainter *painter, const QRectF &rect)
{
    QGraphicsView::drawBackground(painter,rect);
}

void AudioTrackAllView::resizeEvent(QResizeEvent *event)
{
    m_allViewTrack->reLayout();
    QGraphicsView::resizeEvent(event);
}

} // namespace Avocado

