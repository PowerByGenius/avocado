#include "captionview.h"
#include "ui_captionview.h"
#include <QPainter>
#include <QPlainTextEdit>
#include <QTextStream>
#include <QVBoxLayout>

namespace Avocado {

QSize CaptionListDelegate::CaptionListDelegate::iconSize = QSize(32, 32);
int CaptionListDelegate::CaptionListDelegate::padding = 5;

CaptionListDelegate::CaptionListDelegate(CaptionListModel *model, QObject * parent):
    QStyledItemDelegate(parent),
    m_model(model)
{
//    m_headerFont = QApplication::font();
    m_headerFont.setPointSize(11);
//    m_headerFont.setBold(true);
//    m_subheaderFont = QApplication::font();
    m_subheaderFont.setPointSize(18);
}

void CaptionListDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    if(!index.isValid())
        return;

    painter->save();

    if (option.state & QStyle::State_Selected){
        QRect selectedRect = option.rect;
        selectedRect.setLeft(0);
        selectedRect.setWidth(7);
//        painter->fillRect(selectedRect, option.palette.highlight());
        painter->fillRect(selectedRect, Qt::darkGreen);
    }

    qint64 start = index.data(CaptionListModel::PositionStartRole).toReal();
    qint64 end = index.data(CaptionListModel::PositionEndRole).toReal();
    qint64 position = m_model->position();
    bool currentPosition = false;
    if(position >=start and position < end){
        currentPosition = true;
        QRect selectedRect = option.rect;

        selectedRect.setLeft(0);
        selectedRect.setWidth(7);
        painter->fillRect(selectedRect, Qt::darkRed);
    }

//    bool currentPosition = index.data(CaptionListModel::CurrentPositionRole).toBool();
//    if (currentPosition){
//        QRect selectedRect = option.rect;
//        selectedRect.setLeft(0);
//        selectedRect.setWidth(7);
//        painter->fillRect(selectedRect, Qt::darkRed);
//    }

    QString headerText = index.data(CaptionListModel::HeaderRole).toString();
    QString subheaderText = index.data(CaptionListModel::SubheaderRole).toString();
    QString seqText = index.data(CaptionListModel::SeqRole).toString();
    headerText = QString("%1 %2").arg(seqText).arg(headerText);

    QFontMetrics headerFm(m_headerFont);
    QFontMetrics subheaderFm(m_subheaderFont);

    QRect headerRect =
            headerFm.boundingRect(option.rect.left() + iconSize.width(), option.rect.top() + padding,
                                  option.rect.width() - iconSize.width(), 0,
                                  Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                  headerText);
    QRect subheaderRect =
            subheaderFm.boundingRect(headerRect.left(), headerRect.bottom()+padding,
                                     option.rect.width() - iconSize.width(), 0,
                                     Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                     subheaderText);

    painter->setPen(Qt::gray);
    painter->setFont(m_headerFont);
    painter->drawText(headerRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, headerText);

    painter->setPen(Qt::black);
    if (option.state & QStyle::State_Selected)
        painter->setPen(Qt::darkGreen);

    if (currentPosition)
        painter->setPen(Qt::darkRed);

    painter->setFont(m_subheaderFont);
    painter->drawText(subheaderRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, subheaderText);

    painter->restore();
}

QSize CaptionListDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(!index.isValid())
        return QSize();

    QString headerText = index.data(CaptionListModel::HeaderRole).toString();
    QString subheaderText = index.data(CaptionListModel::SubheaderRole).toString();
    QString seqText = index.data(CaptionListModel::SeqRole).toString();
    headerText = QString("%1 %2").arg(seqText).arg(headerText);

    QFontMetrics headerFm(m_headerFont);
    QFontMetrics subheaderFm(m_subheaderFont);


    QRect headerRect = headerFm.boundingRect(0, 0,
                                             option.rect.width() - iconSize.width(), 0,
                                             Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                             headerText);

    QRect subheaderRect = subheaderFm.boundingRect(0, 0,
                                                   option.rect.width() - iconSize.width(), 0,
                                                   Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                                   subheaderText);

    QSize size(option.rect.width(), headerRect.height() + subheaderRect.height() +  3*padding);

    /* Keep the minimum height needed in mind. */
    if(size.height()<iconSize.height())
        size.setHeight(iconSize.height());

    return size;
}

QWidget *CaptionListDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return new QPlainTextEdit(parent);
}


void CaptionListDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(!index.isValid())
        return;

    QString headerText = index.data(CaptionListModel::HeaderRole).toString();
    QString subheaderText = index.data(CaptionListModel::SubheaderRole).toString();
    QString seqText = index.data(CaptionListModel::SeqRole).toString();
    subheaderText = QString("%1 %2").arg(seqText).arg(subheaderText);
    QFontMetrics headerFm(m_headerFont);
    QFontMetrics subheaderFm(m_subheaderFont);

    QRect headerRect =
            headerFm.boundingRect(option.rect.left() + iconSize.width(), option.rect.top() + padding,
                                  option.rect.width() - iconSize.width(), 0,
                                  Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                  headerText);
    QRect subheaderRect =
            subheaderFm.boundingRect(headerRect.left(), headerRect.bottom()+padding,
                                     option.rect.width() - iconSize.width(), 0,
                                     Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                     subheaderText);

    QPlainTextEdit *textEdit = qobject_cast<QPlainTextEdit*>(editor);
    if(textEdit){

        textEdit->setAttribute(Qt::WA_MacShowFocusRect,false);

        QPalette p = textEdit->palette();
        p.setColor(QPalette::Active, QPalette::Base, Qt::darkRed);
        p.setColor(QPalette::Inactive, QPalette::Base, Qt::darkRed);
        p.setColor(QPalette::Active, QPalette::Text, Qt::white);
        p.setColor(QPalette::Inactive, QPalette::Text, Qt::white);
        textEdit->setPalette(p);

        textEdit->setFont(m_subheaderFont);
        subheaderRect.setHeight(subheaderRect.height()+padding);
        subheaderRect.setRight(option.rect.right());
        textEdit->setFrameShape(QPlainTextEdit::NoFrame);
        textEdit->setGeometry(subheaderRect);
        textEdit->setReadOnly(true);
    }
}


CaptionListModel::CaptionListModel(QObject *parent) :
    QStandardItemModel(parent),
    m_position(0)
{
}

void CaptionListModel::loadSrtText(const QString &text)
{
    QRegExp rx("(\\d+)\\s*(\\d{2}:\\d{2}:\\d{2},\\d{3}) --> (\\d{2}:\\d{2}:\\d{2},\\d{3})");

    QList<QVariantMap> list;
    int pos = 0;
    int posNext = 0;
    while ((pos = rx.indexIn(text, pos)) != -1)
    {
        QVariantMap map;
        map.insert("seq",rx.cap(1).trimmed());
        map.insert("startString",rx.cap(2).trimmed());
        map.insert("endString",rx.cap(3).trimmed());

        pos += rx.matchedLength();
        posNext = text.indexOf(rx,pos);
        QString tmp;
        if(posNext > 0){
            tmp = text.mid(pos,posNext-pos);
        }else{
            tmp = text.mid(pos);
        }
        map.insert("text",tmp.trimmed().replace("\n"," "));

        list.append(map);
    }

    this->loadData(list);
}

void CaptionListModel::loadSrtFile(const QString &path)
{
    QFile file(path);
    QTextStream textStream(&file);
    file.open(QFile::ReadOnly);
    QString str = textStream.readAll();
    loadSrtText(str);

}

void CaptionListModel::loadData(const QList<QVariantMap> &list)
{

    this->clear();

    for (int i = 0; i < list.size(); ++i) {

        QVariantMap map = list.at(i);
        QStandardItem *item = new QStandardItem();
        item->setEditable(false);
        item->setDragEnabled(false);
        QString seq = map.value("seq",QString()).toString();
        QString start = map.value("startString",QString()).toString();
        QString end = map.value("endString",QString()).toString();
        QString text = map.value("text",QString()).toString();


        item->setText(text);
        item->setData(start,CaptionListModel::HeaderRole);
        item->setData(text,CaptionListModel::SubheaderRole);
        item->setData(seq,CaptionListModel::SeqRole);
        item->setData(start,CaptionListModel::StartTimeRole);
        item->setData(end,CaptionListModel::EndTimeRole);
        item->setData(false,CaptionListModel::CurrentPositionRole);
        item->setData(StringToPosition(start),CaptionListModel::PositionStartRole);
        item->setData(StringToPosition(end),CaptionListModel::PositionEndRole);
        appendRow(item);
    }
}

qint64 CaptionListModel::StringToPosition(const QString &text)
{
    qreal time = 0;
    QRegExp rx("(\\d{2}):(\\d{2}):(\\d{2}),(\\d{3})");
    if(rx.exactMatch(text))
    {
         time = rx.cap(4).toInt()
                +rx.cap(3).toInt() * 1000
                +rx.cap(2).toInt() * 60  * 1000
                +rx.cap(1).toInt() * 60 * 60 * 1000;
    }
    return time;
}


CaptionListView::CaptionListView(QWidget *parent) : QListView(parent)
{
    setAttribute(Qt::WA_MacShowFocusRect,false);
    setFrameStyle(QFrame::NoFrame);
    //    setDragEnabled(true);
}

void CaptionListView::wheelEvent(QWheelEvent *event)
{
    emit wheel();
    QListView::wheelEvent(event);
}

void CaptionListView::resizeEvent(QResizeEvent *event)
{
    QListView::resizeEvent(event);
}



CaptionView::CaptionView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CaptionView),
    m_captionListModel(new CaptionListModel(this)),
    m_isAutoScroll(true)
{
    ui->setupUi(this);
    ui->captionListView->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->captionListView->setItemDelegate(new CaptionListDelegate(m_captionListModel,this));
    ui->captionListView->setModel(m_captionListModel);

    connect(ui->captionListView,&CaptionListView::doubleClicked,
            [this](const QModelIndex & index){
                emit positionChanged(index.data(CaptionListModel::PositionStartRole).toReal());
            });

    connect(ui->captionListView,&CaptionListView::wheel,
            [=](){
                ui->btnAutoScroll->setChecked(false);
            });

}

CaptionView::~CaptionView()
{
    delete ui;
}

void CaptionView::loadSrtFile(const QString &path)
{
    m_captionListModel->loadSrtFile(path);
}

void CaptionView::loadSrtText(const QString &text)
{
    m_captionListModel->loadSrtText(text);
}

void CaptionView::setPosition(const qint64 position)
{
    m_captionListModel->setPosition(position);
    for(int i=0;i<m_captionListModel->rowCount();i++)
    {
        QStandardItem *item = m_captionListModel->item(i);
        qint64 start = item->data(CaptionListModel::PositionStartRole).toReal();
        qint64 end = item->data(CaptionListModel::PositionEndRole).toReal();

        if(position >=start and position < end){
                ui->captionListView->viewport()->update();
                if(m_isAutoScroll)
                    ui->captionListView->scrollTo(item->index());
            break;
        }
    }
}

void CaptionView::setAutoScroll(bool value)
{
    m_isAutoScroll = value;
}

} // namespace Avocado

