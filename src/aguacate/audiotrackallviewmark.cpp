#include "audiotrackallviewmark.h"
#include "audiotrackallviewtrack.h"
#include "audiotrackallview.h"
#include "audiotrackview.h"
#include "audiotrack.h"
#include <QPainter>
#include <QDebug>

namespace Avocado {

AudioTrackAllViewMark::AudioTrackAllViewMark(AudioTrackAllView *view,AudioTrackMark *audioTrackMark, QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_view(view),
    m_audioTrackMark(audioTrackMark),
    m_positionStart(0),m_positionEnd(0)
{
}

void AudioTrackAllViewMark::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->fillRect(boundingRect(),QColor(170, 242, 0,16));
}

QRectF AudioTrackAllViewMark::boundingRect() const
{
    QRectF rect;
    if(parentItem()) rect = parentItem()->boundingRect();
    qreal xStart = 0;
    qreal xEnd = 10;
    if(!m_view->m_audioTrack.isNull())
    {
        if(m_view->m_audioTrack.data()->length() > 0)
        {
            qreal q =  rect.width() / m_view->m_audioTrack.data()->length();
            if(q<0 || q !=q || q > std::numeric_limits<qreal>::max() || q < -std::numeric_limits<qreal>::max()) q = 0;
            xStart = (m_positionStart * q);
            xEnd = (m_positionEnd * q);
        }else{
            xStart=0;
            xEnd = 0;
        }

    }

    //if(xEnd-xStart < 1) xEnd=1;
//    qDebug() << m_positionStart << m_positionEnd;
//    qDebug() << xEnd << xStart;
//    qDebug() << QRectF(rect.left()+xStart,0,(xEnd-xStart),rect.height());
    return QRectF(rect.left()+xStart,0,(xEnd-xStart),rect.height());
}

void AudioTrackAllViewMark::setPosition(const qint64 &start, const qint64 &end)
{
    m_positionStart = start;
    m_positionEnd = end;
    prepareGeometryChange();
}

void AudioTrackAllViewMark::reLayout()
{
    setPosition(m_positionStart,m_positionEnd);
}

} // namespace Avocado

