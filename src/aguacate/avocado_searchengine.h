#ifndef AVOCADO_SEARCHENGINE_H
#define AVOCADO_SEARCHENGINE_H

#include <QObject>
#include <QThread>
#include <QWaitCondition>

class QCLuceneIndexSearcher;

namespace Avocado {

class SearchEngine : public QObject
{
    Q_OBJECT
public:
    explicit SearchEngine(QObject *parent = 0);
    ~SearchEngine();

private:
    QMutex mutex;
    QWaitCondition waitCondition;
signals:
    void searchResult(const QVariantList &result);
public slots:
    void search(const QString &text);
};

} // namespace Avocado

#endif // AVOCADO_SEARCHENGINE_H
