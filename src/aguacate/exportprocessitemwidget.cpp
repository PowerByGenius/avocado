#include "exportprocessitemwidget.h"
#include "ui_exportprocessitemwidget.h"
#include <QFileInfo>
#include <QProcess>
#include <QRegExp>
#include <QTimer>
#include <QDebug>
namespace Avocado {

qint64 textToSec(const QString text){
    QRegExp reg("(\\d+):(\\d+):(\\d+)\\.(\\d+)");
    if(reg.indexIn(text) > -1)
    {
        return reg.cap(1).toInt()*3600+reg.cap(2).toInt()*60+reg.cap(3).toInt();
    }
    return 0;
}

QString toFileTime(const QString text){
    QRegExp reg("(\\d+):(\\d+):(\\d+)\\.(\\d+)");
    if(reg.indexIn(text) > -1)
    {
        //return reg.cap(1).toInt()*3600+reg.cap(2).toInt()*60+reg.cap(3).toInt();
        return QString("%1%2%3").arg(reg.cap(1)).arg(reg.cap(2)).arg(reg.cap(3));
    }
    return QString();
}

ExportProcessItemWidget::ExportProcessItemWidget(QListWidgetItem *item, const QVariantMap &data, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ExportProcessItemWidget),
    m_item(item),
    m_data(data),
    m_process(new QProcess(this))
{
    ui->setupUi(this);
    setState(Init);
    m_ffmpeg = "/opt/local/bin/ffmpeg";
    m_targetFolder = "/Users/kiko/Desktop/abc";


    m_process->setProcessChannelMode(QProcess::MergedChannels);

    //    connect(m_process,&QProcess::readyRead,[=]{
    //        qDebug() << "readyRead";
    //        qDebug() << m_process->readAll();
    //    });

    connect(m_process,&QProcess::readChannelFinished,[=]{
        //qDebug() << "readChannelFinished";
        setState(Done);
        emit finished(this);
    });

    connect(m_process,&QProcess::readyReadStandardOutput,[=]{

        QRegExp regHeader("ffmpeg version ([0-9\\.]+) Copyright \\(c\\) [0-9\\-]+ the FFmpeg developers");

        QRegExp regInputNotFound("(.*): No such file or directory");
        QRegExp regOutputNotFound("At least one output file must be specified");
        QRegExp regCannotAllocateMemory("(.*): Cannot allocate memory");
        QRegExp regError("^Error\\s+(.*)\n$");
        QRegExp regFileExist(QLatin1String("File '(.*)' already exists. Overwrite ?"));

        QRegExp regMetadataTag(QLatin1String("^\\s{2}Metadata:\\n$"));
        QRegExp regMetadataItem(QLatin1String("^\\s{4,}(\\w+)\\s{0,}:\\s{0,}(.*)\\s{0,}\\n$"));

        QRegExp regDurationTag(QLatin1String("^\\s{2}Duration: ([0-9:\\.]+).*\\n$"));

        QRegExp regProcessTime(QLatin1String("time=(\\d{2}:\\d{2}:\\d{2}\\.\\d{2})"));
        QRegExp regProcessFrame(QLatin1String("frame=\\s{0,}([0-9\\.]+)\\s{0,}fps=\\s{0,}([0-9\\.\\-]+)\\s{0,}q=\\s{0,}([0-9\\.\\-]+)\\s{0,}size=\\s{0,}([0-9\\.]+)\\s{0,}kB\\s{0,}time=\\s{0,}([0-9:\\.]+)\\s{0,}bitrate=\\s{0,}([0-9\\.\\/A-Z]+)\\s{0,}kbits/s\\s{0,}\\r?\\n?"));
        QRegExp regProcessSize(QLatin1String("size=\\s{0,}([0-9\\.]+)\\s{0,}kB\\s{0,}time=\\s{0,}([0-9:\\.]+)\\s{0,}bitrate=\\s{0,}([0-9\\.\\/A-Za-z]+)\\s{0,}kbits/s\\s{0,}\\r?\\n?"));
        QRegExp regProcessEnd(QLatin1String("Lsize=\\s{0,}([0-9\\.]+)\\s{0,}kB\\s{0,}time=\\s{0,}([0-9:\\.]+)"));
        //Lsize + \n 是结尾


        while(!m_process->atEnd())
        {
            QString line = m_process->readLine();

            if(regFileExist.indexIn(line)>-1){

                //qDebug() << "Error: FileExist";
                m_process->write("Y");
                m_process->write("\n");
                continue;
            }
            //qDebug() << line;
            bool isProcess = false;
            foreach (QString text, line.split("\r")) {

                if(regProcessFrame.exactMatch(text))
                {
                    processingTime(regProcessFrame.cap(5));
                    isProcess = true;
                }else if(regProcessSize.exactMatch(text)){
                    processingTime(regProcessSize.cap(2));
                    isProcess = true;
                }else if(regProcessEnd.indexIn(text)> -1){
                    processingTime(regProcessEnd.cap(2));
                    isProcess = true;
                }else if(regProcessTime.indexIn(text) > -1){
                    processingTime(regProcessTime.cap(1));
                    isProcess = true;
                }
            }
            if(isProcess) continue;


            if(regProcessFrame.exactMatch(line))
            {
                processingTime(regProcessFrame.cap(5));
                continue;
            }else if(regProcessSize.exactMatch(line)){
                processingTime(regProcessSize.cap(2));
                continue;
            }else if(regProcessEnd.indexIn(line)> -1){
                processingTime(regProcessEnd.cap(2));
                continue;
            }else if(regProcessTime.indexIn(line) > -1){
                processingTime(regProcessTime.cap(1));
                continue;
            }



            if(regMetadataTag.exactMatch(line))
            {
                line = m_process->readLine();
                while(regMetadataItem.exactMatch(line))
                {
                    //qDebug() << regMetadataItem.cap(1) << regMetadataItem.cap(2);
                    line = m_process->readLine();
                }
            }


            if(regHeader.indexIn(line)>-1)
            {
                //qDebug() << "version:" << regHeader.cap(1);
                continue;

            }else if(regDurationTag.indexIn(line)>-1){

                QString text = regDurationTag.cap(1);
                //qDebug() << "Duration:" << text;

                if(!m_data.value("end",QString()).toString().isEmpty()){
                    ui->progressBar->setMinimum(0);
                    ui->progressBar->setMaximum(textToSec(m_data["end"].toString())-textToSec(m_data["start"].toString()));
                }else{
                    ui->progressBar->setRange(0,textToSec(text));
                }

                continue;

            }else if(regInputNotFound.indexIn(line)>-1){

                //qDebug() << "Error: InputNoFound " << regInputNotFound.cap(1);
                continue;

            }else if(regOutputNotFound.indexIn(line)>-1){

                //qDebug() << "Error: OutputNoFound";
                continue;

            }else if(regError.indexIn(line)>-1){

                //qDebug() << "Error: " << regError.cap(1);
                continue;


            }else if(regCannotAllocateMemory.indexIn(line)>-1){

                //qDebug() << "Error: CannotAllocateMemory";
                continue;
            }

            //qDebug() << line;
        }
    });

    connect(m_process,&QProcess::readyReadStandardError,[=]{
        //qDebug() << "readyReadStandardError";

        //        QString text = m_process->readAllStandardError();

        //        QRegExp regFileExist(QLatin1String("File '(.*)' already exists. Overwrite ?"));

        //        if(regFileExist.indexIn(text)>-1)
        //        {
        //            m_process->write("Y");
        //            m_process->write("\n");
        //        }

        //qDebug() << m_process->readAllStandardError();
    });



    setState(Processing);
}

ExportProcessItemWidget::~ExportProcessItemWidget()
{
    m_process->close();
    m_process->waitForFinished();
    delete ui;
}

void ExportProcessItemWidget::setState(ExportProcessItemWidget::States state)
{
    switch (state) {
    case Init:
    {
        ui->labelText->setText(QString());
        ui->labelIcon->show();
        ui->btnFinder->show();
        ui->btnCancel->hide();
        ui->progressBar->setRange(0,100);
        ui->progressBar->setValue(0);
        ui->rowWidget_1->show();
        ui->rowWidget_2->hide();
        m_item->setSizeHint(QSize(64,32));
        break;
    }
    case Processing:
    {
        ui->labelIcon->show();
        ui->btnFinder->hide();
        ui->btnCancel->show();
        ui->rowWidget_1->show();
        ui->rowWidget_2->show();
        m_item->setSizeHint(QSize(64,64));
        break;
    }
    case Done:
    {
        ui->labelIcon->show();
        ui->btnFinder->show();
        ui->btnCancel->hide();
        ui->rowWidget_1->show();
        ui->rowWidget_2->hide();
        m_item->setSizeHint(QSize(64,32));
        break;
    }
    case Error:
    {
        ui->labelIcon->show();
        ui->btnFinder->show();
        ui->btnCancel->hide();
        ui->rowWidget_1->show();
        ui->rowWidget_2->hide();
        m_item->setSizeHint(QSize(64,32));
        break;
    }
    default:
        break;
    }
}

} // namespace Avocado

void Avocado::ExportProcessItemWidget::on_btnFinder_clicked()
{
    if(m_targetPath.isEmpty()) return;
    QString path = m_targetPath;

    if(!QFileInfo(m_targetPath).exists()) return;

    QTimer::singleShot(0,[=](){

#if defined(Q_OS_WIN)
        const QString explorer = "explorer";
        QStringList param;
        if (!QFileInfo(path).isDir())
            param << QLatin1String("/select,");
        param << QDir::toNativeSeparators(path);
        QProcess::startDetached(explorer, param);
#elif defined(Q_OS_MAC)
        QStringList scriptArgs;
        scriptArgs << QLatin1String("-e")
                   << QString::fromLatin1("tell application \"Finder\" to reveal POSIX file \"%1\"")
                      .arg(path);
        QProcess::execute(QLatin1String("/usr/bin/osascript"), scriptArgs);

        scriptArgs.clear();
        scriptArgs << QLatin1String("-e")
                   << QLatin1String("tell application \"Finder\" to activate");
        QProcess::execute("/usr/bin/osascript", scriptArgs);
#endif

    });

}

void Avocado::ExportProcessItemWidget::on_btnCancel_clicked()
{
    switch(m_process->state())
    {
    case QProcess::Starting:
    case QProcess::Running:
        m_process->close();
        return;
        break;
    case QProcess::NotRunning:
        break;
    }
}

void Avocado::ExportProcessItemWidget::processingTime(const QString text)
{
    ui->progressBar->setValue(textToSec(text));
}

void Avocado::ExportProcessItemWidget::start()
{

    ui->labelIcon->setPixmap(m_data["icon"].value<QPixmap>());

    QMap<QString,QString> metaMap = getMetadata(m_data["file"].toString());


    QString targetBasename = getTargetBasename(metaMap);
    QString targetPath = QString("%1/%2.%3").arg(m_targetFolder).arg(targetBasename).arg(m_data["file_target_suff"].toString());

    m_targetPath = targetPath;
    m_item->setData(Qt::UserRole+1000,m_targetPath);
    resize(size());

    QStringList scriptArgs;
    QStringList presetList = QStringList() << "videocopy" << "audiocopy";
    if(presetList.indexOf(m_data.value("preset_name",QString("none")).toString()) > -1){

        scriptArgs << QLatin1String("-i")
                   << m_data["file"].toString()

                   << QLatin1String("-ss") << m_data["start"].toString()
                   << QLatin1String("-to") << m_data["end"].toString();
    }else{
        scriptArgs << QLatin1String("-ss") << m_data["start"].toString()
                   << QLatin1String("-i")
                   << m_data["file"].toString()
                   << QLatin1String("-t") << m_data["length"].toString();
    }



    scriptArgs.append(m_data["argument"].toString().split(" "));
    scriptArgs.append(targetPath);

    m_process->start(m_ffmpeg,scriptArgs);
    //qDebug() << m_process->arguments();
}

void Avocado::ExportProcessItemWidget::setSelected(bool check)
{
    //        if(check)
    //        {
    //            QPalette pe = ui->labelText->palette();
    //            pe.setColor(QPalette::WindowText,Qt::white);
    //            ui->labelText->setPalette(pe);
    //        }else{
    //            QPalette pe = ui->labelText->palette();
    //            pe.setColor(QPalette::WindowText,Qt::black);
    //            ui->labelText->setPalette(pe);
    //        }
}


void Avocado::ExportProcessItemWidget::resizeEvent(QResizeEvent *event)
{
    QFontMetrics fm(ui->labelText->font());
    ui->labelText->setText(fm.elidedText(m_targetPath,Qt::ElideLeft,ui->labelText->width()));
    QWidget::resizeEvent(event);
}


QMap<QString, QString> Avocado::ExportProcessItemWidget::getMetadata(const QString &filepath)
{
    QRegExp regMetadataTag(QLatin1String("^\\s{2}Metadata:$"));
    QRegExp regMetadataItem(QLatin1String("^\\s{4,}(\\w+)\\s{0,}:\\s{0,}(.*)\\s{0,}$"));
    QRegExp regDurationTag(QLatin1String("^\\s{2}Duration: ([0-9:\\.]+).*$"));
    QRegExp regStreamAudio(QLatin1String("^\\s{4,}Stream (.*) Audio: (\\w+)"));
    QRegExp regStreamVideo(QLatin1String("^\\s{4,}Stream (.*) Video: (\\w+)"));

    QMap<QString, QString> map;
    QFileInfo info(filepath);
    map["file_src_path"] = filepath;
    map["file_src_suff"] = info.suffix();
    map["file_src_basename"] = info.completeBaseName();
    QByteArray buff;
    QProcess process;
    process.setProcessChannelMode(QProcess::MergedChannels);
    QStringList scriptArgs;

    scriptArgs << QLatin1String("-i")
               << filepath;
    process.start(m_ffmpeg,scriptArgs);

    if(process.waitForFinished())
    {
        buff = process.readAll();
    }
    process.close();

    QTextStream stream(&buff,QIODevice::ReadOnly);

    while(!stream.atEnd())
    {
        QString line = stream.readLine();
        //qDebug() << "LINE:" << line;
        if(regMetadataTag.exactMatch(line))
        {
            line = stream.readLine();
            while(regMetadataItem.exactMatch(line))
            {
                map[QString("meta_%1").arg(regMetadataItem.cap(1).trimmed())]= regMetadataItem.cap(2).trimmed();
                line = stream.readLine();
            }
        }

        if(regDurationTag.indexIn(line) > -1)
        {
            map["duration"] = regDurationTag.cap(1).trimmed();

        }else if(regStreamAudio.indexIn(line) > -1)
        {
            map["audio"] = regStreamAudio.cap(2).trimmed();
        }else if(regStreamVideo.indexIn(line) > -1)
        {
            map["video"] = regStreamVideo.cap(2).trimmed();
        }
    }

    return map;

}

QString Avocado::ExportProcessItemWidget::getTargetBasename(const QMap<QString, QString> &map) const
{
    QString basename = map["file_src_basename"];

    if(!map.value("meta_title",QString()).isEmpty())
    {
        basename = map.value("meta_title");
    }

    QString timeStart = toFileTime(m_data.value("start",QString()).toString());
    QString timeEnd = toFileTime(m_data.value("end",QString()).toString());

    QString time;

    if(!timeStart.isEmpty() && !timeEnd.isEmpty())
    {
        time = QString("%1-%2").arg(timeStart).arg(timeEnd);
    }

    if(!time.isEmpty())
    {
        basename.append(" ").append(time);
    }

    if(!m_data.value("preset_name",QString()).toString().isEmpty())
    {
        basename.append(" ").append(m_data.value("preset_name",QString()).toString());
    }

    return QString(basename);
}

