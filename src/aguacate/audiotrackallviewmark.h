#ifndef AVOCADO_AUDIOTRACKALLVIEWMARK_H
#define AVOCADO_AUDIOTRACKALLVIEWMARK_H

#include <QGraphicsObject>

namespace Avocado {
class AudioTrackMark;
class AudioTrackAllView;
class AudioTrackAllViewMark : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit AudioTrackAllViewMark(AudioTrackAllView *view,AudioTrackMark *audioTrackMark, QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    QRectF boundingRect() const;
    void setPosition(const qint64 &start,const qint64 &end);
    void reLayout();
    inline AudioTrackMark* audioTrackMark(){return m_audioTrackMark;}
private:
    AudioTrackAllView *m_view;
    AudioTrackMark *m_audioTrackMark;
    qint64 m_positionStart;
    qint64 m_positionEnd;
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKALLVIEWMARK_H
