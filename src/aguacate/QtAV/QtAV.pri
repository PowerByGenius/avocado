

LIBS += -lavformat -lavcodec -lavutil -lswscale -lswresample
LIBS += -framework Security \
        -framework CoreMedia \
        -framework CoreAudio \
        -framework CoreVideo \
        -framework VideoDecodeAcceleration \
        -framework AudioToolbox \
        -framework VideoToolbox \
        -framework AudioUnit \
        -framework Carbon
LIBS += -lz -lbz2


DEFINES += BUILD_QTAV_STATIC


INCLUDEPATH += $$PWD \
               $$PWD/QtAV \
               $$PWD/widgets

DEPENDPATH += $$PWD \
              $$PWD/QtAV \
              $$PWD/widgets

#SOURCES += $$PWD/output/audio/AudioOutputPortAudio.cpp
#DEFINES *= QTAV_HAVE_PORTAUDIO=1
#LIBS *= -lportaudio

DEFINES += QTAV_HAVE_SWRESAMPLE=1
SOURCES += $$PWD/AudioResamplerFF.cpp



SOURCES += $$PWD/output/audio/AudioOutputOpenAL.cpp
DEFINES *= QTAV_HAVE_OPENAL=1
static_openal: DEFINES += AL_LIBTYPE_STATIC
LIBS += -framework OpenAL
DEFINES += HEADER_OPENAL_PREFIX



DEFINES *= QTAV_HAVE_VDA=1
SOURCES += $$PWD/codec/video/VideoDecoderVDA.cpp
LIBS += -framework VideoDecodeAcceleration -framework CoreVideo -framework CoreFoundation \
        -framework IOSurface

#DEFINES *= QTAV_HAVE_VIDEOTOOLBOX=1
#SOURCES += codec/video/VideoDecoderVideoToolbox.cpp
#LIBS += -framework CoreVideo -framework CoreFoundation -framework CoreMedia \
#        -framework IOSurface -framework VideoToolbox

OTHER_FILES += $$PWD/shaders/planar.f.glsl $$PWD/shaders/rgb.f.glsl
  SDK_HEADERS *= \
    $$PWD/QtAV/OpenGLRendererBase.h \
    $$PWD/QtAV/OpenGLVideo.h \
    $$PWD/QtAV/VideoShader.h
  SDK_PRIVATE_HEADERS = \
    $$PWD/QtAV/private/OpenGLRendererBase_p.h
  HEADERS *= \
    $$PWD/utils/OpenGLHelper.h \
    $$PWD/ShaderManager.h
  SOURCES *= \
    $$PWD/output/video/OpenGLRendererBase.cpp \
    $$PWD/OpenGLVideo.cpp \
    $$PWD/VideoShader.cpp \
    $$PWD/ShaderManager.cpp \
    $$PWD/utils/OpenGLHelper.cpp


SDK_HEADERS *= \
    $$PWD/widgets/QtAVWidgets/QtAVWidgets \
    $$PWD/widgets/QtAVWidgets/QtAVWidgets.h \
    $$PWD/widgets/QtAVWidgets/global.h \
    $$PWD/widgets/QtAVWidgets/version.h \
    $$PWD/widgets/QtAVWidgets/VideoPreviewWidget.h \
    $$PWD/widgets/QtAVWidgets/GraphicsItemRenderer.h \
    $$PWD/widgets/QtAVWidgets/WidgetRenderer.h

HEADERS *= $$PWD/output/video/VideoOutputEventFilter.h

SOURCES *= \
    $$PWD/widgets/global.cpp \
    $$PWD/widgets/VideoPreviewWidget.cpp \
    $$PWD/output/video/VideoOutputEventFilter.cpp \
    $$PWD/output/video/GraphicsItemRenderer.cpp \
    $$PWD/output/video/WidgetRenderer.cpp

  SDK_HEADERS *= $$PWD/widgets/QtAVWidgets/OpenGLWidgetRenderer.h
  SOURCES *= $$PWD/output/video/OpenGLWidgetRenderer.cpp

  DEFINES *= QTAV_HAVE_GL=1
  SOURCES += $$PWD/output/video/GLWidgetRenderer2.cpp
  SDK_HEADERS += $$PWD/widgets/QtAVWidgets/GLWidgetRenderer2.h








SOURCES += \
    $$PWD/AVCompat.cpp \
    $$PWD/QtAV_Global.cpp \
    $$PWD/subtitle/CharsetDetector.cpp \
    $$PWD/subtitle/PlainText.cpp \
    $$PWD/subtitle/PlayerSubtitle.cpp \
    $$PWD/subtitle/Subtitle.cpp \
    $$PWD/subtitle/SubtitleProcessor.cpp \
    $$PWD/subtitle/SubtitleProcessorFFmpeg.cpp \
    $$PWD/utils/GPUMemCopy.cpp \
    $$PWD/utils/Logger.cpp \
    $$PWD/AudioThread.cpp \
    $$PWD/utils/internal.cpp \
    $$PWD/AVThread.cpp \
    $$PWD/AudioFormat.cpp \
    $$PWD/AudioFrame.cpp \
    $$PWD/AudioResampler.cpp \
    $$PWD/AudioResamplerTemplate.cpp \
    $$PWD/AudioResamplerTypes.cpp \
    $$PWD/codec/audio/AudioDecoder.cpp \
    $$PWD/codec/audio/AudioDecoderFFmpeg.cpp \
    $$PWD/codec/audio/AudioEncoder.cpp \
    $$PWD/codec/audio/AudioEncoderFFmpeg.cpp \
    $$PWD/codec/AVDecoder.cpp \
    $$PWD/codec/AVEncoder.cpp \
    $$PWD/AVMuxer.cpp \
    $$PWD/AVDemuxer.cpp \
    $$PWD/AVDemuxThread.cpp \
    $$PWD/ColorTransform.cpp \
    $$PWD/Frame.cpp \
    $$PWD/filter/Filter.cpp \
    $$PWD/filter/FilterContext.cpp \
    $$PWD/filter/FilterManager.cpp \
    $$PWD/filter/LibAVFilter.cpp \
    $$PWD/filter/SubtitleFilter.cpp \
    $$PWD/filter/EncodeFilter.cpp \
    $$PWD/ImageConverter.cpp \
    $$PWD/ImageConverterFF.cpp \
    $$PWD/Packet.cpp \
    $$PWD/PacketBuffer.cpp \
    $$PWD/AVError.cpp \
    $$PWD/AVPlayer.cpp \
    $$PWD/AVPlayerPrivate.cpp \
    $$PWD/AVTranscoder.cpp \
    $$PWD/AVClock.cpp \
    $$PWD/VideoCapture.cpp \
    $$PWD/VideoFormat.cpp \
    $$PWD/VideoFrame.cpp \
    $$PWD/io/MediaIO.cpp \
    $$PWD/io/QIODeviceIO.cpp \
    $$PWD/output/audio/AudioOutput.cpp \
    $$PWD/output/audio/AudioOutputBackend.cpp \
    $$PWD/output/audio/AudioOutputNull.cpp \
    $$PWD/output/video/VideoRenderer.cpp \
    $$PWD/output/video/VideoOutput.cpp \
    $$PWD/output/video/QPainterRenderer.cpp \
    $$PWD/output/AVOutput.cpp \
    $$PWD/output/OutputSet.cpp \
    $$PWD/Statistics.cpp \
    $$PWD/codec/video/VideoDecoder.cpp \
    $$PWD/codec/video/VideoDecoderTypes.cpp \
    $$PWD/codec/video/VideoDecoderFFmpegBase.cpp \
    $$PWD/codec/video/VideoDecoderFFmpeg.cpp \
    $$PWD/codec/video/VideoDecoderFFmpegHW.cpp \
    $$PWD/codec/video/VideoEncoder.cpp \
    $$PWD/codec/video/VideoEncoderFFmpeg.cpp \
    $$PWD/VideoThread.cpp \
    $$PWD/VideoFrameExtractor.cpp \
    $$PWD/CommonTypes.cpp

SDK_HEADERS *= \
    $$PWD/QtAV/QtAV \
    $$PWD/QtAV/QtAV.h \
    $$PWD/QtAV/dptr.h \
    $$PWD/QtAV/QtAV_Global.h \
    $$PWD/QtAV/AudioResampler.h \
    $$PWD/QtAV/AudioResamplerTypes.h \
    $$PWD/QtAV/AudioDecoder.h \
    $$PWD/QtAV/AudioEncoder.h \
    $$PWD/QtAV/AudioFormat.h \
    $$PWD/QtAV/AudioFrame.h \
    $$PWD/QtAV/AudioOutput.h \
    $$PWD/QtAV/AVDecoder.h \
    $$PWD/QtAV/AVEncoder.h \
    $$PWD/QtAV/AVDemuxer.h \
    $$PWD/QtAV/AVMuxer.h \
    $$PWD/QtAV/CommonTypes.h \
    $$PWD/QtAV/Filter.h \
    $$PWD/QtAV/FilterContext.h \
    $$PWD/QtAV/LibAVFilter.h \
    $$PWD/QtAV/EncodeFilter.h \
    $$PWD/QtAV/Frame.h \
    $$PWD/QtAV/QPainterRenderer.h \
    $$PWD/QtAV/Packet.h \
    $$PWD/QtAV/AVError.h \
    $$PWD/QtAV/AVPlayer.h \
    $$PWD/QtAV/AVTranscoder.h \
    $$PWD/QtAV/VideoCapture.h \
    $$PWD/QtAV/VideoRenderer.h \
    $$PWD/QtAV/VideoOutput.h \
    $$PWD/QtAV/MediaIO.h \
    $$PWD/QtAV/AVOutput.h \
    $$PWD/QtAV/AVClock.h \
    $$PWD/QtAV/VideoDecoder.h \
    $$PWD/QtAV/VideoDecoderTypes.h \
    $$PWD/QtAV/VideoEncoder.h \
    $$PWD/QtAV/VideoFormat.h \
    $$PWD/QtAV/VideoFrame.h \
    $$PWD/QtAV/VideoFrameExtractor.h \
    $$PWD/QtAV/FactoryDefine.h \
    $$PWD/QtAV/Statistics.h \
    $$PWD/QtAV/Subtitle.h \
    $$PWD/QtAV/SubtitleFilter.h \
    $$PWD/QtAV/SurfaceInterop.h \
    $$PWD/QtAV/version.h

SDK_PRIVATE_HEADERS *= \
    $$PWD/QtAV/private/factory.h \
    $$PWD/QtAV/private/mkid.h \
    $$PWD/QtAV/private/prepost.h \
    $$PWD/QtAV/private/singleton.h \
    $$PWD/QtAV/private/PlayerSubtitle.h \
    $$PWD/QtAV/private/SubtitleProcessor.h \
    $$PWD/QtAV/private/AVCompat.h \
    $$PWD/QtAV/private/AudioOutputBackend.h \
    $$PWD/QtAV/private/AudioResampler_p.h \
    $$PWD/QtAV/private/AVDecoder_p.h \
    $$PWD/QtAV/private/AVEncoder_p.h \
    $$PWD/QtAV/private/MediaIO_p.h \
    $$PWD/QtAV/private/AVOutput_p.h \
    $$PWD/QtAV/private/Filter_p.h \
    $$PWD/QtAV/private/Frame_p.h \
    $$PWD/QtAV/private/VideoShader_p.h \
    $$PWD/QtAV/private/VideoRenderer_p.h \
    $$PWD/QtAV/private/QPainterRenderer_p.h

# QtAV/private/* may be used by developers to extend QtAV features without changing QtAV library
# headers not in QtAV/ and it's subdirs are used only by QtAV internally
HEADERS *= \
    $$SDK_HEADERS \
    $$SDK_PRIVATE_HEADERS \
    $$PWD/AVPlayerPrivate.h \
    $$PWD/AVDemuxThread.h \
    $$PWD/AVThread.h \
    $$PWD/AVThread_p.h \
    $$PWD/AudioThread.h \
    $$PWD/PacketBuffer.h \
    $$PWD/VideoThread.h \
    $$PWD/ImageConverter.h \
    $$PWD/ImageConverter_p.h \
    $$PWD/codec/video/VideoDecoderFFmpegBase.h \
    $$PWD/codec/video/VideoDecoderFFmpegHW.h \
    $$PWD/codec/video/VideoDecoderFFmpegHW_p.h \
    $$PWD/filter/FilterManager.h \
    $$PWD/subtitle/CharsetDetector.h \
    $$PWD/subtitle/PlainText.h \
    $$PWD/utils/BlockingQueue.h \
    $$PWD/utils/GPUMemCopy.h \
    $$PWD/utils/Logger.h \
    $$PWD/utils/SharedPtr.h \
    $$PWD/utils/ring.h \
    $$PWD/utils/internal.h \
    $$PWD/output/OutputSet.h \
    $$PWD/QtAV/ColorTransform.h

RESOURCES += \
    $$PWD/shaders/shaders.qrc

