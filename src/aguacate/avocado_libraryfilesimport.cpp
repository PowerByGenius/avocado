#include "avocado_libraryfilesimport.h"
#include <fileref.h>
#include <tag.h>
#include <id3v2tag.h>
#include <mpegfile.h>
#include <mpegheader.h>
#include <attachedpictureframe.h>
#include <mp4tag.h>
#include <mp4coverart.h>

#include <QFileInfo>
#include <QDebug>
#include <QTime>
#include <QElapsedTimer>
#include <QImage>
#include <QDir>
#include <QBuffer>
#include <QSettings>

#ifdef __cplusplus
extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>

#include <libavutil/samplefmt.h>
}
#endif /*__cplusplus*/

void get_format_range(enum AVSampleFormat format, int *min, int *max) {

    // figure out the range of sample values we're dealing with
    switch (format) {
    case AV_SAMPLE_FMT_FLT:
    case AV_SAMPLE_FMT_FLTP:
    case AV_SAMPLE_FMT_DBL:
    case AV_SAMPLE_FMT_DBLP:
        *min = -1;
        *max = 1;
        break;
    case AV_SAMPLE_FMT_U8:
    case AV_SAMPLE_FMT_U8P:
        *min = 0;
        *max = 255;
        break;
    case AV_SAMPLE_FMT_S16:
    case AV_SAMPLE_FMT_S16P:
        *min = pow(2, 2 * 8) / -2;
        *max = pow(2, 2 * 8) / 2 - 1;
        break;
    case AV_SAMPLE_FMT_S32:
    case AV_SAMPLE_FMT_S32P:
        *min = pow(2, 4 * 8) / -2;
        *max = pow(2, 4 * 8) / 2 - 1;
        break;
    case AV_SAMPLE_FMT_NONE:
    case AV_SAMPLE_FMT_NB:
        break;
    }
}

double get_sample(enum AVSampleFormat format,uint8_t *data) {
    double value = 0.0;

    switch (format) {
    case AV_SAMPLE_FMT_U8:
    case AV_SAMPLE_FMT_U8P:
        value += *((int8_t *) data);
        break;
    case AV_SAMPLE_FMT_S16:
    case AV_SAMPLE_FMT_S16P:
        value += *((int16_t *) data);
        break;
    case AV_SAMPLE_FMT_S32:
    case AV_SAMPLE_FMT_S32P:
        value += *((int32_t *) data);
        break;
    case AV_SAMPLE_FMT_FLT:
    case AV_SAMPLE_FMT_FLTP:
        value += *((float *) data);
        break;
    case AV_SAMPLE_FMT_DBL:
    case AV_SAMPLE_FMT_DBLP:
        value += *((double *) data);
        break;
    case AV_SAMPLE_FMT_NONE:
    case AV_SAMPLE_FMT_NB:
        break;
    }

    if (format == AV_SAMPLE_FMT_FLT ||
            format == AV_SAMPLE_FMT_FLTP ||
            format == AV_SAMPLE_FMT_DBL ||
            format == AV_SAMPLE_FMT_DBLP ) {
        if (value < -1.0) {
            value = -1.0;
        } else if (value > 1.0) {
            value = 1.0;
        }
    }
    return value;
}

namespace Avocado {

LibraryFilesImport::LibraryFilesImport(QObject *parent) : QObject(parent)
{
    class AVInitializer {
    public:
        AVInitializer() {
//            qDebug("av_register_all and avformat_network_init");
//            av_log_set_level(AV_LOG_QUIET);
            av_register_all();
//            avformat_network_init();
        }
        ~AVInitializer() {
//            qDebug("avformat_network_deinit");
//            avformat_network_deinit();
        }
    };
    static AVInitializer sAVInit;
    Q_UNUSED(sAVInit);
}

QByteArray LibraryFilesImport::fileChecksum(const QString &fileName,
                                            QCryptographicHash::Algorithm hashAlgorithm)
{
    QFile f(fileName);
    if (f.open(QFile::ReadOnly)) {
        QCryptographicHash hash(hashAlgorithm);
        if (hash.addData(&f)) {
            return hash.result();
        }
    }
    return QByteArray();
}

qint64 LibraryFilesImport::generateWaveform(const QString &fileName, const QString &output)
{

    AVFormatContext *pFormatContext = NULL;
    AVCodecContext *pDecoderContext = NULL;
    AVCodec *pDecoder = NULL;
    int stream_index = 0;
    QString path = fileName;

    try{

        if (avformat_open_input(&pFormatContext, path.toLocal8Bit().data(), NULL, NULL) < 0) throw -1;
        if (avformat_find_stream_info(pFormatContext, NULL) < 0) throw -2;
        stream_index = av_find_best_stream(pFormatContext, AVMEDIA_TYPE_AUDIO, -1, -1, &pDecoder, 0);
        if (stream_index < 0) throw -3;
        pDecoderContext = pFormatContext->streams[stream_index]->codec;
        if (avcodec_open2(pDecoderContext, pDecoder, NULL) < 0) throw -4;

        //av_dump_format(pFormatContext, 0, 0, 0);

        int sample_size = av_get_bytes_per_sample(pDecoderContext->sample_fmt);
        int sample_rate = pDecoderContext->sample_rate;
        int channels =   pDecoderContext->channels;
        AVSampleFormat sample_fmt = pDecoderContext->sample_fmt;

        int bit_rate    = pFormatContext->bit_rate;
        double duration = pFormatContext->duration / (double) AV_TIME_BASE;


        av_log_set_level(0);

        QList<qint16> list;

        int         testCount = 0;

        int         sample_min;
        int         sample_max;
        get_format_range(sample_fmt, &sample_min, &sample_max);

        uint32_t    sample_range = sample_max - sample_min;
        double      channel_average_multiplier = 1.0 / channels;
        int         scale = sample_rate/1000 * 10;
        int         track_height = 255;
        int         padding = 0;

        QVector<double> buff;
        int         buffer_size = 8192; //每读取达到这个数量的Frame执行一次数据处理
        int         frame_size = 0;


        int total_size = 0;

        int is_planar = av_sample_fmt_is_planar(sample_fmt);

        AVFrame *pFrame = NULL;
        if (!(pFrame = av_frame_alloc())) throw -11;

        AVPacket packet;
        av_init_packet(&packet);

        while (av_read_frame(pFormatContext, &packet) == 0) {

            int frame_finished = 0;
            if (avcodec_decode_audio4(pDecoderContext, pFrame, &frame_finished, &packet) < 0) {
                // 偶尔会发生读不来，让它去吧
                continue;
            }

            if (frame_finished) {

                int data_size = av_samples_get_buffer_size(
                            is_planar ? &pFrame->linesize[0] : NULL,
                            channels,
                            pFrame->nb_samples,
                            sample_fmt,
                            1
                            );


                if (is_planar) {
                    int i = 0;
                    int c = 0;
                    for (; i < data_size / channels; i += sample_size) {
                        double value = 0;
                        for (c = 0; c < channels; c++) {
                            value += get_sample(sample_fmt,&pFrame->extended_data[c][i]) * channel_average_multiplier;
                        }
                        buff.append(value);
                        total_size ++;
                    }
                } else {
                    int i = 0;
                    for (; i < data_size; i += sample_size) {
                        double value = 0;
                        value = get_sample(sample_fmt,&pFrame->extended_data[0][i]);
                        buff.append(value);
                        total_size ++;
                    }

                }
            }

            av_free_packet(&packet);


            if(frame_size % buffer_size == 0)
            {
                if(buff.size() > scale)
                {
                    int t = buff.size() / scale;
                    for (int i = 0; i < t ; ++i) {
                        double min = sample_max;
                        double max = sample_min;
                        double value = 0;
                        for (int x = 0; x < scale; ++x) {
                            value = buff.at((i*scale)+x);
                            if (value < min) min = value;
                            if (value > max) max = value;
                        }
                        int y_max = track_height - ((min - sample_min) * track_height / sample_range) + padding;
                        int y_min = track_height - ((max - sample_min) * track_height / sample_range) + padding;
                        qint16 d = (y_min << 8) | (y_max & 0xFF);
                        list.append(d);
                        testCount++;
                    }
                    QVector<double> temp;
                    for (int i = t*scale; i < buff.size(); ++i) {
                        temp.append(buff.at(i));
                    }
                    buff.clear();

                    for (int i = 0; i < temp.size(); ++i) {
                        buff.append(temp.at(i));
                    }
                    temp.clear();
                }
            }

            frame_size++;
        } // end while


        av_frame_free(&pFrame);
        av_free_packet(&packet);


        // 如果长度小于buffer_size，前面没有执行，这里补多次
        if(buff.size() > scale)
        {
            int t = buff.size() / scale;
            for (int i = 0; i < t ; ++i) {
                double min = sample_max;
                double max = sample_min;
                double value = 0;
                for (int x = 0; x < scale; ++x) {
                    value = buff.at((i*scale)+x);
                    if (value < min) min = value;
                    if (value > max) max = value;
                }
                int y_max = track_height - ((min - sample_min) * track_height / sample_range) + padding;
                int y_min = track_height - ((max - sample_min) * track_height / sample_range) + padding;
                qint16 d = (y_min << 8) | (y_max & 0xFF);
                list.append(d);
                testCount++;
            }
            QVector<double> temp;
            for (int i = t*scale; i < buff.size(); ++i) {
                temp.append(buff.at(i));
            }
            buff.clear();

            for (int i = 0; i < temp.size(); ++i) {
                buff.append(temp.at(i));
            }
            temp.clear();
        }


        // Buff剩下的采样

        if(buff.size() > 0)
        {
            double min = sample_max;
            double max = sample_min;
            double value = 0;
            for (int x = 0; x < buff.size(); ++x) {
                value = buff.takeFirst();
                if (value < min) min = value;
                if (value > max) max = value;
            }
            int y_max = track_height - ((min - sample_min) * track_height / sample_range) + padding;
            int y_min = track_height - ((max - sample_min) * track_height / sample_range) + padding;
            qint16 d = (y_min << 8) | (y_max & 0xFF);
            list.append(d);
            testCount++;
        }

        //qDebug() << scale << testCount << buff.size();


        QFile file(output);
        file.remove();
        file.open(QIODevice::WriteOnly);
        QDataStream stream(&file);
        stream << list;
        //qDebug() << list.size();
        list.clear();


        //        if (total_size == 0) {
        //            // not a single packet could be read.
        //            return;
        //        }


    }catch (int e){

        switch (e) {
        case -1:
            qDebug("Cannot open input file.");
            break;
        case -2:
            qDebug("Cannot find stream information.\n");
            break;
        case -3:
            qDebug("Unable to find audio stream in file.\n");
            break;
        case -4:
            qDebug("Cannot open audio decoder.\n");
            break;
        case -11:
            //free_audio_data(data);
            qDebug("Cannot alloc frame.\n");
            break;
        default:
            break;
        }

        if(pDecoderContext){
            avcodec_close(pDecoderContext);
            pDecoderContext = 0;
        }
        if(pFormatContext){
            avformat_close_input(&pFormatContext);
            pFormatContext = 0;
        }
    }

    if(pDecoderContext){
        avcodec_close(pDecoderContext);
        pDecoderContext = 0;
    }
    if(pFormatContext){
        avformat_close_input(&pFormatContext);
        pFormatContext = 0;
    }

    return 0;
}

void LibraryFilesImport::importFiles(const QStringList &urls)
{
    QStringList suffixs = QStringList()
            << "m4a" << "m4r" << "m4b" << "m4p" << "3g2" << "mp4" << "mp3";
    QStringList files;

    foreach (QString url, urls) {
        QString suffix = QFileInfo(url).suffix().toLower();
        if(suffixs.contains(suffix)) files.append(url);
    }

    QVariantList list;

//    QElapsedTimer hashTimer;
//    hashTimer.start();


//    QString coverPath = QString("%1/%2/40/%3.png")
//            .arg(QSettings().value("library_path").toString())
//            .arg("cover")
//            .arg(data["meta_coverart"].toString());

//    QString mediaPath = QString("%1/%2/%3")
//            .arg(QSettings().value("library_path").toString())
//            .arg("media")
//            .arg(data["file_path"].toString());

//    QString wavePath = QString("%1/%2/%3.data")
//            .arg(QSettings().value("library_path").toString())
//            .arg("waveform")
//            .arg(data["file_hash"].toString());


    QString library_path = QSettings().value("library_path").toString();


    foreach (QString path, files)
    {

        QString mediaFolder = QString("%1/media").arg(library_path);
        QString coverFolder = QString("%1/cover").arg(library_path);
        QString waveformFolder = QString("%1/waveform").arg(library_path);

        QString hash = fileChecksum(path,QCryptographicHash::Md5).toHex();
        // checking file exit
        QFileInfo info(path);
        QString storePath = QString("%1.%2").arg(hash).arg(info.suffix());


        QVariantMap map;
        QString fileExt = info.suffix();
        map["file_source"] = path;
        map["file_hash"] = hash;
        map["file_name"] = storePath;
        map["file_path"] = storePath;
        map["file_size"] = info.size();
        map["file_ext"] = fileExt;
        map["file_type"] = fileExt;

        map["meta_title"] = QString();
        map["meta_album"] = QString();
        map["meta_artist"] = QString();
        map["meta_comment"] = QString();
        map["meta_genre"] = QString();
        map["meta_tracknumber"] = -1;
        map["meta_year"] = -1;
        map["meta_date"] =  QString();
        map["meta_coverart"] =  QString();

        map["stream_length"] =  -1;
        map["stream_bitrate"] = -1;
        map["stream_samplerate"] = -1;
        map["stream_channels"] =  -1;

        map["meta_caption_en"] =  QString();
        map["meta_caption_cn"] =  QString();

        TagLib::FileRef f(path.toUtf8().data());
        if(!f.isNull() && f.tag() && f.audioProperties())
        {
            TagLib::Tag *tag = f.tag();
            TagLib::AudioProperties *audio = f.audioProperties();

            map["meta_title"] = TStringToQString(tag->title());
            map["meta_album"] = TStringToQString(tag->album());
            map["meta_artist"] = TStringToQString(tag->artist());
            map["meta_comment"] = TStringToQString(tag->comment());
            map["meta_genre"] = TStringToQString(tag->genre());
            map["meta_tracknumber"] = tag->track();
            map["meta_year"] = tag->year();

            map["stream_length"] =  audio->length();
            map["stream_bitrate"] = audio->sampleRate();
            map["stream_samplerate"] = audio->bitrate();
            map["stream_channels"] =  audio->channels();


            auto saveImage = [=](const char * data, qint64 maxSize){
                QCryptographicHash hash(QCryptographicHash::Md5);
                hash.addData(data,maxSize);
                QString imageSourceHash = hash.result().toHex();

                QString sourcePath = QString("%1/source/%2.data").arg(coverFolder)
                        .arg(imageSourceHash);

                if(!QFile(sourcePath).exists())
                {
                    QFile file(sourcePath);
                    file.open(QFile::WriteOnly);
                    file.write(data,maxSize);
                    file.close();
                }

                QImage image;
                image.loadFromData((const uchar *) data,maxSize);


                QString coverThumb40 = QString("%1/40/%2.png").arg(coverFolder)
                        .arg(imageSourceHash);

                if(!QFile(coverThumb40).exists())
                {
                    image.scaled(40,40,Qt::KeepAspectRatioByExpanding,Qt::SmoothTransformation)
                            .save(coverThumb40);
                }

                return imageSourceHash;
            };

            QStringList mp4Ext = QStringList()
                    << "m4a" << "m4r" << "m4b" << "m4p" << "3g2" << "mp4";

            if(mp4Ext.contains(fileExt.toLower()))
            {
                TagLib::MP4::Tag *tagmp4 = static_cast<TagLib::MP4::Tag *>(f.tag());

                TagLib::MP4::ItemListMap itemsListMap = tagmp4->itemListMap();

                if (itemsListMap.contains("covr"))
                {
                    TagLib::MP4::Item coverItem = itemsListMap["covr"];
                    TagLib::MP4::CoverArtList coverArtList = coverItem.toCoverArtList();
                    TagLib::MP4::CoverArt coverArt = coverArtList.front();

                    map["meta_coverart"] = saveImage(coverArt.data().data(),coverArt.data().size());
                }

            } else if(fileExt.toLower() == "mp3"){
                TagLib::MPEG::File *file = static_cast<TagLib::MPEG::File *>(f.file());
                if(file->hasID3v2Tag())
                {
                    TagLib::ID3v2::FrameList l = file->ID3v2Tag()->frameList("APIC");
                    if(!l.isEmpty())
                    {
                        TagLib::ID3v2::AttachedPictureFrame *f =
                                static_cast<TagLib::ID3v2::AttachedPictureFrame *>(l.front());

                        map["meta_coverart"] = saveImage(f->picture().data(), f->picture().size());
                    }
                }
            }

        }else{
            continue;
        }

        QRegExp exp("(\\d{2}\\s+)?(\\d{4}-\\d{2}-\\d{2}) (.*)");

        if(exp.exactMatch(info.baseName()))
        {
            map["meta_date"] =exp.cap(2);
        }

        QString srtfileEn = QString("%1/%2.srt")
                .arg(info.absoluteDir().absolutePath())
                .arg(info.completeBaseName());

        if(QFile(srtfileEn).exists())
        {
            QFile file(srtfileEn);
            if(file.open(QFile::ReadOnly))
            {
                QTextStream stream(&file);
                map["meta_caption_en"] = stream.readAll();
                file.close();
            }
        }

        QString srtfileCN = QString("%1/%2.cn.srt")
                .arg(info.absoluteDir().absolutePath())
                .arg(info.completeBaseName());

        if(QFile(srtfileCN).exists())
        {
            QFile file(srtfileCN);
            if(file.open(QFile::ReadOnly))
            {
                QTextStream stream(&file);
                map["meta_caption_cn"] = stream.readAll();
                file.close();
            }
        }

        list.append(map);

        QString mediaPath = QString("%1/%2").arg(mediaFolder).arg(storePath);
        if(QFile(mediaPath).exists()) {

            //check waveform
            QString waveformPath = QString("%1/%2.data").arg(waveformFolder).arg(hash);
            if(!QFile(waveformPath).exists())
            {
                generateWaveform(mediaPath,waveformPath);
            }

            continue;
        }
        if(!QFile::copy(path,mediaPath)) continue;
    }

    emit importFilesFinished(list);

//    qDebug() << list;
//    qDebug() << hashTimer.elapsed();

}

} // namespace Avocado

