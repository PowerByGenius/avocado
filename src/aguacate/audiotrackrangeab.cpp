#include "audiotrackrangeab.h"
#include "audiotrack.h"
#include "audiotrackview.h"
#include "audiotrackwaveform.h"
#include <QDebug>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

namespace Avocado {


AudioTrackRangeABHandle::AudioTrackRangeABHandle (const HandleTypes types,AudioTrackRangeAB *parent):
    QGraphicsItem(parent),
    m_trackRange(parent),
    m_handleType(types),
    m_size(25)
{
    this->setFlag(QGraphicsItem::ItemIsMovable,true);
    this->setData(Qt::UserRole+100,true); // unhanddragable
}

void AudioTrackRangeABHandle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->fillRect(this->boundingRect(),QColor(255, 102, 0,207));
    painter->setPen(Qt::white);
    switch(m_handleType){
    case HandleStart:
        painter->drawText(boundingRect(),Qt::AlignCenter,"A");
        break;
    case HandleEnd:
        painter->drawText(boundingRect(),Qt::AlignCenter,"B");
        break;
    }

}

QRectF AudioTrackRangeABHandle::boundingRect() const
{
    switch(m_handleType){
    case HandleStart:
        return QRectF(m_trackRange->boundingRect().left()-m_size,
                      m_trackRange->boundingRect().height()-m_size*1.2,m_size,m_size);
        break;
    case HandleEnd:
        return QRectF(m_trackRange->boundingRect().right(),
                      m_trackRange->boundingRect().height()-m_size*1.2,m_size,m_size);
        break;
    }
    return QRectF(0,0,0,0);
}

void AudioTrackRangeABHandle::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    event->accept();
    switch(m_handleType){
    case HandleStart:
    {
        qreal x = event->scenePos().rx()+m_buttonDownPos.rx();
        if(x<0) x =0;
        if(x>m_trackRange->rangePosEnd()) x = m_trackRange->rangePosEnd();
        m_trackRange->setRangePosStart(x);
        break;
    }

    case HandleEnd:
    {
        qreal x = event->scenePos().rx() - m_buttonDownPos.rx();

        if(x < m_trackRange->rangePosStart())
        {
            x = m_trackRange->rangePosStart();
        }
        if(x> m_trackRange->m_audioTrack->length())
        {
            x = m_trackRange->m_audioTrack->length();
        }
        m_trackRange->setRangePosEnd(x);
        break;
    }

    }
    //    QGraphicsItem::mouseMoveEvent(event);
}

void AudioTrackRangeABHandle::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_buttonDownPos = QPointF(event->scenePos().rx()
                              -mapToScene(this->boundingRect().topLeft()).rx(),0);

    QGraphicsItem::mousePressEvent(event);
}

void AudioTrackRangeABHandle::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
}


AudioTrackRangeAB::AudioTrackRangeAB(AudioTrack *audioTrack, QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_audioTrack(audioTrack),
    m_handleStart(new AudioTrackRangeABHandle(AudioTrackRangeABHandle::HandleStart,this)),
    m_handleEnd(new AudioTrackRangeABHandle(AudioTrackRangeABHandle::HandleEnd,this)),
    m_posStart(0),m_posEnd(0),isMoving(false)
{
    this->setZValue(1000);
}

void AudioTrackRangeAB::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    QRectF rect = this->boundingRect();


    painter->fillRect(rect.left(),0,1,rect.height(),QColor(255, 102, 0));
    painter->fillRect(rect.right(),0,1,rect.height(),QColor(255, 102, 0));

    // painter->fillRect(rightLine,QColor(255, 102, 0,64));
}

QRectF AudioTrackRangeAB::boundingRect() const
{
    qreal height = m_audioTrack->view()->viewport()->height();
    return QRectF(m_posStart,0,m_posEnd-m_posStart,height);
}

void AudioTrackRangeAB::setRangePosStart(qreal value)
{
    m_posStart=value;
    prepareGeometryChange();
    m_audioTrack->view()->rangePositionHandDragChanged(m_posStart,m_posEnd);
}

void AudioTrackRangeAB::setRangePosEnd(qreal value)
{
    m_posEnd = value;
    prepareGeometryChange();
    m_audioTrack->view()->rangePositionHandDragChanged(m_posStart,m_posEnd);
}

void AudioTrackRangeAB::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseMoveEvent(event);
}

void AudioTrackRangeAB::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_buttonDownPos = event->buttonDownPos(Qt::LeftButton);
    QGraphicsObject::mousePressEvent(event);
}

void AudioTrackRangeAB::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseReleaseEvent(event);
}



} // namespace Avocado

