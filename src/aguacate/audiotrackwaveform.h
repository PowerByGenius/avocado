#ifndef AVOCADO_AUDIOTRACKWAVEFORM_H
#define AVOCADO_AUDIOTRACKWAVEFORM_H

#include <QGraphicsObject>
#include "audiotrack.h"

namespace Avocado {

class AudioTrackWaveform : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit AudioTrackWaveform(AudioTrack *audioTrack,QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
private:
    AudioTrack *m_audioTrack;
signals:


};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKWAVEFORM_H
