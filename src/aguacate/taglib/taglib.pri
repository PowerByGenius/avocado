
DEFINES += TAGLIB_STATIC

TAGLIBDIR = $${PWD}/taglib

INCLUDEPATH += $${PWD}/. $${PWD}/.. \
               $$TAGLIBDIR \
               $$TAGLIBDIR/../ \
               $$TAGLIBDIR/ape \
               $$TAGLIBDIR/asf \
               $$TAGLIBDIR/flac \
               $$TAGLIBDIR/it \
               $$TAGLIBDIR/mod \
               $$TAGLIBDIR/mp4 \
               $$TAGLIBDIR/mpc \
               $$TAGLIBDIR/mpeg \
               $$TAGLIBDIR/mpeg/id3v1 \
               $$TAGLIBDIR/mpeg/id3v2 \
               $$TAGLIBDIR/mpeg/id3v2/frames \
               $$TAGLIBDIR/ogg \
               $$TAGLIBDIR/ogg/flac \
               $$TAGLIBDIR/ogg/opus \
               $$TAGLIBDIR/ogg/speex \
               $$TAGLIBDIR/ogg/vorbis \
               $$TAGLIBDIR/riff \
               $$TAGLIBDIR/riff/aiff \
               $$TAGLIBDIR/riff/wav \
               $$TAGLIBDIR/s3m \
               $$TAGLIBDIR/toolkit \
               $$TAGLIBDIR/trueaudio \
               $$TAGLIBDIR/wavpack \
               $$TAGLIBDIR/xm

DEPENDPATH += $$INCLUDEPATH

HEADERS  += $$TAGLIBDIR/audioproperties.h \
    $$TAGLIBDIR/fileref.h \
    $$TAGLIBDIR/tag.h \
    $$TAGLIBDIR/taglib_export.h \
    $$TAGLIBDIR/tagunion.h \
    $$TAGLIBDIR/taglib_config.h \
    $$TAGLIBDIR/ape/apefile.h \
    $$TAGLIBDIR/ape/apefooter.h \
    $$TAGLIBDIR/ape/apeitem.h \
    $$TAGLIBDIR/ape/apeproperties.h \
    $$TAGLIBDIR/ape/apetag.h \
    $$TAGLIBDIR/asf/asfattribute.h \
    $$TAGLIBDIR/asf/asffile.h \
    $$TAGLIBDIR/asf/asfpicture.h \
    $$TAGLIBDIR/asf/asfproperties.h \
    $$TAGLIBDIR/asf/asftag.h \
    $$TAGLIBDIR/flac/flacfile.h \
    $$TAGLIBDIR/flac/flacmetadatablock.h \
    $$TAGLIBDIR/flac/flacpicture.h \
    $$TAGLIBDIR/flac/flacproperties.h \
    $$TAGLIBDIR/flac/flacunknownmetadatablock.h \
    $$TAGLIBDIR/it/itfile.h \
    $$TAGLIBDIR/it/itproperties.h \
    $$TAGLIBDIR/mod/modfile.h \
    $$TAGLIBDIR/mod/modfilebase.h \
    $$TAGLIBDIR/mod/modfileprivate.h \
    $$TAGLIBDIR/mod/modproperties.h \
    $$TAGLIBDIR/mod/modtag.h \
    $$TAGLIBDIR/mp4/mp4atom.h \
    $$TAGLIBDIR/mp4/mp4coverart.h \
    $$TAGLIBDIR/mp4/mp4file.h \
    $$TAGLIBDIR/mp4/mp4item.h \
    $$TAGLIBDIR/mp4/mp4properties.h \
    $$TAGLIBDIR/mp4/mp4tag.h \
    $$TAGLIBDIR/mpc/mpcfile.h \
    $$TAGLIBDIR/mpc/mpcproperties.h \
    $$TAGLIBDIR/ogg/flac/oggflacfile.h \
    $$TAGLIBDIR/ogg/opus/opusfile.h \
    $$TAGLIBDIR/ogg/opus/opusproperties.h \
    $$TAGLIBDIR/ogg/speex/speexfile.h \
    $$TAGLIBDIR/ogg/speex/speexproperties.h \
    $$TAGLIBDIR/ogg/oggfile.h \
    $$TAGLIBDIR/ogg/oggpage.h \
    $$TAGLIBDIR/ogg/oggpageheader.h \
    $$TAGLIBDIR/ogg/xiphcomment.h \
    $$TAGLIBDIR/ogg/vorbis/vorbisfile.h \
    $$TAGLIBDIR/ogg/vorbis/vorbisproperties.h \
    $$TAGLIBDIR/riff/aiff/aifffile.h \
    $$TAGLIBDIR/riff/aiff/aiffproperties.h \
    $$TAGLIBDIR/riff/wav/infotag.h \
    $$TAGLIBDIR/riff/wav/wavfile.h \
    $$TAGLIBDIR/riff/wav/wavproperties.h \
    $$TAGLIBDIR/riff/rifffile.h \
    $$TAGLIBDIR/s3m/s3mfile.h \
    $$TAGLIBDIR/s3m/s3mproperties.h \
    $$TAGLIBDIR/toolkit/taglib.h \
    $$TAGLIBDIR/toolkit/tbytevector.h \
    $$TAGLIBDIR/toolkit/tbytevectorlist.h \
    $$TAGLIBDIR/toolkit/tbytevectorstream.h \
    $$TAGLIBDIR/toolkit/tdebug.h \
    $$TAGLIBDIR/toolkit/tdebuglistener.h \
    $$TAGLIBDIR/toolkit/tfile.h \
    $$TAGLIBDIR/toolkit/tfilestream.h \
    $$TAGLIBDIR/toolkit/tiostream.h \
    $$TAGLIBDIR/toolkit/tlist.h \
    $$TAGLIBDIR/toolkit/tmap.h \
    $$TAGLIBDIR/toolkit/tpropertymap.h \
    $$TAGLIBDIR/toolkit/trefcounter.h \
    $$TAGLIBDIR/toolkit/tstring.h \
    $$TAGLIBDIR/toolkit/tstringlist.h \
    $$TAGLIBDIR/toolkit/tutils.h \
    $$TAGLIBDIR/toolkit/unicode.h \
    $$TAGLIBDIR/trueaudio/trueaudiofile.h \
    $$TAGLIBDIR/trueaudio/trueaudioproperties.h \
    $$TAGLIBDIR/wavpack/wavpackfile.h \
    $$TAGLIBDIR/wavpack/wavpackproperties.h \
    $$TAGLIBDIR/xm/xmfile.h \
    $$TAGLIBDIR/xm/xmproperties.h \
    $$TAGLIBDIR/mpeg/id3v1/id3v1genres.h \
    $$TAGLIBDIR/mpeg/id3v1/id3v1tag.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/attachedpictureframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/commentsframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/generalencapsulatedobjectframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/ownershipframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/popularimeterframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/privateframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/relativevolumeframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/textidentificationframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/uniquefileidentifierframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/unknownframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/unsynchronizedlyricsframe.h \
    $$TAGLIBDIR/mpeg/id3v2/frames/urllinkframe.h \
    $$TAGLIBDIR/mpeg/id3v2/id3v2extendedheader.h \
    $$TAGLIBDIR/mpeg/id3v2/id3v2footer.h \
    $$TAGLIBDIR/mpeg/id3v2/id3v2frame.h \
    $$TAGLIBDIR/mpeg/id3v2/id3v2framefactory.h \
    $$TAGLIBDIR/mpeg/id3v2/id3v2header.h \
    $$TAGLIBDIR/mpeg/id3v2/id3v2synchdata.h \
    $$TAGLIBDIR/mpeg/id3v2/id3v2tag.h \
    $$TAGLIBDIR/mpeg/mpegfile.h \
    $$TAGLIBDIR/mpeg/mpegheader.h \
    $$TAGLIBDIR/mpeg/mpegproperties.h \
    $$TAGLIBDIR/mpeg/xingheader.h

SOURCES += $$TAGLIBDIR/audioproperties.cpp \
    $$TAGLIBDIR/fileref.cpp \
    $$TAGLIBDIR/tag.cpp \
    $$TAGLIBDIR/tagunion.cpp \
    $$TAGLIBDIR/ape/apefile.cpp \
    $$TAGLIBDIR/ape/apefooter.cpp \
    $$TAGLIBDIR/ape/apeitem.cpp \
    $$TAGLIBDIR/ape/apeproperties.cpp \
    $$TAGLIBDIR/ape/apetag.cpp \
    $$TAGLIBDIR/asf/asfattribute.cpp \
    $$TAGLIBDIR/asf/asffile.cpp \
    $$TAGLIBDIR/asf/asfpicture.cpp \
    $$TAGLIBDIR/asf/asfproperties.cpp \
    $$TAGLIBDIR/asf/asftag.cpp \
    $$TAGLIBDIR/flac/flacfile.cpp \
    $$TAGLIBDIR/flac/flacmetadatablock.cpp \
    $$TAGLIBDIR/flac/flacpicture.cpp \
    $$TAGLIBDIR/flac/flacproperties.cpp \
    $$TAGLIBDIR/flac/flacunknownmetadatablock.cpp \
    $$TAGLIBDIR/it/itfile.cpp \
    $$TAGLIBDIR/it/itproperties.cpp \
    $$TAGLIBDIR/mod/modfile.cpp \
    $$TAGLIBDIR/mod/modfilebase.cpp \
    $$TAGLIBDIR/mod/modproperties.cpp \
    $$TAGLIBDIR/mod/modtag.cpp \
    $$TAGLIBDIR/mp4/mp4atom.cpp \
    $$TAGLIBDIR/mp4/mp4coverart.cpp \
    $$TAGLIBDIR/mp4/mp4file.cpp \
    $$TAGLIBDIR/mp4/mp4item.cpp \
    $$TAGLIBDIR/mp4/mp4properties.cpp \
    $$TAGLIBDIR/mp4/mp4tag.cpp \
    $$TAGLIBDIR/mpc/mpcfile.cpp \
    $$TAGLIBDIR/mpc/mpcproperties.cpp \
    $$TAGLIBDIR/ogg/flac/oggflacfile.cpp \
    $$TAGLIBDIR/ogg/opus/opusfile.cpp \
    $$TAGLIBDIR/ogg/opus/opusproperties.cpp \
    $$TAGLIBDIR/ogg/speex/speexfile.cpp \
    $$TAGLIBDIR/ogg/speex/speexproperties.cpp \
    $$TAGLIBDIR/ogg/oggfile.cpp \
    $$TAGLIBDIR/ogg/oggpage.cpp \
    $$TAGLIBDIR/ogg/oggpageheader.cpp \
    $$TAGLIBDIR/ogg/xiphcomment.cpp \
    $$TAGLIBDIR/ogg/vorbis/vorbisfile.cpp \
    $$TAGLIBDIR/ogg/vorbis/vorbisproperties.cpp \
    $$TAGLIBDIR/riff/aiff/aifffile.cpp \
    $$TAGLIBDIR/riff/aiff/aiffproperties.cpp \
    $$TAGLIBDIR/riff/wav/infotag.cpp \
    $$TAGLIBDIR/riff/wav/wavfile.cpp \
    $$TAGLIBDIR/riff/wav/wavproperties.cpp \
    $$TAGLIBDIR/riff/rifffile.cpp \
    $$TAGLIBDIR/s3m/s3mfile.cpp \
    $$TAGLIBDIR/s3m/s3mproperties.cpp \
    $$TAGLIBDIR/toolkit/tlist.tcc \
    $$TAGLIBDIR/toolkit/tmap.tcc \
    $$TAGLIBDIR/toolkit/tbytevector.cpp \
    $$TAGLIBDIR/toolkit/tbytevectorlist.cpp \
    $$TAGLIBDIR/toolkit/tbytevectorstream.cpp \
    $$TAGLIBDIR/toolkit/tdebug.cpp \
    $$TAGLIBDIR/toolkit/tdebuglistener.cpp \
    $$TAGLIBDIR/toolkit/tfile.cpp \
    $$TAGLIBDIR/toolkit/tfilestream.cpp \
    $$TAGLIBDIR/toolkit/tiostream.cpp \
    $$TAGLIBDIR/toolkit/tpropertymap.cpp \
    $$TAGLIBDIR/toolkit/trefcounter.cpp \
    $$TAGLIBDIR/toolkit/tstring.cpp \
    $$TAGLIBDIR/toolkit/tstringlist.cpp \
    $$TAGLIBDIR/toolkit/unicode.cpp \
    $$TAGLIBDIR/trueaudio/trueaudiofile.cpp \
    $$TAGLIBDIR/trueaudio/trueaudioproperties.cpp \
    $$TAGLIBDIR/wavpack/wavpackfile.cpp \
    $$TAGLIBDIR/wavpack/wavpackproperties.cpp \
    $$TAGLIBDIR/xm/xmfile.cpp \
    $$TAGLIBDIR/xm/xmproperties.cpp \
    $$TAGLIBDIR/mpeg/id3v1/id3v1genres.cpp \
    $$TAGLIBDIR/mpeg/id3v1/id3v1tag.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/attachedpictureframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/commentsframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/generalencapsulatedobjectframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/ownershipframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/popularimeterframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/privateframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/relativevolumeframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/textidentificationframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/uniquefileidentifierframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/unknownframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/unsynchronizedlyricsframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/frames/urllinkframe.cpp \
    $$TAGLIBDIR/mpeg/id3v2/id3v2extendedheader.cpp \
    $$TAGLIBDIR/mpeg/id3v2/id3v2footer.cpp \
    $$TAGLIBDIR/mpeg/id3v2/id3v2frame.cpp \
    $$TAGLIBDIR/mpeg/id3v2/id3v2framefactory.cpp \
    $$TAGLIBDIR/mpeg/id3v2/id3v2header.cpp \
    $$TAGLIBDIR/mpeg/id3v2/id3v2synchdata.cpp \
    $$TAGLIBDIR/mpeg/id3v2/id3v2tag.cpp \
    $$TAGLIBDIR/mpeg/mpegfile.cpp \
    $$TAGLIBDIR/mpeg/mpegheader.cpp \
    $$TAGLIBDIR/mpeg/mpegproperties.cpp \
    $$TAGLIBDIR/mpeg/xingheader.cpp

DISTFILES += \
    $$TAGLIBDIR/ape/ape-tag-format.txt \
    $$TAGLIBDIR/mpeg/id3v2/id3v2.2.0.txt \
    $$TAGLIBDIR/mpeg/id3v2/id3v2.3.0.txt \
    $$TAGLIBDIR/mpeg/id3v2/id3v2.4.0-frames.txt \
    $$TAGLIBDIR/mpeg/id3v2/id3v2.4.0-structure.txt
