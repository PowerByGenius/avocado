#ifndef AVOCADO_AUDIOTRACKALLVIEWTRACK_H
#define AVOCADO_AUDIOTRACKALLVIEWTRACK_H

#include <QGraphicsScene>
#include <QGraphicsObject>
//#include "audiotrackallview.h"
namespace Avocado {

class AudioTrackAllView;
class AudioTrackAllViewWaveform;
class AudioTrackAllViewMarginLeft;
class AudioTrackAllViewMarginRight;
class AudioTrackAllViewRangeAB;
class AudioTrackAllViewIndicator;
class AudioTrackAllViewMark;
class AudioTrackMark;


// AudioTrackAllViewMarginRangeText
// =============================================================================

class AudioTrackAllViewMarginRangeText : public QGraphicsObject
{
    Q_OBJECT
public:
    enum Ranges{Start =0,End};
    explicit AudioTrackAllViewMarginRangeText(AudioTrackAllView *view, Ranges renge,QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void setPosition(qint64 position);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
private:
    AudioTrackAllView *m_view;
    QString m_text;
    QFont m_font;
    Ranges m_range;
signals:

public slots:
};


// AudioTrackAllViewMarginLeft
// =============================================================================

class AudioTrackAllViewMarginLeft : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit AudioTrackAllViewMarginLeft(AudioTrackAllView *view, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    void setRangePosition(qint64 position);
    void setPosition(qint64 position);
private:
    AudioTrackAllView *m_view;
    QFont m_font;
    AudioTrackAllViewMarginRangeText *m_rangeText;
    QString m_text;
signals:

public slots:
};

// AudioTrackAllViewMarginRight
// =============================================================================
class AudioTrackAllViewMarginRight : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit AudioTrackAllViewMarginRight(AudioTrackAllView *view, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    void setRangePosition(qint64 position);
    void setPosition(qint64 position);
private:
    AudioTrackAllView *m_view;
    QFont m_font;
    AudioTrackAllViewMarginRangeText *m_rangeText;
    QString m_text;
signals:

public slots:
};


// AudioTrackAllViewTrack
// =============================================================================
class AudioTrackAllViewTrack : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit AudioTrackAllViewTrack(AudioTrackAllView *view, QObject *parent = 0);
    void reLayout();
    inline AudioTrackAllViewIndicator *indicator(){return m_indicator;}
    inline AudioTrackAllViewRangeAB *rangeAB(){return m_rangeAB;}
    AudioTrackAllView *view();
    qreal waveformLength() const;
private:
    AudioTrackAllView *m_view;
    AudioTrackAllViewWaveform *m_waveform;
    AudioTrackAllViewMarginLeft *m_marginLeft;
    AudioTrackAllViewMarginRight *m_marginRight;
    AudioTrackAllViewIndicator *m_indicator;
    AudioTrackAllViewRangeAB *m_rangeAB;
    QList<AudioTrackAllViewMark*> m_markItems;
signals:

public slots:
    void waveformFileHasChanged();
    void setPosition(const qint64 &position);
    void modifyRageABPosition(const qint64 &start,const qint64 &end);
    void setRangeABLoop(bool value);
    void audioTrackMarkInsert(Avocado::AudioTrackMark *audioTrackMark);
    void audioTrackMarkRemoved(Avocado::AudioTrackMark *audioTrackMark);
    void audioTrackDataChanged(Avocado::AudioTrackMark *audioTrackMark);

};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKALLVIEWTRACK_H
