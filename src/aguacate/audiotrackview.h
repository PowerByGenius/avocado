#ifndef AVOCADO_AUDIOTRACKVIEW_H
#define AVOCADO_AUDIOTRACKVIEW_H

#include <QGraphicsView>

namespace Avocado {

class AudioTrack;
class AudioTrackViewIndicator;
class AudioTrackMark;

// AudioTrackViewIndicator
// =============================================================================

class AudioTrackViewIndicator : public QWidget
{
    Q_OBJECT
public:
    explicit AudioTrackViewIndicator(QWidget *parent = 0);
protected:
    void paintEvent(QPaintEvent *event);
};

// AudioTrackView
// =============================================================================

class AudioTrackView : public QGraphicsView
{
    Q_OBJECT
public:
    friend class AudioTrackAllView;

    explicit AudioTrackView(QWidget *parent = 0);
    inline AudioTrack* track(){return m_track;}
protected:
    void resizeEvent(QResizeEvent * event);
private:
    AudioTrack* m_track;
public:
    AudioTrackViewIndicator *m_indicator;

    // Mouse Event
    // --------------------------------
protected:
    void wheelEvent(QWheelEvent *event);
    void mouseMoveEvent(QMouseEvent * event);
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent * event);

    // Position
    // --------------------------------
public:
    qint64 currrentPosition() const;
public slots:
    void setPosition(qint64 position); //for media player
    void modifyPosition(qint64 position);
signals:
    void positionChanged(qint64 position);
private:
    qint64 m_currrentPosition;

    // Caption
    // --------------------------------
public slots:
    void showCaption(bool value);
    void setCaptionTextBrowserInteraction(bool value);

    // Hand Drag
    // --------------------------------
public:
    inline bool isHandDragging(){return m_isHandDragging;}
private:
    bool m_isHandDragging;
signals:
    void handDragStared();
    void handDragPosition(qint64 position);
    void handDragDone();

    // Range AB Position
    // --------------------------------
public:
    qint64 rangeABPositionStart() const;
    qint64 rangeABPositionEnd() const;
    void rangePositionHandDragChanged(qint64 start,qint64 end);
public slots:
    void setCurrentPositionForRangeA();
    void setCurrentPositionForRangeB();
    void setRangeAB(qint64 start,qint64 end);
    void cleanRangeAB();
    bool isRangeABLoop();
    void setRangeABLoop(bool value);

signals:
    void rangeABValueHasChanged(qint64 start,qint64 end);
    void setRangeABLoopHasChanged(bool value);
    void RangeABHasCleaned();

    // Mark
    // --------------------------------
public slots:
    void addMarkOnRangeABPosition();
    void marksCleanAll();
    void markFromListViewActivated(Avocado::AudioTrackMark *mark);
    void markStoreLoadAll();
signals:
    void markInserted(Avocado::AudioTrackMark *mark);
    void markMoved(Avocado::AudioTrackMark *mark);
    void markRemoved(Avocado::AudioTrackMark *mark);
    void markDataChanged(Avocado::AudioTrackMark *mark);
    void markAboutToBeInserted(Avocado::AudioTrackMark *mark);
    void markAboutToBeMoved(Avocado::AudioTrackMark *mark,int oldIndex,int newIndex);
    void markAboutToBeRemoved(Avocado::AudioTrackMark *mark);
    void marksAboutToBeCleaned();



    // Files load
    // --------------------------------
public:
    inline QString mediaFilePath() const{return m_mediapath;}
    inline void setMediaFilePath(const QString &filepath){m_mediapath = filepath;}
    inline QString mediaUID() const{return m_mediaUID;}
    inline void setMediaUID(const QString &mediaUID){m_mediaUID = mediaUID;}
private:
    QString m_mediapath;
    QString m_mediaUID;
public slots:
    void loadWaveformFile(const QString &filepath);
    void loadSrtFile(const QString &filepath);
    void loadSrtText(const QString &text);
signals:
    void waveformFileHasChanged();
    void srtFileHasChanged();

};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKVIEW_H
