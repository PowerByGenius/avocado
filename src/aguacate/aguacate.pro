#-------------------------------------------------
#
# Project created by QtCreator 2015-10-23T11:15:55
#
#-------------------------------------------------

QT       += core gui sql opengl multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = aguacate
TEMPLATE = app
CONFIG +=c++11



include(taglib/taglib.pri)

QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.11
INCLUDEPATH += $$PWD/../../thirdparty/build/osx/include
LIBS += -L$$PWD/../../thirdparty/build/osx/lib

LIBS += -lavformat -lavcodec -lavutil -lswscale -lswresample
LIBS += -framework Security \
        -framework CoreMedia \
        -framework CoreAudio \
        -framework CoreVideo \
        -framework VideoDecodeAcceleration \
        -framework AudioToolbox \
        -framework VideoToolbox \
        -framework AudioUnit \
        -framework Carbon
LIBS += -lz -lbz2


#include(QtAV/QtAV.pri)
#include(SoundTouch/SoundTouch.pri)
include(clucene/fulltextsearch.pri)
SOURCES += main.cpp\
    mainwindow.cpp \
    wheelspeed.cpp \
    audiotrack.cpp \
    audiotrackallview.cpp \
    audiotrackallviewindicator.cpp \
    audiotrackallviewmark.cpp \
    audiotrackallviewrange.cpp \
    audiotrackallviewrangeab.cpp \
    audiotrackallviewtrack.cpp \
    audiotrackallviewwaveform.cpp \
    audiotrackcaption.cpp \
    audiotrackmark.cpp \
    audiotrackmarklistview.cpp \
    audiotrackrange.cpp \
    audiotrackrangeab.cpp \
    audiotrackselection.cpp \
    audiotrackview.cpp \
    audiotrackwaveform.cpp \
    exportdroppresetwidget.cpp \
    exportdropwidget.cpp \
    exportprocessitemwidget.cpp \
    exportprocesslistview.cpp \
    exportprocesswidget.cpp \
    librarypagewidget.cpp \
    avocado_libraryfilesimport.cpp \
    maintoolbarwidget.cpp \
    searchresultspanel.cpp \
    avocado_searchengine.cpp \
    captionview.cpp

HEADERS  += mainwindow.h \
    wheelspeed.h \
    audiotrack.h \
    audiotrackallview.h \
    audiotrackallviewindicator.h \
    audiotrackallviewmark.h \
    audiotrackallviewrange.h \
    audiotrackallviewrangeab.h \
    audiotrackallviewtrack.h \
    audiotrackallviewwaveform.h \
    audiotrackcaption.h \
    audiotrackicon.h \
    audiotrackmark.h \
    audiotrackmarklistview.h \
    audiotrackrange.h \
    audiotrackrangeab.h \
    audiotrackselection.h \
    audiotrackview.h \
    audiotrackwaveform.h \
    exportdroppresetwidget.h \
    exportdropwidget.h \
    exportprocessitemwidget.h \
    exportprocesslistview.h \
    exportprocesswidget.h \
    librarypagewidget.h \
    avocado_libraryfilesimport.h \
    maintoolbarwidget.h \
    searchresultspanel.h \
    avocado_searchengine.h \
    captionview.h

FORMS    += mainwindow.ui \
    exportdroppresetwidget.ui \
    exportdropwidget.ui \
    exportprocessitemwidget.ui \
    exportprocesswidget.ui \
    librarypagewidget.ui \
    searchresultspanel.ui \
    captionview.ui

RESOURCES += \
    audiotrack.qrc
