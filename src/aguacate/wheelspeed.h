#ifndef AVOCADO_WHEELSPEED_H
#define AVOCADO_WHEELSPEED_H

#include <QWidget>

namespace Avocado {

class WheelSpeed : public QWidget
{
    Q_OBJECT
public:
    explicit WheelSpeed(QWidget *parent = 0);
    qreal value() const;
signals:
    void valueChanged(qreal value);

public slots:
    void setValue(const qreal &value);
    inline void setDefault(const qreal &value){m_value = value;}
    inline void setStep(const qreal &value){m_step = value;}
    inline void setMaximum(const qreal &value){m_max = value;}
    inline void setMinimum(const qreal &value){m_min = value;}
    void valueUp();
    void valueDown();

    // QWidget interface
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void paintEvent(QPaintEvent *event);
    int m_wheelStep;
    QPoint m_mouseButtonDownPos;
    bool m_isMoving;
    qreal m_default;
    qreal m_value;
    qreal m_max;
    qreal m_min;
    qreal m_step;
};

} // namespace Avocado

#endif // AVOCADO_WHEELSPEED_H
