#ifndef AVOCADO_LIBRARYFILESIMPORT_H
#define AVOCADO_LIBRARYFILESIMPORT_H

#include <QObject>
#include <QVariantList>
#include <QCryptographicHash>

namespace Avocado {

class LibraryFilesImport : public QObject
{
    Q_OBJECT
public:
    explicit LibraryFilesImport(QObject *parent = 0);
protected:
    QByteArray fileChecksum(const QString &fileName, QCryptographicHash::Algorithm hashAlgorithm);
    qint64 generateWaveform(const QString &fileName,const QString &output);
signals:
    void importFilesFinished(const QVariantList &list);
public slots:
    void importFiles(const QStringList &urls);

};

} // namespace Avocado

#endif // AVOCADO_LIBRARYFILESIMPORT_H
