#ifndef AVOCADO_AUDIOTRACKCAPTION_H
#define AVOCADO_AUDIOTRACKCAPTION_H

#include <QGraphicsTextItem>

namespace Avocado {

class AudioTrack;

class AudioTrackCaption : public QGraphicsTextItem
{
    Q_OBJECT
public:
    explicit AudioTrackCaption(AudioTrack *audioTrack,QGraphicsItem *parent = 0);
    AudioTrackCaption(AudioTrack *audioTrack,const QString & text, QGraphicsItem * parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget);
    void setCaptionText(const QString &text);
private:
    AudioTrack *m_audioTrack;
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_AUDIOTRACKCAPTION_H
