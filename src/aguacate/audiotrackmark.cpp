#include "audiotrackmark.h"
#include "audiotrack.h"
#include "audiotrackview.h"
#include "audiotrackwaveform.h"
#include <QDebug>
#include <QPainter>
#include <QTextDocument>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>

namespace Avocado {

///* XPM */
//static const char * const xpm_iconLock[]={
//"32 32 75 2",
//".P c None",
//"Qt c None",
//".R c #000000",
//".S c #000000",
//".9 c #000000",
//".T c #000000",
//"#. c #000000",
//".8 c #000000",
//"## c #000000",
//".# c #c2c2c2",
//"#a c #000000",
//"#b c #000000",
//"#c c #000000",
//".d c #c2c2c2",
//"#d c #000000",
//".i c #c2c2c2",
//"#e c #000000",
//"#f c #000000",
//"#g c #000000",
//"#h c #000000",
//"#i c #000000",
//".w c #c2c2c2",
//".v c #c2c2c2",
//".U c #939393",
//".7 c #888888",
//".a c #c2c2c2",
//".j c #c2c2c2",
//".o c #c2c2c2",
//".x c #c2c2c2",
//".b c #c2c2c2",
//".y c #c2c2c2",
//".6 c #a7a7a7",
//".V c #acacac",
//".e c #c2c2c2",
//".p c #c2c2c2",
//".u c #c2c2c2",
//".5 c #9b9b9b",
//".4 c #9c9c9c",
//".3 c #9d9d9d",
//".2 c #9f9f9f",
//".1 c #a0a0a0",
//".0 c #a2a2a2",
//".Z c #a4a4a4",
//".Y c #a5a5a5",
//".X c #a7a7a7",
//".W c #a9a9a9",
//".c c #c2c2c2",
//".O c #c8c8c8",
//".s c #cacaca",
//".N c #d1d1d1",
//".f c #d2d2d2",
//".k c #d3d3d3",
//".r c #d6d6d6",
//".g c #dddddd",
//".q c #dedede",
//".t c #dfdfdf",
//".h c #e0e0e0",
//".n c #e7e7e7",
//".l c #eaeaea",
//".Q c #ececec",
//".m c #efefef",
//".A c #f2f2f2",
//".B c #f3f3f3",
//".C c #f4f4f4",
//".M c #f5f5f5",
//".D c #f6f6f6",
//".L c #f7f7f7",
//".E c #f8f8f8",
//".F c #f9f9f9",
//".K c #fafafa",
//".G c #fbfbfb",
//".H c #fcfcfc",
//".I c #fdfdfd",
//".J c #fefefe",
//".z c #ffffff",
//"QtQtQtQtQtQtQtQt.#.a.b.c.c.c.c.c.c.c.c.c.c.b.a.#QtQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.d.e.f.g.h.h.h.h.h.h.h.h.h.h.g.f.e.iQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.j.k.l.m.n.h.h.h.h.h.h.h.h.n.m.l.k.oQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.p.q.m.r.s.c.c.c.c.c.c.c.c.s.r.m.t.uQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.n.s.vQtQtQtQtQtQtQtQt.v.s.n.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQt.w.x.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.x.wQtQtQt",
//"QtQtQt.y.l.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.l.yQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.z.z.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.z.z.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.z.z.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.f.c.c.f.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.c.N.N.c.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.c.N.N.c.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.k.O.O.k.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.c.c.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.c.c.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.c.c.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.c.c.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.c.c.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.z.z.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//".P.P.P.c.z.A.B.C.D.E.F.G.H.I.J.z.z.z.J.J.I.H.G.K.F.L.M.z.c.P.P.P",
//".P.P.P.b.Q.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.Q.b.P.P.P",
//".R.S.T.U.V.W.X.Y.Z.0.1.2.3.3.4.5.5.5.4.3.3.2.1.0.Z.Y.X.6.7.8.T.S",
//".P.P.P.R.9#.###a#b#c#d#e#f#g#h#i#i#i#h#g#f#e#d#c#b#a###..9.P.P.P"};

///* XPM */
//static const char * const xpm_iconUnlock[]={
//"32 32 75 2",
//".P c None",
//"Qt c None",
//".R c #000000",
//".S c #000000",
//".9 c #000000",
//".T c #000000",
//"#. c #000000",
//".8 c #000000",
//"## c #000000",
//".# c #c2c2c2",
//"#a c #000000",
//"#b c #000000",
//"#c c #000000",
//".d c #c2c2c2",
//"#d c #000000",
//".i c #c2c2c2",
//"#e c #000000",
//"#f c #000000",
//"#g c #000000",
//"#h c #000000",
//"#i c #000000",
//".w c #c2c2c2",
//".v c #c2c2c2",
//".U c #939393",
//".7 c #888888",
//".a c #c2c2c2",
//".j c #c2c2c2",
//".o c #c2c2c2",
//".x c #c2c2c2",
//".b c #c2c2c2",
//".y c #c2c2c2",
//".6 c #a7a7a7",
//".V c #acacac",
//".e c #c2c2c2",
//".p c #c2c2c2",
//".u c #c2c2c2",
//".5 c #9b9b9b",
//".4 c #9c9c9c",
//".3 c #9d9d9d",
//".2 c #9f9f9f",
//".1 c #a0a0a0",
//".0 c #a2a2a2",
//".Z c #a4a4a4",
//".Y c #a5a5a5",
//".X c #a7a7a7",
//".W c #a9a9a9",
//".c c #c2c2c2",
//".O c #c8c8c8",
//".s c #cacaca",
//".N c #d1d1d1",
//".f c #d2d2d2",
//".k c #d3d3d3",
//".r c #d6d6d6",
//".g c #dddddd",
//".q c #dedede",
//".t c #dfdfdf",
//".h c #e0e0e0",
//".n c #e7e7e7",
//".l c #eaeaea",
//".Q c #ececec",
//".m c #efefef",
//".A c #f2f2f2",
//".B c #f3f3f3",
//".C c #f4f4f4",
//".M c #f5f5f5",
//".D c #f6f6f6",
//".L c #f7f7f7",
//".E c #f8f8f8",
//".F c #f9f9f9",
//".K c #fafafa",
//".G c #fbfbfb",
//".H c #fcfcfc",
//".I c #fdfdfd",
//".J c #fefefe",
//".z c #ffffff",
//"QtQtQtQtQtQtQtQt.#.a.b.c.c.c.c.c.c.c.c.c.c.b.a.#QtQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.d.e.f.g.h.h.h.h.h.h.h.h.h.h.g.f.e.iQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.j.k.l.m.n.h.h.h.h.h.h.h.h.n.m.l.k.oQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.p.q.m.r.s.c.c.c.c.c.c.c.c.s.r.m.t.uQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.n.s.vQtQtQtQtQtQtQtQt.v.s.n.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQt.c.c.c.cQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
//"QtQtQtQtQtQtQt.c.h.h.cQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
//"QtQtQt.w.x.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.c.x.wQtQtQt",
//"QtQtQt.y.l.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.l.yQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.z.z.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.z.z.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.z.z.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.f.c.c.f.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.c.N.N.c.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.c.N.N.c.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.k.O.O.k.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.c.c.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.c.c.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.c.c.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.c.c.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.c.c.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//"QtQtQt.c.z.A.B.C.D.E.F.G.H.I.J.z.z.z.J.J.I.H.G.K.F.L.M.z.cQtQtQt",
//".P.P.P.c.z.A.B.C.D.E.F.G.H.I.J.z.z.z.J.J.I.H.G.K.F.L.M.z.c.P.P.P",
//".P.P.P.b.Q.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.z.Q.b.P.P.P",
//".R.S.T.U.V.W.X.Y.Z.0.1.2.3.3.4.5.5.5.4.3.3.2.1.0.Z.Y.X.6.7.8.T.S",
//".P.P.P.R.9#.###a#b#c#d#e#f#g#h#i#i#i#h#g#f#e#d#c#b#a###..9.P.P.P"};



// AudioTrackMarkHandle
// =============================================================================

AudioTrackMarkHandle::AudioTrackMarkHandle (const HandleTypes types,AudioTrackMark *parent):
    QGraphicsItem(parent),
    m_trackMark(parent),
    m_handleType(types),
    m_size(20)
{
    this->setFlag(QGraphicsItem::ItemIsMovable,true);
    this->setData(Qt::UserRole+100,true); // unhanddragable
}

void AudioTrackMarkHandle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)


    painter->fillRect(this->boundingRect(),QColor(255, 102, 0,207));
    painter->setPen(Qt::white);
    painter->setRenderHint(QPainter::TextAntialiasing,true);

    QString text = QString("%1").arg(m_trackMark->index()+1);

    painter->setFont(m_font);

    if(text.length()>2)
    {
        QFont font = painter->font();
        font.setPointSize(10);
        painter->setFont(font);
    }


    painter->drawText(boundingRect(),Qt::AlignCenter,text);

//    switch(m_handleType){
//    case HandleStart:
//    {

//        painter->drawText(boundingRect(),Qt::AlignCenter,text);

//        QRectF rect = boundingRect();
//        QPointF points[6] = {
//            rect.topLeft(),
//            QPointF(rect.left(), rect.bottom()-rect.height()/2),
//            QPointF(rect.left()+rect.width()/2, rect.bottom()-rect.height()/2),
//            QPointF(rect.right(), rect.bottom()-rect.height()/4),
//            QPointF(rect.right(), rect.bottom()-rect.height()/2),
//            rect.topRight()
//        };
//        painter->setPen(Qt::NoPen);
//        painter->setBrush(QColor(255,102,0,207));
//        painter->drawPolygon(points,6);

//        break;
//    }
//    case HandleEnd:
//    {
//        painter->setFont(m_font);
//        if(text.length()>2)
//        {
//            QFont font = painter->font();
//            font.setPointSize(10);
//            painter->setFont(font);
//        }
//        painter->drawText(boundingRect(),Qt::AlignCenter,text);
//        break;
//    }
//    }

}

QRectF AudioTrackMarkHandle::boundingRect() const
{
    switch(m_handleType){
    case HandleStart:
        return QRectF(m_trackMark->boundingRect().left()-m_size,20+m_size*0.2,m_size,m_size*1.5);
        break;
    case HandleEnd:
        return QRectF(m_trackMark->boundingRect().right(),20+m_size*0.4 + m_size*1.5,m_size,m_size*1.5);
        break;
    }
    return QRectF(0,0,0,0);
}

void AudioTrackMarkHandle::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    event->accept();
    switch(m_handleType){
    case HandleStart:
    {
        qreal x = event->scenePos().rx()+m_buttonDownPos.rx();
        if(x<0) x =0;
        if(x>m_trackMark->markPosEnd()) x = m_trackMark->markPosEnd();
        m_trackMark->setMarkPosStart(x);
        break;
    }

    case HandleEnd:
    {
        qreal x = event->scenePos().rx() - m_buttonDownPos.rx();

        if(x < m_trackMark->markPosStart())
        {
            x = m_trackMark->markPosStart();
        }
        if(x> m_trackMark->m_audioTrack->length())
        {
            x = m_trackMark->m_audioTrack->length();
        }
        m_trackMark->setMarkPosEnd(x);
        break;
    }

    }
}

void AudioTrackMarkHandle::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_buttonDownPos = QPointF(event->scenePos().rx()
                              -mapToScene(this->boundingRect().topLeft()).rx(),0);

    QGraphicsItem::mousePressEvent(event);
}

void AudioTrackMarkHandle::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    m_trackMark->m_audioTrack->markStoreUpdate(m_trackMark);
    QGraphicsItem::mouseReleaseEvent(event);
}



// AudioTrackMarkText
// =============================================================================

AudioTrackMarkText::AudioTrackMarkText(AudioTrackMark *parent):
    QGraphicsTextItem(parent),
    m_trackMark(parent)

{
    setTextInteractionFlags(Qt::TextEditorInteraction);
    setDefaultTextColor(Qt::white);

}

QRectF AudioTrackMarkText::boundingRect() const
{
    QRectF rect = m_trackMark->boundingRect();
    return QRect(0,0,rect.width(),rect.height()-20);
}

void AudioTrackMarkText::reLayout()
{
    prepareGeometryChange();
    setTextWidth(boundingRect().width());
}



// AudioTrackMark
// =============================================================================

AudioTrackMark::AudioTrackMark(AudioTrack *audioTrack, QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_audioTrack(audioTrack),
    m_handleStart(new AudioTrackMarkHandle(AudioTrackMarkHandle::HandleStart,this)),
    m_handleEnd(new AudioTrackMarkHandle(AudioTrackMarkHandle::HandleEnd,this)),
    m_textItem(new AudioTrackMarkText(this)),
    m_posStart(0),m_posEnd(0),isMoving(false),
    m_removeAction(new QAction(this))
{
    this->setZValue(1000);


    m_removeAction->setText(tr("Remove"));
    connect(m_removeAction,&QAction::triggered,[=](bool checked = false){
        Q_UNUSED(checked)
        m_audioTrack->removeMark(this);
    });

    connect(m_textItem->document(),&QTextDocument::contentsChanged,[=](){
        emit m_audioTrack->view()->markDataChanged(this);
        m_audioTrack->markStoreUpdate(this);
    });
}

void AudioTrackMark::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

//    QRectF rectRuler = this->boundingRect();
//    rectRuler.setHeight(20);
//    painter->fillRect(rectRuler,QColor(255, 102, 0,32));

//    QRectF rectBackground = this->boundingRect();
//    rectBackground.adjust(0,21,0,0);
//    painter->fillRect(rectBackground,QColor(30,144,255,32));

    painter->fillRect(boundingRect(),QColor(173,255,47,32));

    QRectF rect = this->boundingRect();
    painter->fillRect(rect.left(),0,1,rect.height(),QColor(255, 102, 0));
    painter->fillRect(rect.right(),0,1,rect.height(),QColor(255, 102, 0));

    //    if(m_isRangeABLoop)
    //        painter->fillRect(rect,QColor(255, 102, 0,64));

//    QRectF rectIconLock = boundingRect();
//    rectIconLock.setTop(10);
//    rectIconLock.setWidth(16);
//    rectIconLock.setHeight(16);
//    m_rectIconLock = rectIconLock;
//    painter->drawPixmap(m_rectIconLock.toRect(),QPixmap(xpm_iconUnlock));

}

QRectF AudioTrackMark::boundingRect() const
{
    qreal height = m_audioTrack->view()->viewport()->height();
    return QRectF(m_posStart,0,m_posEnd-m_posStart,height);
}

void AudioTrackMark::setMarkPosStart(qreal value)
{
    m_posStart=value;
    prepareGeometryChange();
    m_textItem->setPos(boundingRect().left(),20);
    m_textItem->reLayout();
    m_audioTrack->reIndexMark(this);
    emit m_audioTrack->view()->markDataChanged(this);
}

void AudioTrackMark::setMarkPosEnd(qreal value)
{
    m_posEnd = value;
    prepareGeometryChange();
     m_textItem->setPos(boundingRect().left(),20);
    m_textItem->reLayout();
    m_audioTrack->reIndexMark(this);
    emit m_audioTrack->view()->markDataChanged(this);
}

QString AudioTrackMark::mediaFilePath() const
{
    return m_audioTrack->view()->mediaFilePath();
}

int AudioTrackMark::index()
{
    return m_audioTrack->indexOfMark(this);
}

bool AudioTrackMark::toAssending(AudioTrackMark *s1, AudioTrackMark *s2)
{
        return s1->markPosStart() < s2->markPosStart();
}

void AudioTrackMark::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseMoveEvent(event);
}

void AudioTrackMark::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_buttonDownPos = event->buttonDownPos(Qt::LeftButton);
    QGraphicsObject::mousePressEvent(event);
}

void AudioTrackMark::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseReleaseEvent(event);
}

void AudioTrackMark::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{

    QMenu menu;
    menu.addAction(m_removeAction);
    menu.exec(event->screenPos());
}




} // namespace Avocado

