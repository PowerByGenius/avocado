#include "audiotrack.h"
#include "audiotrackview.h"
#include "audiotrackwaveform.h"
//#include "audiotrackselection.h"
#include "audiotrackrange.h"
#include "audiotrackrangeab.h"
#include "audiotrackcaption.h"
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QFile>
#include <QTimer>
#include <QUuid>
#include <QDateTime>
#include <QTextDocument>
#include <QSqlQuery>

namespace Avocado {



QList<QVariantMap> captionParser(const QString &text)
{
    QRegExp rx("(\\d+)\\s*(\\d{2}:\\d{2}:\\d{2},\\d{3}) --> (\\d{2}:\\d{2}:\\d{2},\\d{3})");

    QList<QVariantMap> list;
    int pos = 0;
    int posNext = 0;
    while ((pos = rx.indexIn(text, pos)) != -1)
    {
        QVariantMap map;
        map.insert("index",rx.cap(1).trimmed());
        map.insert("start",rx.cap(2).trimmed());
        map.insert("end",rx.cap(3).trimmed());

        pos += rx.matchedLength();
        posNext = text.indexOf(rx,pos);
        QString temp;
        if(posNext > 0){
            temp = text.mid(pos,posNext-pos);
        }else{
            temp = text.mid(pos);
        }
        map.insert("caption",temp.trimmed());

        list.append(map);
    }

    return list;
}

qint64 textToPosition(const QString &text)
{
    qreal time = 0;
    QRegExp rx("(\\d{2}):(\\d{2}):(\\d{2}),(\\d{3})");
    if(rx.exactMatch(text))
    {
        time = rx.cap(4).toInt()
                +rx.cap(3).toInt() * 1000
                +rx.cap(2).toInt() * 60  * 1000
                +rx.cap(1).toInt() * 60 * 60 * 1000;
    }
    return time;
}


// AudioTrackMarginItem
// =============================================================================

AudioTrackMarginItem::AudioTrackMarginItem(AudioTrack *audioTrack, QGraphicsItem *parent):
    QGraphicsItem(parent),
    m_audioTrack(audioTrack)
{
}

void AudioTrackMarginItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter)
    Q_UNUSED(option)
    Q_UNUSED(widget)
    //    painter->fillRect(boundingRect(),Qt::green);
    return;
}

QRectF AudioTrackMarginItem::boundingRect() const
{
    QRect rect = m_audioTrack->view()->viewport()->rect();

    return QRectF(-rect.width()/2,0,rect.width()/2,rect.height());
}

void AudioTrackMarginItem::relayout()
{
    this->prepareGeometryChange();
}

// AudioTrack
// =============================================================================

AudioTrack::AudioTrack(AudioTrackView *audioTrackView, QObject *parent) :
    QGraphicsScene(parent),
    m_trackView(audioTrackView),
    m_marginItem(new AudioTrackMarginItem(this)),
    m_trackWaveform(new AudioTrackWaveform(this)),
    /*
        m_trackSelection(new AudioTrackSelection(this)),
        isStartedStelect(false),
            */
    m_rangeAB(new AudioTrackRangeAB(this)),
    m_isRangeABLoop(false)
{
    setItemIndexMethod(QGraphicsScene::NoIndex);
    //    addItem(m_marginItem);
    /*    addItem(m_trackSelection);*/

    addItem(m_trackWaveform);

    m_rangeAB->setRangePosStart(0);
    m_rangeAB->setRangePosEnd(length());
    addItem(m_rangeAB);

}

void AudioTrack::relayout()
{

    QRect  rectViewPort = m_trackView->viewport()->rect();
    //    int p = rectViewPort.width()/2;
    //    const qint64 c = currentPosition()-p;

//    int index = 0;
    int padding = rectViewPort.height() * 0.05;
    padding = 0;


    for (int i = 0; i < m_captionItems.count(); ++i) {

        AudioTrackCaption *item = m_captionItems.at(i);

        if(i % 2==0){
            item->setY(rectViewPort.height()/2-item->boundingRect().height()-padding);
        }else{
            item->setY(rectViewPort.height()/2+padding);
        }

        if(i>1)
        {
            AudioTrackCaption *prev = m_captionItems.at(i-2);
            if(item->collidesWithItem(prev))
            {
                int length = item->x() - prev->x();
                prev->setTextWidth(length);
            }
        }

    }

//    foreach (QGraphicsTextItem *item, m_captionItems) {
//        if(index % 2==0){
//            item->setY(rectViewPort.height()/2-item->boundingRect().height()-padding);
//        }else{
//            item->setY(rectViewPort.height()/2+padding);
//        }
//        index++;
//    }

    QRectF itemBoundingRect = this->m_trackWaveform->boundingRect();
    itemBoundingRect.adjust(-rectViewPort.width()/2,0,rectViewPort.width()/2,0);
    this->setSceneRect(itemBoundingRect);

}

void AudioTrack::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    /*
    // 点击鼠标可以选择
    if(isStartedStelect && 0)
    {
        qreal posX = mouseEvent->scenePos().rx();
        qreal buttonDownX = m_buttonDownScenePos.rx();
        qreal waveformStart = m_trackWaveform->boundingRect().left();
        qreal waveformEnd = m_trackWaveform->boundingRect().right();

        if(posX < buttonDownX )
        {
            qreal endPos = buttonDownX-posX;
            qreal startPos = posX;

            if(startPos < waveformStart)
            {
                endPos = buttonDownX;
                startPos = waveformStart;
                if(endPos<startPos) endPos = 0;
            }
            if(startPos > waveformEnd) startPos = waveformEnd;
            if(buttonDownX >= waveformEnd)endPos = waveformEnd-startPos;

            m_trackSelection->setSelectPosEnd(endPos);
            m_trackSelection->setSelectPosStart(startPos);

        }else{

            qreal startPos = buttonDownX;
            qreal endPos = posX-buttonDownX;

            if(startPos <= waveformStart)
            {
                startPos = waveformStart;
                endPos = posX-startPos;
                if(endPos < startPos) endPos = 0;
            }

            if(posX > waveformEnd) endPos = waveformEnd-startPos;
            if(startPos >= waveformEnd) startPos = endPos;

            m_trackSelection->setSelectPosStart(startPos);
            m_trackSelection->setSelectPosEnd(endPos);
        }

        mouseEvent->accept();
    }
    */

    QGraphicsScene::mouseMoveEvent(mouseEvent);
}

void AudioTrack::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    m_buttonDownScenePos = mouseEvent->buttonDownScenePos(Qt::LeftButton);

    /*
    // Stelection
    if(mouseEvent->button() == Qt::LeftButton) {
        QList<QGraphicsItem*> items = this->items(m_buttonDownScenePos);

        bool isSelectable = true;
        foreach (QGraphicsItem *item, items) {
            if(item == m_trackSelection)
            {
                isSelectable = false;
                mouseEvent->accept();
                break;
            }
        }

        isStartedStelect = isSelectable;
    }
    */

    QGraphicsScene::mousePressEvent(mouseEvent);
}

void AudioTrack::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    /*
    // Stelection
    isStartedStelect = false;
    */

    QGraphicsScene::mouseReleaseEvent(mouseEvent);
}


// Position
// -----------------------------------------------------------------------------

qint64 AudioTrack::currentPosition() const
{
    //    QRect rect = m_trackView->viewport()->rect();
    //    QRectF rectViewScene = m_trackView->mapToScene(rect).boundingRect();
    //    int padding = rect.width()/2;

    //    return rectViewScene.left() + padding;
    return m_trackView->currrentPosition();
}

qint64 AudioTrack::length() const
{
    return m_audioWaveformData.count();
}

// Section
// -----------------------------------------------------------------------------
/*
qint64 AudioTrack::selectedPositionStart()
{
    if(m_trackSelection->isVisible())
    {
        return m_trackSelection->selectPosStart();
    }
    return 0;
}

qint64 AudioTrack::selectedPositionEnd()
{
    if(m_trackSelection->isVisible())
    {
        return m_trackSelection->selectPosStart()+m_trackSelection->selectPosEnd();
    }
    return m_audioWaveformData.count();
}
*/

// Range AB Position
// -----------------------------------------------------------------------------

void AudioTrack::showCaption(bool value)
{
    foreach (AudioTrackCaption *item, m_captionItems) {
        if(value){
            item->show();
        }else{
            item->hide();
        }
    }
}

void AudioTrack::setCaptionTextBrowserInteraction(bool value)
{
    foreach (AudioTrackCaption *item, m_captionItems) {
        if(value){
            item->setTextInteractionFlags(Qt::TextBrowserInteraction);
        }else{
            item->setTextInteractionFlags(Qt::NoTextInteraction);
        }
    }
}

// Range AB Position
// -----------------------------------------------------------------------------

qint64 AudioTrack::rangeABPositionStart() const
{
    return m_rangeAB->rangePosStart();
}


qint64 AudioTrack::rangeABPositionEnd() const
{
    return m_rangeAB->rangePosEnd();
}


void AudioTrack::setCurrentPositionForRangeA()
{
    m_rangeAB->setRangePosStart(currentPosition());

    if(m_rangeAB->rangePosStart() > m_rangeAB->rangePosEnd())
    {
        m_rangeAB->setRangePosEnd(m_rangeAB->rangePosStart() + 100);
    }

}

void AudioTrack::setCurrentPositionForRangeB()
{
    m_rangeAB->setRangePosEnd(currentPosition());

    if(m_rangeAB->rangePosEnd() < m_rangeAB->rangePosStart())
    {
        int x = m_rangeAB->rangePosEnd();
        if(x>100)
        {
            x = 100;
        }
        m_rangeAB->setRangePosStart(m_rangeAB->rangePosEnd() - x);
    }

}

void AudioTrack::setRangeAB(qint64 start, qint64 end)
{
    m_rangeAB->setRangePosStart(start);
    m_rangeAB->setRangePosEnd(end);
}

void AudioTrack::cleanRangeAB()
{
    m_rangeAB->setRangePosStart(0);
    m_rangeAB->setRangePosEnd(length());
}

bool AudioTrack::isRangeABLoop()
{
    return m_isRangeABLoop;
}

void AudioTrack::setRangeABLoop(bool value)
{
    m_isRangeABLoop = value;
    this->update();
}

int AudioTrack::indexOfMark(AudioTrackMark *mark) const
{
    return m_markItems.indexOf(mark);
}

int AudioTrack::findMarkIndexByPostion(const qint64 &start, const qint64 &end)
{
    for (int i = 0; i < m_markItems.count(); ++i) {
        AudioTrackMark * mark = m_markItems.at(i);
        if(mark->markPosStart() == start && mark->markPosEnd() == end)
        {
            return i;
        }
    }
    return -1;
}


bool AudioTrack::addMarkOnRangeABPosition()
{
    if(findMarkIndexByPostion(rangeABPositionStart(),rangeABPositionEnd()) < 0)
    {
        AudioTrackMark *mark = new AudioTrackMark(this);
        mark->setMarkPosStart(rangeABPositionStart());
        mark->setMarkPosEnd(rangeABPositionEnd());
        m_markItems.append(mark);
        addItem(mark);
        qSort(m_markItems.begin(),m_markItems.end(),AudioTrackMark::toAssending);
        update();
        markStoreCreate(mark);
        emit view()->markInserted(mark);
        return true;
    }
    return false;
}

void AudioTrack::removeMark(AudioTrackMark *mark)
{
    if(m_markItems.indexOf(mark)>-1)
    {
        emit view()->markAboutToBeRemoved(mark);
        m_markItems.takeAt(m_markItems.indexOf(mark));
        removeItem(mark);
        markStoreDelete(mark);
    }
}

void AudioTrack::reIndexMark(AudioTrackMark *mark)
{
    int oldIndex = m_markItems.indexOf(mark);
    qSort(m_markItems.begin(),m_markItems.end(),AudioTrackMark::toAssending);
    int newIndex = m_markItems.indexOf(mark);
    if(oldIndex != newIndex)
    {
        update();
        emit view()->markAboutToBeMoved(mark,oldIndex,newIndex);
    }

}

void AudioTrack::marksCleanAll()
{
    for (int i = 0; i < m_markItems.count(); ++i) {
        AudioTrackMark *mark = m_markItems.takeAt(i);
        removeItem(mark);
    }
}

void AudioTrack::markStoreCreate(AudioTrackMark *mark)
{
    QString uuid = QUuid::createUuid().toString().mid(1,36).toUpper();
    mark->setUUID(uuid);
    QSqlQuery query;
    query.prepare("INSERT OR REPLACE INTO avocado_marks (unique_id,media_id,start,end,desc_text,desc_html,creation_date,last_modified_date) VALUES (:unique_id,:media_id,:start,:end,:desc_text,:desc_html,:creation_date,:last_modified_date)");
    query.bindValue(":unique_id",uuid);
    query.bindValue(":media_id",view()->mediaUID());
    query.bindValue(":start",mark->markPosStart());
    query.bindValue(":end",mark->markPosEnd());
    query.bindValue(":desc_text",mark->textDocument()->toPlainText());
    query.bindValue(":desc_html",mark->textDocument()->toHtml());
    query.bindValue(":creation_date",QDateTime::currentDateTime());
    query.bindValue(":last_modified_date",QDateTime::currentDateTime());
    query.exec();
}

void AudioTrack::markStoreUpdate(AudioTrackMark *mark)
{
    QSqlQuery query;
    query.prepare("UPDATE avocado_marks SET start=:start,end=:end,desc_text=:desc_text,desc_html=:desc_html WHERE unique_id=:unique_id");
    query.bindValue(":unique_id",mark->uuid());
    query.bindValue(":start",mark->markPosStart());
    query.bindValue(":end",mark->markPosEnd());
    query.bindValue(":desc_text",mark->textDocument()->toPlainText());
    query.bindValue(":desc_html",mark->textDocument()->toHtml());
    query.bindValue(":last_modified_date",QDateTime::currentDateTime());
    query.exec();
}

void AudioTrack::markStoreDelete(AudioTrackMark *mark)
{
    QSqlQuery query;
    query.prepare("DELETE from avocado_marks WHERE unique_id=:unique_id");
    query.bindValue(":unique_id",mark->uuid());
    query.exec();
}

void AudioTrack::markStoreLoadAll()
{
    view()->marksCleanAll();

    QSqlQuery query;
    query.prepare("SELECT * from avocado_marks WHERE media_id=:media_id");
    query.bindValue(":media_id",view()->mediaUID());
    query.exec();
    while(query.next())
    {
        AudioTrackMark *mark = new AudioTrackMark(this);
        mark->setUUID(query.value("unique_id").toString());
        mark->setMarkPosStart(query.value("start").toReal());
        mark->setMarkPosEnd(query.value("end").toReal());
        mark->textDocument()->setHtml(query.value("desc_html").toString());
        m_markItems.append(mark);
        addItem(mark);
        emit view()->markInserted(mark);
    }
    qSort(m_markItems.begin(),m_markItems.end(),AudioTrackMark::toAssending);
    update();
}

// Range AB Position
// -----------------------------------------------------------------------------

void AudioTrack::loadWaveformFile(const QString &filepath)
{

    QFile file(filepath);
    file.open(QIODevice::ReadOnly);
    QDataStream stream(&file);
    m_audioWaveformData.clear();
    stream >> m_audioWaveformData;
    file.close();

    m_rangeAB->setRangePosStart(0);
    m_rangeAB->setRangePosEnd(length());
    relayout();

}

void AudioTrack::loadSrtFile(const QString &filepath)
{

    QFile file(filepath);
    file.open(QIODevice::ReadOnly);
    QString text = file.readAll();
    this->loadSrtText(text);
    file.close();
}

void AudioTrack::loadSrtText(const QString &text)
{
    foreach (AudioTrackCaption *item, m_captionItems) {
        removeItem(item);
    }
    m_captionItems.clear();

    QFont fontCaption;
    fontCaption.setPointSize(18);
    foreach (QVariantMap map, captionParser(text)) {
        qint64 start = textToPosition(map.value("start").toString())/10;
        qint64 end = textToPosition(map.value("end").toString())/10;
        int index = map.value("index").toInt();
        QString caption = map.value("caption").toString().replace("\n"," ");
        AudioTrackCaption *item = new AudioTrackCaption(this);
        item->setCaptionText(caption);
        item->setFont(fontCaption);
        //        item->setTextInteractionFlags(Qt::TextBrowserInteraction);
        item->setDefaultTextColor(Qt::white);

        item->setData(Qt::UserRole+100,start);
        item->setData(Qt::UserRole+110,end);
        item->setData(Qt::UserRole+120,index);
        item->setData(Qt::UserRole+130,caption);
        item->setParent(m_trackWaveform);
        item->setPos(start,100);
        this->addItem(item);
        m_captionItems.append(item);
    }

    relayout();
}


} // namespace Avocado

