#ifndef AVOCADO_EXPORTPROCESSLISTVIEW_H
#define AVOCADO_EXPORTPROCESSLISTVIEW_H

#include <QListWidget>
#include <QItemDelegate>
namespace Avocado {

class ExportProcessListDelgate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit ExportProcessListDelgate(QWidget *parent = 0);


    // QAbstractItemDelegate interface
public:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};


class ExportProcessItemWidget;
class ExportProcessListView : public QListWidget
{
    Q_OBJECT
public:
    explicit ExportProcessListView(QWidget *parent = 0);

signals:
private slots:
    void onItemWidgetFinished(ExportProcessItemWidget *itemWidget);
public slots:
    void addTask(const QVariantMap &data);

    // QAbstractItemView interface
protected:
    QMimeData *mimeData(const QList<QListWidgetItem *> items) const;
    void startDrag(Qt::DropActions supportedActions);
};

} // namespace Avocado

#endif // AVOCADO_EXPORTPROCESSLISTVIEW_H
