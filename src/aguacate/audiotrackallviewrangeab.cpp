#include "audiotrackallviewrangeab.h"
#include "audiotrackallView.h"
#include "audiotrackallviewtrack.h"
#include "audiotrackallviewwaveform.h"
#include "audiotrack.h"
#include <QDebug>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

namespace Avocado {

AudioTrackAllViewRangeABHandle::AudioTrackAllViewRangeABHandle (const HandleTypes types,AudioTrackAllViewRangeAB *parent):
    QGraphicsItem(parent),
    m_trackRange(parent),
    m_handleType(types),
    m_size(18)
{
    this->setFlag(QGraphicsItem::ItemIsMovable,true);
}

void AudioTrackAllViewRangeABHandle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->fillRect(this->boundingRect(),QColor(255, 102, 0,207));
    painter->setPen(Qt::white);
    switch(m_handleType){
    case HandleStart:
        painter->drawText(boundingRect(),Qt::AlignCenter,"A");
        break;
    case HandleEnd:
        painter->drawText(boundingRect(),Qt::AlignCenter,"B");
        break;
    }

}

QRectF AudioTrackAllViewRangeABHandle::boundingRect() const
{
    switch(m_handleType){
    case HandleStart:
        return QRectF(m_trackRange->boundingRect().left()-m_size,
                      m_trackRange->boundingRect().height()-m_size*1.2,m_size,m_size);
        break;
    case HandleEnd:
        return QRectF(m_trackRange->boundingRect().right(),
                      m_trackRange->boundingRect().height()-m_size*1.2,m_size,m_size);
        break;
    }
    return QRectF(0,0,0,0);
}

void AudioTrackAllViewRangeABHandle::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    event->accept();
    switch(m_handleType){
    case HandleStart:
    {
        qreal x = event->scenePos().rx() - m_trackRange->parentItem()->boundingRect().left();
        if(x<0) x =0;
        if(x>m_trackRange->rangePosEnd()) x = m_trackRange->rangePosEnd();
        m_trackRange->setRangePosStart(x);
        m_trackRange->rangeDragChanged();
        break;
    }

    case HandleEnd:
    {
        qreal x = event->scenePos().rx() - m_trackRange->parentItem()->boundingRect().left() - m_buttonDownPos.rx();

        if(x < m_trackRange->rangePosStart())
        {
            x = m_trackRange->rangePosStart();
        }

        if(x> m_trackRange->parentItem()->boundingRect().width())
        {
            x = m_trackRange->parentItem()->boundingRect().width();
        }
        m_trackRange->setRangePosEnd(x);
        m_trackRange->rangeDragChanged();
        break;
    }

    }
}

void AudioTrackAllViewRangeABHandle::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_buttonDownPos = QPointF(event->scenePos().rx()
                              -mapToScene(this->boundingRect().topLeft()).rx(),0);

    QGraphicsItem::mousePressEvent(event);
}

void AudioTrackAllViewRangeABHandle::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
}


AudioTrackAllViewRangeAB::AudioTrackAllViewRangeAB(AudioTrackAllView *view, QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_view(view),
    m_handleStart(new AudioTrackAllViewRangeABHandle(AudioTrackAllViewRangeABHandle::HandleStart,this)),
    m_handleEnd(new AudioTrackAllViewRangeABHandle(AudioTrackAllViewRangeABHandle::HandleEnd,this)),
    m_posStart(0),m_posEnd(0),isMoving(false),
    m_positionStart(0),m_positionEnd(0),m_isRangeABLoop(false)
{
    this->setZValue(10000);
}

void AudioTrackAllViewRangeAB::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    QRectF rect = this->boundingRect();


    painter->fillRect(rect.left(),0,1,rect.height(),QColor(255, 102, 0));
    painter->fillRect(rect.right(),0,1,rect.height(),QColor(255, 102, 0));

    if(m_isRangeABLoop)
        painter->fillRect(rect,QColor(255, 102, 0,64));
}

QRectF AudioTrackAllViewRangeAB::boundingRect() const
{
    qreal height = m_view->viewportHeight();
   // return QRectF(m_posStart,0,m_posEnd-m_posStart,height);

    QRectF rect;
    if(parentItem()) rect = parentItem()->boundingRect();
    return QRectF(rect.left()+m_posStart,0,(m_posEnd-m_posStart),height);
}

void AudioTrackAllViewRangeAB::setRangePosition(const qint64 &start, const qint64 &end)
{
    if(m_view->m_audioTrack.isNull())
    {
        setRangePosStart(0);
        setRangePosEnd(0);
        return;
    }

    m_positionStart = start;
    m_positionEnd = end;

    QRectF rect;
    if(parentItem()) rect = parentItem()->boundingRect();
    qreal widthWaveform = rect.width();
    qreal q =  widthWaveform/m_view->length();

    qreal xStart = (start * q);
    qreal xEnd = (end * q);

    setRangePosStart(xStart);
    setRangePosEnd(xEnd);
}

void AudioTrackAllViewRangeAB::rangeDragChanged()
{

    QRectF rect;
    if(parentItem()) rect = parentItem()->boundingRect();

    qreal widthWaveform = rect.width();
    qreal q =  widthWaveform/m_view->length();
    m_positionStart = qRound(m_posStart/q);
    m_positionEnd = qRound(m_posEnd/q);
    emit rangePositionChanged(m_positionStart,m_positionEnd);
}

void AudioTrackAllViewRangeAB::reLayout()
{
    QRectF rect;
    if(parentItem()) rect = parentItem()->boundingRect();

    qreal widthWaveform = rect.width();
    qreal q =  widthWaveform/m_view->length();

    qreal xStart = (m_positionStart * q);
    qreal xEnd = (m_positionEnd * q);
    setRangePosStart(xStart);
    setRangePosEnd(xEnd);
}

void AudioTrackAllViewRangeAB::setRangeABLoop(bool value)
{
    m_isRangeABLoop = value;
}

void AudioTrackAllViewRangeAB::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseMoveEvent(event);
}

void AudioTrackAllViewRangeAB::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_buttonDownPos = event->buttonDownPos(Qt::LeftButton);
    QGraphicsObject::mousePressEvent(event);
}

void AudioTrackAllViewRangeAB::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseReleaseEvent(event);
}


} // namespace Avocado

