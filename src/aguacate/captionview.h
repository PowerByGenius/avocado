#ifndef AVOCADO_CAPTIONVIEW_H
#define AVOCADO_CAPTIONVIEW_H

#include <QObject>
#include <QStyledItemDelegate>
#include <QStandardItemModel>
#include <QListView>

namespace Ui {
class CaptionView;
}

namespace Avocado {

class CaptionListModel;
class CaptionListDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:

    explicit CaptionListDelegate(CaptionListModel *model,QObject * parent = 0);
    void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;

    QWidget *createEditor(QWidget * parent, const QStyleOptionViewItem & option, const QModelIndex & index) const;
    void updateEditorGeometry(QWidget * editor, const QStyleOptionViewItem & option, const QModelIndex & index) const;
private:
    static QSize iconSize;
    static int padding;
    QFont m_headerFont;
    QFont m_subheaderFont;
    CaptionListModel *m_model;
};

class CaptionListModel : public QStandardItemModel
{
    Q_OBJECT
public:
    enum datarole { HeaderRole = Qt::UserRole + 100,
                    SubheaderRole,StartTimeRole,EndTimeRole,SeqRole,
                    PositionStartRole,PositionEndRole,CurrentPositionRole};

    explicit CaptionListModel(QObject *parent = 0);
    void loadSrtText(const QString &text);
    void loadSrtFile(const QString &path);
    inline void setPosition(const qint64 position){ m_position = position;}
    inline qint64 position() const {return m_position;}
private:
    void loadData(const QList<QVariantMap> &list);
    qint64 StringToPosition(const QString &text);
    qint64 m_position;
signals:

public slots:
};

class CaptionListView : public QListView
{
    Q_OBJECT
public:
    explicit CaptionListView(QWidget *parent = 0);
protected:
    void wheelEvent(QWheelEvent *event);
    void resizeEvent(QResizeEvent *event);
signals:
    void wheel();
public slots:
};

class CaptionView : public QWidget
{
    Q_OBJECT
public:
    explicit CaptionView(QWidget *parent = 0);
    ~CaptionView();
    void init();
private:
    Ui::CaptionView *ui;
private:
    CaptionListModel *m_captionListModel;
    bool m_isAutoScroll;
signals:
    void positionChanged(qreal);
public slots:
    void loadSrtFile(const QString &path);
    void loadSrtText(const QString &text);
    void setPosition(const qint64 position);
    void setAutoScroll(bool value);
};


} // namespace Avcado

#endif // AVOCADO_CAPTIONVIEW_H
