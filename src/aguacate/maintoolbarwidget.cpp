#include "maintoolbarwidget.h"
#include <QMouseEvent>

MainToolbarWidget::MainToolbarWidget(QWidget *parent) : QWidget(parent)
{

}

void MainToolbarWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && childAt(event->pos()) == 0) {
        m_isDragging = true;
        m_dragPosition = event->globalPos() - topLevelWidget()->frameGeometry().topLeft();
        event->accept();
    }
    QWidget::mousePressEvent(event);
}

void MainToolbarWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton && m_isDragging) {
        topLevelWidget()->move(event->globalPos() - m_dragPosition);
        event->accept();
    }

    QWidget::mouseMoveEvent(event);
}

void MainToolbarWidget::mouseReleaseEvent(QMouseEvent *event)
{
    m_isDragging = false;
    QWidget::mouseReleaseEvent(event);
}

