#-------------------------------------------------
#
# Project created by QtCreator 2015-10-21T00:30:01
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = player
TEMPLATE = app
CONFIG += C++11

include (QtAV/QtAV.pri)

QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.11
INCLUDEPATH += $$PWD/../../thirdparty/build/osx/include
LIBS += -L$$PWD/../../thirdparty/build/osx/lib





SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
