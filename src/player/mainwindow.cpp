#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QHBoxLayout>
#include <QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_player = new QtAV::AVPlayer(this);
    m_out = new QtAV::VideoOutput(this);
    m_player->setRenderer(m_out);
    QWidget * w = m_out->widget();
    ui->verticalLayout->addWidget(w);
    //w->resize(400,400);
    m_player->setFile("/Users/kiko/Desktop/abc/Friends.with.Benefits.2011.BluRay.720p.x264.AC3-WOFEI.mp4");
    m_player->play();
}

MainWindow::~MainWindow()
{
    delete ui;
}
