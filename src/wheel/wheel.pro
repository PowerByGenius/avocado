#-------------------------------------------------
#
# Project created by QtCreator 2015-10-21T23:58:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = wheel
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    wheel.cpp

HEADERS  += mainwindow.h \
    wheel.h

FORMS    += mainwindow.ui
