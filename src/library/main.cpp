#include "librarypagewidget.h"
#include "testwidget.h"
#include <QApplication>
#include <QSqlDatabase>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QApplication::setApplicationName("Avocado");
    QApplication::setApplicationDisplayName("Avocado");
    QApplication::setApplicationVersion("1.0");

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    // db.setDatabaseName("/Users/kiko/Desktop/Library/Avocado/Qt/Avocado/src/data/mnemosyne.sqlite");
    db.setDatabaseName("/Users/kiko/Desktop/Library/Avocado/Qt/Avocado/database/avocado.sqlite");
    bool ok = db.open();
    qDebug() << ok << db.databaseName();
    LibraryPageWidget w;
    w.show();

    return a.exec();

}
