#include "avocado_libraryfilesimport.h"
#include <fileref.h>
#include <tag.h>
#include <id3v2tag.h>
#include <mpegfile.h>
#include <mpegheader.h>
#include <attachedpictureframe.h>
#include <mp4tag.h>
#include <mp4coverart.h>

#include <QFileInfo>
#include <QDebug>
#include <QTime>
#include <QElapsedTimer>
#include <QImage>
#include <QDir>
#include <QBuffer>
namespace Avocado {

LibraryFilesImport::LibraryFilesImport(QObject *parent) : QObject(parent)
{

}

QByteArray LibraryFilesImport::fileChecksum(const QString &fileName,
                                            QCryptographicHash::Algorithm hashAlgorithm)
{
    QFile f(fileName);
    if (f.open(QFile::ReadOnly)) {
        QCryptographicHash hash(hashAlgorithm);
        if (hash.addData(&f)) {
            return hash.result();
        }
    }
    return QByteArray();
}

void LibraryFilesImport::importFiles(const QStringList &urls)
{
    QStringList suffixs = QStringList()
            << "m4a" << "m4r" << "m4b" << "m4p" << "3g2" << "mp4" << "mp3";
    QStringList files;

    foreach (QString url, urls) {
        QString suffix = QFileInfo(url).suffix().toLower();
        if(suffixs.contains(suffix)) files.append(url);
    }

    QVariantList list;

//    QElapsedTimer hashTimer;
//    hashTimer.start();

    foreach (QString path, files)
    {

        QString mediaFolder = "/Users/kiko/Desktop/media/media";
        QString coverFolder = "/Users/kiko/Desktop/media/cover";

        QString hash = fileChecksum(path,QCryptographicHash::Md5).toHex();
        // checking file exit
        QFileInfo info(path);
        QString storePath = QString("%1.%2").arg(hash).arg(info.suffix());
        QVariantMap map;
        QString fileExt = info.suffix();
        map["file_source"] = path;
        map["file_hash"] = hash;
        map["file_name"] = storePath;
        map["file_path"] = storePath;
        map["file_size"] = info.size();
        map["file_ext"] = fileExt;
        map["file_type"] = fileExt;

        map["meta_title"] = QString();
        map["meta_album"] = QString();
        map["meta_artist"] = QString();
        map["meta_comment"] = QString();
        map["meta_genre"] = QString();
        map["meta_tracknumber"] = -1;
        map["meta_year"] = -1;
        map["meta_date"] =  QString();
        map["meta_coverart"] =  QString();

        map["stream_length"] =  -1;
        map["stream_bitrate"] = -1;
        map["stream_samplerate"] = -1;
        map["stream_channels"] =  -1;

        map["meta_caption_en"] =  QString();
        map["meta_caption_cn"] =  QString();

        TagLib::FileRef f(path.toUtf8().data());
        if(!f.isNull() && f.tag() && f.audioProperties())
        {
            TagLib::Tag *tag = f.tag();
            TagLib::AudioProperties *audio = f.audioProperties();

            map["meta_title"] = TStringToQString(tag->title());
            map["meta_album"] = TStringToQString(tag->album());
            map["meta_artist"] = TStringToQString(tag->artist());
            map["meta_comment"] = TStringToQString(tag->comment());
            map["meta_genre"] = TStringToQString(tag->genre());
            map["meta_tracknumber"] = tag->track();
            map["meta_year"] = tag->year();

            map["stream_length"] =  audio->length();
            map["stream_bitrate"] = audio->sampleRate();
            map["stream_samplerate"] = audio->bitrate();
            map["stream_channels"] =  audio->channels();


            auto saveImage = [=](const char * data, qint64 maxSize){
                QCryptographicHash hash(QCryptographicHash::Md5);
                hash.addData(data,maxSize);
                QString imageSourceHash = hash.result().toHex();

                QString sourcePath = QString("%1/source/%2.data").arg(coverFolder)
                        .arg(imageSourceHash);

                if(!QFile(sourcePath).exists())
                {
                    QFile file(sourcePath);
                    file.open(QFile::WriteOnly);
                    file.write(data,maxSize);
                    file.close();
                }

                QImage image;
                image.loadFromData((const uchar *) data,maxSize);


                QString coverThumb40 = QString("%1/40/%2.png").arg(coverFolder)
                        .arg(imageSourceHash);

                if(!QFile(coverThumb40).exists())
                {
                    image.scaled(40,40,Qt::KeepAspectRatioByExpanding,Qt::SmoothTransformation)
                            .save(coverThumb40);
                }

                return imageSourceHash;
            };

            QStringList mp4Ext = QStringList()
                    << "m4a" << "m4r" << "m4b" << "m4p" << "3g2" << "mp4";

            if(mp4Ext.contains(fileExt.toLower()))
            {
                TagLib::MP4::Tag *tagmp4 = static_cast<TagLib::MP4::Tag *>(f.tag());

                TagLib::MP4::ItemListMap itemsListMap = tagmp4->itemListMap();

                if (itemsListMap.contains("covr"))
                {
                    TagLib::MP4::Item coverItem = itemsListMap["covr"];
                    TagLib::MP4::CoverArtList coverArtList = coverItem.toCoverArtList();
                    TagLib::MP4::CoverArt coverArt = coverArtList.front();

                    map["meta_coverart"] = saveImage(coverArt.data().data(),coverArt.data().size());
                }

            } else if(fileExt.toLower() == "mp3"){
                TagLib::MPEG::File *file = static_cast<TagLib::MPEG::File *>(f.file());
                if(file->hasID3v2Tag())
                {
                    TagLib::ID3v2::FrameList l = file->ID3v2Tag()->frameList("APIC");
                    if(!l.isEmpty())
                    {
                        TagLib::ID3v2::AttachedPictureFrame *f =
                                static_cast<TagLib::ID3v2::AttachedPictureFrame *>(l.front());

                        map["meta_coverart"] = saveImage(f->picture().data(), f->picture().size());
                    }
                }
            }
        }else{
            continue;
        }

        QRegExp exp("(\\d{2}\\s+)?(\\d{4}-\\d{2}-\\d{2}) (.*)");

        if(exp.exactMatch(info.baseName()))
        {
            map["meta_date"] =exp.cap(2);
        }

        QString srtfileEn = QString("%1/%2.srt")
                .arg(info.absoluteDir().absolutePath())
                .arg(info.completeBaseName());

        if(QFile(srtfileEn).exists())
        {
            QFile file(srtfileEn);
            if(file.open(QFile::ReadOnly))
            {
                QTextStream stream(&file);
                map["meta_caption_en"] = stream.readAll();
                file.close();
            }
        }

        QString srtfileCN = QString("%1/%2.cn.srt")
                .arg(info.absoluteDir().absolutePath())
                .arg(info.completeBaseName());

        if(QFile(srtfileCN).exists())
        {
            QFile file(srtfileCN);
            if(file.open(QFile::ReadOnly))
            {
                QTextStream stream(&file);
                map["meta_caption_cn"] = stream.readAll();
                file.close();
            }
        }

        list.append(map);

        QString mediaPath = QString("%1/%2").arg(mediaFolder).arg(storePath);
        if(QFile(mediaPath).exists()) continue;
        if(!QFile::copy(path,mediaPath)) continue;
    }

    emit importFilesFinished(list);

//    qDebug() << list;
//    qDebug() << hashTimer.elapsed();

}

} // namespace Avocado

