#-------------------------------------------------
#
# Project created by QtCreator 2015-10-01T01:54:50
#
#-------------------------------------------------

QT       += core gui
CONFIG += C++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets sql

TARGET = library
TEMPLATE = app

include(taglib/taglib.pri)
#include(../taglib/taglib.pri)

SOURCES += main.cpp\
        testwidget.cpp \
    librarypagewidget.cpp \
    avocado_libraryfilesimport.cpp

HEADERS  += testwidget.h \
    librarypagewidget.h \
    avocado_libraryfilesimport.h

FORMS    += testwidget.ui \
    librarypagewidget.ui
