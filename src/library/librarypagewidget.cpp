#include "librarypagewidget.h"
#include "ui_librarypagewidget.h"
#include "avocado_libraryfilesimport.h"

#include <QSqlQuery>
#include <QSqlRecord>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QDebug>
#include <QUuid>
#include <QDateTime>

namespace DataRule {
    enum ItemRules{
        unique_id = Qt::UserRole+1000,file_path,file_size,file_type,file_ext,file_hash,file_name,file_source,meta_title,meta_artist,meta_album,meta_comment,meta_genre,meta_year,meta_tracknumber,meta_date,meta_coverart,meta_caption_en,meta_caption_cn,stream_length,stream_bitrate,stream_samplerate,stream_channels,creation_date,last_modified_date
    };
}

const QString SqlFiledString = "meta_title,stream_length,meta_artist,meta_album";
const QString SqlLibraryTableName = "avocado_media";

LibraryPageWidget::LibraryPageWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LibraryPageWidget),
    m_libraryFileImport(new Avocado::LibraryFilesImport()),
    m_libraryNavigationTreeModel(new QStandardItemModel(this)),
    m_libraryItemsModel(new QStandardItemModel(this)),
    m_libraryItemsListModel(new QSortFilterProxyModel(this)),
    m_artistsListModel(new QSortFilterProxyModel(this)),
    m_artistsAlbumListModel(new QSortFilterProxyModel(this)),
    m_libraryProxyModel(new QIdentityProxyModel(this)),
    m_artistsTreeModel(new QStandardItemModel(this)),
    m_albumsTreeModel(new QStandardItemModel(this)),
    m_albumsListModel(new QSortFilterProxyModel(this))
{
    ui->setupUi(this);
    setAcceptDrops(true);

    connect(this,&LibraryPageWidget::requestImportFiles,
            m_libraryFileImport,&Avocado::LibraryFilesImport::importFiles);

    connect(m_libraryFileImport,&Avocado::LibraryFilesImport::importFilesFinished,
            this,&LibraryPageWidget::importFilesFinished);

    m_libraryFileImport->moveToThread(&m_libraryFileImportThread);
    m_libraryFileImportThread.start();
    init();
}

LibraryPageWidget::~LibraryPageWidget()
{
    m_libraryFileImportThread.quit();
    m_libraryFileImportThread.wait();
    m_libraryFileImport->deleteLater();
    delete ui;
}

void LibraryPageWidget::appSaveState()
{
//    QSettings settings;
//    settings.setValue("mainWindow/geometry", saveGeometry());
//    settings.setValue("mainWindow/windowState", saveState());
//    ui->centralWidget->appSaveState();
}

void LibraryPageWidget::appResotreState()
{
//    QSettings settings;
//    restoreGeometry(settings.value("mainWindow/geometry").toByteArray());
//    restoreState(settings.value("mainWindow/windowState").toByteArray());
//    ui->centralWidget->appRestoreState();
}



void LibraryPageWidget::init()
{

    ui->splitter_1->setStretchFactor(0,0);
    ui->splitter_1->setStretchFactor(1,1);

    ui->splitter_2->setStretchFactor(0,0);
    ui->splitter_2->setStretchFactor(1,1);
    ui->splitter_2->setCollapsible(0,false);

    ui->splitter_3->setStretchFactor(0,0);
    ui->splitter_3->setStretchFactor(1,1);
    ui->splitter_3->setCollapsible(0,false);

    ui->libraryNavigationTreeView->setAttribute(Qt::WA_MacShowFocusRect,false);
    ui->libraryNavigationTreeView->setRootIsDecorated(false);
    ui->libraryNavigationTreeView->setAnimated(true);
    ui->libraryNavigationTreeView->setItemsExpandable(false);
    ui->libraryNavigationTreeView->setHeaderHidden(true);

    ui->libraryItemsTreeView->setAttribute(Qt::WA_MacShowFocusRect,false);
    ui->artistsTreeView->setAttribute(Qt::WA_MacShowFocusRect,false);
    ui->artistsListTreeView->setAttribute(Qt::WA_MacShowFocusRect,false);
    ui->artistsTreeView->setHeaderHidden(true);

    ui->albumsTreeView->setAttribute(Qt::WA_MacShowFocusRect,false);
    ui->albumsListTreeView->setAttribute(Qt::WA_MacShowFocusRect,false);

    ui->albumsTreeView->setHeaderHidden(true);


    // init libraryNavigationTreeView
    // ----------------------------------

    ui->libraryNavigationTreeView->setModel(m_libraryNavigationTreeModel);

    {
        auto initItem = [](QStandardItem *item){
            item->setEditable(false);
            item->setCheckable(false);
            item->setDragEnabled(false);
            item->setDropEnabled(false);
            item->setSelectable(true);
        };

        QStandardItem *library = new QStandardItem(tr("Library"));
        initItem(library);
        library->setEnabled(false);
        library->setData("library",Qt::UserRole+100);

        QStandardItem *songs = new QStandardItem(tr("Songs"));
        songs->setData("songs",Qt::UserRole+100);
        initItem(songs);
        library->appendRow(songs);
        QStandardItem *artists = new QStandardItem(tr("Artists"));
        artists->setData("artists",Qt::UserRole+100);
        initItem(artists);
        library->appendRow(artists);
        QStandardItem *albums = new QStandardItem(tr("Albums"));
        albums->setData("albums",Qt::UserRole+100);
        initItem(albums);
        library->appendRow(albums);
        m_libraryNavigationTreeModel->appendRow(library);
        ui->libraryNavigationTreeView->expandAll();
    }

    connect(ui->libraryNavigationTreeView->selectionModel(),&QItemSelectionModel::currentChanged,
            [=](const QModelIndex & current, const QModelIndex & previous)
    {
        Q_UNUSED(previous)
        if(!current.isValid()) return;
        QString itemName = current.data(Qt::UserRole+100).toString();
        if(itemName == "songs") ui->libraryStackedWidget->setCurrentIndex(0);
        if(itemName == "artists") ui->libraryStackedWidget->setCurrentIndex(1);
        if(itemName == "albums") ui->libraryStackedWidget->setCurrentIndex(2);

    });


    m_libraryItemsListModel->setSourceModel(m_libraryItemsModel);
    ui->libraryItemsTreeView->setModel(m_libraryItemsListModel);
    ui->libraryItemsTreeView->setSortingEnabled(true);

    m_artistsListModel->setSourceModel(m_libraryItemsModel);
    m_artistsAlbumListModel->setSourceModel(m_artistsListModel);
    ui->artistsListTreeView->setModel(m_artistsAlbumListModel);
    ui->artistsListTreeView->setSortingEnabled(true);

    m_albumsListModel->setSourceModel(m_libraryItemsModel);
    ui->albumsListTreeView->setModel(m_albumsListModel);
    ui->albumsListTreeView->setSortingEnabled(true);


    m_libraryProxyModel->setSourceModel(m_libraryItemsModel);

    connect(m_libraryProxyModel,&QIdentityProxyModel::modelReset,[this](){
        {
            {
                m_artistsTreeModel->clear();
                QSqlQuery query(QString("SELECT meta_artist,meta_album FROM %1 GROUP BY meta_album ORDER BY meta_artist ASC")
                                .arg(SqlLibraryTableName));
                int meta_artist = query.record().indexOf("meta_artist");
                int meta_album = query.record().indexOf("meta_album");
                QStandardItem *artistItem = new QStandardItem();
                while (query.next()) {
                    QString artist = query.value(meta_artist).toString();
                    QString album = query.value(meta_album).toString();
                    if(artistItem->text() != artist){
                        artistItem = new QStandardItem(artist);
                        m_artistsTreeModel->appendRow(artistItem);
                    }
                    QStandardItem *albumItem = new QStandardItem(album);
                    artistItem->appendRow(albumItem);
                }
                ui->artistsTreeView->expandAll();
            }

            {
                m_albumsTreeModel->clear();
                QSqlQuery query(QString("SELECT meta_artist,meta_album FROM %1 GROUP BY meta_album ORDER BY meta_album ASC")
                                .arg(SqlLibraryTableName));
                int meta_artist = query.record().indexOf("meta_artist");
                int meta_album = query.record().indexOf("meta_album");
                while (query.next()) {
                    QString artist = query.value(meta_artist).toString();
                    QString album = query.value(meta_album).toString();
                    QStandardItem *albumItem = new QStandardItem(album);
                    m_albumsTreeModel->appendRow(albumItem);
                }
                ui->albumsTreeView->expandAll();
            }
        }
    });


    ui->artistsTreeView->setModel(m_artistsTreeModel);

    connect(ui->artistsTreeView->selectionModel(),&QItemSelectionModel::currentChanged,
            [this](const QModelIndex & current, const QModelIndex & previous)
    {
        Q_UNUSED(previous)
        if(!current.isValid()) return;

        if(!current.parent().isValid()){

            m_artistsListModel->setFilterFixedString(current.data(Qt::DisplayRole).toString());
            m_artistsListModel->setFilterKeyColumn(2);
            m_artistsAlbumListModel->setFilterFixedString(QString());
        }else{

            m_artistsListModel->setFilterFixedString(current.parent().data(Qt::DisplayRole).toString());
            m_artistsListModel->setFilterKeyColumn(2);
            m_artistsAlbumListModel->setFilterFixedString(current.data(Qt::DisplayRole).toString());
            m_artistsAlbumListModel->setFilterKeyColumn(3);
        }

    });

    ui->albumsTreeView->setModel(m_albumsTreeModel);

    connect(ui->albumsTreeView->selectionModel(),&QItemSelectionModel::currentChanged,
            [=](const QModelIndex & current, const QModelIndex & previous)
    {
        Q_UNUSED(previous)
        if(!current.isValid()) return;
        m_albumsListModel->setFilterFixedString(current.data(Qt::DisplayRole).toString());
        m_albumsListModel->setFilterKeyColumn(3);
    });


    reflash();
}

void LibraryPageWidget::reflash()
{
    m_libraryItemsModel->clear();
    QSqlQuery query(QString("SELECT * FROM %1").arg(SqlLibraryTableName));
    query.exec();

    QList<QStandardItem *> list1,list2,list3,list4,list5;
    while (query.next()) {
            QStandardItem *item = new QStandardItem();
            item->setText(query.value("meta_title").toString());
            list1.append(item);

            QStandardItem *column = new QStandardItem();
            column->setTextAlignment(Qt::AlignRight);
            int time = query.value("stream_length").toInt();
            if(time > 60*60)
            {
                column->setText(QString("%1:%2:%3")
                                .arg(time/(60*60),2,10,QChar('0'))
                                .arg(time/60 % 60,2,10,QChar('0'))
                                .arg(time%60,2,10,QChar('0')));
            }else{
                column->setText(QString("%1:%2")
                                .arg(time/60,2,10,QChar('0'))
                                .arg(time%60,2,10,QChar('0')));
            }

            list2.append(column);

            column = new QStandardItem();
            column->setText(query.value("meta_artist").toString());
            list3.append(column);

            column = new QStandardItem();
            column->setText(query.value("meta_album").toString());
            list4.append(column);
    }
    m_libraryItemsModel->insertColumn(0,list1);
    m_libraryItemsModel->insertColumn(1,list2);
    m_libraryItemsModel->insertColumn(2,list3);
    m_libraryItemsModel->insertColumn(3,list4);

    m_libraryItemsModel->setHeaderData(0,Qt::Horizontal,tr("Title"));
    m_libraryItemsModel->setHeaderData(1,Qt::Horizontal,tr("Time"));
    m_libraryItemsModel->setHeaderData(2,Qt::Horizontal,tr("Artist"));
    m_libraryItemsModel->setHeaderData(3,Qt::Horizontal,tr("Album"));
}

void LibraryPageWidget::dragEnterEvent(QDragEnterEvent *event)
{


    auto isSupportedFiles = [=](const QList <QUrl> &urls){

        QStringList suffixs = QStringList()
                << "m4a" << "m4r" << "m4b" << "m4p" << "3g2" << "mp4" << "mp3";
        int count = 0;
        foreach (QUrl url, urls) {
            if(!url.isLocalFile()) continue;
            QString suffix = QFileInfo(url.toLocalFile()).suffix();
            if(suffixs.contains(suffix)) count++;
        }

        if(count > 0) return true;

        return false;
    };

    if(!event->mimeData()->hasUrls()) event->ignore();

    if(isSupportedFiles(event->mimeData()->urls())){
        event->setDropAction(Qt::CopyAction);
        event->accept();
    }else{
        event->ignore();
    }
    QWidget::dragEnterEvent(event);
}

void LibraryPageWidget::dropEvent(QDropEvent *event)
{
    if(event->mimeData()->hasUrls())
    {

        QStringList urls;
        foreach (QUrl url, event->mimeData()->urls()) {
            urls.append(url.toLocalFile());
        }
        emit requestImportFiles(urls);
        event->accept();
    }

    QWidget::dropEvent(event);
}

void LibraryPageWidget::importFilesFinished(const QVariantList &list)
{

    foreach (QVariant var, list) {
        QVariantMap map = var.toMap();
        QSqlQuery q1(QString("SELECT unique_id FROM %1 WHERE file_hash='%2' LIMIT 1")
                     .arg(SqlLibraryTableName)
                     .arg(map["file_hash"].toString()));
        q1.exec();

        QString uuid;

        if(q1.first())
        {
            uuid = q1.value("unique_id").toString();
        }


        if(uuid.isEmpty()) uuid = QUuid::createUuid().toString().mid(1,36).toUpper();

        QSqlQuery query;
        query.prepare("INSERT OR REPLACE INTO avocado_media (unique_id,file_path,file_size,file_type,file_ext,file_hash,file_name,file_source,meta_title,meta_artist,meta_album,meta_comment,meta_genre,meta_year,meta_tracknumber,meta_date,meta_coverart,meta_caption_en,meta_caption_cn,stream_length,stream_bitrate,stream_samplerate,stream_channels,creation_date,last_modified_date) VALUES (:unique_id,:file_path,:file_size,:file_type,:file_ext,:file_hash,:file_name,:file_source,:meta_title,:meta_artist,:meta_album,:meta_comment,:meta_genre,:meta_year,:meta_tracknumber,:meta_date,:meta_coverart,:meta_caption_en,:meta_caption_cn,:stream_length,:stream_bitrate,:stream_samplerate,:stream_channels,:creation_date,:last_modified_date)");
        query.bindValue(":unique_id",uuid);
        query.bindValue(":file_path",map["file_path"]);
        query.bindValue(":file_size",map["file_size"]);
        query.bindValue(":file_type",map["file_type"]);
        query.bindValue(":file_ext",map["file_ext"]);
        query.bindValue(":file_hash",map["file_hash"]);
        query.bindValue(":file_name",map["file_name"]);
        query.bindValue(":file_source",map["file_source"]);
        query.bindValue(":meta_title",map["meta_title"]);
        query.bindValue(":meta_artist",map["meta_artist"]);
        query.bindValue(":meta_album",map["meta_album"]);
        query.bindValue(":meta_comment",map["meta_comment"]);
        query.bindValue(":meta_genre",map["meta_genre"]);
        query.bindValue(":meta_year",map["meta_year"]);
        query.bindValue(":meta_tracknumber",map["meta_tracknumber"]);
        query.bindValue(":meta_date",map["meta_date"]);
        query.bindValue(":meta_coverart",map["meta_coverart"]);
        query.bindValue(":meta_caption_en",map["meta_caption_en"]);
        query.bindValue(":meta_caption_cn",map["meta_caption_cn"]);
        query.bindValue(":stream_length",map["stream_length"]);
        query.bindValue(":stream_bitrate",map["stream_bitrate"]);
        query.bindValue(":stream_samplerate",map["stream_samplerate"]);
        query.bindValue(":stream_channels",map["stream_channels"]);
        query.bindValue(":creation_date",QDateTime::currentDateTime());
        query.bindValue(":last_modified_date",QDateTime::currentDateTime());
        query.exec();
    }
    qDebug() << list.count();
    reflash();
}
