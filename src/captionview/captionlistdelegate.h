#ifndef CAPTIONLISTDELEGATE_H
#define CAPTIONLISTDELEGATE_H

#include <QStyledItemDelegate>

namespace Avocado {
class CaptionListModel;
class CaptionListDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:

    explicit CaptionListDelegate(CaptionListModel *model,QObject * parent = 0);
    void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;

    QWidget *createEditor(QWidget * parent, const QStyleOptionViewItem & option, const QModelIndex & index) const;
    void updateEditorGeometry(QWidget * editor, const QStyleOptionViewItem & option, const QModelIndex & index) const;
private:
    static QSize iconSize;
    static int padding;
    QFont m_headerFont;
    QFont m_subheaderFont;
    CaptionListModel *m_model;
};
}
#endif // CAPTIONLISTDELEGATE_H
