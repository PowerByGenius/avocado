#ifndef AVOCADO_CAPTIONLISTMODEL_H
#define AVOCADO_CAPTIONLISTMODEL_H

#include <QStandardItemModel>
#include <QVariantList>

namespace Avocado {


class CaptionListModel : public QStandardItemModel
{
    Q_OBJECT
public:
    enum datarole { HeaderRole = Qt::UserRole + 100,
                    SubheaderRole,StartTimeRole,EndTimeRole,SeqRole,
                    PositionStartRole,PositionEndRole,CurrentPositionRole};

    explicit CaptionListModel(QObject *parent = 0);
    void loadSrtFile(const QString &path);
    inline void setPosition(const qint64 position){ m_position = position;}
    inline qint64 position() const {return m_position;}
private:
    void loadData(const QList<QVariantMap> &list);
    qint64 StringToPosition(const QString &text);
    qint64 m_position;
signals:

public slots:
};

} // namespace Avocado

#endif // AVOCADO_CAPTIONLISTMODEL_H
