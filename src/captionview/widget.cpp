#include "widget.h"
#include "ui_widget.h"

#include <QTime>
#include <QDebug>
#include <QFileInfo>
#include <QFileDialog>
#include <QMediaMetaData>
#include <QStandardPaths>
#include <QDragEnterEvent>
#include <QMimeData>

QString durationToString(qint64 position)
{
    qint64 duration = position / 1000;
        QTime totalTime((duration/3600)%60, (duration/60)%60, duration%60, (duration*1000)%1000);
        QString format = "mm:ss";
        if (duration > 3600)
            format = "hh:mm:ss";
        return totalTime.toString(format);

}

QString positionToString(qint64 position)
{
    qint64 duration = position / 1000;
        QTime totalTime((duration/3600)%60, (duration/60)%60, duration%60, position%1000);
        QString format = "mm:ss.zzz";
        if (duration > 3600)
            format = "hh:mm:ss.zzz";
        return totalTime.toString(format);

}


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    m_player(new QMediaPlayer(this)),
    m_playerlist(new QMediaPlaylist(this)),
    m_videoWidget(new QVideoWidget()),
    m_timeSliderChanged(false)
{
    ui->setupUi(this);
    m_player->setVideoOutput(m_videoWidget);
    m_videoWidget->resize(800,500);
    setWindowTitle("Caption");

    connect(m_player,&QMediaPlayer::durationChanged,
            [this](qint64 durations){

        ui->timeSlider->setRange(0,durations/1000);
        ui->timeSlider->setValue(0);
        ui->labelTimeEnd->setText(durationToString(durations));

    });


//    connect(m_player,&QMediaPlayer::currentMediaChanged,
//            [this](const QMediaContent & media){
//        qDebug() << "currentMediaChanged";
//    });

//    connect(m_player,&QMediaPlayer::mediaChanged,
//            [this](const QMediaContent & media){
//        qDebug() << "mediaChanged";
//    });

    connect(m_player,static_cast< void (QMediaPlayer::*)()>(&QMediaPlayer::metaDataChanged),
            [this](){
        if(m_player->metaData(QMediaMetaData::Title).isValid())
        {
            ui->label->setText(m_player->metaData(QMediaMetaData::Title).toString());
        }else{
            ui->label->setText(m_player->currentMedia().resources().first().url().fileName());
        }

    });

    connect(m_player,&QMediaPlayer::positionChanged,
            [this](qint64 position){
        if(m_timeSliderChanged) return;
        ui->timeSlider->setValue(position/1000);
        ui->labelTimeStart->setText(durationToString(position));
    });



    connect(m_player,&QMediaPlayer::stateChanged,
            [this](QMediaPlayer::State state){
        switch(state){
        case QMediaPlayer::StoppedState:
            break;
        case QMediaPlayer::PlayingState:
            break;
        case QMediaPlayer::PausedState:
            break;
        }
    });

    connect(m_player,&QMediaPlayer::mediaStatusChanged,
            [this](QMediaPlayer::MediaStatus status){
        switch(status){
        case QMediaPlayer::UnknownMediaStatus:
            break;
        case QMediaPlayer::NoMedia:
            break;
        case QMediaPlayer::LoadingMedia:
            break;
        case QMediaPlayer::LoadedMedia:
            break;
        case QMediaPlayer::StalledMedia:
            break;
        case QMediaPlayer::BufferingMedia:
            break;
        case QMediaPlayer::BufferedMedia:
            break;
        case QMediaPlayer::EndOfMedia:
            break;
        case QMediaPlayer::InvalidMedia:
            break;
        }
    });

    connect(m_player,&QMediaPlayer::stateChanged,
            [this](QMediaPlayer::State state){
        switch(state){
        case QMediaPlayer::StoppedState:
            ui->btnPlay->setText("Play");
            break;
        case QMediaPlayer::PlayingState:
            ui->btnPlay->setText("Pause");
            break;
        case QMediaPlayer::PausedState:
            ui->btnPlay->setText("Play");
            break;
        }
    });

    ui->spinBoxRate->setAttribute(Qt::WA_MacShowFocusRect,false);

    connect(ui->spinBoxRate,static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            [this](double value){
        m_player->setPlaybackRate(value);
    });

    connect(ui->timeSlider,&QSlider::valueChanged,
            [this](){
    });

    connect(ui->timeSlider,&QSlider::sliderPressed,
            [this](){
        m_timeSliderChanged = true;
    });

    connect(ui->timeSlider,&QSlider::sliderMoved,
            [this](){
        m_timeSliderChanged = true;
        ui->labelTimeStart->setText(durationToString(ui->timeSlider->value()*1000));
        m_player->setPosition(ui->timeSlider->value()*1000);
    });

    connect(ui->timeSlider,&QSlider::sliderReleased,
            [this](){
        m_timeSliderChanged = false;
        m_player->setPosition(ui->timeSlider->value()*1000);
        //m_player->play();
    });

    connect(m_player,&QMediaPlayer::positionChanged,
            ui->captionViewWidget,&Avocado::CaptionView::setPosition,Qt::QueuedConnection);

    connect(ui->captionViewWidget,&Avocado::CaptionView::positionChanged,
            [this](qint64 position){
        qDebug() << position;
        m_player->setPosition(position);
        m_player->play();
    });
    this->setAcceptDrops(true);
}

Widget::~Widget()
{
    m_videoWidget->deleteLater();
    delete ui;
}

void Widget::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasUrls())
    {
        event->accept();
    }

    QWidget::dragEnterEvent(event);
}

void Widget::dropEvent(QDropEvent *event)
{
    QList<QUrl> urls =  event->mimeData()->urls();

    QUrl url = urls.first();

    if(QFileInfo(url.toLocalFile()).suffix() != "srt")
        m_player->setMedia(url);

    QString srt = url.toLocalFile();
    int lastPoint = srt.lastIndexOf(".");
    srt = srt.left(lastPoint).append(".srt");
    if(QFile(srt).exists())
        ui->captionViewWidget->loadSrtFile(srt);

    if(m_player->isAvailable())
        m_player->play();

    event->accept();
    QWidget::dropEvent(event);
}

void Widget::on_btnVideoScreen_clicked()
{
    if(m_videoWidget->isHidden())
    {
        m_videoWidget->show();
    }
    m_videoWidget->activateWindow();
}

void Widget::on_btnPlay_clicked()
{
    switch(m_player->state()){
    case QMediaPlayer::StoppedState:
        m_player->play();
        break;
    case QMediaPlayer::PlayingState:
        m_player->pause();
        break;
    case QMediaPlayer::PausedState:
        m_player->play();
        break;
    }
}

void Widget::on_btnOpen_clicked()
{
    QString path = QFileDialog::getOpenFileName(this,"Open...",QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),"media file (*.mp3 *.mp4 *.m4a *.m4v *.mov)");
    if(path.isEmpty()) return;

    m_player->stop();

    m_player->setMedia(QUrl::fromLocalFile(path));
    QString srt = path.section(".",0,0).append(".srt");
    if(QFile(srt).exists())
        ui->captionViewWidget->loadSrtFile(srt);
    if(m_player->isAvailable())
        m_player->play();
}

void Widget::on_btnStop_clicked()
{
    switch(m_player->state()){
    case QMediaPlayer::StoppedState:
        break;
    case QMediaPlayer::PlayingState:
        m_player->stop();
        break;
    case QMediaPlayer::PausedState:
        m_player->stop();
        break;
    }
}

void Widget::on_btnCaption_clicked()
{
    QString file = QFileDialog::getOpenFileName(this,"Open...",
                                                QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),"srt (*.srt)");
    if(file.isEmpty()) return;

    ui->captionViewWidget->loadSrtFile(file);
}

void Widget::on_btnScroll_clicked()
{
    ui->captionViewWidget->setAutoScroll(true);
}
