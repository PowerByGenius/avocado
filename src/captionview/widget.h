#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QVideoWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent * event);
private:

private slots:
    void on_btnVideoScreen_clicked();

    void on_btnPlay_clicked();

    void on_btnOpen_clicked();

    void on_btnStop_clicked();

    void on_btnCaption_clicked();

    void on_btnScroll_clicked();

private:
    Ui::Widget *ui;
    QMediaPlayer *m_player;
    QMediaPlaylist *m_playerlist;
    QVideoWidget *m_videoWidget;


    bool m_timeSliderChanged;
};

#endif // WIDGET_H
