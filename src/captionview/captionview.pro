#-------------------------------------------------
#
# Project created by QtCreator 2015-09-29T11:54:29
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets
CONFIG   +=C++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#INCLUDEPATH += -F$$PWD/framework
#LIBS += -framework VLCQtCore

#LIBS += -F$$PWD/framework -framework VLCQtCore

#QMAKE_CXXFLAGS += -F$$PWD/framework
#QMAKE_LFLAGS += -F$$PWD/framework


TARGET = app
TEMPLATE = app

HEADERS += \
    captionview.h \
    widget.h \
    captionlistview.h \
    captionlistmodel.h \
    captionlistdelegate.h

SOURCES += \
    captionview.cpp \
    main.cpp \
    widget.cpp \
    captionlistview.cpp \
    captionlistmodel.cpp \
    captionlistdelegate.cpp

FORMS += \
    widget.ui
