#ifndef AVCADO_CAPTIONLISTVIEW_H
#define AVCADO_CAPTIONLISTVIEW_H

#include <QListView>

namespace Avocado {

class CaptionListView : public QListView
{
    Q_OBJECT
public:
    explicit CaptionListView(QWidget *parent = 0);
protected:
    void wheelEvent(QWheelEvent *event);
signals:
    void wheel();
public slots:
};

} // namespace Avcado

#endif // AVCADO_CAPTIONLISTVIEW_H
