#include "captionview.h"
#include "captionlistview.h"
#include "captionlistmodel.h"
#include "captionlistdelegate.h"
#include <QLayout>
#include <QDebug>


namespace Avocado {

CaptionView::CaptionView(QWidget *parent) :
    QWidget(parent),
    m_captionListView(new CaptionListView(this)),
    m_captionListModel(new CaptionListModel(this)),
    m_isAutoScroll(true)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(QMargins());
    layout->addWidget(m_captionListView);

    m_captionListView->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    m_captionListView->setItemDelegate(new CaptionListDelegate(m_captionListModel,this));
    m_captionListView->setModel(m_captionListModel);

    connect(m_captionListView,&CaptionListView::doubleClicked,
            [this](const QModelIndex & index){
                emit positionChanged(index.data(CaptionListModel::PositionStartRole).toReal());
            });

    connect(m_captionListView,&CaptionListView::wheel,
            [=](){
                m_isAutoScroll = false;
            });

    connect(m_captionListView,&CaptionListView::wheel,
            [=](){
                m_isAutoScroll = false;
            });



}

void CaptionView::loadSrtFile(const QString &path)
{
    m_captionListModel->loadSrtFile(path);
}

void CaptionView::setPosition(const qint64 position)
{
    m_captionListModel->setPosition(position);
    for(int i=0;i<m_captionListModel->rowCount();i++)
    {
        QStandardItem *item = m_captionListModel->item(i);
        qint64 start = item->data(CaptionListModel::PositionStartRole).toReal();
        qint64 end = item->data(CaptionListModel::PositionEndRole).toReal();

        if(position >=start and position < end){
                m_captionListView->viewport()->update();
                if(m_isAutoScroll)
                    m_captionListView->scrollTo(item->index());
            break;
        }
    }
}

void CaptionView::setAutoScroll(bool value)
{
    m_isAutoScroll = value;
}

} // namespace Avcado

