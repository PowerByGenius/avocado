#include "captionlistview.h"
#include <QStyledItemDelegate>
#include <QMouseEvent>
#include <QDebug>

namespace Avocado {

CaptionListView::CaptionListView(QWidget *parent) : QListView(parent)
{
    setAttribute(Qt::WA_MacShowFocusRect,false);
    //    setDragEnabled(true);
}

void CaptionListView::wheelEvent(QWheelEvent *event)
{
    emit wheel();
    QListView::wheelEvent(event);
}


} // namespace Avcado

