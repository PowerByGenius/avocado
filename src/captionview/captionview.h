#ifndef AVCADO_CAPTIONVIEW_H
#define AVCADO_CAPTIONVIEW_H

#include <QWidget>
#include <QModelIndex>
#include <QStandardItem>

namespace Avocado {
class CaptionListView;
class CaptionListModel;
class CaptionView : public QWidget
{
    Q_OBJECT
public:
    explicit CaptionView(QWidget *parent = 0);
    void init();
private:
    CaptionListView *m_captionListView;
    CaptionListModel *m_captionListModel;
    bool m_isAutoScroll;
signals:
    void positionChanged(qreal);
public slots:
    void loadSrtFile(const QString &path);
    void setPosition(const qint64 position);
    void setAutoScroll(bool value);
};

} // namespace Avcado

#endif // AVCADO_CAPTIONVIEW_H
