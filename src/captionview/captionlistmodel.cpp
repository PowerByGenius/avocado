#include "captionlistmodel.h"
#include "captionlistdelegate.h"
#include <QCoreApplication>
#include <QFile>
#include <QRegExp>
#include <QDebug>

namespace Avocado {

CaptionListModel::CaptionListModel(QObject *parent) :
    QStandardItemModel(parent),
    m_position(0)
{
}

void CaptionListModel::loadSrtFile(const QString &path)
{
    QFile file(path);
    QTextStream textStream(&file);
    file.open(QFile::ReadOnly);
    QString str = textStream.readAll();
    QRegExp rx("(\\d+)\\s*(\\d{2}:\\d{2}:\\d{2},\\d{3}) --> (\\d{2}:\\d{2}:\\d{2},\\d{3})");

    QList<QVariantMap> list;
    int pos = 0;
    int posNext = 0;
    while ((pos = rx.indexIn(str, pos)) != -1)
    {
        qApp->processEvents();
        QVariantMap map;
        map.insert("seq",rx.cap(1).trimmed());
        map.insert("startString",rx.cap(2).trimmed());
        map.insert("endString",rx.cap(3).trimmed());

        pos += rx.matchedLength();
        posNext = str.indexOf(rx,pos);
        QString text;
        if(posNext > 0){
            text = str.mid(pos,posNext-pos);
        }else{
            text = str.mid(pos);
        }
        map.insert("text",text.trimmed());

        list.append(map);
    }

    this->loadData(list);
}

void CaptionListModel::loadData(const QList<QVariantMap> &list)
{

    this->clear();

    for (int i = 0; i < list.size(); ++i) {

        QVariantMap map = list.at(i);
        QStandardItem *item = new QStandardItem();
        item->setEditable(false);
        item->setDragEnabled(false);
        QString seq = map.value("seq",QString()).toString();
        QString start = map.value("startString",QString()).toString();
        QString end = map.value("endString",QString()).toString();
        QString text = map.value("text",QString()).toString();


        item->setText(text);
        item->setData(start,CaptionListModel::HeaderRole);
        item->setData(text,CaptionListModel::SubheaderRole);
        item->setData(seq,CaptionListModel::SeqRole);
        item->setData(start,CaptionListModel::StartTimeRole);
        item->setData(end,CaptionListModel::EndTimeRole);
        item->setData(false,CaptionListModel::CurrentPositionRole);
        item->setData(StringToPosition(start),CaptionListModel::PositionStartRole);
        item->setData(StringToPosition(end),CaptionListModel::PositionEndRole);
        appendRow(item);
    }
}

qint64 CaptionListModel::StringToPosition(const QString &text)
{
    qreal time = 0;
    QRegExp rx("(\\d{2}):(\\d{2}):(\\d{2}),(\\d{3})");
    if(rx.exactMatch(text))
    {
         time = rx.cap(4).toInt()
                +rx.cap(3).toInt() * 1000
                +rx.cap(2).toInt() * 60  * 1000
                +rx.cap(1).toInt() * 60 * 60 * 1000;
    }
    return time;
}

} // namespace Avocado

