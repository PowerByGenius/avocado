#include "captionlistdelegate.h"
#include "captionlistmodel.h"
#include <QStyledItemDelegate>
#include <QPainter>
#include <QApplication>
#include <QPlainTextEdit>
#include <QDebug>

namespace Avocado {

QSize CaptionListDelegate::CaptionListDelegate::iconSize = QSize(60, 60);
int CaptionListDelegate::CaptionListDelegate::padding = 5;

CaptionListDelegate::CaptionListDelegate(CaptionListModel *model, QObject * parent):
    QStyledItemDelegate(parent),
    m_model(model)
{
    m_headerFont = QApplication::font();
    m_headerFont.setPointSize(11);
//    m_headerFont.setBold(true);
    m_subheaderFont = QApplication::font();
    m_subheaderFont.setPointSize(18);
}

void CaptionListDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    if(!index.isValid())
        return;

    painter->save();

    if (option.state & QStyle::State_Selected){
        QRect selectedRect = option.rect;
        selectedRect.setLeft(0);
        selectedRect.setWidth(7);
//        painter->fillRect(selectedRect, option.palette.highlight());
        painter->fillRect(selectedRect, Qt::darkGreen);
    }

    qint64 start = index.data(CaptionListModel::PositionStartRole).toReal();
    qint64 end = index.data(CaptionListModel::PositionEndRole).toReal();
    qint64 position = m_model->position();
    bool currentPosition = false;
    if(position >=start and position < end){
        currentPosition = true;
        QRect selectedRect = option.rect;

        selectedRect.setLeft(0);
        selectedRect.setWidth(7);
        painter->fillRect(selectedRect, Qt::darkRed);
    }

//    bool currentPosition = index.data(CaptionListModel::CurrentPositionRole).toBool();
//    if (currentPosition){
//        QRect selectedRect = option.rect;
//        selectedRect.setLeft(0);
//        selectedRect.setWidth(7);
//        painter->fillRect(selectedRect, Qt::darkRed);
//    }


    QString headerText = index.data(CaptionListModel::HeaderRole).toString();
    QString subheaderText = index.data(CaptionListModel::SubheaderRole).toString();
    QString seqText = index.data(CaptionListModel::SeqRole).toString();
    headerText = QString("%1 %2").arg(seqText).arg(headerText);

    QFontMetrics headerFm(m_headerFont);
    QFontMetrics subheaderFm(m_subheaderFont);

    /*
     * The x,y coords are not (0,0) but values given by 'option'. So, calculate the
     * rects again given the x,y,w.
     * Note that the given height is 0. That is because boundingRect() will return
     * the suitable height if the given geometry does not fit. And this is exactly
     * what we want.
     */
    QRect headerRect =
            headerFm.boundingRect(option.rect.left() + iconSize.width(), option.rect.top() + padding,
                                  option.rect.width() - iconSize.width(), 0,
                                  Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                  headerText);
    QRect subheaderRect =
            subheaderFm.boundingRect(headerRect.left(), headerRect.bottom()+padding,
                                     option.rect.width() - iconSize.width(), 0,
                                     Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                     subheaderText);

    painter->setPen(Qt::gray);
    painter->setFont(m_headerFont);
    painter->drawText(headerRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, headerText);

    painter->setPen(Qt::black);
    if (option.state & QStyle::State_Selected)
        painter->setPen(Qt::darkGreen);

    if (currentPosition)
        painter->setPen(Qt::darkRed);

    painter->setFont(m_subheaderFont);
    painter->drawText(subheaderRect, Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap, subheaderText);

    painter->restore();
}

QSize CaptionListDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(!index.isValid())
        return QSize();

    QString headerText = index.data(CaptionListModel::HeaderRole).toString();
    QString subheaderText = index.data(CaptionListModel::SubheaderRole).toString();
    QString seqText = index.data(CaptionListModel::SeqRole).toString();
    headerText = QString("%1 %2").arg(seqText).arg(headerText);

    QFontMetrics headerFm(m_headerFont);
    QFontMetrics subheaderFm(m_subheaderFont);

    /* No need for x,y here. we only need to calculate the height given the width.
     * Note that the given height is 0. That is because boundingRect() will return
     * the suitable height if the given geometry does not fit. And this is exactly
     * what we want.
     */
    QRect headerRect = headerFm.boundingRect(0, 0,
                                             option.rect.width() - iconSize.width(), 0,
                                             Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                             headerText);

    QRect subheaderRect = subheaderFm.boundingRect(0, 0,
                                                   option.rect.width() - iconSize.width(), 0,
                                                   Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                                   subheaderText);

    QSize size(option.rect.width(), headerRect.height() + subheaderRect.height() +  3*padding);

    /* Keep the minimum height needed in mind. */
    if(size.height()<iconSize.height())
        size.setHeight(iconSize.height());

    return size;
}

QWidget *CaptionListDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return new QPlainTextEdit(parent);
}


void CaptionListDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(!index.isValid())
        return;

    QString headerText = index.data(CaptionListModel::HeaderRole).toString();
    QString subheaderText = index.data(CaptionListModel::SubheaderRole).toString();
    QString seqText = index.data(CaptionListModel::SeqRole).toString();
    subheaderText = QString("%1 %2").arg(seqText).arg(subheaderText);
    QFontMetrics headerFm(m_headerFont);
    QFontMetrics subheaderFm(m_subheaderFont);

    QRect headerRect =
            headerFm.boundingRect(option.rect.left() + iconSize.width(), option.rect.top() + padding,
                                  option.rect.width() - iconSize.width(), 0,
                                  Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                  headerText);
    QRect subheaderRect =
            subheaderFm.boundingRect(headerRect.left(), headerRect.bottom()+padding,
                                     option.rect.width() - iconSize.width(), 0,
                                     Qt::AlignLeft|Qt::AlignTop|Qt::TextWordWrap,
                                     subheaderText);

    QPlainTextEdit *textEdit = qobject_cast<QPlainTextEdit*>(editor);
    if(textEdit){

        textEdit->setAttribute(Qt::WA_MacShowFocusRect,false);

        QPalette p = textEdit->palette();
        p.setColor(QPalette::Active, QPalette::Base, Qt::darkRed);
        p.setColor(QPalette::Inactive, QPalette::Base, Qt::darkRed);
        p.setColor(QPalette::Active, QPalette::Text, Qt::white);
        p.setColor(QPalette::Inactive, QPalette::Text, Qt::white);
        textEdit->setPalette(p);

        textEdit->setFont(m_subheaderFont);
        subheaderRect.setHeight(subheaderRect.height()+padding);
        subheaderRect.setRight(option.rect.right());
        textEdit->setFrameShape(QPlainTextEdit::NoFrame);
        textEdit->setGeometry(subheaderRect);
        textEdit->setReadOnly(true);
    }
}

}
