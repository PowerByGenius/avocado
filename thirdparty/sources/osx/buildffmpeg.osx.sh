#!/bin/sh

./configure --prefix=$PWD/../../../build/osx \
--extra-cflags="-I$PWD/../../../build/osx/include" \
--extra-ldflags="-L$PWD/../../../build/osx/lib" \
--enable-nonfree \
--enable-small \
--disable-doc \
--disable-encoders \
--disable-parsers \
--enable-parser=aac \
--enable-parser=h264 \
--enable-parser=aac_latm \
--enable-parser=mpeg4video \
--disable-decoders \
--enable-decoder=h264 \
--enable-decoder=aac \
--disable-programs \
--disable-bsfs \
--disable-protocols \
--enable-protocol=file \
--enable-protocol=crypto \
--disable-muxers \
--disable-debug \
--disable-logging \
--disable-devices \
--disable-error-resilience \
--disable-iconv

# --disable-fft \
# --disable-debug \
# --enable-nonfree \
# --disable-logging \
# --disable-devices 

# --disable-yasm \
#--disable-filters 

# --enable-muxer=mp4 \
# --enable-muxer=image2
# --disable-demuxers \
# --enable-demuxer=aac \
# --enable-demuxer=h264 \
# --enable-demuxer=m4v \
# --enable-demuxer=pcm_alaw \
# --enable-demuxer=pcm_f32be \
# --enable-demuxer=pcm_f32le \
# --enable-demuxer=pcm_f64be \
# --enable-demuxer=pcm_f64le \
# --enable-demuxer=pcm_mulaw \
# --enable-demuxer=pcm_s16be \
# --enable-demuxer=pcm_s16le \
# --enable-demuxer=pcm_s24be \
# --enable-demuxer=pcm_s24le \
# --enable-demuxer=pcm_s32be \
# --enable-demuxer=pcm_s32le \
# --enable-demuxer=pcm_s8 \
# --enable-demuxer=pcm_u16be \
# --enable-demuxer=pcm_u16le \
# --enable-demuxer=pcm_u24be \
# --enable-demuxer=pcm_s24le \
# --enable-demuxer=pcm_u32be \
# --enable-demuxer=pcm_u32le \
# --enable-demuxer=pcm_u8 \
# --enable-demuxer=wav \
# --enable-demuxer=ffmetadata \
# --enable-demuxer=rawvideo \
# --enable-demuxer=yuv4mpegpipe \
# --enable-demuxer=data



#--enable-demuxer=image2 \
	
# --disable-decoders \
# --enable-decoder=h264 \
# --enable-decoder=aac \

# --disable-protocols \
# --enable-protocol=file \
# --enable-protocol=data \
# --disable-filters \